	base_url = $('#base_url').val();
	vodjenje_lagera = $('#vodjenje_lagera').val();
	$(document).ready(function() {

		$('.JScomment_add').click(function() {
			var ocena = $('#JSreview-number').val();
			var ime = $('#JScomment_name').val();
			var pitanje = $('#JScomment_message').val();
			var roba_id = $('#JSroba_id').val();

			if (ime == '' || ime == ' ') {
				$('#JScomment_name').css("border", "1px solid red").focus().fadeIn(200);
			} else if (pitanje == '' || pitanje == ' ') {
				$('#JScomment_message').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$.ajax({
					type: "POST",
					url: base_url + 'coment_add',
					data: { ime: ime, pitanje: pitanje, roba_id: roba_id, ocena: ocena },
					success: function(msg) {
						$('.JSinfo-popup').show().delay(1000).fadeOut(1000);
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Vaš komentar je poslat.</p>");
						$('#JScomment_name').val('');
						$('#JScomment_message').val('');
						$('#JSreview-number').val('');
					}
				});
			}
		});


	 // SLAJDER ZA CENU  
	if(typeof min_web_cena_init !== 'undefined' && typeof max_web_cena_init !== 'undefined'){
    $( "#JSslider-range" ).slider({
      range: true,
      min: min_web_cena_init,
      max: max_web_cena_init,
      values: [ min_web_cena, max_web_cena ],
      slide: function( event, ui ) {
        $( "#JSamount" ).html("Cena od " + parseFloat(ui.values[ 0 ]/kurs).toFixed(2) + "  do " + parseFloat(ui.values[ 1 ]/kurs).toFixed(2));
      },
      stop: function(event,ui) {
      		var url=$("#JSfilters-url").val();
			var proizvodjaci=$("#JSniz_proiz").val();
			var karakteristike=$("#JSniz_kara").val();
            window.location.href=base_url+url+'/'+proizvodjaci+'/'+karakteristike+'/'+ui.values[ 0 ]+'-'+ui.values[ 1 ];
	  }
    });
    $( "#JSamount" ).html("Cena od " + parseFloat($( "#JSslider-range" ).slider( "values", 0 )/kurs).toFixed(2) +
     " do " + parseFloat($( "#JSslider-range" ).slider( "values", 1 )/kurs).toFixed(2));
	}
		$('.JSreset-filters-button').click(function() {
			var url = $("#JSfilters-url").val();
			window.location.href = base_url + url + '/0/0' + '/0-0';
		});
		$('.JSfilter-close').click(function() {
			var tip = $(this).data("type");
			var rb = $(this).data("rb");
			var element = $(this).data("element");
			var url = $("#JSfilters-url").val();
			var niz_proiz = $("#JSniz_proiz").val();
			var niz_kara = $("#JSniz_kara").val();
			var proizvodjaci = "";
			var karakteristike = "";
			if (tip == 1) {
				karakteristike = niz_kara;
				niz = niz_proiz.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (el != element) {
							if (br == 0) {
								proizvodjaci += el;
							} else {
								proizvodjaci += "-" + el;
							}
							br = br + 1;
						}
					});
				} else {
					proizvodjaci = 0;
				}
			}
			if (tip == 2) {
				proizvodjaci = niz_proiz;
				niz = niz_kara.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (el != element) {
							if (br == 0) {
								karakteristike += el;
							} else {
								karakteristike += "-" + el;
							}
							br = br + 1;
						}
					});
				} else {
					karakteristike = 0;
				}
			}
			//alert(base_url+'filter/'+url+'/'+proizvodjaci+'/'+karakteristike);
			window.location.href = base_url + url + '/' + proizvodjaci + '/' + karakteristike + '/0-0';
		});


		/* Compare button and modal */
		$(document).on('click', '.JScompare', function() {
			var $this = $(this),
				dataId = $this.data('id');
			if ($(this).hasClass('active')) {
				$.post(base_url + 'clearCompare', { clear_id: dataId }, function(response) {
				});
				$this.removeClass('active');
			} else {
				$(this).addClass('active');
				$.post(base_url + 'compareajax', { id: dataId }, function(response) {
				});
				$('#JScompareTable').text('');
			}
		});
		$(document).on('click', '#JScompareArticles', function() {
			$.post(base_url + 'compare', { id: $(this).data('id') }, function(response) {
				$('#JScompareTable').html(response.content);
			});
		});
		$(document).on('click', '.JSclearCompare', function() {
			var $this = $(this),
				clearDataId = $this.data('id');
			$('.JScompare').filter('[data-id="' + clearDataId + '"]').removeClass('active');
			$.post(base_url + 'compare', { clear_id: clearDataId }, function(response) {
				$('#JScompareTable').html(response.content);
			});
		});
		
	});


	$('#JSForgotPassword').click(function(){
		$('#JSEmailError').text('');
		$('#JSPasswordlError').text('');
		var email = $('#JSemailLogin').val();
		$.ajax({
			type: "POST",
			url: base_url + 'zaboravljena-lozinka-ajax',
			data: { email_fg: email },
			success: function(response) {
				if(response.success){
					swal("Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi.", {
					  buttons: {
					    yes: {
					      text: "Zatvori",
					      value: true,
					    }
					  },
					}).then(function(value){ });

				}else{
					$('#JSEmailError').text(response.email_fg);
				}
			}
		});
	});

	//search
	function search() {
		search = document.getElementById('JSsearch').value;
		grupa_pr_id = $('.JSSearchGroup').val();
		search = encodeURIComponent(search);
		if(search != ''){
			window.location.href = base_url + "search/" + search + "/" + grupa_pr_id;
		}
	}

	function search2() {
		search = document.getElementById('JSsearch2').value;
		grupa_pr_id = $('.JSSearchGroup2').val();
		search = encodeURIComponent(search);
		if(search != ''){
			window.location.href = base_url + "search/" + search + "/" + grupa_pr_id;
		}
	}
	function newsletter() {
		email = document.getElementById('newsletter').value;
		if (isValidEmailAddress(email) == true) {
			/*$.post(base_url+'newsletter',{ emeil:email }, success: function( msg ) {
		alert( msg );
		});*/
			$.ajax({
				type: "POST",
				url: base_url + 'newsletter',
				data: { email: email },
				success: function(msg) {
					alertSuccess(msg);
				}
			});
		} else {
			alertError("E-mail adresa nije validna.");
		}
	}
	$('#JSpassword_login').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSpassword_login').val() != ''){
        	$("#login").trigger("click");
    	}
    });
	$('#JSemail_login').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSemail_login').val() != ''){
        	$("#login").trigger("click");
    	}
    });
	function user_login() {
		email = document.getElementById('JSemail_login').value;
		password = document.getElementById('JSpassword_login').value;

			$.ajax({
				type: "POST",
				url: base_url + 'login',
				data: { email_login: email , lozinka_login:password},
				success: function(response) {
					$('#JSLoginErrorMessage').text('');

					if(response.success){
						setTimeout(function(){ location.reload(); }, 800);
						alertSuccess('Uspešno ste se prijavili');
					}else{
						var error = 'E-mail ili lozinka nisu validni!';
						if(response.errors.email_error){
							error = response.errors.email_error;
						}else if(response.errors.lozinka_error){
							error = response.errors.lozinka_error;
						}
						alertError(error);
					}
				}
			});
	}

	function filters_send() {
		var strChoices = "";
		var proiz = "";
		var objCBarray2 = document.getElementsByName('proizvodjac[]');
		var objCBarray = document.getElementsByName('vrednost[]');
		var url = $("#JSfilters-url").val();
		var vrednost_arr = new Array();
		var proiz_arr = new Array();
		var j = 0;
		var p = 0;
		for (i = 0; i < objCBarray.length; i++) {
			if (objCBarray[i].checked) {
				vrednost_arr[j] = objCBarray[i].value;
				j++;
			}
		}
		for (i = 0; i < vrednost_arr.length; i++) {
			if (i == 0) {
				strChoices += vrednost_arr[i] + "";
			} else {
				strChoices += "-" + vrednost_arr[i] + ""
			}
		}
		for (i = 0; i < objCBarray2.length; i++) {
			if (objCBarray2[i].checked) {
				proiz_arr[p] = objCBarray2[i].value;
				p++;
			}
		}
		for (n = 0; n < proiz_arr.length; n++) {
			if (n == 0) {
				proiz += proiz_arr[n] + "";
			} else {
				proiz += "-" + proiz_arr[n] + "";
			}
		}
		if (vrednost_arr.length == 0) {
			strChoices = 0;
		}
		if (proiz_arr.length == 0) {
			proiz = 0;
		}
		//alert(base_url+'filters/'+url+'/'+proiz+'/'+strChoices);
		window.location.href = base_url + url + '/' + proiz + '/' + strChoices + '/0-0';
	}

	function check_fileds(polje) {
		polje2 = document.getElementById(polje).value;
		if (polje == 'email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JSwithout-reg-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JSwithout-reg-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JSwithout-reg-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JSkontakt-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JSkontakt-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JSkontakt-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else {
			if (polje2 == '' || polje2 == '') {
				$('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
			}
		}
	}

	function meil_send() { 
		name = document.getElementById('JSkontakt-name').value;
		email = document.getElementById('JSkontakt-email').value;
		message = document.getElementById('message').value;
		if (name == '' || name == ' ') {
			$('#JSkontakt-name').css("border", "1px solid red").focus().fadeIn(200);
		} else if (isValidEmailAddress(email) == false) {
			$('#JSkontakt-email').css("border", "1px solid red").focus().fadeIn(200);
		} else {
			$.ajax({
				type: "POST",
				url: base_url + 'meil_send',
				data: { name: name, message: message, email: email },
				success: function() {
					$('.JSinfo-popup').show().delay(1000).fadeOut(1000);
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Vaša poruka je poslata!</p>");
					$('.JSpopup').fadeOut("fast");
					location.reload();
				}
			});
		}
	}

		function user_forgot_password() {
		email = document.getElementById('JSemail_login').value;
			$.ajax({
				type: "POST",
				url: base_url + 'zaboravljena-lozinka-ajax',
				data: { email_fg: email },
				success: function(response) {
					if(response.success){
						$('#JSForgotSuccess').removeClass('JShidden-msg');
					}else{
						$('.JSinfo-popup').show().delay(1000).fadeOut(1000);
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>" +response.email_fg+ "</p>");	
					}
				}
			});
	}
	
	//Proverava da li je polje prazno ili sadrzi nedozvoljene karaktere
	function isValidField(fields) {
		var check = true;
		var field = "";
		var script = /["-*<->-(){}`|^]+$/; // Invalid characters:  !"#$%^&*'() <=?> {}`|^ Valid characters: +-_., SPACE a-z A-Z 0-1
		for (i = 0; i < fields.length; i++) {
			var ind = fields[i].value.search(script);
			//For empty fields
			if ((fields[i].value == "") || (ind != -1)) {
				check = false;
				//Fokusira se na polje.Treba napraviti obavestenje za to polje, zvezdica ili nesto slicno
				fields[i].focus(fields[i].id);
				$('#' + fields[i].id).css("border", "1px solid red").next().fadeIn(200);
			} else {
				$('#' + fields[i].id).css("border", "1px solid gray").next().fadeOut(200);
			}
		}
		return check;
	}
	//Proverava da li je validna e-mail adresa
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	//Dozvoljava samo numeric values
	function isNumberKey(charCode) {
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
 