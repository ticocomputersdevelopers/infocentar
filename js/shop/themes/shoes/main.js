

/* Navigation link */
/*var navigationLength = $('.site-navigation li').length;
if( navigationLength > 5 && $(window).width() > 1024) {
	$('.site-navigation').append('<div class="dropdown-nav"><a href="#!">Više <i class="fas fa-chevron-down"></i></a> <ul class="dropdown-ul"></ul></div>');

	$('.site-navigation li').each(function(index) {
		 
		if( index  > 4 ) {
			$(this).appendTo('.dropdown-ul')
		}	
		if ($(window).width() < 1400) {
			if( index  > 3 ) {
				$(this).appendTo('.dropdown-ul')
			}	
		}
		if ($(window).width()< 1215) {
			if( index  > 2 ) {
				$(this).appendTo('.dropdown-ul')
			}	
		}
	})
}*/


if($(window).width() > 1024) {
	$('.dropdown-nav').on('mouseover', function() {
		$(this).find('.dropdown-ul').show();
	})
	.on('mouseout', function() {
		$(this).find('.dropdown-ul').hide();
	})

	$('.dropdown-nav .dropdown_div').find('a').on('mouseover', function() {
		$(this).next('i').css('color', 'var(--link-hover)');
	}).on('mouseout', function() {
		$(this).next('i').css('color', '#fff');
	})
}

if($(window).width() < 1024) {
	$('.JSDropdown-scroll').click(function() {
		$(this).parent('.dropdown_div').next().slideToggle(250);
		$(this).toggleClass('JSDropdown-scroll-rotate');
	})
}



$(document).ready(function () {

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
	// AKCIJA
	
	var akcija_products = $(".JSsale-products .JSproduct");
    for(var i = 0; i < akcija_products.length; i+=12) {
    akcija_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// IZDVAJAMO
	
	var izdvajamo_products = $(".JSfetured-products .JSproduct");
    for(var i = 0; i < izdvajamo_products.length; i+=12) {
    izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}

	// POPUP BANER
	if ($(window).width() > 991 ) {
		var first_banner = $('.JSfirst-popup'),
		popup_img = $('.popup-img'),
		close_banner = $('.JSclose-me-please');
		if($('.transp').length){ //|| $('body').is('#product-page')
			
		 	setTimeout(function(){ 
			 	first_banner.animate({ top: '50%' }, 700);
			}, 1000);   

			close_banner.click(function(){ $('.JSfirst-popup').hide(); });  
		} 
	}
	
 });
	// SLICK SLIDER INCLUDE
	if ($("#JSmain-slider")[0]){
		if($('#admin_id').val()){
			$('#JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});
		}else{
			$('#JSmain-slider').slick({
				autoplay: true
			});
		}
	
	}

	$('.JSslider-container').css('margin-top', -$('header').outerHeight() + 'px');
 
	/* Search */ 
	var searchTime;
	$('#JSsearch').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch').val() != ''){
        	$(".JSsearch-button").trigger("click");
    	}

		clearTimeout(searchTime);
		searchTime = setTimeout(timer, 500);
	});

		var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}

		clearTimeout(searchTime);
		searchTime = setTimeout(timer2, 500);
	});

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});



	$('.JSSearchGroup').change(function(){
		timer();
	});

	$('.JSSearchGroup2').change(function(){
		timer2();
	});


	function timer(){
		$('.JSsearch_list').remove();
		var search = $('#JSsearch').val();
		if (search != '' && search.length > 2) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup').val()}, function (response){
				$('.JSsearchContent').append(response);
			});
		} 
	}

	function timer2(){
		$('.JSsearch_list').remove();
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > 2) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				$('.JSsearchContent2').append(response);
			});
		} 
	}


	// SCROLL TO TOP
	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });


 	// SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');	

	/* Moblie navigation */
	if($(window).width() < 1024) {
		$('.site-navigation').css('right', - $('.site-navigation').width() - 60)
		$('#navigationToggle').click(function() {
			$('.site-navigation').css('right', '0px');
			$('body').css({ 
				"right": $('.site-navigation').outerWidth(), 
				"overflow": "hidden",
				"transition": "all 0.4s"
			});
			$('.body-overlay').css('display', 'block');
		})

		$('.site-navigation .exit, .body-overlay').click(function() {
			$('.site-navigation').css('right', - $('.site-navigation').width() - 60);
			$('body').css({ 
				"right": "0px", 
				"overflow": "visible"
			})
			$('.body-overlay').css('display', 'none');
		})
	}

function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	

function alertAmount(message) {
	swal(message , {
	  buttons: false,
	});
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}	


if($(window).width() > 1024) {
	$(document).on('mouseover', '.product-image-wrapper', function(){ 
		if($(this).find('.product-image').attr('src') != $(this).find('.second-product-image').attr('src')){
			$(this).find('.second-product-image').css('opacity', '1');
			$(this).find('.second-product-image').css('visibility', 'visible');

			$(this).find('.product-image').css('opacity', '0');
			$(this).find('.product-image').css('visibility', 'hidden');
		}
	});

	$(document).on('mouseout', '.product-image-wrapper', function(){ 
		if($(this).find('.product-image').attr('src') != $(this).find('.second-product-image').attr('src')){
			$(this).find('.product-image').css('opacity', '1');
			$(this).find('.product-image').css('visibility', 'visible');

			$(this).find('.second-product-image').css('opacity', '0');
			$(this).find('.second-product-image').css('visibility', 'hidden');
		}
	});

	$(document).on('mouseover', '.product-image-div .button-div', function(){ 
		$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '1');
		$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'visible');

		$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '0');
		$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'hidden');
	});

	$(document).on('mouseout', '.product-image-div .button-div', function(){ 
		$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '1');
		$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'visible');

		$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '0');
		$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'hidden');
	});
}

// SOCIAL ICONS
$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
$('.social-icons .youtube').append('<i class="fa fa-youtube-square"></i>');


 