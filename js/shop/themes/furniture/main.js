
$(document).ready(function () {

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
 
 if($('body').is('#start-page') || $('body').is('#artical-page')){		// START PAGE ==============
	// AKCIJA
	var akcija_products = $(".JSsale-products .JSproduct");
    for(var i = 0; i < akcija_products.length; i+=12) {
    akcija_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	// IZDVAJAMO
	var izdvajamo_products = $(".JSfetured-products .JSproduct");
    for(var i = 0; i < izdvajamo_products.length; i+=12) {
    izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// SLICK SLIDER INCLUDE
	if ($("#JSmain-slider")[0]){
		if($('#admin_id').val()){
			$('#JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});
		}else{
			$('#JSmain-slider').slick({
				autoplay: true
			});
		}
	
	}
 
	
	// $(function() { 
	// 	var BROWSER = navigator.userAgent;  
	// 	 if (BROWSER.indexOf("Chrome") > -1) {
	// 	 	var transform = $('#JSmain-slider .slick-track').css('transform');
	// 	 	transform = '-webkit-' + transform;
	// 	    console.log(transform);
	// 	}  
	// });
	
	if ($(".JSproduct-slider")[0]){
		$('.JSsale-products, .JSfetured-products').slick({
		    autoplay: true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
	}
	
	if ($(".JSrelated-products")[0]){
		$('.JSproduct-slider').slick({
		    infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	}

	// most popular products slider
	if ($(".JSMostPopularProducts")[0]){
		$('.JSMostPopularProducts').slick({
		    infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	}

	// best seller product
	if ($(".JSBestSellerProducts")[0]){
		$('.JSBestSellerProducts').slick({
		    infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	}
} 	// START PAGE END ==============
	
	// CATEGORIES - MOBILE
	if ($(window).width() < 1024 ) {
		$('#JScategories .fa-bars').click(function () {
	    	$('#JScategories .JSlevel-1').slideToggle();
		});
	 
		$('#JScategories .JSlevel-2 li').parent().parent().addClass('parent').append('<span class="JSsubcategory-toggler"><span class="glyphicon glyphicon-menu-down"></span></span>');
		$('.JSsubcategory-toggler').click(function () {
		    $(this).siblings('ul').slideToggle();
			$(this).parent().siblings().children('ul').slideUp();
		});
	}
	// SUBCATEGORIES
	 setTimeout(function(){
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=3) {
			subitems.slice(i, i+3).wrapAll("<div class='clearfix'></div>");
			}
		});
		
		$('.JSCategoryLinkExpend').click(function (e) {
			e.preventDefault();
			$(this).next('ul').children().slideToggle('fast');
		});
  	 }, 2000);
 
	// PRODUCT PREVIEW IMAGE
    if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({
			gallery:'gallery_01', 
			cursor: 'pointer',  
			imageCrossfade: true,  
			zoomType: 'lens',
			lensShape: 'round', 
			lensSize: 200, 
			borderSize: 1, 
			containLensZoom: true
		}); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    }
    setTimeout(function(){
    	var zoomer = $('.zoomContainer'),
    	    zoomerImg = $('.zoomWrapper'); 
		zoomer.css({
			width: zoomerImg.width(),
			height: zoomerImg.height()
		}); 
// NO ZOOM FOR NO IMAGE
		if($('body').is('#artical-page')){	
			var art_img =  $('#art-img'), 
				tarr = art_img.attr('src').split('/'),
				file = tarr[tarr.length-1],
				img_src = file.split('.')[0];
			if(img_src == 'no-image'){
				$('.zoomContainer').hide();
			}
		}
    }, 500);
 
// ======== SEARCH ============
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
		clearTimeout(searchTime);
		searchTime = setTimeout(timer2, 500);
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		$('.JSsearch_list').remove();
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > 2) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				$('.JSsearchContent2').append(response);
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
/*===============*/
//CHECK VIEW GET RELATED AND LINKED ARTCLES
	if($('body').find('.JSrelated-products').text().trim() == ''){
		$('.JSRelated').hide();
	}
	if($('body').find('.vezani_artikli').find('.JSproduct').text().trim() == ''){
		$('.JSLinked').hide();
	}
 
/***************** B2B ******************/

	$('#b2b-login-icon').click(function () {
	    $('#b2b-login').addClass('login-popup popup-opened');
	});
 
// OCENE
(function($){
	// On Click
	$('#JSstar1').on('click', function(){
			$('#JSreview-number').val('1');
			$('#JSstar1').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar2').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar3').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star").addClass("fa-star-o");
	});
	$('#JSstar2').on('click', function(){
			$('#JSreview-number').val('2');
			$('#JSstar1').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar3').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star").addClass("fa-star-o");
	});
	$('#JSstar3').on('click', function(){
			$('#JSreview-number').val('3');
			$('#JSstar1').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star").addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star").addClass("fa-star-o");
	});
	$('#JSstar4').on('click', function(){
			$('#JSreview-number').val('4');
			$('#JSstar1').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar4').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star").addClass("fa-star-o");
	});
	$('#JSstar5').on('click', function(){
			$('#JSreview-number').val('5');
			$('#JSstar1').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar4').addClass("fa-star").removeClass("fa-star-o");
			$('#JSstar5').addClass("fa-star").removeClass("fa-star-o");
	});
})(jQuery);
 
 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("openMe");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("openMe");		 
	});
 
// SCROLL TO TOP
	setTimeout(function(){ 
	 	$(window).scroll(function () {
	        if ($(this).scrollTop() > 150) {
	            $('.JSscroll-top').css('right', '40px');
	        } else {
	            $('.JSscroll-top').css('right', '-70px');
	        }
	    });
	 
	    $('.JSscroll-top').click(function () {
	        $('body,html').animate({
	            scrollTop: 0
	        }, 600);
	        return false;
	    });
	}, 1000);
// filters slide down
	$('.JScustom-dropdown').click(function(){
	    $(this).next('.JScustom-dropdown-content').slideToggle('fast');
	});
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	
 /*======= MAIN CONTENT HEIGHT ========*/
	if ($(window).width() > 1024 && $('#main-content').height() < $('.JSlevel-1').height()) {
		$('#JScategories').on('mouseover' ,function(){
			$('#main-content').height($('.JSlevel-1').height());
		});
		$('#JScategories').on('mouseleave' ,function(){
			$('#main-content').height('');
		});
	 }

 /*=========== FAST LOOK ======= */
	$(document).on('click', '.JSQuickViewButton', function() {
		$('#JSQuickView').modal('show');
		$('#JSQuickViewContent').html('<img class="gif-loader" src="'+base_url+'images/quick_view_loader.gif">');	
		$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
			$('#JSQuickViewContent').html(response);
		});	
	});
	$(document).on('click', '#JSQuickViewCloseButton', function() { 
		$('#JSQuickView').modal('hide');
	});

	// SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fa fa-facebook"></i>');
	$('.social-icons .twitter').append('<i class="fa fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fa fa-google"></i>');
	$('.social-icons .skype').append('<i class="fa fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fa fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fa fa-youtube-square"></i>');
 
 // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', base_url+'images/no-image.jpg').addClass('img-responsive').css('width', 'auto');
	});

	// POPUP BANER
	if ($(window).width() > 991 ) {
		var first_banner = $('.JSfirst-popup'),
		popup_img = $('.popup-img'),
		close_banner = $('.JSclose-me-please');
		if($('body').is('#start-page')){ //|| $('body').is('#product-page')
			
		 	setTimeout(function(){ 
			 	first_banner.animate({ top: '50%' }, 700);
			}, 1000);   

			close_banner.click(function(){ $('.JSfirst-popup').hide(); });  
		} 
	}
 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		setTimeout(function(){ 
			$('body').before($('.JSleft-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('body').after($('.JSright-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('.JSleft-body-link, .JSright-body-link').width(($(window).outerWidth() - $('.container').outerWidth())/2)
			.height($('body').height()-$('footer').outerHeight()-$('header').outerHeight()-$('#preheader').outerHeight());   
		}, 1000);
  	} 

 
}); // document ready end