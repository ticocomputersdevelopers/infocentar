
$(window).load(function() {

	categori_dropdown();
	
	// REMOVE DOUBLE ZOOM CONTAINER
	
	setTimeout(function() {
	
	    $('.zoomContainer:nth-child(even)').remove();

	}, 300);
	
});


$(document).ready(function () {
	//  fundation init
	$(document).foundation();
	
	$(window).scroll(function () {

		var vScroll = $(window).scrollTop(); 


		if(vScroll > 150){
			$('.JSscroll-top').addClass('show');
		}else{
			$('.JSscroll-top').removeClass('show');
		}
	
		  
	});
	
	
	// Facebook
	window.fbAsyncInit = function() {
		FB.init({
		  appId      : 'your-app-id',
		  xfbml      : true,
		  version    : 'v2.4'
		});
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	   
	// AKCIJA
	
	var akcija_products = $(".JSsale-products .JSproduct");
    for(var i = 0; i < akcija_products.length; i+=12) {
    akcija_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// IZDVAJAMO
	
	var izdvajamo_products = $(".JSfetured-products .JSproduct");
    for(var i = 0; i < izdvajamo_products.length; i+=12) {
    izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// SLICK SLIDER INCLUDE
	
	if ($("#JSmain-slider")[0]){
	
		$('#JSmain-slider').slick({
			autoplay: true
		});
	
	}
	
	if ($(".JSproduct-slider")[0]){
	
		$('.JSsale-products, .JSfetured-products').slick({
		    autoplay: true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
	
	}
	
	if ($(".JSrelated-products")[0]){
	
		$('.JSproduct-slider').slick({
		    infinite: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	
	}

	// most popular products slider
	if ($(".JSMostPopularProducts")[0]){
	
		$('.JSMostPopularProducts').slick({
		    infinite: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	
	}

	// best seller product
	if ($(".JSBestSellerProducts")[0]){
	
		$('.JSBestSellerProducts').slick({
		    infinite: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	
	}
	
	// CATEGORIES - MOBILE
	if ($(window).width() < 1024) {
		$('#JScategories .JStoggler, #JScat_hor .JStoggler, .JScategories-titile').click(function () {
	    	$('#JScategories .JSlevel-1, #JScat_hor .JSlevel-1').slideToggle();
		});
	}
	$('#JScategories .JSlevel-2 li').parent().parent().addClass('parent').append('<span class="JSsubcategory-toggler"><span>+</span></span>');
	$('.JSsubcategory-toggler').click(function () {
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp();
	});
	
	// SUBCATEGORIES
	
	if ($(window).width() > 641 ) {
	
		$('.JSlevel-1 li').each(function () {
			
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=2) {
			subitems.slice(i, i+2).wrapAll("<div class='clearfix'></div>");
			}
		
		});
		
		$('.JSCategoryLinkExpend').click(function (e) {
			e.preventDefault();
			$(this).parent().parent().find('ul').first().slideToggle();
			// $(this).parent().siblings().children('ul').slideUp();
			// $(this).parents('.JSlevel-2 div').siblings().find('ul').slideUp();
		});
		
	}
	
	// POPUP
	
	$('.popup-close').click(function () {
	    $('.popup').removeClass('popup-opened');
	});
	
	// $(".banners-right").css({'height':($(".slick-list").height()+'px')});
	// LOGIN POPUP
	
	// $('#login-icon, .cart-login-button').click(function () {
	// 	$('#b2b-login').removeClass('login-popup popup-opened');
	//     $('.login-popup').addClass('popup-opened');
	// });
	
	// REGISTRATION POPUP & FORM
	
	// $('#registration-icon, .cart-registration-button').click(function () {
	// 	$('#b2b-registration').removeClass('login-popup popup-opened');
	// $('.registration-popup').addClass('popup-opened');
	
	// });  
	/*
	$('#registration-icon, .cart-registration-button').click(function () {
		$('.registration-popup').addClass('popup-opened');
	    $('.registration-form-open-buttons, .company-fields').hide();
		$('.person-fields, .registration-popup h3').show();
	    $('.registration-form').slideDown(440);
		$('.registration-popup').addClass('registration-popup-high');
	});

	$('.registration-cancel').click(function () {

	    $('.popup').removeClass('popup-opened');
	});
	
*/
	$('.person-registration').click(function () {
	    $('.registration-form-open-buttons, .company-fields').hide();
		$('.person-fields, .registration-popup h3').show();
	    $('.registration-form').slideDown(440);
		$('.registration-popup').addClass('registration-popup-high');
	});  
	
	$('.company-registration').click(function () {
	    $('.registration-form-open-buttons, .person-fields').hide();
		$('.company-fields, .registration-popup h3').show();
	    $('.registration-form').slideDown(440);
		$('.registration-popup').addClass('registration-popup-high');
	}); 
	
	$('.registration-cancel').click(function () {
	    $('.registration-form').slideUp(320);
		$('.registration-popup h3').hide();
		$('.registration-popup').removeClass('registration-popup-high');
		
		setTimeout(function() {
		    $('.registration-form-open-buttons').fadeIn();
		}, 440);
		
	});


	// SELECT ARROW - FIREFOX FIX
	
		$('select').wrap('<span class="select-wrapper"></span>');
		$('.select-wrapper').append('<span></span>');
	
	// PRODUCT LIST - SELECT FIELD
	
	$('.JSselect-field').click(function (e) {
	    $(this).children('ul').slideToggle('fast');
		$('.JSselect-field ul').not($(this).children('ul')).slideUp('fast');
		e.stopPropagation();
	});
	
	$('body').click(function () {
	    $('.JSselect-field ul').slideUp('fast');
	});
	
	$(".JSselect-field ul").click(function(e) {
        e.stopPropagation();
    });
	
	// PAGINATION DOWN
	
	if ($('.pagination').prev('.JSproduct-list')) {
	    $('.pagination').addClass('down');
		$('.JSproduct-list-options .pagination').removeClass('down');
	}
	
	// FILTERS
	
	$(".JSselect-field").click(function(){
		$(this).children('.JSmultiselect').toggleClass('m-shown');
		$(".JSselect-field").not(this).children('.JSmultiselect').removeClass('m-shown');
	});

	// PRODUCT PREVIEW IMAGE
	
	if ($(".JSproduct-preview-image")[0]){
	
		jQuery(".JSzoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true }); 
		
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
	
    }
	
	// PRODUCT PREVIEW TABS
	
    $('.JStab-titles li').click(function () {
	    $('.JStab-titles li, .tab-content').removeClass('active');
		$(this).addClass('active');
	});
	
	$('.JStab-title-description').click(function () {
		$('.JSdescription-content').addClass('active');
	});
	
	$('.JStab-title-comments').click(function () {
		$('.JScomments-content').addClass('active');
	});

 	$('.tab-title-tehnicaldoc').click(function () {
		$('.tehnicaldoc-content').addClass('active');
	});
	
	
	// CART
	
	$(".JScart-image img").one("load", function() {

		var maxHeight = -1;

		$('.JScart-item li').each(function() {
		  maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
		});

		$('.JScart-item li').each(function() {
		  $(this).height(maxHeight).css({'line-height': maxHeight + 'px'});
		});
	
	}).each(function() {
    if(this.complete) $(this).load();
	
    });
	
	$('.JScart-without-button').click(function () {
	    $('.JSsubmit-order-area').slideDown();
		$('.JScart-more-icon').addClass('JScart-more-icon-opened');
	});
	
	// SCROLL TO TOP
	
	$('.JSscroll-top').click(function () {
	    $('html, body').animate({scrollTop: 0}, 600);
	});

	var searchTime;
	$('#JSsearch').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch').val() != ''){
        	$(".JSsearch-button").trigger("click");
    	}

		clearTimeout(searchTime);
		searchTime = setTimeout(timer, 500);
	});



	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		
//CHECK VIEW GET RELATED AND LINKED ARTCLES
	if($('body').find('.JSrelated-products').text().trim() == ''){
		$('.JSRelated').hide();
	}
	if($('body').find('.vezani_artikli').find('.JSproduct').text().trim() == ''){
		$('.JSLinked').hide();
	}
//****************


});


function timer(){
	$('.JSfilter-box__titlesearch_list').remove();
	var search = $('#JSsearch').val();
	if (search != '' && search.length > 2) {
		$.post(base_url + 'livesearch', {search:search}, function (response){
			$('.JSsearchContent').append(response);
		});
	} 
}

function categori_dropdown(){
	$('.JSlevel-1 .parent').each(function(){
		var leftOffset = $(this).offset().left;
		var windowWidth = $(window).width();
		
		if (leftOffset > (windowWidth / 2)) {
			$(this).addClass('dropdown-left');
		}
	});
}


/*****************B2B******************/

	$('#b2b-login-icon').click(function () {
	    $('#b2b-login').addClass('login-popup popup-opened');
	});


	$('#b2b-registration-icon').click(function () {
		
	$('#b2b-registration').addClass('registration-popup popup-opened');
	
	});



// // filters JAVASCIRP
// (function($){
// 	"use strict";

// 	// var init
// 	// f => filters
// 	var 	$fTitle,
// 			$fBox,
// 			$fMultiselect;

// 	// events binding
// 	function bindings() {
// 	}

// 	function init (){
// 		if ($fBox.length) {
// 	        $fBox.accordion_2({
// 	            "transitionSpeed": 400
// 	        });
//        	}
// 	}
// 	// on ready
// 	$(document).ready(function() {
// 		$fTitle = $(".JSfilterBox");
// 		$fBox = $fTitle.parent();
// 		$fMultiselect = $fBox.find(".JSmultiselect");


// 		init();
// 		bindings();
// 	});
// })(jQuery);

(function($){
	//$("a.grouped_elements").fancybox();
	$(".JSmenu-icon").on("click", function(){
		$("#JSpreheader").show("slow");
		$(".JSmain-nav").show("slow");
	});
	$(".menu-close").on("click", function(){
		$("#JSpreheader").hide("slow");
		$(".JSmain-nav").hide("slow");
	});
})(jQuery);


// OCENE
(function($){
	
	// On Click
	$('#JSstar1').on('click', function(){
			$('#review-number').val('1');

			$('#JSstar1').addClass("fa-star");
			$('#JSstar1').removeClass("fa-star-o");
			$('#JSstar2').removeClass("fa-star");
			$('#JSstar2').addClass("fa-star-o");
			$('#JSstar3').removeClass("fa-star");
			$('#JSstar3').addClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star");
			$('#JSstar4').addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star");
			$('#JSstar5').addClass("fa-star-o");
	});

	$('#JSstar2').on('click', function(){
			$('#review-number').val('2');

			$('#JSstar1').addClass("fa-star");
			$('#JSstar1').removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star");
			$('#JSstar2').removeClass("fa-star-o");
			$('#JSstar3').removeClass("fa-star");
			$('#JSstar3').addClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star");
			$('#JSstar4').addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star");
			$('#JSstar5').addClass("fa-star-o");
	});

	$('#JSstar3').on('click', function(){
			$('#review-number').val('3');

			$('#JSstar1').addClass("fa-star");
			$('#JSstar1').removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star");
			$('#JSstar2').removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star");
			$('#JSstar3').removeClass("fa-star-o");
			$('#JSstar4').removeClass("fa-star");
			$('#JSstar4').addClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star");
			$('#JSstar5').addClass("fa-star-o");
	});

	$('#JSstar4').on('click', function(){
			$('#review-number').val('4');

			$('#JSstar1').addClass("fa-star");
			$('#JSstar1').removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star");
			$('#JSstar2').removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star");
			$('#JSstar3').removeClass("fa-star-o");
			$('#JSstar4').addClass("fa-star");
			$('#JSstar4').removeClass("fa-star-o");
			$('#JSstar5').removeClass("fa-star");
			$('#JSstar5').addClass("fa-star-o");
	});

	$('#JSstar5').on('click', function(){
			$('#review-number').val('5');

			$('#JSstar1').addClass("fa-star");
			$('#JSstar1').removeClass("fa-star-o");
			$('#JSstar2').addClass("fa-star");
			$('#JSstar2').removeClass("fa-star-o");
			$('#JSstar3').addClass("fa-star");
			$('#JSstar3').removeClass("fa-star-o");
			$('#JSstar4').addClass("fa-star");
			$('#JSstar4').removeClass("fa-star-o");
			$('#JSstar5').addClass("fa-star");
			$('#JSstar5').removeClass("fa-star-o");
	});

})(jQuery);

// $(function(){
//   	$('#login-icon, .login_popup').hover(function() {
//     	$('#registration-icon').addClass('login_popup_hover');
// 		$('#login_popup_email').focus();
//   	}, function() {
//     	$('#registration-icon').removeClass('login_popup_hover');
// 		$('#login_popup_email').blur().val('');
//   	})	
// })



$(function () {
    if ($('.JSlevel-1').height() > 380) {
    	$('#JSshift_right').addClass("medium-9 medium-offset-3");
		
		window.onresize = function(event) {
			$(function () {
				if ($(window).width() <= 1024) {
					$('#JSshift_right').removeClass("medium-9 medium-offset-3");
				} else {
					$('#JSshift_right').addClass("medium-9 medium-offset-3");
				}
			});
		};
    }
});
 
// filters slide down
	$(".click-for-slide").click(function(){
	    $(this).next(".slide-me").slideToggle('fast');
	});

   // Main Content Height
	$(function () {
		if ($(window).width() > 1024 ) {
			if ($('.JSlevel-1').height() > $('#main-content').height()) {
			 	$('#main-content').css('min-height', $('.JSlevel-1').height() + 20);
			 }
	 	}
	});