
/*======= FIXED HEADER ==========*/ 
$(window).scroll(function(){

	var header = $('header');
	var header_height = $('header').height();
	var offset_top = $('header').offset().top;
	var offset_bottom = offset_top + header.outerHeight(true);

 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {
	  		if($('#admin-menu').length){ $('.JSsticky_header').css('top', $('#admin-menu').outerHeight() + 'px'); }
			$('#JSfixed_header').addClass('JSsticky_header');
			header.css('height', header_height + 'px');	     
			$('#top-head-logo').removeClass('col-md-3');   
			$('#top-head-logo').addClass('col-md-2');   
	    }else {
			$('#JSfixed_header').removeClass('JSsticky_header');   
			header.css('height', 'initial');
			$('#top-head-logo').addClass('col-md-3');   
			$('#top-head-logo').removeClass('col-md-2');  
	    }
	}
});    
 
 