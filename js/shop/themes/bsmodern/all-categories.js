// PRIKAZ SVIH KATEGORIJA U PRODAVNICI = SVE KATEGORIJE 
$(document).ready(function () {

    smoothScroll(250);
    openAllCategoryList();
    
    var fixed_header = $('#JSfixed_header');
    var cat_sidebar_list = $('.JScategory-sidebar__list');
    var cat_sidebar_list_Offset = cat_sidebar_list.offset().top;
    var sidebarMarginTop = 20;

    $(window).scroll(function () {
         var vScroll = $(window).scrollTop();
 
        if (fixed_header.hasClass('JSsticky_header')){
            var fixedHeader = true;
        }else{
            var fixedHeader = false;
        } 
        if (fixedHeader) { sidebarMarginTop = 80; } 
        
        if (fixedHeader) { 
            if(vScroll > cat_sidebar_list_Offset - sidebarMarginTop) {
                cat_sidebar_list.addClass('category-sidebar__list--fix').css('top', 75);
            }
            else {
                cat_sidebar_list.removeClass('category-sidebar__list--fix');
            }    
        } else{ 
            if(vScroll > cat_sidebar_list_Offset - sidebarMarginTop) {
                cat_sidebar_list.addClass('category-sidebar__list--fix');
            }
            else {
                cat_sidebar_list.removeClass('category-sidebar__list--fix');
            }   
        }
    });
 
    function smoothScroll (duration) {
        $('a[href^="#"]').on('click', function(event) { 
            var target = $( $(this).attr('href') ); 
            if( target.length ) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - sidebarMarginTop
                }, duration);
            } 
        });
    }

    // category sidebar toggler
    function openAllCategoryList(){
        $('.JScategory-sidebar__list__toggler').on('click', function(){
            $(this).parent().toggleClass('category-sidebar__list--open');
        });
    }
});



 