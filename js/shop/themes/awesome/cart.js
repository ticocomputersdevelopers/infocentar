$(document).ready(function(){
	$('#JSAddCartSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSAddCartForm').submit();
	});
	$(document).on('click','.JSadd-to-cart',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'list-cart-add',{roba_id: roba_id},function(response){
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");

			var results = $.parseJSON(response);
			if(parseInt(results.check_available) == 0){
				obj.after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.remove();
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);

		});
	});

	$('.JSadd-to-cart-vezani').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var kolicina = obj.closest('div').find('.JSkolicina').val();
		$.post(base_url+'vezani-cart-add',{roba_id: roba_id, kolicina: kolicina},function(response){
			var results = $.parseJSON(response);
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");					
			if(results.check_available <= 0){
				obj.closest('div').after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.closest('div').remove();			
			}

			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);
		});
	});

	$('.JScart-less, .JScart-more').click(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina_temp = obj.closest('li').find('.JScart-amount').val();
		var kolicina;
		if(obj.attr('class') == 'JScart-less'){
			kolicina = parseInt(kolicina_temp) - 1;
		}
		else if(obj.attr('class') == 'JScart-more'){
			kolicina = parseInt(kolicina_temp) + 1;
		}

		obj.closest('li').find('.JScart-amount').val(kolicina);
		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				if(response=='kolicina-changed'){
					location.reload(true);
					$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina je promenjena.</p>");
				}else{
					$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");
				}
			});
		}else{
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina ne sme biti manja od 1!</p>");
		}
	});

	$(document).on('click','.JSDeleteStavka',function(){
		var stavka_id = $(this).data('stavka_id');
		$.post(base_url+'cart-stavka-delete',{stavka_id: stavka_id},function(response){
			location.reload(true);
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je uklonjen iz korpe.</p>");
		});
	});
	$('#JSDeleteCart').click(function(){
		var check = confirm("Da li ste sigurni da želite da ispraznite korpu?");
		if(check){		
			$.post(base_url+'cart-delete',{},function(response){
				location.reload(true);
				$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
				$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Korpa je ispražnjena.</p>");
			});
		}
	});
	
	if($('select[name="web_nacin_placanja_id"]').val() == 3){
		$('#JSCaptcha').removeAttr('hidden');
	}
	$(document).on('change','select[name="web_nacin_placanja_id"]',function(){
		if($(this).val() == 3){
			$('#JSCaptcha').removeAttr('hidden');
		}else{
			$('#JSCaptcha').attr('hidden','hidden');
		}
	});
});

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 