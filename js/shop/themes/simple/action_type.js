
// Izdvajanje akcija i tipova na pocetnoj strani B2C 

// AJVASCRIPT FOR TIP SLIDER
(function($, window){

    // INIT VARABLES
    var base_url,
        $body,
        $ajaxContent,
        $arrowNext,
        $arrowPrev,
        $arrowNextAndPrev,
        $tipLoader,
        $slideArea,
        offset,
        stopslide = false,
        $window,
        productToShow = 5,
        kind;

    // FUNCTIONS
    
    function onarrowNextAndPrevClick() {
        var $this = $(this);
        var tip_id = $this.data('id');

        // da li je kliknuto na next li prev
        if($this.hasClass('next')){
            offset = $this.data('offset') + 1;
        }else{
            offset = $this.data('offset') - 1;
        }

        // pozivamo ajax
        ajaxChangeSlideArticles(tip_id);
    }

    // popunjava slider drugim articlima
    function ajaxChangeSlideArticles(tip_id) {
        if(tip_id == 'akcija'){
            kind = 'akcija';
        }else{
            kind = 'tip';
        }

        $.ajax({
            url: base_url + 'tip-ajax',
            type: 'POST',
            data: {
                tip_id : tip_id,
                offset : offset,
                limit: productToShow,
                kind: kind
            },
            async: false,
            success:function(response) {
                var results = JSON.parse(response);
                $('#JS'+tip_id+'-content').html(results.list);
                $('#JS'+tip_id+'-next, #JS'+tip_id+'-previous').data('offset', results.offset);
            },
            // beforeSend: function() {
            //     // otvaramo loader
            //    $('.js-loader-' + tip_id).removeClass('hide');
            // },
            // complete: function() {
            //     // sklanjamo loader
            //    $('.js-loader-' + tip_id).addClass('hide');
            // }
        });
    }

    // popunjava slider sa proizvodima
    function initArticles() {
        $ajaxContent.each(function(i, obj) {
            var tip_id = $(obj).data('tipid');
            ajaxInitArticles(tip_id);
        });
    }

    function nextSlideEvent() {
        $ajaxContent.each(function(i, obj) {
            var tip_id = $(obj).data('tipid');
            // if(stopslide != tip_id){
            if(!stopslide){        
                offset = $(obj).closest('section').find('.slider-next').data('offset') + 1;
                ajaxChangeSlideArticles(tip_id);
            }
        });
    }

    // ajax za popunjavanje slidera
    function ajaxInitArticles(tip_id) {
        if(tip_id == 'akcija'){
            kind = 'akcija';
        }else{
            kind = 'tip';
        }

        $.ajax({
            url: base_url + 'tip-ajax',
            type: 'POST',
            data: {
                tip_id : tip_id,
                offset : 1,
                limit: productToShow,
                kind: kind
            },
            success:function(response) {
                var results = JSON.parse(response);
                $('#JS'+tip_id+'-content').html(results.list);
            },
            // beforeSend: function() {
            //     // otvaramo loader
            //    $tipLoader.removeClass('hide');
            // },
            // complete: function() {
            //     // sklanjamo loader
            //    $tipLoader.addClass('hide');
            // }
        });
    }

    // BINDING EVENTS
    function bindings(){
        $arrowNextAndPrev.on('click', onarrowNextAndPrevClick);
    }

    function checkSliderEvent(){
        $slideArea.mouseenter(function(){
            stopslide = true;
            // stopslide = $(this).find('.ajax-content').data('tipid');
        }).mouseleave(function(){
            stopslide = false;
        });
    }

    function init() {

        var windowWidth = $window.width();
        if (windowWidth > 1160) {
            productToShow = 4;
        }
        else if (windowWidth < 1160 && windowWidth > 880) {
            productToShow = 3;
        } else if (windowWidth < 880  && windowWidth > 640) {
            productToShow = 2;
        } else if (windowWidth < 640) {
            productToShow = 1;
        }
    }

    $(document).ready(function (){
        $body = $('body');
        $ajaxContent = $body.find('.ajax-content');
        $arrowNextAndPrev = $body.find('.next, .previous');
        $tipLoader = $body.find('.js-tip-loader');
        $slideArea = $body.find('.sale-list, .tip-list');
        $window = $(window);

        base_url = $("#base_url").val();
        bindings();

        // popunjava slider sa proizvodima
        init();
        initArticles();
        
        $window.resize(function() {
            init();
        });

        checkSliderEvent();

        setInterval(nextSlideEvent,5000);
    });
})(jQuery, window);
