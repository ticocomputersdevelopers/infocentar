

/* Navigation link */

var navigationLength = $('.site-navigation li').length;
if( navigationLength > 5 && $(window).width() > 1024) {
	$('.site-navigation').append('<div class="dropdown-nav"><a href="#!">Više <i class="fas fa-chevron-down"></i></a> <ul class="dropdown-ul"></ul></div>');

	$('.site-navigation li').each(function(index) {
		 
		if( index  > 4 ) {
			$(this).appendTo('.dropdown-ul')
		}	
		if ($(window).width() < 1335) {
			if( index  > 3 ) {
				$(this).appendTo('.dropdown-ul')
			}	
		}
		if ($(window).width()< 1215) {
			if( index  > 2 ) {
				$(this).appendTo('.dropdown-ul')
			}	
		}
	})
}

$('.dropdown-nav').find('a').on('mouseover', function() {
	$('.dropdown-ul').show();
})
.on('mouseout', function() {
	$('.dropdown-ul').hide();
})


$(document).ready(function () {

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
	// AKCIJA
	
	var akcija_products = $(".JSsale-products .JSproduct");
    for(var i = 0; i < akcija_products.length; i+=12) {
    akcija_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// IZDVAJAMO
	
	var izdvajamo_products = $(".JSfetured-products .JSproduct");
    for(var i = 0; i < izdvajamo_products.length; i+=12) {
    izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
});
	// SLICK SLIDER INCLUDE
	if ($("#JSmain-slider")[0]){
		if($('#admin_id').val()){
			$('#JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});
		}else{
			$('#JSmain-slider').slick({
				autoplay: true
			});
		}
	
	}


	// SCROLL TO TOP
	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });


 	// SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');	

	/* Moblie navigation */
	if($(window).width() < 1024) {
		$('.site-navigation').css('right', - $('.site-navigation').width() - 60)
		$('#navigationToggle').click(function() {
			$('.site-navigation').css('right', '0px');
			$('.site-navigation').css('z-index', '999');
			$('body').css({ 
				"right": $('.site-navigation').width(), 
				"overflow": "hidden",
				"transition": "all 0.4s"
			});
			$('.body-overlay').css('display', 'block');
		})

		$('.site-navigation .exit, .body-overlay').click(function() {
			$('.site-navigation').css('right', - $('.site-navigation').width() - 60);
			$('body').css({ 
				"right": "0px", 
				"overflow": "visible"
			})
			$('.body-overlay').css('display', 'none');
		})
	}

function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	

function alertAmount(message) {
	swal(message , {
	  buttons: false,
	});
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}	


// SOCIAL ICONS
$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');


