	base_url = $('#base_url').val();

	function newsletter() {
		email = document.getElementById('newsletter').value;
		if (isValidEmailAddress(email) == true) {
			/*$.post(base_url+'newsletter',{ emeil:email }, success: function( msg ) {
		alert( msg );
		});*/
			$.ajax({
				type: "POST",
				url: base_url + 'newsletter',
				data: { email: email },
				success: function(msg) {
					alertSuccess(msg);
				}
			});
		} else {
			alertError("E-mail adresa nije validna.");
		}
	}


	function check_fileds(polje) {
		polje2 = document.getElementById(polje).value;
		if (polje == 'email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JSwithout-reg-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JSwithout-reg-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JSwithout-reg-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JSkontakt-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JSkontakt-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JSkontakt-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else {
			if (polje2 == '' || polje2 == '') {
				$('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
			}
		}
	}

	function meil_send() { 
		name = document.getElementById('JSkontakt-name').value;
		email = document.getElementById('JSkontakt-email').value;
		message = document.getElementById('message').value;
		if (name == '' || name == ' ') {
			$('#JSkontakt-name').css("border", "1px solid red").focus().fadeIn(200);
		} else if (isValidEmailAddress(email) == false) {
			$('#JSkontakt-email').css("border", "1px solid red").focus().fadeIn(200);
		} else {
			$.ajax({
				type: "POST",
				url: base_url + 'meil_send',
				data: { name: name, message: message, email: email },
				success: function() {
					alertSuccess('Vaša poruka je poslata!');

					setTimeout(function() {
						location.reload(true);
					}, 1500);
				}
			});
		}
	}
	//Proverava da li je polje prazno ili sadrzi nedozvoljene karaktere
	function isValidField(fields) {
		var check = true;
		var field = "";
		var script = /["-*<->-(){}`|^]+$/; // Invalid characters:  !"#$%^&*'() <=?> {}`|^ Valid characters: +-_., SPACE a-z A-Z 0-1
		for (i = 0; i < fields.length; i++) {
			var ind = fields[i].value.search(script);
			//For empty fields
			if ((fields[i].value == "") || (ind != -1)) {
				check = false;
				//Fokusira se na polje.Treba napraviti obavestenje za to polje, zvezdica ili nesto slicno
				fields[i].focus(fields[i].id);
				$('#' + fields[i].id).css("border", "1px solid red").next().fadeIn(200);
			} else {
				$('#' + fields[i].id).css("border", "1px solid gray").next().fadeOut(200);
			}
		}
		return check;
	}
	//Proverava da li je validna e-mail adresa
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	//Dozvoljava samo numeric values
	function isNumberKey(charCode) {
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
 