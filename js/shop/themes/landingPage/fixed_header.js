

$('header').css('position', 'fixed');




if($('#admin-menu').length > 0) {
	$('#admin-menu').addClass('admin-menu-active');
	$('header').css('top', $('#admin-menu').outerHeight());
}


$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
       $('#fixed_header').addClass('animate-header');
    } else {
        $('#fixed_header').removeClass('animate-header');
    }
});


/* Body padding */
$(document).ready(function () {

		if($('#admin-menu').length > 0) {
			$('body').css('transition', 'none');
			$('body').css('padding-top', $('header').outerHeight() + $('#admin-menu').outerHeight());
		} else {
			$('body').css('transition', 'none');
			$('body').css('padding-top', $('header').outerHeight());
		}
	
});

