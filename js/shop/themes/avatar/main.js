



$(".header-sub-div").append( "<div class='float-right-div'> </div>" );

if($(window).width() < 1024) {
	$('#responsive-nav').append($('#top-navigation'));

	$(".float-right-div").append( $('.cart-wish-div') );


	$('.JSSearchIcon').click(function() {
		$('.resp-icon').find('ul').fadeOut(50);
		$('.search-div-content').fadeToggle(100);
		$(this).find('i').toggleClass("active")
	})
}


if($(window).width() > 1024) {
	$('.dropdown-nav').on('mouseover', function() {
		$(this).find('.dropdown-ul').show();
	})
	.on('mouseout', function() {
		$(this).find('.dropdown-ul').hide();
	})
}

if($(window).width() < 1024) {
	$('.JSDropdown-scroll').click(function() {
		$(this).parent('.dropdown_div').next().slideToggle(250);
		$(this).toggleClass('JSDropdown-scroll-rotate');
	})
}

function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	

function alertAmount(message) {
	swal(message , {
	  buttons: false,
	});
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}	


/* Kategorije hover */
$('.level-1-list').hover(function() {
	$($(this).find('.level-1-link')).css('border-right', 'none'); 
},function() {
	$($(this).find('.level-1-link')).css('border-right', '1px solid var(--border-color)');
})


$(document).ready(function () {

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
	// AKCIJA
	
	var akcija_products = $(".JSsale-products .JSproduct");
    for(var i = 0; i < akcija_products.length; i+=12) {
    akcija_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
	// IZDVAJAMO
	
	var izdvajamo_products = $(".JSfetured-products .JSproduct");
    for(var i = 0; i < izdvajamo_products.length; i+=12) {
    izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
	}
	
 
	// SLICK SLIDER INCLUDE
	if ($("#JSmain-slider")[0]){
		if($('#admin_id').val()){
			$('#JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});
		}else{
			$('#JSmain-slider').slick({
				autoplay: true
			});
		}
	
	}
	
	if ($(".JSproduct-slider")[0]){
	
		$('.JSsale-products, .JSfetured-products').slick({
		    autoplay: true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
	
	}
	
	
 if($('body').is('#start-page') || $('body').is('#artical-page')){
	$('.JSproduct-slider').slick({
	    infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		]
	});

   $('.multiple-items-news').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  responsive: [
		{
		  breakpoint: 1100,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
		  }
		},
		{
		  breakpoint: 800,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 580,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	]
	});

   	$('.JSBrandSlider').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 1000,
	  responsive: [
	  {
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
			autoplay: true,
			autoplaySpeed: 1000,
		  }
		},
		{
		  breakpoint: 800,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			autoplay: true,
			autoplaySpeed: 1000,
		  }
		},
		{
		  breakpoint: 700,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
			autoplay: true,
			autoplaySpeed: 1000,
		  }
		},
		{
		  breakpoint: 580,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1000
		  }
		}
	]
	});
 }
 
	// CATEGORIES - MOBILE
	if ($(window).width() < 1024 ) {
		$('#JScat_hor .JStoggler, #JScategories .fa-bars').click(function () {
	    	$('#JScategories .JSlevel-1, #JScat_hor .JSlevel-1').slideToggle();
		});
	}
	$('#JScategories .JSlevel-2 li').parent().parent().addClass('parent').append('<span class="JSsubcategory-toggler"><span class="glyphicon glyphicon-menu-down"></span></span>');

	$('.JSsubcategory-toggler').click(function () {
		$(this).parent().siblings().find('.JSlevel-4').slideUp();
		$(this).parent().siblings().find('.JSlevel-3').slideUp();
		$(this).parent().siblings().find('.JSlevel-2').slideUp();

		$(this).parent().siblings().find('.JSsubcategory-toggler').find('span').removeClass('rotate-arrow');
		
	    $(this).siblings('ul').slideToggle();	
	    $(this).find('span').toggleClass('rotate-arrow');
	});
	
	// SUBCATEGORIES
	 
		$('.JSlevel-1 li').each(function () {
			
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=2) {
			subitems.slice(i, i+2).wrapAll("<div class='clearfix'></div>");
			}
		
		});
		
		$('.JSCategoryLinkExpend').click(function (e) {
			e.preventDefault();
			$(this).next('ul').slideToggle('fast');
			$(this).toggleClass('rotate-arrow')
			// $(this).parent().siblings().children('ul').slideUp();
			// $(this).parents('.JSlevel-2 div').siblings().find('ul').slideUp();
		});
  
	// PAGINATION DOWN
	
	if ($('.pagination').prev('.JSproduct-list')) {
	    $('.pagination').addClass('down');
		$('.JSproduct-list-options .pagination').removeClass('down');
	}
 
	// PRODUCT PREVIEW IMAGE
	
	if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true }); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    }
	


 
// ====================

	var searchTime;
	$('#JSsearch').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch').val() != ''){
        	$(".JSsearch-button").trigger("click");
    	}

		clearTimeout(searchTime);
		searchTime = setTimeout(timer, 500);
	});

		var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}

		clearTimeout(searchTime);
		searchTime = setTimeout(timer2, 500);
	});

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		
//CHECK VIEW GET RELATED AND LINKED ARTCLES
	if($('body').find('.JSrelated-products').text().trim() == ''){
		$('.JSRelated').hide();
	}
	if($('body').find('.vezani_artikli').find('.JSproduct').text().trim() == ''){
		$('.JSLinked').hide();
	}

/////////
$('.JSSearchGroup').change(function(){
	timer();
});

$('.JSSearchGroup2').change(function(){
	timer2();
});

function timer(){
	$('.JSsearch_list').remove();
	var search = $('#JSsearch').val();
	if (search != '' && search.length > 2) {
		$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup').val()}, function (response){
			$('.JSsearchContent').append(response);
		});
	} 
}

function timer2(){
	$('.JSsearch_list').remove();
	var search = $('#JSsearch2').val();
	if (search != '' && search.length > 2) {
		$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
			$('.JSsearchContent2').append(response);
		});
	} 
}

/*****************B2B******************/

	$('#b2b-login-icon').click(function () {
	    $('#b2b-login').addClass('login-popup popup-opened');
	});

	$('#b2b-registration-icon').click(function () {
		 $('#b2b-registration').addClass('registration-popup popup-opened');
	});

 
// OCENE
(function($){
	
	// On Click
	$('#JSstar1').on('click', function(){
			$('#JSreview-number').val('1');

			$('#JSstar1').addClass("review-star-active");
			$('#JSstar2').removeClass("review-star-active");		
			$('#JSstar3').removeClass("review-star-active");
			$('#JSstar4').removeClass("review-star-active");
			$('#JSstar5').removeClass("review-star-active");
			
	});

	$('#JSstar2').on('click', function(){
			$('#JSreview-number').val('2');

			$('#JSstar1').addClass("review-star-active");
			$('#JSstar2').addClass("review-star-active");
			$('#JSstar3').removeClass("review-star-active");
			$('#JSstar4').removeClass("review-star-active");	
			$('#JSstar5').removeClass("review-star-active");
	});

	$('#JSstar3').on('click', function(){
			$('#JSreview-number').val('3');
	
			$('#JSstar1').addClass("review-star-active");
			$('#JSstar2').addClass("review-star-active");	
			$('#JSstar3').addClass("review-star-active");	
			$('#JSstar4').removeClass("review-star-active");		
			$('#JSstar5').removeClass("review-star-active");
	});

	$('#JSstar4').on('click', function(){
			$('#JSreview-number').val('4');
	
			$('#JSstar1').addClass("review-star-active");
			$('#JSstar2').addClass("review-star-active");	
			$('#JSstar3').addClass("review-star-active");
			$('#JSstar4').addClass("review-star-active");	
			$('#JSstar5').removeClass("review-star-active");
	});

	$('#JSstar5').on('click', function(){
			$('#JSreview-number').val('5');
	
			$('#JSstar1').addClass("review-star-active");
	      	$('#JSstar2').addClass("review-star-active");
			$('#JSstar3').addClass("review-star-active");
			$('#JSstar4').addClass("review-star-active");	
			$('#JSstar5').addClass("review-star-active");
	});

})(jQuery);
 

$( document ).ready(function() {

	if ($(window).width() >= 1024 && $('.JSlevel-1').height() > $('.main-slider').height()) {
		$('.banners-right').css('margin-left', '270px');
	} else {

		$('.banners-right').css('margin-left', '0');
	}

	if($('.JSlevel-1').height() > ($('.main-slider').height() + $('.baners-div').height() - 5 ) && $(window).width() >= 1024) {
		$('.action-type-section').css('margin-top', 20 + $('.JSlevel-1').height() - ($('.main-slider').height() + $('.baners-div').height()));
	}

});

 
 /*console.log($('.main-slider').height() + $('.baners-div').height())

 console.log($('.JSlevel-1').height())*/

   /*  BRENDOVI SLAJDER  */
      
 //      $('.JSBrandSlider').slick({
 //        autoplay: true,
 //        autoplaySpeed: 3000,
 //        slidesToShow: 6,
 //        speed: 1000,
 //        slidesToScroll: 3,
 //      responsive: [
 //      	{
	//   breakpoint: 900,
	// 	settings: {
	// 	slidesToShow: 4,
	// 	slidesToScroll: 2,
	// 	infinite: true,
	// 	dots: true
	// 	}
	// 	},
 //        {
 //      breakpoint: 600,
 //        settings: {
 //        slidesToShow: 3,
 //        slidesToScroll: 2,
 //        }
	//     },
	//     {
	//   breakpoint: 480,
	// 	settings: {
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1
	//   }
	// }
 //   ]
 // });
 

 // RESPONSIVE NAVIGATON
 	$('.resp-nav-btn').on('click', function(){  
		$('#responsive-nav').toggleClass('openMe');		 
		$(this).toggleClass('active');
		$('.body-overlay').show();
	});
	$('.JSclose-nav, .body-overlay').on('click', function(){  
		$('#responsive-nav').removeClass('openMe');		
		$('.resp-nav-btn').removeClass('active')
		$('.body-overlay').hide();
	});






// SCROLL TO TOP
 $(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });

// filters slide down
	$(".JSfilters-slide-toggle").click(function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
	});
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<div class="select-wrapper"></div>');
 

// level-1-list

 $(function(){ 
 	var level1Link = $('.level-1-list');
 	if($('.JSmain-slider').length > 0  && $(window).width() > 1024) {
	 	level1Link.find('.JSlevel-2').css('width', $('#JSmain-slider .JSmain-slider').width() + 5);
	}
	else {
		level1Link.find('.JSlevel-2').css('width', $(window).width()/2);
	}

 	$(level1Link).hover(function (){
 		$(this).find('.JSlevel-2').css({
 			'visibility': 'visible',
 			'opacity': '1' 
 		});
 	}, function() {
 		$(this).find('.JSlevel-2').css({
 			'visibility': 'hidden',
 			'opacity': '0' 
 		});
 	});
 });

 
/* Wow Animation */
 
	if($(window).width() > 640) {
		new WOW().init();
	}
 
/* Change produc img on hover */
/*$(document).ready(function(){ 
	$('.product-image-wrapper').on('hover',function() {


	}, function() {

		$(this).find('.product-image').css('opacity', '1');
		$(this).find('.product-image').css('visibility', 'visible');
		$(this).find('.second-product-image').css('opacity', '0');
		$(this).find('.second-product-image').css('visibility', 'hidden');

	});
});
*/


$(document).on('mouseover', '.product-image-wrapper', function(){ 
	if($(this).find('.product-image').attr('src') != $(this).find('.second-product-image').attr('src')){
		$(this).find('.second-product-image').css('opacity', '1');
		$(this).find('.second-product-image').css('visibility', 'visible');

		$(this).find('.product-image').css('opacity', '0');
		$(this).find('.product-image').css('visibility', 'hidden');
	}
});


$(document).on('mouseout', '.product-image-wrapper', function(){ 
	if($(this).find('.product-image').attr('src') != $(this).find('.second-product-image').attr('src')){
		$(this).find('.product-image').css('opacity', '1');
		$(this).find('.product-image').css('visibility', 'visible');

		$(this).find('.second-product-image').css('opacity', '0');
		$(this).find('.second-product-image').css('visibility', 'hidden');
	}
});

$(document).on('mouseover', '.sale-label', function(){ 

	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '1');
	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'visible');

	$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '0');
	$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'hidden');

});


$(document).on('mouseout', '.sale-label', function(){ 

	$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '1');
	$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'visible');

	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '0');
	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'hidden');

});

$(document).on('mouseover', '.multiple-function-div', function(){ 

	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '1');
	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'visible');

	$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '0');
	$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'hidden');

});


$(document).on('mouseout', '.multiple-function-div', function(){ 

	$(this).siblings('.product-image-wrapper').find('.product-image').css('opacity', '1');
	$(this).siblings('.product-image-wrapper').find('.product-image').css('visibility', 'visible');

	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('opacity', '0');
	$(this).siblings('.product-image-wrapper').find('.second-product-image').css('visibility', 'hidden');

});

 

	/* Empty cart remove padding */
	if($('.header-cart-div .JSheader-cart-content ul').length < 1) {
		$('.JSheader-cart-content').css('display', 'none');
	} 
	$(document).on('click', '.JSadd-to-cart, .JSAddToCartQuickView', function() {
		$('.JSheader-cart-content').css('display', 'block');
	})


	/* Article preview add active class */
	
	$($('.JSproduct-preview-image #gallery_01 .elevatezoom-gallery')[0]).addClass('active');


	/* Product-preview-tabs */
	$('.product-preview-tabs .tab-titles a').click(function(event) {

		$('.product-preview-tabs .tab-titles a').removeClass('active');
		$(this).addClass('active');

	

		if($(event.target).is('.description')) {
			$('#description-tab').slideDown();
			$('#technical-docs').slideUp();
			$('#the-comments').slideUp();
		}
		else if($(event.target).is('.documentation')) {
			$('#technical-docs').slideDown();
			$('#description-tab').slideUp();
			$('#the-comments').slideUp();
		}
		else if($(event.target).is('.comments')) {
			$('#the-comments').slideDown();
			$('#description-tab').slideUp();
			$('#technical-docs').slideUp();
		}
	})

	/* Related article */
	if($('.JSrelated-products .JSproduct').length < 1) {
		$('.related-section').hide();
		$('.product-preview-tabs').css('padding-bottom', '0px');
	}

	/* Bound article */
	if($('.bound-section .JSproduct').length < 1) {
		$('.bound-section').hide();
	}

	/* Article details margin */
	if($('.osobine').length > 0 ) {
		$('.article-details-page .add-to-cart-area').css('margin-bottom', '24px');
		$('.article-details-page .add-to-cart-area').css('padding-top', '8px');
		$('.article-details-page .product-preview-price').css('margin-bottom', '0px');
		$('.article-details-page .product-preview-price').css('padding-top', '12px');
	}


	/* Pagination padding */
	if($('.pagination-div .pagination li').length < 1) {
		$('.pagination-div').css('padding', '0px');
	}

	if($('.tag-content').length > 0) {
		$('.product-preview-tabs').css('padding-top', '50px');
	}

		// SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fa fa-facebook-square"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fa fa-youtube-square"></i>'); 



/* Compare button and modal */
$(document).on('click', '.JScompare', function() {
	var $this = $(this),
		dataId = $this.data('id');
	if ($(this).hasClass('active')) {
		$.post(base_url + 'clearCompare', { clear_id: dataId }, function(response) { });
		$this.removeClass('active');
	} else {
		$(this).addClass('active');
		$.post(base_url + 'compareajax', { id: dataId }, function(response) { });
		$('#JScompareTable').text('');
	}
});
$(document).on('click', '#JScompareArticles', function() {
	$.post(base_url + 'compare', { id: $(this).data('id') }, function(response) {
		$('#JScompareTable').html(response.content);
	});
});
$(document).on('click', '.JSclearCompare', function() {
	var $this = $(this),
		clearDataId = $this.data('id');
	$('.JScompare').filter('[data-id="' + clearDataId + '"]').removeClass('active');
	$.post(base_url + 'compare', { clear_id: clearDataId }, function(response) {
		$('#JScompareTable').html(response.content);
	});
});


/* Quick view modal */
$(document).on('click', '.JSQuickViewButton', function() {
	$('#JSQuickView').modal('show');
	$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
		$('#JSQuickViewContent').html(response);
	});	
});


$(document).on('click', '#JSQuickViewCloseButton', function() {
	$('#JSQuickViewContent').html('<img class="gif-loader" src="'+base_url+'images/quick_view_loader.gif">');	
	$('#JSQuickView').modal('hide');
});

/* End of Quick view modal */


 	if ($(window).width() > 1024){ 
		$('#JScategories').on('mouseover', function(){ $('.JSktgDarken').show(); }); 
		$('#JScategories').on('mouseleave', function(){ $('.JSktgDarken').hide(); });
	}


	// POPUP BANER
	if ($(window).width() > 991 ) {
		var first_banner = $('.JSfirst-popup'),
		popup_img = $('.popup-img'),
		close_banner = $('.JSclose-me-please');
		if($('body').is('#start-page')){ //|| $('body').is('#product-page')
			
		 	setTimeout(function(){ 
			 	first_banner.animate({ top: '50%' }, 700);
			}, 1000);   

			close_banner.click(function(){ $('.JSfirst-popup').hide(); });  
		} 
	}
 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		setTimeout(function(){ 
			$('body').before($('.JSleft-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('body').after($('.JSright-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('.JSleft-body-link, .JSright-body-link').width(($(window).outerWidth() - $('.container').outerWidth())/2)
			.height($('body').height()-$('footer').outerHeight()-$('header').outerHeight()-$('#preheader').outerHeight());   
		}, 1000);
  	} 

}); // DOCUMENT READY END


/* Show Filter */
if($(window).width() < 992)
$('.JSShowFilters').click(function() {
	$(this).text(($(this).text() == 'Sakrij filtere') ? 'Prikaži filtere' : 'Sakrij filtere')
	$('.filters').slideToggle();
})











