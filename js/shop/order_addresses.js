$(document).ready(function () {
	$(document).on('change','#JSOpstina',function(){
		$.post(base_url+'select-narudzbina-mesto',{narudzbina_opstina_id: $(this).val()},function(response){
			var response_arr = response.split('===DEVIDER===');
			$('#JSMesto').html(response_arr[0] ? response_arr[0] : '');
			$('#JSUlica').html(response_arr[1] ? response_arr[1] : '');
		});
	});

	$(document).on('change','#JSMesto',function(){
		$.post(base_url+'select-narudzbina-ulica',{narudzbina_mesto_id: $(this).val()},function(response){
			$('#JSUlica').html(response);
		});
	});

});