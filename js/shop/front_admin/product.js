

$(document).ready(function(){

	$(document).on('click','#JSFAArticleSubmit',function(){
		var formData = new FormData();
		var image_upload = false;

		$("#JSFAArticleForm").find('input').each(function(index,value){
			var input_element = $(value);
			if(input_element.attr('name') == 'slike[]'){
				var slike_element = $('input[name="slike[]"]')[0];
				for(var i = 0; i < slike_element.files.length; i++) {
					var slika_obj = slike_element.files[i];
			    	formData.append('slike[]', slika_obj, slika_obj.name);
			    	image_upload = true;
				}
			}else if(input_element.attr('name') == 'akcija'){
				formData.append('akcija',$("input[name='akcija']:checked").val());
			}else{
				formData.append(input_element.attr('name'), input_element.val());
			}
		});
		$("#JSFAArticleForm").find('select').each(function(index,value){
			var input_element = $(value);
			formData.append(input_element.attr('name'), input_element.val());
		});
		formData.append('web_opis', tinymce.get('JSFAweb_opis').getContent());


		$.ajax({
			url: base_url + 'admin/product-edit-short',
			type: 'POST',
			data: formData,
			async: false,
			cache: false,
			contentType: false,
			enctype: 'multipart/form-data',
			processData: false,
			success: function (response) {
				$("#JSFAnaziv_web_error").text('');
				$("#JSFAweb_cena_error").text('');
				$("#JSFAkolicina_error").text('');
				$("#JSFAsku_error").text('');
				$("#JSFAgrupa_pr_grupa_error").text('');
				$("#JSFAroba_flag_cene_error").text('');
				$("#JSFAtip_artikla_error").text('');
				$("#JSSlikaError").text('');
					
				if(response.success_message != ''){
		    		swal(response.success_message, "", "success");
					setTimeout(function() { sweetAlert.close(); }, 2000);

					if(image_upload || formData.get('roba_id') == 0){
						product(response.roba_id);
					}
					var executed_element = $(".JSFAProductOnGrid[data-roba_id='"+formData.get('roba_id')+"']");
					executed_element.find('.JSFAflag_prikazi_u_cenovniku').text((formData.get('flag_prikazi_u_cenovniku') == 1 ? 'DA' : 'NE'));
					executed_element.find('.JSFAkolicina').text(formData.get('kolicina'));
					executed_element.find('.JSFANaziv').text(formData.get('naziv_web'));

				}else{

					if(response.error_messages.naziv_web && response.error_messages.naziv_web[0]){
						$("#JSFAnaziv_web_error").text(response.error_messages.naziv_web[0]);
					}
					if(response.error_messages.web_cena && response.error_messages.web_cena[0]){
						$("#JSFAweb_cena_error").text(response.error_messages.web_cena[0]);
					}
					if(response.error_messages.kolicina && response.error_messages.kolicina[0]){
						$("#JSFAkolicina_error").text(response.error_messages.kolicina[0]);
					}
					if(response.error_messages.sku && response.error_messages.sku[0]){
						$("#JSFAsku_error").text(response.error_messages.sku[0]);
					}
					if(response.error_messages.grupa_pr_grupa && response.error_messages.grupa_pr_grupa[0]){
						$("#JSFAgrupa_pr_grupa_error").text(response.error_messages.grupa_pr_grupa[0]);
					}
					if(response.error_messages.roba_flag_cene && response.error_messages.roba_flag_cene[0]){
						$("#JSFAroba_flag_cene_error").text(response.error_messages.roba_flag_cene[0]);
					}
					if(response.error_messages.tip_artikla && response.error_messages.tip_artikla[0]){
						$("#JSFAtip_artikla_error").text(response.error_messages.tip_artikla[0]);
					}

					if(response.error_messages.slike && response.error_messages.slike[0]){
						$("#JSSlikaError").text(response.error_messages.slike[0]);
					}
				}
			}
	   });
	});

	$(document).on("change","input[name='slike[]']", function(){
		$('.JSUploadImage').remove();

		for (var i = this.files.length - 1; i >= 0; i--) {
	        var extension = this.files[i].name.split('.').pop().toLowerCase();
	        if(['png','jpg','jpeg'].includes(extension)){
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('#JSUploadImages').prepend('<img src="'+e.target.result+'" class="JSUploadImage" style="width:200px">');
		        }
		        reader.readAsDataURL(this.files[i]);
		    }
		}
	});
	//slika delete
	$(document).on('click','.JSDeleteSlika',function(){
		var roba_id = $(this).data('roba_id');
		var web_slika_id = $(this).data('web_slika_id');
		$.get(base_url+'admin/slika-delete/'+roba_id+'/'+web_slika_id, { }, function(response) {
			product(roba_id);
			swal('Uspešno ste obrisali sliku', "", "success");
			setTimeout(function() { sweetAlert.close(); }, 2000);
		});
	});
	//article delete
	$(document).on('click','#JSFAArticleDelete',function(){
		var roba_id = $(this).data('roba_id');

		swal("Arkal će biti obrisan. Da li ste sigurni?", {
		  buttons: {
		    no: {
		      text: "Ne",
		      value: false,
		    },
		    yes: {
		      text: "Da",
		      value: true,
		    }
		  },
		}).then(function(value_confirm){
			if(value_confirm){
				$.get(base_url+'admin/product-delete/'+roba_id, { }, function(response) {
					if(response.success){
						$('#FAProductModal').modal('hide');
						swal(response.message, "", "success");
						setTimeout(function() { sweetAlert.close(); }, 2000);
						$('#JSFAProductListSelectCount').text('0');
						$('#JSFAProductListContent').html('');
						product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
					}else{
						swal(response.message+"\nIpak obriši?", {
						  buttons: {
						    no: {
						      text: "Ne",
						      value: false,
						    },
						    yes: {
						      text: "Da",
						      value: true,
						    }
						  },
						}).then(function(value_check){
							if(value_check){
								$.get(base_url+'admin/product-delete/'+roba_id+'/true', { }, function(response) {
									$('#FAProductModal').modal('hide');
									swal(response.message, "", "success");
									setTimeout(function() { sweetAlert.close(); }, 2000);
									$('#JSFAProductListSelectCount').text('0');
									$('#JSFAProductListContent').html('');
									product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
								});
							}
						});
					}
				});
			}
		});
	});

	$(document).click(function(event){
		if(
			!$(event.target).hasClass('JSListaGrupa') 
			&& $(event.target).attr('name') != 'grupa_pr_grupa'
		){
			$('#JSListaGrupe').attr('hidden','hidden');	
		}
		if(
			!$(event.target).hasClass('JSListaTipArtikla') 
			&& $(event.target).attr('name') != 'tip_artikla'
		){
			$('#JSListaTipaArtikla').attr('hidden','hidden');	
		}
		if(
			!$(event.target).hasClass('JSListaFlagCena') 
			&& $(event.target).attr('name') != 'roba_flag_cene'
		){
			$('#JSListaFlagCene').attr('hidden','hidden');	
		}
	});
	//grupe
	$(document).on('click','input[name="grupa_pr_grupa"]',function(){
		if($('#JSListaGrupe').attr('hidden') == 'hidden'){
			$('#JSListaGrupe').removeAttr('hidden');
		}else{
			$('#JSListaGrupe').attr('hidden','hidden');	
		}	
	});
	$(document).on('click','.JSListaGrupa',function(){
		var grupa = $(this).data('grupa');
		$('input[name="grupa_pr_grupa"]').val(grupa);
		$('#JSListaGrupe').attr('hidden','hidden');	
	});

	//tip
	$(document).on('click','input[name="tip_artikla"]',function(){
		if($('#JSListaTipaArtikla').attr('hidden') == 'hidden'){
			$('#JSListaTipaArtikla').removeAttr('hidden');
		}else{
			$('#JSListaTipaArtikla').attr('hidden','hidden');	
		}	
	});
	$(document).on('click','.JSListaTipArtikla',function(){
		var grupa = $(this).data('tip_artikla');
		$('input[name="tip_artikla"]').val(grupa);
		$('#JSListaTipaArtikla').attr('hidden','hidden');	
	});

	//flag cene
	$(document).on('click','input[name="roba_flag_cene"]',function(){
		if($('#JSListaFlagCene').attr('hidden') == 'hidden'){
			$('#JSListaFlagCene').removeAttr('hidden');
		}else{
			$('#JSListaFlagCene').attr('hidden','hidden');	
		}	
	});
	$(document).on('click','.JSListaFlagCena',function(){
		var grupa = $(this).data('roba_flag_cene');
		$('input[name="roba_flag_cene"]').val(grupa);
		$('#JSListaFlagCene').attr('hidden','hidden');	
	});


	$(document).on("click",".JSGrupaNazivRemove",function(){
		$(this).closest('li').find("input[type='radio']").removeAttr('checked');
	});

	$(document).on('click','#datum_od_delete',function(){
		$('#datum_akcije_od').val('');
	});
	$(document).on('click','#datum_do_delete',function(){
		$('#datum_akcije_do').val('');
	});

	$(document).on('change','#JSAkcijaFlagPrimeni',function(){
		if($(this).val() == 1){
			$('#JSAkcijaDatum').removeAttr('hidden');
		}else{
			$('#JSAkcijaDatum').attr('hidden','hidden');
		}
	});

});