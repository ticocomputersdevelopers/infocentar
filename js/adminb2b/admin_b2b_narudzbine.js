$(document).ready(function () {

	$(".order-status__select").change(function() {

		$selected = $(this).children(":selected");

		var status_target = $selected.data('status-target');
		var web_b2b_narudzbina_id = $selected.data('narudzbina-id');

		$.ajax({
				type: "POST",
				url: base_url+'admin/b2b/ajax/b2b_promena_statusa_narudzbine',
				data: { status_target: status_target, web_b2b_narudzbina_id: web_b2b_narudzbina_id },
				success: function(msg) {
						// alert(msg);
						location.reload();
				}
		}); 

	});
	
	//filtriranje po dodatnom statusu
	$("#narudzbina_status_id").change(function(){
	  		var narudzbina_status_id = $(this).val();
	  		var search = $('#search-btn').val();
	  		var status = $(this).data('status');
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
					search = '0';
		 		}
				var status_str= location.pathname.split('/')[4];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
	  	});

		//filtriranje po statusu
		$('.JSStatusNarudzbine').click(function() {
			var search = $('#search-btn').val();
			var status = $(this).data('status');
			var narudzbina_status_id = $('#narudzbina_status_id').val();
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}

				var status_str= location.pathname.split('/')[4];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/b2b/narudzbine/' + statuses.join('-') + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});
		
		$('#JSFilterClear').click(function(){

			window.location.href = base_url+'admin/b2b/narudzbine/sve/0/0/0/0';

		});

		$('#search-btn').click(function(){

		var search = $('#search').val();
		var status = $(this).data('status');
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $('#datepicker-from').val();
		var datum_do = $('#datepicker-to').val();
		var status_str= location.pathname.split('/')[4];
			if(!datum_od){
			datum_od = '0';
			}
			if(!datum_do){
			datum_do = '0';
		 	}
		 	if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 	}
		 	if(!search){
			search = '0';
		 	}
				

		window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		
		});

		$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    	});

		//kalendar za filtriranje po datumu
		$('#datepicker-from').change(function() {
		var search = $('#search-btn').val();	
		var status_str= location.pathname.split('/')[3];
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $(this).val();
		var datum_do = $(this).data('datumdo');

		if(!datum_od){
				datum_od = '0';
		}
		if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		}
		if(!search){
			search = '0';
		}

		window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		$('#datepicker-to').change(function() {
		 var search = $('#search-btn').val();
		 var datum_do = $(this).val();
		 var status_str= location.pathname.split('/')[3];
		 var narudzbina_status_id = $('#narudzbina_status_id').val();
		 var datum_od = $(this).data('datumod');
		 if(!datum_do){
				datum_do = '0';
		 }
		 if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 }
		 if(!search){
			search = '0';
		 }	

		 window.location.href = base_url + 'admin/b2b/narudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});

	$('.kolicina').keydown(false);
	$('.kolicina').change(function() {
		var stavka_id = $(this).data('stavka-id');
		var kolicina = $(this).val();  
		var narudzbina_id = $('#narudzbina_id').val();
		var lager = $(this).closest('tr').find('.JSLager');

		if(kolicina == 0 || kolicina =='') {
			$(this).css('border-color', 'red');
		} else {
			$(this).css('border-color', '');
		}

		$.post(base_url + 'admin/b2b/ajax/update_kolicina', {stavka_id:stavka_id, kolicina:kolicina, narudzbina_id:narudzbina_id}, function (response){
			var data = JSON.parse(response);
			$('#cena-artikla').text(data.cena_artikla);
			$('#troskovi-isporuke').text(data.troskovi_isporuke);
			$('#ukupna-cena').text(data.ukupna_cena);
			lager.text(data.lager);
		 });
	});
	

		

});