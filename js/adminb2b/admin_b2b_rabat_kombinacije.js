$(document).ready(function () {

	$(".JSUpdatePartnerRabatGrupa").click(function(){
		var id = $(this).data('partner_rabat_grupa_id');
		var rabat = $(this).closest('tr').find('.JSKombinacijaRabat').val();

		if(!isNaN(rabat)){
	    	$.post(
				base_url+'admin/b2b/ajax/rabat_edit', {
					action: 'rabat_kombinacija_edit',
					id: id,
					rabat: rabat
				}, function (response){
					if(response == 1) {
						alertify.success('Uspešno ste sačuvali rabat.');
					} else {
						alertify.error('Greška prilikom upisivanja u bazu.');
					}
				}
			);
	    }else{
	    	alertify.error('Neodgovarajući sadržaj polja.');
	    }
	});

	$(".JSDeletePartnerRabatGrupa").click(function(){
		var $this = $(this);
		var id = $this.data('partner_rabat_grupa_id');

    	$.post(
			base_url+'admin/b2b/rabat_kombinacije/'+id+'/delete', {}, 
			function (response){
				if(response == 1) {
					$this.closest('tr').remove();
					alertify.success('Uspešno izbrisano');

				} else {
					alertify.error('Došlo je do greška');
				}
			}
		);
	});

});
