
 /*======= FIXED HEADER ==========*/ 
$(window).scroll(function(){

	var header = $('.JSfixed-hdr');
	var header_height = $('.JSfixed-hdr').height();
	var offset_top = $('.JSfixed-hdr').offset().top;
	var offset_bottom = offset_top + header.outerHeight(true);

 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {
			$('#JSfixed_header').addClass('JSsticky_header');
			header.css('height', header_height + 'px');	        
	    }else {
			$('#JSfixed_header').removeClass('JSsticky_header');   
			header.css('height', 'initial');
	    }
	}
});    