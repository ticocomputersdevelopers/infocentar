$(document).ready(function () {

	$('#JSFuterSekcijaTip').change(function(){
		var futer_sekcija_tip_id = $(this).val();

		if(futer_sekcija_tip_id != 1){
			$('#JSFooterSectionImage').attr('hidden','hidden');
		}else{
			$('#JSFooterSectionImage').removeAttr('hidden');
		}

		if(['1','2'].includes(futer_sekcija_tip_id)){
			$('#JSFooterSectionContent').removeAttr('hidden');
		}else{
			$('#JSFooterSectionContent').attr('hidden','hidden');
		}

		if(futer_sekcija_tip_id != 3){
			$('#JSFooterSectionLinks').attr('hidden','hidden');
		}else{
			$('#JSFooterSectionLinks').removeAttr('hidden');
		}
	});

	$(function() {
		$('.JSListFooterSections').sortable({
			//observe the update event...
			update: function(event, ui) {
				//create the array that hold the positions...
				var order = []; 
				//loop trought each li...
				$('.JSListFooterSections li').each( function(e) {

				//add each li position to the array...     
				// the +1 is for make it start from 1 instead of 0
				order.push( $(this).attr('id') );				 
				});
				$.ajax({
					type: "POST",
					url: base_url+'admin/position-footer-section',
					data: {order:order},
					success: function(msg) {
					  // console.log('test');
					}
				});
			}
		});					   
		$( ".JSListFooterSections").disableSelection();			
	}); 

});