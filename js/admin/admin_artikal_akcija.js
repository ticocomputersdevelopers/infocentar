$(document).ready(function(){
	$("input[name='akcija_popust']").keyup(function(){
		var popust = $(this).val();
		var web = $("input[name='web_cena']").val();
		if($.isNumeric(popust) && $.isNumeric(web)){	  		
	  	var akcijska = web*(1-popust/100);
	  	$("input[name='akcijska_cena']").val(Math.round(akcijska)+'.00');
		}
	});
	$("input[name='akcijska_cena']").keyup(function(){
		var akcijska = $(this).val();
		var web = $("input[name='web_cena']").val();

		if($.isNumeric(akcijska) && $.isNumeric(web)){
	  	var popust = 100*(1-akcijska/web);
	  	$("input[name='akcija_popust']").val(round(popust,2));
	}
	});
	$("input[name='web_cena']").keyup(function(){
		var web = $(this).val();
		var popust = $("input[name='akcija_popust']").val();
		var akcijska = $("input[name='akcijska_cena']").val();

		if($.isNumeric(web)){	  		
		  	if($.isNumeric(popust)){
		  		if($("input[name='akcijska_cena']").val()!=''){	  			
				  	var akcijska = web*(1-popust/100);
				  	$("input[name='akcijska_cena']").val(akcijska);
		  		}
			}else if($.isNumeric(akcijska)){
			  	var popust = 100*(1-akcijska/web);
			  	$("input[name='akcija_popust']").val(round(popust,2));
			}
		}
	});

	$(function() {
		$( "#datum_akcije_od, #datum_akcije_do" ).datepicker();
	});
	$('#datum_akcije_od').keydown(false);
	$('#datum_akcije_do').keydown(false);
	$('#datum_od_delete').click(function(){
		$('#datum_akcije_od').val('');
	});
	$('#datum_do_delete').click(function(){
		$('#datum_akcije_do').val('');
	});
});

function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}