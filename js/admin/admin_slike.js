$(document).ready(function(){

    $("input[name='slika[]']").on("change", function(){
        $('.JSUploadImage').remove();
        $('#JSSlikaBigRow').remove();

        for (var i = this.files.length - 1; i >= 0; i--) {
            var extension = this.files[i].name.split('.').pop().toLowerCase();
            if(['png','jpg','jpeg'].includes(extension)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#JSUploadImages').prepend('<img src="'+e.target.result+'" class="JSUploadImage">');
                }
                reader.readAsDataURL(this.files[i]);
            }
        }
    });

    //select all
    $('#JSSlikaSelected').click(function(){
        if(!$(this).attr("checked")){
            $(this).attr('checked',true);
            $(this).prop('checked',true);
            $(".JSSlikaSelected").attr('checked',true);
            $(".JSSlikaSelected").prop("checked",true);
        }else{
            $(this).attr('checked',false);
            $(this).prop('checked',false);
            $(".JSSlikaSelected").attr('checked',false);
            $(".JSSlikaSelected").prop("checked",false);
        }
        $("#JSSlikaExecute").val("").change();
    });
    $('.JSSlikaSelected').click(function(){
        if($(this).attr("checked")){
            $(this).attr('checked',false);
            $(this).prop('checked',false);
        }else{
            $(this).attr('checked',true);
            $(this).prop('checked',true);
        }
        $("#JSSlikaExecute").val("").change();
    });

	$('.JSAkcija').click(function(){
		var web_slika_id = $(this).data('id');
		var roba_id = $(this).data('robaid');
		$.post(base_url+'admin/ajax/articles', {action: 'update_slika', roba_id: roba_id, web_slika_id: web_slika_id}, function (response){ location.reload(true); });
	});

    //description content
    $('.JSSlikaAlt').keyup(function(){
        var target_button = $('.JSSlikaAltSave[data-id="'+$(this).data('id')+'"]');
        if(target_button.attr('hidden')){
            target_button.removeAttr('hidden');
        }
    });
    // $('.JSSlikaAlt').focusin(function(){
    //     var target_button = $('.JSSlikaAltSave').attr('hidden','hidden');
    // });
    $('.JSSlikaAltSave').click(function(){
        var this_button = $(this);
        var target_area = $('.JSSlikaAlt[data-id="'+this_button.data('id')+'"]');
        var web_slika_id = target_area.data('id');
        var web_slika_alt = target_area.val();

        $.post(base_url+'admin/ajax/articles', {action: 'update_slika_alt', web_slika_id: web_slika_id, web_slika_alt: web_slika_alt}, function (response){
            alertify.success('Opis slike je sačuvan.');
            this_button.attr('hidden','hidden');
        });
    });


    $('#JSSlikaExecute').change(function(){
        var action = $(this).val();
        var web_slika_ids = $(".JSSlikaSelected[checked='checked']").map(function(){ return $(this).data('id'); }).get();
        
        if(web_slika_ids.length > 0 && action != ''){
            
            if(action == 'active' || action == 'unactive'){
                if(action == 'active'){
                    var prikaz = 1;
                    var message = 'Slike su prebačene u aktivne.';
                }else if(action == 'unactive'){
                    var prikaz = 0;
                    var message = 'Slike su prebačene u neaktivne.';
                }

                $.post(base_url+'admin/ajax/articles', {action: 'prikazi', web_slika_ids: web_slika_ids, prikaz: prikaz}, function (response){
                    alertify.success(message);
                    setTimeout(function(){ location.reload(true); },500);
                });
            
            }else if(action == 'delete'){
                alertify.confirm("Da li želite da obrišete slike?",
                    function(e){
                    if(e){
                        $.post(base_url+'admin/slike-more-delete', { web_slika_ids: web_slika_ids }, function (response){
                            alertify.success('Slike su uspešno obrisane.');
                            setTimeout(function(){ location.reload(true); },500);
                        });
                    }
                });
            }

        }
    });

});