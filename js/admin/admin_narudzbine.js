$(document).ready(function() {

		$("#JSKupacDetail").click(function() {
			window.open(base_url + 'admin/kupci_partneri/kupci/' + $("#JSWebKupac").val(), '_blank');
		});	

		$("#JSWebKupac").change(function() {
			$.post(base_url + 'admin/ajax/narudzbina-kupac', {web_kupac_id:$(this).val()}, function (response){
				$('#JSKupacAjaxContent').html(response);
			 });
		});

		//filtriranje po dodatnom statusu
	  	$("#narudzbina_status_id").change(function(){
	  		var narudzbina_status_id = $(this).val();
	  		var search = $('#search-btn').val();
	  		var status = $(this).data('status');
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
					search = '0';
		 		}
				var status_str= location.pathname.split('/')[3];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/porudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
	  	});
		
		
		//filtriranje po statusu
		$('.JSStatusNarudzbine').click(function() {
			var search = $('#search-btn').val();
			var status = $(this).data('status');
			var narudzbina_status_id = $('#narudzbina_status_id').val();
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}

				var status_str= location.pathname.split('/')[3];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/porudzbine/' + statuses.join('-') + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});
		
		$('#JSFilterClear').click(function(){

			window.location.href = base_url+'admin/porudzbine/sve/0/0/0/0';

		});

		$('#search-btn').click(function(){

		var search = $('#search').val();
		var status = $(this).data('status');
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $('#datepicker-from').val();
		var datum_do = $('#datepicker-to').val();
		var status_str= location.pathname.split('/')[3];
			if(!datum_od){
			datum_od = '0';
			}
			if(!datum_do){
			datum_do = '0';
		 	}
		 	if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 	}
		 	if(!search){
			search = '0';
		 	}
				

		window.location.href = base_url + 'admin/porudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		
		});

		$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    	});

		//kalendar za filtriranje po datumu
		$('#datepicker-from').change(function() {
		var search = $('#search-btn').val();	
		var status_str= location.pathname.split('/')[3];
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $(this).val();
		var datum_do = $(this).data('datumdo');

		if(!datum_od){
				datum_od = '0';
		}
		if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		}
		if(!search){
			search = '0';
		}

		window.location.href = base_url + 'admin/porudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		$('#datepicker-to').change(function() {
		 var search = $('#search-btn').val();
		 var datum_do = $(this).val();
		 var status_str= location.pathname.split('/')[3];
		 var narudzbina_status_id = $('#narudzbina_status_id').val();
		 var datum_od = $(this).data('datumod');
		 if(!datum_do){
				datum_do = '0';
		 }
		 if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 }
		 if(!search){
			search = '0';
		 }	

		 window.location.href = base_url + 'admin/porudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		// promena statusa narudzbine
		$(".order-status__select").change(function() {

		$selected = $(this).children(":selected");

		var status_target = $selected.data('status-target');
		var web_b2c_narudzbina_id = $selected.data('narudzbina-id');

		$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/promena_statusa_narudzbine',
				data: { status_target: status_target, web_b2c_narudzbina_id: web_b2c_narudzbina_id },
				success: function(msg) {
						// alert(msg);
						location.reload();
				}
		}); // alert(web_b2c_narudzbina_id+' '+status_target);
		});

		$('.JSMoreWiew').click(function() {
				var web_b2c_narudzbina_id = $(this).data('id-narudzbina');
				window.location.href = base_url + 'admin/narudzbina/' + web_b2c_narudzbina_id;
				// $.ajax({
				//     type: "POST",
				//     url: base_url + 'admin/ajax/porudzbina_vise',
				//     data: { web_b2c_narudzbina_id: web_b2c_narudzbina_id },
				//     success: function(msg) {
				//         $('#ordersDitailsModal').find('.content').html(msg);
				//         $('#ordersDitailsModal').foundation('reveal', 'open');
				//     }
				// });
		});





 $('.JSDeleting').on('click', function(){
			var web_b2c_narudzbina_id = $(this).data('id-narudzbina');
  			alertify.confirm("Da li ste sigurni da želite da obrišete narudžbinu?",
            function(e){
                if(e){
              window.location.href = base_url + 'admin/porudzbina/delete/' + web_b2c_narudzbina_id;
                	}
          });

});



	var articlesTime = 0;
	$('#JSsearch_porudzbine').on("keyup", function() {
				clearTimeout(articlesTime);
				articlesTime = setTimeout(function(){
					$('.articles_list').remove();
					var articles = $('#JSsearch_porudzbine').val();
					var narudzbina_id = $('#narudzbina_id').val();
					if (articles != '' && articles.length > 2) {
						$.post(base_url + 'admin/ajax/narudzbine_search', {articles:articles,narudzbina_id:narudzbina_id}, function (response){
							$('#search_content').html(response);
						});
					}
				}, 500);
	});

	$('.JSDeleteStavka').click(function() {

		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();

		$.post(base_url + 'admin/ajax/delete_stavka', {stavka_id:stavka_id}, function (response){
			window.location.href = base_url + 'admin/narudzbina/' + narudzbina_id;
		});
	});

	$('.JSCenaInput').hide();

		$('.JSEditCenaBtn').on('click', function(){
		$(this).parent().children('.JSCenaInput').show();
		$(this).parent().children('.JSCenaInput').focus();
		});

		$('.JSCenaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSCenaInput').on("keyup", function(event) {
		var val = $(this).val();
		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();
		

		if(event.keyCode == 13){
			$(this).closest('.cena').children('.JSCenaVrednost').html(val);
	    	$(this).focusout();

    	$.post(base_url+'admin/ajax/editCena', {stavka_id:stavka_id, val: val, narudzbina_id: narudzbina_id}, function (response){
    		var data = JSON.parse(response);
			$('#cena-artikla').text(data.cena_artikla);
			$('#troskovi-isporuke').text(data.troskovi_isporuke);
			$('#ukupna-cena').text(data.ukupna_cena);

    	});
  		}
		});

	$('.kolicina').keydown(false);
	$('.kolicina').change(function() {
		var stavka_id = $(this).data('stavka-id');
		var kolicina = $(this).val();  
		var narudzbina_id = $('#narudzbina_id').val();
		var lager = $(this).closest('tr').find('.JSLager');

		if(kolicina == 0 || kolicina =='') {
			$(this).css('border-color', 'red');
		} else {
			$(this).css('border-color', '');
		}

		$.post(base_url + 'admin/ajax/update_kolicina', {stavka_id:stavka_id, kolicina:kolicina, narudzbina_id:narudzbina_id}, function (response){
			var data = JSON.parse(response);
			$('#cena-artikla').text(data.cena_artikla);
			$('#troskovi-isporuke').text(data.troskovi_isporuke);
			$('#ukupna-cena').text(data.ukupna_cena);
			lager.text(data.lager);
		 });
	});

	$('.datum-val').keydown(false);

	$('.JS-realizuj').click(function(){
		var narudzbina_id = $('#narudzbina_id').val();
		if($('#poslato').is(':checked')){
			$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id}, function (response){
				location.reload();
			});
		} else {
			
			if(confirm('Niste poslali, da li zelite da realizujete?')){
				$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id}, function (response){
					location.reload();
				});
			} else {

			}
		}
	});

	$('.JS-prihvati').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/prihvati', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
			
		});
	});

	$('.JS-storniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/storniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});

	$('.JS-nestorniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/nestorniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});
	$('.JS-delete').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/obrisi', {narudzbina_id:narudzbina_id}, function (response){	
			alertify.alert('Narudžbina je obrisana!');
			window.location.href = base_url + 'admin/porudzbine/sve/0/0/0/0';
		});
	});

	$('.JS-nazad').click(function() {	
		window.location.href = base_url + 'admin/porudzbine/sve/0/0/0/0';
	});

	$('#JSPostaSlanje').change(function(){	
		var id = $(this).val();
		var api_id = $(this).data('api_id');
		var parent_div = $(this).closest('div');

		if(id == api_id){
			parent_div.removeClass('medium-6');
			parent_div.addClass('medium-4');
			parent_div.after('<div id="JSPosaljiDiv" class="field-group columns medium-2"><label>Pošalji</label><input name="posta_zahtev" id="JSPosalji" type="checkbox"></div>');
		}else{
			parent_div.removeClass('medium-4');
			parent_div.addClass('medium-6');
			$('#JSPosaljiDiv').remove();
		}
	});


});