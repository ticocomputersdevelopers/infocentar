<div class="centered-modal"> 
<div class="modal fade" id="FAProductsModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content fast-edit-articles modal-scroller">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>

                <div class="modal-heading">
                    <h4 class="modal-title text-center">{{ Language::trans('Artikli') }}</h4>
                </div>
            </div>

            <div class="modal-body">
                <div class="row choose-all"> 
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center">{{ Language::trans('Ukupno') }}: 
                        <span id="JSFAProductListCount"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" placeholder="Pretraži..." id="JSFAArticleSearch" class="form-control search">
                         <span class="fas fa-search form-control-feedback"></span>
                    </div>
                    <div id="JSFAProductNewModalCall" class="add-new-article col-md-3 col-sm-3 col-xs-12 text-center">
                        <button class="btn btn-primary">{{ Language::trans('Dodaj novi artikal') }}</button>
                    </div>
                </div>
                <div class="fast-filters">
                    <div class="first_1"> 
                        <label>Grupe:</label>
                        <select id="JSFAGrupaSelect">           
                            {{ AdminSupport::selectGroups() }}
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Na Webu') }}:</label>
                        <select id="JSFANaWebuSelect">           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Na akciji') }}:</label>
                        <select id="JSFANaAkcijiSelect">           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Slike') }}:</label>
                        <select id="JSFASlikeSelect">           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_2">
                        <label>{{ Language::trans('Opis') }}:</label>
                        <select id="JSFAOpisSelect">           
                            <option value="null">{{ Language::trans('Svi') }}</option>
                            <option value="1">{{ Language::trans('Da') }}</option>
                            <option value="0">{{ Language::trans('Ne') }}</option>
                        </select>
                    </div>
                    <div class="first_6">
                        <label>{{ Language::trans('Komande') }}:</label>
                        <select id="JSFAActionSelect">           
                            <option value="web_on">{{ Language::trans('Stavi na Web') }}</option>
                            <option value="web_off">{{ Language::trans('Skini sa Weba') }}</option>
                            <option value="sell_on">{{ Language::trans('Stavi na Akciju') }}</option>
                            <option value="sell_off">{{ Language::trans('Skini sa Akcije') }}</option>
                            <option value="delete">{{ Language::trans('Obriši') }}</option>
                        </select>
                    </div>
                    <button class="btn" id="JSFAActionExecute">{{Language::trans('Primeni')}}</button>
                </div>
                <ul class="list-inline row custom-heading">
                    <li class="tooltipz" aria-label="Selektuj sve"> 
                        <input type="checkbox" id="JSRobaSelected">
                        <span id="JSFAProductListSelectCount"></span>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-5 text-center">&nbsp;Slika</li>
                    <li class="col-md-2 col-sm-2 col-xs-3">Na Webu</li>
                    <li class="col-md-2 col-sm-2 col-xs-3">Količina</li>
                    <li class="col-md-6 col-sm-6 col-xs-2 hidden-xs text-center">Naziv</li>
                </ul>
                <ul id="JSFAProductListContent" class="FAProductListContent">
                   <!-- product_list -->
                </ul> 
            </div>
        </div>   
    </div>
</div>
</div>
