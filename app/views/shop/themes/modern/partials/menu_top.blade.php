@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
	<section class="row" id="admin-menu">
      <div class="main-wrapper clearfix text-right"> 
          <span>Prijavljeni ste kao administrator | </span>
          <span class="admin-links"><a target="_blank" href="{{ Options::base_url() }}admin">Admin Panel</a></span>|
          <span class="admin-links"><a href="{{ Options::domain() }}admin-logout">Odjavi se</a></span>
      </div>
	</section>
@endif

<section id="JSpreheader" class="row">
    <div class="social-icons show-for-medium-up">
        {{Options::social_icon()}} 
    </div>
 
	<!-- LOGIN & REGISTRATION ICONS --> 
    <section class="main-wrapper clearfix">
         
        @if(Options::user_registration()==1)
            <section class="small-12 columns preheader-icons">
                <div class="medium-8 columns no-padding"> 
                    <span class="top-menu-links">
                  @foreach(All::menu_top_pages() as $row)
                    @if($row->grupa_pr_id != -1 and $row->grupa_pr_id != 0)
                    <li>
                        <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($row->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($row->title) }}</a>
                    </li>
                    @elseif($row->tip_artikla_id == -1)
                    <li> 
                        <a href="{{ Options::base_url().$row->naziv_stranice }}">
                            {{ ($row->title) }}
                        </a>
                     </li>
                    @else
                        @if($row->tip_artikla_id==0)
                        <li> 
                            <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                {{ ($row->title) }}
                            </a>
                        </li>
                        @else
                        <li> 
                            <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                {{ ($row->title) }}
                            </a>
                        </li>
                        @endif
                    @endif
                  @endforeach
                    </span>
                </div>

            <div class="medium-4 columns text-right small-only-text-center"> 
                @if(Options::checkB2B())
                <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="btn confirm">B2B</a><i class="pipes">|</i>
                @endif
                 <!-- LOGIN REGISTRATION -->
           
              @if(Session::has('b2c_kupac'))
                <div class="logout-wrapper clearfix">                   
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                      <span>{{ WebKupac::get_user_name() }}</span></a>
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                      <span>{{ WebKupac::get_company_name() }}</span></a>
                    <a id="logout-button" href="{{Options::base_url()}}logout"><span>Odjavi se</span></a>
                </div>
              @else 
                 <i class="fa fa-sign-in"></i>
                  <!-- ====== CLICK FOR MODAL ========== -->
                 <a href="#" id="login-icon" class="" data-reveal-id="myModal-login"><span>Uloguj se </span></a><i class="pipes">|</i>
                 <i class="fa fa-user-plus"></i>
                 <a id="registration-icon" href="{{Options::base_url()}}registracija"><span>Registracija</span></a>
               @endif
           </div>
           </section>
         @endif
    </section>
    
    <span class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></span>
</section>

 <!-- MODAL ZA LOGIN -->
<div id="myModal-login" class="reveal-modal tiny" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="medium-12 columns login-form-wrapper">
          <div class="row">
              <div class="medium-12 columns">
                  <p class="welcome-login"><span>Dobrodošli</span>Za pristup Vašem nalogu unesite Vaš E-mail i lozinku.</p>
              </div>
          </div>
          <div class="field-group row login-input-row">             
             <div class="medium-12 columns">
               <label for="email_login">E-mail</label>
              <?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
              <input class="login-form__input" placeholder="E-mail adresa" id="JSemail_login" type="text" value="{{ $old_mail }}" autocomplete="off">
              </div>
          </div>
          
         <div class="field-group row login-input-row"> 
              <div class="medium-12 columns">
                <label for="lozinka_login">Lozinka</label>
                  <input class="login-form__input" placeholder="Lozinka" autocomplete="off" id="JSpassword_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
              </div>
          </div>

          <div class="row">
              <div class="medium-12 columns modal-login-text-align-right">
                  <button type="submit" id="login" onclick="user_login()" class="login-form-button admin-login">Uloguj see</button>
              </div>
          </div>
 
      <div class="field-group error-login">
          <div class="row">
              <div class="medium-12 columns wrong-email">
          <?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

         @if(Session::get('confirm'))
              Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
          @endif

         @if(Session::get('message'))
              Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
          @endif
              </div>
          </div>
      </div>
    </div> <!-- end of .login-form-wrapper -->
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>