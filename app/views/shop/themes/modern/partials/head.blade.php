
<title>{{ $title }}</title>

<meta http-equiv="Cоntent-Type" content="text/html" charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{{ $description }}" />
<meta name="keywords" content="{{ $keywords }}" />
<meta name="author" content="{{Options::company_name()}}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="icon" type="image/png" href="{{Options::domain()}}favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Architects+Daughter" rel="stylesheet">
<link href="{{Options::domain()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
<link href="{{Options::domain()}}css/normalize.css" rel="stylesheet" type="text/css" />
<link href="{{Options::domain()}}css/fancybox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> 
@if($strana == All::get_page_start())
<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />
@endif
@if(Options::enable_filters()==1 AND $strana=='artikli' AND $filter_prikazi )
<link rel="stylesheet" type="text/css" href="{{Options::domain()}}css/jquery-ui.css">
@endif

<link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}style.css" rel="stylesheet" type="text/css" />
<link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}custom.css" rel="stylesheet" type="text/css" />
@if(Options::gnrl_options(3021) == 0)
<link href="{{ Options::domain().'css/themes/'.Support::theme_style_path()}}color.css" rel="stylesheet" type="text/css" />
@else <!-- CUSTOM COLORS -->
<style type="text/css">
:root{
	--header_bg: {{ Options::prodavnica_boje(1) }}; 	        
	--top_menu_bg:{{ Options::prodavnica_boje(2) }};  		
	--top_menu_color: {{ Options::prodavnica_boje(3) }};	
	--menu_bg: {{ Options::prodavnica_boje(4) }};
	--footer_bg: {{ Options::prodavnica_boje(5) }};
	--body_color: {{ Options::prodavnica_boje(6) }};		
	--paragraph_color: {{ Options::prodavnica_boje(7) }};		
	--body_bg: {{ Options::prodavnica_boje(8) }};	
	--h2_color: {{ Options::prodavnica_boje(9) }};
	--h5_color: {{ Options::prodavnica_boje(10) }};	
	--btn_bg: {{ Options::prodavnica_boje(11) }};
	--btn_hover_bg: {{ Options::prodavnica_boje(12) }};
	--btn_color: {{ Options::prodavnica_boje(13) }};
	--a_href_color: {{ Options::prodavnica_boje(14) }};
	--level_1_color: {{ Options::prodavnica_boje(15) }};
 	--level_2_color: {{ Options::prodavnica_boje(16) }};
 	--categories_title_bg: {{ Options::prodavnica_boje(17) }};
 	--categories_level_1_bg: {{ Options::prodavnica_boje(18) }};
 	--categories_level_2_bg: {{ Options::prodavnica_boje(19) }};
 	--product_bg: {{ Options::prodavnica_boje(20) }};
 	--product_bottom_bg: {{ Options::prodavnica_boje(21) }};
 	--review_star_color: {{ Options::prodavnica_boje(22) }};
 	--product_title_color: {{ Options::prodavnica_boje(23) }};
 	--article_product_price_color: {{ Options::prodavnica_boje(24) }};
 	--sale_action_price_bg: {{ Options::prodavnica_boje(25) }};
 	--sale_action_bg: {{ Options::prodavnica_boje(26) }};
 	--product_old_price_color: {{ Options::prodavnica_boje(27) }};
 	--product_price_color: {{ Options::prodavnica_boje(28) }};
 	--login_btn_bg: {{ Options::prodavnica_boje(29) }};
 	--login_btn_color: {{ Options::prodavnica_boje(30) }};
 	--cart_number_bg: {{ Options::prodavnica_boje(31) }};
 	--inner_body_bg: {{ Options::prodavnica_boje(32) }};
 	--product_list_bg: {{ Options::prodavnica_boje(33) }};
 	--input_bg_color: {{ Options::prodavnica_boje(34) }};
 	--breadcrumb_bg:  {{ Options::prodavnica_boje(35) }};
 	--currency_bg: {{ Options::prodavnica_boje(36) }};
    --currency_color: {{ Options::prodavnica_boje(37) }};
    --filters_bg: {{ Options::prodavnica_boje(38) }};
    --pagination_bg: {{ Options::prodavnica_boje(39) }};
    --pagination_bg_active: {{ Options::prodavnica_boje(40) }};   
    --pagination_color_active: {{ Options::prodavnica_boje(41) }};
    --discount_price_color: {{ Options::prodavnica_boje(42) }};
    --add_to_cart_btn_bg: {{ Options::prodavnica_boje(43) }};
    --add_to_cart_btn_color: {{ Options::prodavnica_boje(44) }};
    --add_to_cart_btn_bg_hover: {{ Options::prodavnica_boje(45) }};
    --add_to_cart_btn_bg_not_available: {{ Options::prodavnica_boje(46) }};
 	--label_color: {{ Options::prodavnica_boje(47) }};
 	--scroll_top_bg: {{ Options::prodavnica_boje(48) }};
	
	}
</style>
@endif
 
@include('shop/google_analytics')
@include('shop/chat')