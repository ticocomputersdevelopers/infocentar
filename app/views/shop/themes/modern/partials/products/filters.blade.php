<?php // echo $kara; ?>
<div class="filters">
<!-- CHECKED FILTERS -->
   <div class="row">
    	<span class="selected-filters-wrapper text-center medium-12 columns">
		    <span class="selected-filters-title">Izabrani filteri:</span> 
			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
				?>

					<li>
					    {{All::get_manofacture_name($row)}}     
					    <a href="#">
					        <i class="fa fa-close JSfilter-close" data-type="1" data-rb="{{$br}}" data-element="{{$row}}" ></i>
					    </a>
					</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
						{{All::get_fitures_name($row)}}
						<a href="#">
					        <i class="fa fa-close JSfilter-close" data-type="2" data-rb="{{$br}}" data-element="{{$row}}" ></i>
					    </a>
					</li>
					<?php }}?>
			</ul>
			<input type="hidden" id="JSniz_proiz" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSniz_kara" value="{{$kara}}"/>
		</span>	
	</div>
       
    <div class="btn-container text-center">
	    <a class="JSreset-filters-button" href="#">Poništi filtere</a>
    </div>
	<!-- END CHECKED -->
	<div class="filter-field">
		<div class="row"> 
			<ul class="clearfix">
			    <!-- FILTER ITEM -->
				<li class="columns filter-box">
                    <a href="javascript:void(0)" class="label-field filter-box__title click-for-slide JSfilterBox">Proizvođac 
                    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
                    	<i class="fa fa-angle-down label-field__arrow"></i></a>
					<div class="slide-me">
						<div class="select-fields clearfix">

							<div class="JSmultiselect multiselect-proizvodjaci">
								@foreach($manufacturers as $key => $value)
								<?php if(in_array($key,$niz_proiz)){ ?>
									<label>
										<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
										<p class="filter-text">
											{{All::get_manofacture_name($key)}} 
										</p>
										<span class="filter-count">
											@if(Options::filters_type()) {{ $value }} @endif
										</span>
									</label>
								<?php }else {?>
									<label>
										<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
										<p class="filter-text">
											{{All::get_manofacture_name($key)}} 
										</p>
										<span class="filter-count">
											@if(Options::filters_type()) {{ $value }} @endif
										</span>
									</label>
								<?php } ?>
								@endforeach
							</div>
						</div>
					</div>
				</li>

				@foreach($characteristics as $keys => $values)
					<li class="columns filter-box">
						<a href="javascript:void(0)" class="label-field filter-box__title click-for-slide JSfilterBox"> {{$keys}} 
						<!--  <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
						 <i class="fa fa-angle-down label-field__arrow"></i>  							
						</a>
						<div class="slide-me">
							<div class="select-fields clearfix">
								<div class="JSmultiselect multiselect-vrednosti">
									@foreach($values as $key => $value)
									<?php if(in_array($key, $niz_karakteristike)){ ?>
										<label>
											<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
											<p class="filter-text">
												{{All::get_fitures_name($key)}}
											</p>
											<span class="filter-count">
												@if(Options::filters_type()) {{ $value }} @endif
											</span>

										</label>
										<?php }else {?>
										<label>
											<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
											<p class="filter-text">
												{{All::get_fitures_name($key)}}
											</p>
											<span class="filter-count">
												@if(Options::filters_type()) {{ $value }} @endif
											</span>
										</label>
										<?php } ?>
									@endforeach
								</div>
							</div>
						</div> <!-- end .accordion -->
						
					</li>
				@endforeach				
            </ul>
		</div>
		<input type="hidden" id="JSfilters-url" value="{{$url}}" />
	</div>

<!-- SLAJDER ZA CENU -->
	<div class="medium-12 columns text-center"> 
		<p id="JSamount"></p>
		<div id="JSslider-range"></div>
	</div>
	<script type="text/javascript" >
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>
</div>


 