<div class="JSproduct"> 
	<div class="product-content clearfix"> 
		@if(All::provera_akcija($row->roba_id))
		<div class="product-sale shake">- {{ Product::getSale($row->roba_id) }} din</div>
		@endif
		<a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="product-image-wrapper">
			<img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
		<a class="product-title" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			{{ Product::short_title($row->roba_id) }}
		</a>
		
		<section class="action-holder clearfix">
			<div class="price-holder">
	            <span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
	            @if(All::provera_akcija($row->roba_id))
	            <span class="old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
	            @endif
	        </div>
			@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)

				<div class="btn-container"> 

					@if(Cart::kupac_id() > 0)
					<!-- <a class="product-title-details" href="{{Options::base_url()}}{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">Detaljnije</a> -->
					<button data-roba_id="{{$row->roba_id}}" title="Dodaj u korpu" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
					<button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
					@else
					<button data-roba_id="{{$row->roba_id}}" title="Dodaj u korpu" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
					<button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn" disabled><i class="fa fa-heart-o"></i></button>
					@endif
	                @if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
					<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
						<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
					</button>
					@endif
				</div>
				@else  
	            <div class="btn-container">
	            	@if(Cart::kupac_id() > 0)
	                <button class="dodavnje not-available" title="Nije dostupno">Nije dostupno</button>
	                <button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
	                @if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
	                <button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
	                	<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
	                </button>
	                @endif	
	                @else
	                <button class="dodavnje not-available" title="Nije dostupno">Nije dostupno</button>
	                <button data-roba_id="{{$row->roba_id}}" title="Doadavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn" disabled><i class="fa fa-heart-o"></i></button>
	                @if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
	                <button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
	                	<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
	                </button>
	                @endif
	                @endif
	            </div>
				@endif
			@else
				<div class="btn-container">
					<button class="dodavanje not-available">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
					@if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
						<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
							<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
						</button>
					@endif				
				</div>			
			@endif
		 </section>
		 @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
		 @endif
	</div>
</div>
