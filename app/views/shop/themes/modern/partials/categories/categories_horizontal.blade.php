<div class="main-wrapper bw clearfix">
<div id="JScat_hor">

    <nav id="JScategories">
    
        <?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
        <h3 class="JScategories-titile">Kategorije </h3>
        <span class="JStoggler"><i class="fa fa-bars"></i></span>
        
        <!-- CATEGORIES LEVEL 1 -->
    
        <ul class="JSlevel-1">
        @if(Options::category_type()==1)
        @foreach ($query_category_first as $row1)
        
                @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                <li> 
                    <a href="{{ Options::base_url()}}{{ $row1->grupa  }}/0/0/0-0">{{ $row1->grupa  }}</a>

                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?></li>
                <ul class="JSlevel-2 clearfix">           
                    @foreach ($query_category_second->get() as $row2)
                         <li>
                            <a href="{{ Options::base_url()}}{{ $row1->grupa }}/{{ $row2->grupa }}/0/0/0-0">{{ $row2->grupa }}
                            </a>
                         @if(Groups::broj_cerki($row2->grupa_pr_id) > 0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                                               
                            <ul class="level-3 clearfix">
                                @foreach($query_category_third as $row3)
                                 <li>
                                    <a href="{{ Options::base_url()}}{{ $row1->grupa }}/{{ $row2->grupa }}/{{ $row3->grupa }}/0/0/0-0">{{ $row3->grupa }}</a>
                                 </li>
                                @endforeach
                            </ul>
                            @endif
                         </li>
                    @endforeach
                </ul>
                @else
                <li><a href="{{ Options::base_url()}}{{ $row1->grupa }}/0/0/0-0">{{ $row1->grupa  }}</a>
                
                @endif
                
                </li>
        @endforeach
        @else
        @foreach ($query_category_first as $row1)
        
        @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
        <li>
            <a href="{{ Options::base_url()}}{{ $row1->grupa  }}/0/0/0-0">{{ $row1->grupa  }}</a>
        <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
        </li>
        <ul class="level-2-category clearfix">     
        @foreach ($query_category_second->get() as $row2)
             <li>
                <a href="{{ Options::base_url()}}{{ $row1->grupa }}/{{ $row2->grupa }}/0/0/0-0">{{ $row2->grupa }}
                </a>
             @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                             
                    <ul class="level-3 clearfix">
                        @foreach($query_category_third as $row3)
                         <li>
                            <a href="{{ Options::base_url()}}{{ $row1->grupa }}/{{ $row2->grupa }}/{{ $row3->grupa }}/0/0/0-0">{{ $row3->grupa }}</a>
                         </li>
                        @endforeach
                    </ul>
                    @endif
             </li>
        @endforeach
        </ul>
        @else
        <li><a href="{{ Options::base_url()}}{{ $row1->grupa }}/0/0/0-0">{{ $row1->grupa  }}</a>
        
        @endif
        
        </li>
        @endforeach             
        @endif

        @if(Options::all_category()==1)
        <li class="category-item-accent">
            <a class="all-category-link" href="/sve-kategorije">Sve Kategorije</a>
        </li>
        @endif      
        </ul>    
    </nav>
</div>
</div>