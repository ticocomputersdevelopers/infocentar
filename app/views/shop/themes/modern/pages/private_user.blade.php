@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row reg-page-padding">
	<div class="medium-5 columns fiz-lice-container">
		<div> 
			<h3 class="registracija-fizickog-lica">Registracija za fizička lica</h3>
		  	<span class="whats-registration"> Prednosti registracije</span>
		</div>
		<div> 
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Brza kupovina.</span><br>
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Mogućnost skupljanja poena i ostvarivanja povremenih popusta. </span><br>
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Istorija porudžbina.</span><br>
	    </div>
	</div>	

<div class="medium-5 columns"> 
<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
<input type="hidden" name="flag_vrsta_kupca" value="0"> 
	<div class="field-group login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="ime">Ime</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
			<div class="error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
			</div>
		</div>
	</div>

	<div class="field-group login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="prezime">Prezime</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
			<div class="error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="email">E-mail</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" autocomplete="false" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
			<div class="error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
			</div>
		</div>
	</div>	

	<div class="field-group row login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="lozinka">Lozinka</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
			<div class="error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="telefon">Telefon</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
			<div class="error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-3 columns text-right small-only-text-left"> 
			<label for="adresa">Adresa</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
			<div class="error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
		</div>
	</div>

	 <div class="field-group row login-input-row">
	   	<div class="medium-3 columns text-right small-only-text-left"> 
	        <label for="without-reg-city"><span class="red-dot"></span> Mesto/Grad </label>
		</div>	

		<div class="medium-8 columns">
			<input class="login-form__input" name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
			<div class="error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
		</div>
	</div>
	<div class="row">
		<div class="medium-11 columns text-right small-only-text-left">   
			<button type="submit" class="login-form-button admin-login">Registruj se</button>
		</div>
	</div>

 </form>
	@if(Session::get('message'))
		<div>Potvrda registracije je poslata na vašu e-mail adresu!</div>
	@endif
 </div>
</div>
 
@endsection

 