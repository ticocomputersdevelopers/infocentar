@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="cart-page">
	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )
	<section class="cart">
		<h2><span class="heading-background">Vaša Korpa</span></h2>
		@if(Session::has('intesa_failure'))
			<h2>{{ Language::trans('Žao nam je, transakcija nije uspela. Pokušajte ponovo.') }}</h2>
		@endif
		<ul class="cart-labels clearfix">
			<li class="medium-6 columns">Proizvod</li>
			<div class="medium-6 columns">
				<li class="medium-3 columns">Cena</li>
				<li class="medium-3 columns">Količina</li>
				<li class="medium-3 columns">Ukupno</li>
				<li class="medium-3 columns">Ukloni</li>
			</div>
		</ul>
		@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
		<ul class="JScart-item clearfix">	
			<div class="medium-6 columns">
				<li class="JScart-image medium-3 small-3 columns">
					<img src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image" /></li>
				<li class="cart-name medium-9 small-9 columns">
					<a href="{{ Options::base_url() }}artikal/{{ Url_mod::url_convert(Product::seo_title($row->roba_id)) }}">
						{{ Product::short_title($row->roba_id) }}
					</a>
					{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}  
				</li>
			</div> 
			
			<div class="medium-6 columns">  				
				<li class="cart-price medium-3 small-3 columns">{{ Cart::cena($row->jm_cena) }}</li>
				<li class="medium-3 small-4 columns">
					<section class="cart-add-amount clearfix">
						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="#"><</a>
						<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)">
						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="#">></a>
					</section>
				</li>
				<li class="cart-total-price medium-3 small-5 columns">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</li>
				<li class="cart-remove medium-3 small-3 columns"><a href="#" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="close JSDeleteStavka">X</a></li>
			</div>
		 </ul>
		@endforeach
	</section>

	<!-- BELOW CART -->
	<section class="below-cart row">
		<div  class="cart-remove-all-wrapper medium-2 small-5 columns">
			<button class="cart-remove-all" id="JSDeleteCart">Isprazni korpu</button>
		</div>
    </section>

    <section class="row"> 
		<div class="cart-summary-wrapper medium-3 right small-12 columns">
			@if(Options::checkTezina() == 1 AND Cart::troskovi_isporuke()>0)
			<section class="cart-summary">
				<span class="summary-label">Cena artikala: </span><span class="summary-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</section>
			<section class="cart-summary">
				<span class="summary-label">Troškovi isporuke: </span><span class="summary-amount">{{Cart::cena(Cart::troskovi_isporuke())}}</span>
			</section>
			<section class="cart-summary">
				<span class="summary-label">Ukupno: </span><span class="summary-amount">{{Cart::cena(Cart::cart_ukupno()+Cart::troskovi_isporuke())}}</span>
			</section>
			@else
			<section class="cart-summary">
				<span class="summary-label">Ukupno: </span><span class="summary-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</section>
			@endif
		</div>
	 </section>
	<!-- CART ACTION BUTTONS -->
	@if(Options::user_registration()==1)
	<ul class="cart-action-buttons clearfix">
		@if(!Session::has('b2c_kupac'))
			<li><button class="JScart-without-button">Kupi bez registracije</button></li>
			<!-- <li><a class="cart-registration-button" href="{{ Options::base_url() }}prijava">Brza kupovina</a></li> -->
			<li><a class="cart-login-button" href="#" data-reveal-id="myModal-login">Uloguj se</a></li>
		@endif
	</ul>
	<!-- SUBMIT ORDER AREA -->
	<section class="JSsubmit-order-area">
		<!-- BUY WITHOUT REGISTRATION FORM -->
		<section class="JSsubmit-order-area" style="display: {{ count(Input::old()) > 0 || Session::has('b2c_kupac')  ? 'block;' : 'none;' }}">
		@if(Options::neregistrovani_korisnici()==1)
		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form row">
			@if(!Session::has('b2c_kupac'))
			<div class="row">
				<div class="medium-12 columns cart-form-no-padding"> 
				@if(Input::old('flag_vrsta_kupca') == 1)
		 			<div class="JSCheckVrsta without-btn personal medium-6 columns cart-form-no-padding left-fiz-face" data-vrsta="personal">
		 				Fizičko Lice <i class="fa fa-check"></i>
		 			</div>

					<div class="JSCheckVrsta without-btn active none-personal medium-6 columns cart-form-no-padding end right-leg-face" data-vrsta="non-personal">				Pravno Lice <i class="fa fa-check"></i>
					</div>
		 		@else
				 	<div class="JSCheckVrsta without-btn active personal medium-6 columns cart-form-no-padding left-fiz-face" data-vrsta="personal">
				 		Fizičko Lice <i class="fa fa-check"></i>
				 	</div>
					<div class="JSCheckVrsta without-btn none-personal medium-6 columns cart-form-no-padding end right-leg-face" data-vrsta="non-personal">
						Pravno Lice <i class="fa fa-check"></i>
					</div>
			 	@endif
				</div>
			</div>
		    <input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">

	 <div class="medium-12 columns">
	 	  <div class="row"> 
			<div class="medium-3 columns cart-form-no-padding JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
				<label for="without-reg-company"><span class="red-dot">*</span> Naziv Firme:</label>
				<input id="without-reg-company" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
			</div>

			<div class="medium-3 columns cart-form-no-padding end JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
			 	<label for="without-reg-pib">PIB:</label>
				<input id="without-reg-pib" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}">
		        <div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
			</div>

			<div class="medium-3 columns cart-form-no-padding JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
				<label for="without-reg-name"><span class="red-dot">*</span> Ime</label>
				<input id="without-reg-name" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
			</div>

			<div class="medium-3 columns cart-form-no-padding JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
				<label for="without-reg-surname"><span class="red-dot">*</span> Prezime</label>
				<input id="without-reg-surname" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
			</div>

			<div class="medium-3 cart-form-no-padding columns"> 
				<label for="without-reg-phone"><span class="red-dot">*</span> Telefon</label>
				<input id="without-reg-phone" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
			</div>

			<div class="medium-3 end cart-form-no-padding columns"> 
				<label for="without-reg-e-mail"><span class="red-dot">*</span> E-mail</label>
				<input id="JSwithout-reg-email" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
		    </div>
 		 </div>
	 
 		 <div class="row">
		 
  		   <div class="medium-6 cart-form-no-padding columns"> 
				<label for="without-reg-address"><span class="red-dot">*</span> Adresa za dostavu</label>
				<input id="without-reg-address" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
		   </div>

		   <div class="medium-6 cart-form-no-padding columns end"> 
			   <label for="without-reg-city"><span class="red-dot">*</span> Mesto/Grad</label>				 
				 <input type="text" name="mesto" tabindex="6">
			    <div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
			</div>

			<!-- <div class="medium-3 columns"> 
				<label>Postanski broj:</label>
				<input type="text" name="postanski_broj">
			</div> -->
 		  </div>
 		  @endif
		 <div class="row"> 
 			<div class="medium-6 cart-form-no-padding columns">
				<label>Način isporuke:</label>
				<select name="web_nacin_isporuke_id">
					{{Order::nacin_isporuke(Input::old('web_nacin_isporuke_id'))}}
				</select>
			</div>

			<div class="medium-6 columns cart-form-no-padding end">
				<label>Način plaćanja:</label>
				<select name="web_nacin_placanja_id">
					{{Order::nacin_placanja(Input::old('web_nacin_placanja_id'))}}
				</select>
			</div>
 		  </div>

			<div class="row"> 
				<div class="medium-12 columns cart-form-no-padding">
					<label>Napomena:</label>
					<textarea class="" name="napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
			  	    <p class="napomena-cart"></p>
				 </div>
			 </div>

			<div class="row"> 
			    <div class="medium-4 columns medium-centered text-center capcha" id="JSCaptcha" {{(Input::old('web_nacin_placanja_id') == 3 ? '' : 'hidden')}}>
			    	<div> 
						<img src="{{ Captcha::img() }}" alt="capcha"><br>
						<span>Unesite kod sa slike</span>
						<input class="form-control" name="captcha" type="text" tabindex="10" autocomplete="off">
						<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
					</div>
				</div>
	 		</div>

			 <div class="row">
			 	<div class="medium-12 columns cart-form-no-padding"> 
					<p class="required-fields"><span>*</span> Obavezna polja</p>
				</div>
			 </div>
		 </div>
			

			<div class="text-center medium-12 columns end">
				<button id="JSOrderSubmit" class="forward">Završi kupovinu</button>
			</div>
		</form>	
		@endif
	</section>
</section>
	@endif
	@else
	<section class="cart">
		<h2><span class="heading-background">Vaša Korpa</span></h2>
		<span class="no-articles">Trenutno nemate artikle u korpi.</span>
	</section>
	@endif
</div>

@endsection