@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    <!-- MAIN SLIDER -->
    <section id="JSmain-slider" class="large-offset-3 medium-10 large-9 columns">
        <?php foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->limit(10)->get() as $row){ ?>
        <div>
            <a href="<?php echo $row->link; ?>">
                <img src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
            </a>
        </div>
        <?php } ?>
    </section>
    <aside class="banners-right medium-9 columns">
        <?php foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->limit(2)->get() as $row){ ?>
            <div class="medium-6 columns text-center">
                <a href="<?php echo $row->link; ?>">
                    <img src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
                </a>
            </div>
        <?php } ?>
    </aside>
@endsection

@section('page')
  @include('shop/themes/'.Support::theme_path().'partials/products/action_type')

    <div class="container">
        <h2><span class="heading-background">Najpopularniji proizvodi</span></h2>
        <section class="JSMostPopularProducts JSproduct-slider">
            @foreach(Articles::mostPopularArticles() as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </section>
    </div>

    @if(count(Articles::bestSeller()))
    <div class="container">
        <h2><span class="heading-background">Najprodavaniji proizvodi</span></h2>
        <section class="JSBestSellerProducts JSproduct-slider">
            @foreach(Articles::bestSeller(4) as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </section>
    </div>
    @endif

    <div class="container">
        <h2><span class="heading-background">Najnoviji proizvodi</span></h2>
        <section class="JSMostPopularProducts JSproduct-slider">
            @foreach(Articles::latestAdded() as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </section>
    </div> 
@endsection