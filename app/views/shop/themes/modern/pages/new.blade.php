@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<section class="news-section">
	<article class="single-news">
		<div class="row">
			<div class="columns medium-12 large-12">
				<img class="vest-slika" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
			</div>
			<div class="columns medium-12 large-12 news-text">
				<h2 class="news-title"><span class="heading-background heading-background-news">{{ $naslov }}</span></h2>
				<p class="">{{ $sadrzaj }}</p>
			</div>
		</div> <!--  end of .row -->
	</article> 
</section>
@endsection