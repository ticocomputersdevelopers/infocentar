@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
    <section id="main-content" class="category-listing medium-9 columns">
        <?php 
            $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
        ?>

        @foreach ($query_category_first as $row1)
            @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                <!-- ROW 1 -->
               <div class="row">
                    <h2 id="{{ $row1->grupa_pr_id  }}" class="category-heading medium-12 columns">{{ $row1->grupa }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                    @foreach ($query_category_second->get() as $row2)
                       <div class="columns medium-4">
                            <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0">{{ $row2->grupa }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                            <ul class="category__list">
                                <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                @foreach($query_category_third as $row3)
                                <li class="category__list__item">
                                    <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0">{{ $row3->grupa }} 
                                        <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                    </a>
                                    <ul class="category__list">
                                        <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                        @foreach($query_category_forth as $row4)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ $row4->grupa }} 
                                                <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach

               </div> <!-- end .row -->
                <!-- END ROW 1 -->
            @else
                <div class="row">
                    <h2 id="{{ $row1->grupa_pr_id  }}" class="category-heading medium-12 columns">{{ $row1->grupa  }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                    @foreach ($query_category_second->get() as $row2)
                       <div class="columns medium-4">
                            <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0">{{ $row2->grupa }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                            <ul class="category__list">
                                <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                @foreach($query_category_third as $row3)
                                <li class="category__list__item">
                                    <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0">{{ $row3->grupa }} 
                                        <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                    </a>
                                    <ul class="category__list">
                                        <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                        @foreach($query_category_forth as $row4)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ $row4->grupa }} 
                                                <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach

               </div> <!-- end .row -->                        

            @endif
        @endforeach 

    </section><!-- end #main-content -->

     <!-- MAIN CONTENT -->
    <section id="category-sidebar" class="medium-3 columns"> 

        <!-- caregories HERE -->

        <ul class="JScategory-sidebar__list">
            <div class="JScategory-sidebar__list__toggler"></div>
            <div class="scrollable"> 
                @foreach ($query_category_first as $row1)
                <li class="category-sidebar__list__item">
                    <a href="#{{ $row1->grupa_pr_id }}" class="category-sidebar__list__item__link">{{ $row1->grupa  }} 
                        <span class="category-sidebar__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span>
                    </a>
                </li>
                @endforeach 
            </div>
        </ul>

    </section>

@endsection