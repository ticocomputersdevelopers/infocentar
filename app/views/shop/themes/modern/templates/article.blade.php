﻿<!DOCTYPE html>
<html>
    <head>
        @include('shop/themes/'.Support::theme_path().'partials/head')
        <script type="application/ld+json">
        {
        "@context" : "http://schema.org",
        "@type" : "Product",
        "name" : "<?php echo $title ?>",
        "image" : "<?php echo Options::base_url()."".Product::web_slika_big($roba_id); ?>",
        "description" : "<?php echo Product::seo_description($roba_id) ?>",
        "brand" : {
        "@type" : "Brand",
        "name" : "<?php echo Product::get_proizvodjac_name($roba_id); ?>"
        },
        "offers" : {
        "@type" : "Offer",
        "price" : "<?php echo Cart::cena(Product::get_price($roba_id)); ?>"
        } 
        }  
        </script>
    </head>
    <body id="artical-page" 
        @if($strana == All::get_page_start()) id=""  @endif 
        @if(Support::getBGimg() != null) 
        @endif
    >
        <div id="main-wrapper">
            <!-- PREHEADER -->
            @include('shop/themes/'.Support::theme_path().'partials/menu_top')
            <!-- HEADER -->
            @include('shop/themes/'.Support::theme_path().'partials/header')
            @if(Options::category_view()==0)
                @include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
            @endif  


            @yield('article_details')

            
            <!-- FOOTER -->
            @include('shop/themes/'.Support::theme_path().'partials/footer')
        </div>
        <a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>

        <!-- LOGIN POPUP -->
        @include('shop/themes/'.Support::theme_path().'partials/popups')

        <!-- BASE REFACTORING -->
        <input type="hidden" id="base_url" value="{{Options::base_url()}}" />
        <input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
        
        <!-- js includes -->
        <script src="{{Options::domain()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/slick.min.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/jquery.elevateZoom-3.0.8.min.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/jquery.fancybox.pack.js" type="text/javascript"></script>
        @if(Session::has('b2c_admin'.Options::server()))
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        @endif
        <script src="{{Options::domain()}}js/foundation.min.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/jquery.accordion.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js" type="text/javascript"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js" type="text/javascript"></script>
      </body>
</html>