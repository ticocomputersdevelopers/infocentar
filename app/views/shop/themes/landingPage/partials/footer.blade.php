<footer>
	<div class="footer-content">
		<div class="container">
			<div class="row"> 


				@foreach(All::footer_sections() as $footer_section)

					@if($footer_section->naziv == 'slika')
					<div class="col-md-3 col-sm-3 col-xs-12">			
						<div class="footer-desc-div">
							@if(!is_null($footer_section->slika))
							<a class="logo" href="{{ $footer_section->link }}">
								 <img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
							</a>
							@else
							<a class="logo" href="/" title="{{Options::company_name()}}">
								 <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
							</a>
							@endif

							<p>
								{{ $footer_section->sadrzaj }} 
							</p>
						</div>
					</div>

					@elseif($footer_section->naziv == 'text')
					<div class="col-md-3 col-sm-3 col-xs-12">
								
						<div class="footer-desc-div">
							<h2 class="footer-title">{{ $footer_section->naslov }}</h2>

							<p>
								{{ $footer_section->sadrzaj }} 
							</p>
						</div>
					</div>

					@elseif($footer_section->naziv == 'linkovi')
					<div class="col-md-3 col-sm-3 col-xs-12">
						<h2 class="footer-title">{{ $footer_section->naslov }}</h2>

						<ul class="footer-links">
							@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
				                @if($page->grupa_pr_id != -1 and $page->grupa_pr_id != 0)
				                <li>
				                    <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($page->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($page->title) }}</a>
				                </li>
				                
								@elseif($page->tip_artikla_id == -1)
									<li>
										<a href="{{ Options::base_url().Url_mod::url_convert($page->naziv_stranice) }}">{{ Language::trans($page->title) }}</a>
									</li>
					            @else

					            @if($page->tip_artikla_id==0)
					                <li>
					                	<a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">{{ Language::trans($page->title) }}</a>
					                </li>
					            @else
					                <li>
					                	<a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($page->tip_artikla_id)) }}">
					                    {{ Language::trans($page->title) }}
					                	</a> 
					                </li>
					            @endif

					        	@endif

							@endforeach
						</ul>  
					</div>

					@elseif($footer_section->naziv == 'kontakt')
					<div class="col-md-3 col-sm-3 col-xs-12 contact-div"> 
						<h2 class="footer-title">{{ $footer_section->naslov }}</h2>

						<span>
							<i class="fas fa-map-marker-alt"></i>
							{{ Options::company_adress() }},
						 	{{ Options::company_city() }}
						</span>

						<span>
							<i class="fas fa-phone-volume"></i>
							{{ Options::company_phone() }}
						</span>

						<span>
							<i class="far fa-envelope-open"></i>
							<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
						</span>
		
					</div>
					@elseif($footer_section->naziv == 'drustvene_mreze')
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="social-icons">
					    	<h2 class="footer-title">
						    	{{ Language::trans('Društvene mreže') }}
						    </h2>
					 		{{ $footer_section->naslov }}
					 	</div>
					</div>						
					@endif

				@endforeach

			</div> 
		</div>
	</div>

	<div class="below-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p>{{ Options::company_name() }} &copy; {{ date('Y') }}. Sva prava su zadržana. - <a href="https://www.selltico.com/">Izrada internet prodavnice</a> - 
						<a href="https://www.selltico.com/"> Selltico. </a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>
  