@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
    <div id="admin-menu"> 
        <div class="container text-right">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <span class="ms admin-links">
                    <a target="_blank" href="{{ Options::base_url() }}admin">
                        {{ Language::trans('Admin Panel') }}
                    </a>
                </span>|

                <span class="ms admin-links">
                    <a href="{{ Options::domain() }}admin-logout">
                        {{ Language::trans('Odjavi se') }}
                    </a>
                </span>
            </div>
        </div>
    </div>  
@endif


<div id="preheader">
    <div class="container">
 
     
        @if(Options::user_registration()==1)
            <div class="row preheader-div">

                <div class="col-md-6 col-sm-6"> 
                    <div class="" id="top-navigation">      
                        <ul class="top-menu-links">
                            @foreach(All::menu_top_pages() as $row)
                                <li>
                                    <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">{{ Language::trans($row->title) }}</a>
                                </li>
                            @endforeach

                            @if(Options::checkB2B())
                                <li>
                                    <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="confirm">B2B</a>
                                </li> 
                            @endif 
                        </ul>
                     </div>
                </div>

                <div class="col-md-6 col-sm-6"> 
                    <div class="data-div">
                        <ul class="data-menu-links">

                            <li>
                                @if(Options::product_currency()==1)
                                    <div class="currency-div">
                                         <a class="dropdown-toggle currency-btn" data-toggle="dropdown">
                                             {{Articles::get_valuta()}} 
                                             <span class="arrow-icon"><i class="fas fa-angle-down"></i></span>
                                         </a>
                                     
                                        <ul class="dropdown-menu currency-list">
                                            <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('valuta') }}/1">{{ Language::trans('RSD') }}</a></li>
                                            <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('valuta') }}/2">{{ Language::trans('EUR') }}</a></li>
                                        </ul>
                                    </div>
                                @endif
                            </li>               

                            @if(Session::has('b2c_kupac'))
                               
                            @if(trim(WebKupac::get_user_name()))
                            <li> 
                                <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                                    {{ WebKupac::get_user_name() }}
                                </a>
                            </li>
                            @endif 

                            @if(trim(WebKupac::get_company_name()))
                            <li>
                                <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                                {{ WebKupac::get_company_name() }}
                                </a>     
                            </li>
                            @endif 

                            <li>
                                <a id="logout-button" href="{{Options::base_url()}}logout">
                                {{ Language::trans('Odjavi se') }}
                                </a>
                            </li>

                            @else 


                            <li>
                                <a class="registration-btn" id="login-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('prijava') }}">
                                    {{ Language::trans('Uloguj se') }}
                                </a>
                            </li>
                            <li>
                                <a class="registration-btn" id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}">
                                    {{ Language::trans('Registracija') }}
                                </a>
                            </li>

                            @endif

                        </ul>
                 


                        @if(Options::stranice_count() > 0)
                            <button type="button" id="navigationToggle">
                                <i class="fas fa-bars"></i>                    
                           </button> 
                        @endif
                        
                    </div>
                </div>         

            </div>
         @endif     
     </div>
</div>

