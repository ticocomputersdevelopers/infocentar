    

<!-- MAIN SLIDER -->   

<div class="main-slider">
    <div id="JSmain-slider" >
        @foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->limit(10)->get() as $row)
            <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
            <div>
                @if($slajd_data->naslov_dugme == '')
                <a class="AllSliderLink" href="<?php echo $row->link; ?>" >
                @endif
                        <div class="JSmain-slider" style="background-image: url('{{ Options::domain() }}<?php echo $row->img; ?>'); ">
                        
                        <div style="max-width: 1440px; padding-left: 20px;padding-right: 20px; width: 100%; margin: 0px auto;">
                            <div class="slider-desc-div">
                            @if($slajd_data->nadnaslov != '')
                                <div>
                                    <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                                        {{ $slajd_data->nadnaslov }}
                                    </p>
                                </div>
                            @endif

                            @if($slajd_data->naslov != '')
                                <div>
                                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                                        {{ $slajd_data->naslov }}
                                    </h2>
                                </div>
                            @endif

                            @if($slajd_data->sadrzaj != '')
                                <div>
                                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                                        {{ $slajd_data->sadrzaj }}
                                    </div>
                                </div>
                            @endif

                            @if($slajd_data->naslov_dugme != '')
                                <div>
                                    <a href="<?php echo $row->link; ?>" class="slider-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                                        {{ $slajd_data->naslov_dugme }}
                                    </a>
                                </div>
                             @endif
                            </div>
                        </div>
                        </div>
                @if($slajd_data->naslov_dugme == '')
                </a>
                @endif
            </div>
        @endforeach
    </div>
</div>


