 


<div class="JSproduct">  
	<div class="shop-product-card"> 

		<!-- SALE PRICE -->
		@if(All::provera_akcija($row->roba_id))
			<div class="sale-label">
				{{ Language::trans('Akcija') }}
			</div>
		@endif
		
	<div class="product-image-div">
 		<!-- PRODUCT IMAGE -->
			<a class="product-image-wrapper" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">

				<img class="product-image img-responsive" 
				src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" 
				alt="{{ Product::seo_title($row->roba_id) }}" />
 
				<img alt="{{ Product::seo_title($row->roba_id) }}" class="second-product-image" src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" >

			</a>
	
	
		<div class="button-div"> 
			@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
			<span class="article-compare JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
				<i title="{{ Language::trans('Uporedi') }}" class="fas fa-exchange-alt"></i>
			</span>
			@endif
							
		 	@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)	 
					<!-- <div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
						{{ Language::trans('Dodaj u korpu') }}			 
			        </div>	 -->
			        <a
						@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
							style='width: auto'
						@endif
				        class="buy-btn" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				        
			        	{{ Language::trans('Detaljnije') }}	
			        </a>			    
					@else 
			        <div class="not-available" title="Nije dostupno">
				        {{Language::trans('Nije dostupno')}}
				    </div>	      
				@endif
			@else  
				<div class="not-available">
					{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }} 
				</div>					 		 	 	
			@endif 

			@if(Cart::kupac_id() > 0)
				<div class="wish-div">
				  	<span data-roba_id="{{$row->roba_id}}" data-toggle="tooltip" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
					  	<i class="far fa-heart"></i>
					</span>
				</div> 
			@else
				<div class="wish-div">
					<a href="{{Options::base_url()}}prijava">
					  	<span data-roba_id="{{$row->roba_id}}" data-toggle="tooltip" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list" disabled>
					  		<i class="far fa-heart"></i>
						</span>
					</a>
				</div>  
			@endif		
		</div>     
	</div>

	<!-- PRODUCT TITLE -->
	<div class="product-name-div">
		<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			<h2> {{ Product::short_title($row->roba_id) }} </h2>
		</a>

		<div class="price-holder">
		    <span class="product-price"> 
		    	{{ Cart::cena(Product::get_price($row->roba_id)) }} 
		    </span>
           <!--  @if(All::provera_akcija($row->roba_id))
                	<span class="product-old-price">
                		{{ Cart::cena(Product::old_price($row->roba_id)) }}
                	</span>
                @endif -->     
        </div>
    </div>


 	<!-- ADMIN BUTTON -->
	@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">
			{{ Language::trans('IZMENI ARTIKAL') }}
		</a> 
	@endif 
	</div>
</div>
 
               
  
  