@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="intesa-page">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
	
		<h3 class="intesa-text">
			Nakon potvrde vrši se preusmeravanje na sajt Banke Inteze, <br>
			gde se na zaštićenoj stranici realizuje proces plaćanja.
		</h3>

		<table>
			<thead>
			    <tr>
			    	<th>Podaci o korisniku:</th>
			    </tr>
			</thead>
			<tbody>
				<tr>
					<td class="desc-td">
						@if($web_kupac->flag_vrsta_kupca == 1)
							Firma
						@else
							Ime
						@endif
					</td>
					<td>
						@if($web_kupac->flag_vrsta_kupca == 1)
							{{ $web_kupac->naziv }}
						@else
							{{ $web_kupac->ime.' '.$web_kupac->prezime }}
						@endif
					</td>
				</tr>

				<tr>
					<td class="desc-td">
						Adresa
					</td>
					<td>
						{{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}
					</td>
				</tr>
			</tbody>
		</table>

		<table>
			<thead>
			    <tr class="desc-td">
			    	<th>Podaci o trgovcu:</th>
			    </tr>
			</thead>
			<tbody>
				<tr>
					<td class="desc-td">
						Preduzeće
					</td>
					<td>
						{{ $preduzece->naziv }}
					</td>
				</tr>

				<tr>
					<td class="desc-td">
						Kontakt osoba
					</td>
					<td>
						{{ $preduzece->kontakt_osoba }}
					</td>
				</tr>

				@if($preduzece->matbr_registra != '')
					<tr>
						<td class="desc-td">
							MBR
						</td>
						<td>
							{{ $preduzece->matbr_registra }}
						</td>
					</tr>
				@endif

				@if($preduzece->pib != '')
					<tr>
						<td class="desc-td">
							VAT Reg No
						</td>
						<td>
							{{ $preduzece->pib }}
						</td>
					</tr>
				@endif

				<tr>
					<td class="desc-td">
						Adresa i mesto
					</td>
					<td>
						{{ $preduzece->adresa }}, {{ $preduzece->mesto }}
					</td>
				</tr>
				<tr>
					<td class="desc-td">
						Tel
					</td>
					<td>
						{{ $preduzece->telefon }}
					</td>
				</tr>
				<tr>
					<td class="desc-td">
						E-mail
					</td>
					<td>
						<a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a>
					</td>
				</tr>
			</tbody>
		</table>
		
		<div class="agree-div">
			<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> 
			Slažem se sa 
			<a href="{{ Options::base_url() }}intesa-uslovi" target="_blank">
				uslovima korišćenja
			</a>
		</div>

		<form method="post" action="https://bib.eway2pay.com/fim/est3Dgate">
		<!-- <form method="post" action="https://testsecurepay.eway2pay.com/fim/est3Dgate"> -->

		 		<button class="orange-btn" type="submit" id="JSIntesaSubmit" disabled>
		 			{{ Language::trans('Završi plaćanje karticom') }}
		 			<i class="fas fa-chevron-right"></i>
			 	</button>

 				<input type="hidden" name="clientid" value="{{ $clientId }}">
				<input type="hidden" name="oid" value="{{ $oid }}">
				<input type="hidden" name="amount" value="{{ $amount }}">
				<input type="hidden" name="okurl" value="{{ $okUrl }}">
				<input type="hidden" name="failUrl" value="{{ $failUrl }}">
				<input type='hidden' name='shopurl' value="{{ $cancelUrl }}">
				<input type="hidden" name="TranType" value="{{ $transactionType }}">
				<input type="hidden" name="currency" value="{{ $currency }}">
				<input type="hidden" name="rnd" value="{{ $rnd }}">
				<input type="hidden" name="hash" value="{{ $hash }}">
				<input type="hidden" name="storetype" value="3D_PAY_HOSTING">
				<input type="hidden" name="hashAlgorithm" value="ver2">
				<input type="hidden" name="encoding" value="utf-8" />
				<input type="hidden" name="lang" value="sr">			
				
		</form>

		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSIntesaSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSIntesaSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>

	</div>
</div>
@endsection