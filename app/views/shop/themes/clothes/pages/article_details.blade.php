@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')
 
    <div id="fb-root"></div>

    <script>
    @if(Session::has('success_add_to_cart'))
    $(document).ready(function(){    
        alertSuccess("Artikal je dodat u korpu.");
    });
    @endif

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  $(document).ready(function () {
    $('.JSproduct-slider').slick({
        infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
          }
        },
        {
          breakpoint: 996,
          settings: {
          slidesToShow: 2,
          slidesToScroll: 2
          }
        },
        {
          breakpoint: 620,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
          }
        }
      ]
    });

    /* Article details margin */
    /*if($('.osobina').length > 0 ) {
      $('.product-preview-price').css('padding', '40px 0px');
    }*/

    $($('.elevatezoom-gallery')[0]).addClass('active');

    if($(window).width() > 1024) {
      $("#zoom_03").elevateZoom({
        gallery: 'gallery_01', 
        cursor: 'pointer', 
        galleryActiveClass: "active",
        imageCrossfade: true,
      });

      $("#zoom_03").bind("click", function(e) {  
        var ez =   $('#zoom_03').data('elevateZoom'); 
        $.fancybox(ez.getGalleryList());
        return false;
      })
    } else {
        $('.elevatezoom-gallery').click(function() {
        $('#zoom_03').attr('src', $(this).data('zoom-image'));
        $('.elevatezoom-gallery').removeClass('active');
        $(this).addClass('active');
      })
    }


    if($('.artical-brand-text').length > 0 && $('.description-tab p').length < 1) {
      $('.article-heading', '.artical-brand-text').css({
        'margin-bottom': '10px', 
        'display': 'inline-block'
      });
    }
    if($('.article-brand-img img').length > 0 && $('.description-tab p').length < 1 ) {
      $('.article-brand-img').css('margin', '20px 0px');
    }

    // if($('#description-tab').height() < 1 && $(window).width() < 420) {
    //   $('.product-preview-price').css('padding', '40px 0px');
    // }


    /* Related article */
    if($('.JSrelated-products .JSproduct').length < 1) {
      $('.related-section').hide();
    }

    /* Bound article */
    if($('.bound-section .JSproduct').length < 1) {
      $('.bound-section').hide();
    }

    if($('.bound-section .JSproduct').length > 0 || $('.JSrelated-products .JSproduct').length > 0) {
      $('.article-details-div').css('padding-bottom', '60px');
      if($(window).width() < 1200) {
        $('.article-details-div').css('padding-bottom', '20px');
      }
      if($(window).width() < 1024) {
        $('.article-details-div').css('padding-bottom', '0px');
      }
    }

    if($(window).width() > 1024 ) { 
      $('.elevatezoom-gallery').click(function() {
          var image = new Image();
          image.src = $(this).find('img').attr("src");

            image.onload = function() {
              if(this.height < 700) {
                $('.zoomWrapper').css('height', this.height )
              } else {
                $('.zoomWrapper').css('height', '700px' )
              }
        
            };
           
        })
    }
  });
    </script>

        <div class="article-details-page">
            <div class="container">

                <div class="row article-details-div">
                  <!-- Product preview image -->
                  <div class="JSproduct-preview-image col-lg-7 col-md-7 col-sm-12 col-xs-12">
                      <div id="gallery_01" class=""> 
                         {{ Product::get_list_images($roba_id) }}
                      </div> 
                    
                      <div class="big-galery-img disableZoomer">
                        <div class="zoomWrapper">
                           <img id="zoom_03" class="JSzoom_03"  itemprop="image" src="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" alt="{{ Product::seo_title($roba_id)}}" />
                        </div>
                      </div>
                  </div>


                        <div class="product-preview-info col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                 <!-- Admin button -->  
                            @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                              <div class="admin-article"> 
                                  <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)">
                                      {{ Language::trans('IZMENI ARTIKAL') }}
                                  </a> 

                                  <span class="supplier"> 
                                      {{ Product::get_dobavljac($roba_id) }}
                                  </span> 

                                  <span class="supplier">
                                      {{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}
                                  </span>
                              </div><!-- End of Admin button -->
                            @endif     
                            
                            <h2 class="article-heading" itemprop="name">
                                {{ Language::trans(Product::seo_title($roba_id)) }}
                            </h2>


                          <!-- Brendovi -->
                          @if($proizvodjac_id != -1)
                               <div class="product-manufacturer" itemprop="brand">
                                       
                                   @if( Product::slikabrenda($roba_id) != null )
                                       <a class="article-brand-img" href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                           <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                                       </a>
                                   @else
                           
                                   <a href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                       <span class="artical-brand-text">{{ product::get_proizvodjac($roba_id) }}</span>
                                   </a>
                           
                                   @endif                                   
                               </div>
                          @endif 


                            <!-- Description -->
                            <div id="description-tab">
                              <p itemprop="description"> {{ Product::get_opis($roba_id) }} </p>
                                <p itemprop="description"> {{ Product::get_karakteristike_short_grupe($roba_id) }} </p>
                            </div>

                            <!-- Price -->
                            <div class="product-preview-price">
                                @if(Product::pakovanje($roba_id))
                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">{{Language::trans('Pakovanje')}}: </span>
                                        <span class="mpPrice">{{ Product::ambalaza($roba_id) }}</span>
                                    </div>
                                @endif                                 
                                @if(All::provera_akcija($roba_id))                                      

                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('Cena:')}} 
                                        </span>
                                        
                                        <span class="mpPrice curent-price">
                                            {{ Cart::cena(Product::get_price($roba_id,false)) }}
                                        </span>
                                    </div>

                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('Akcijska cena:')}}
                                        </span>

                                        <span class="mpPrice curent-price">
                                            {{ Cart::cena(Product::get_price($roba_id)) }}
                                        </span>
                                    </div>

                                    @if(Product::getPopust_akc($roba_id)>0)
                                        <div class="product-preview-price-new" itemprop="price">
                                            <span class="PDV_price">{{Language::trans('Popust')}}: 
                                                
                                            </span>

                                            <span class="discount-price">
                                                {{ Cart::cena(Product::getPopust_akc($roba_id)) }}
                                            </span>
                                        </div>
                                    @endif
                                @else

                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('Cena:')}} 
                                        </span>
                                        
                                        <span class="mpPrice curent-price">
                                            {{ Cart::cena(Product::get_price($roba_id)) }}
                                        </span>
                                    </div>
                                @endif
                             </div> <!-- End of price -->


                            <div class="add-to-cart-area">     
                                 @if(AdminSupport::getStatusArticle($roba_id) == 1)
                                    @if(Cart::check_avaliable($roba_id) > 0)
                                    <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form">                                      
                                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                                        <div class="properties-div">
                                            @if(Product::check_osobine($roba_id))
                                                <div class="osobine">
                                                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                                        <div class="osobina">
                                                            <div class="osobina_naziv">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}:</div>
                                                            <div class="karakteristike_osobine">
                                                                @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id)->get() as $row2)
                                                                <label class="osobina_karakteristika" style="background-color: {{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'boja_css') }}">
                                                                    <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $row2->osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$row2->osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                                                    <span title="{{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'vrednost') }}">
                                                                        {{ Product::osobina_vrednost_checked($osobina_naziv_id, $row2->osobina_vrednost_id) }}
                                                                    </span>
                                                                </label>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div> 
                                        <div class="button-container">
                                            <input type="number" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                                            <button id="JSAddCartSubmit" class="JSadd-to-cart buy-btn orange-btn">
                                                {{ Language::trans('Dodaj u korpu') }}
                                            </button>

                                        </div>

                                        <div class="error-amount">
                                            {{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}
                                        </div>  
                                    </form>
                                    @else
                                        <button class="dodavanje not-available orange-btn">
                                          {{ Language::trans('Nije dostupno') }}
                                        </button>                                  
                                    @endif
                                @else
                                <button class="dodavanje not-available orange-btn" data-roba-id="{{$roba_id}}">
                                    {{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($roba_id),'naziv') }}
                                </button>
                                @endif
                            </div>
    
                      </div> <!-- End of product preview-info -->



                  <div class="col-md-12 col-sm-12 col-xs-12 product-tags"> 
                      @if(Options::checkTags() == 1)
                          @if(Product::tags($roba_id) != '')
                          <div class="tag-content">
                              <div class="tags-title">Tagovi:</div>
                              
                              {{ Product::tags($roba_id) }} 
                             
                          </div>
                          @endif
                      @endif    
                  </div>
  
                    
                </div> <!-- End of row -->

                @include('shop/themes/'.Support::theme_path().'partials/bound-article')

                @include('shop/themes/'.Support::theme_path().'partials/related-article')

                
            </div> <!-- End of container -->
        </div> <!-- End of article-details -->


@endsection 