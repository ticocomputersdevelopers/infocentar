
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="single-news-section">
	<div class="container">

		<div class="row">
			<div class="col-md-12">

				<article class="single-news">

					<h2 class="title">
						{{ $naslov }}
					</h2>
					
					<div class="create-news-date"> 
						<a class="all-news" href="{{Options::base_url()}}{{ Url_mod::convert_url('blog') }}"><i class="fas fa-chevron-left"></i>Svi Blogovi</a>
						{{ Support::date_convert($datum) }}
					</div>

					<img class="img-responsive single-news-img" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
				
					{{ $sadrzaj }} 

					<div class="share-news-div">
						<span>Podelite ovaj post</span>
						<a href="https://www.facebook.com/sharer.php?u={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}" target="_blank" title="Facebook share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							Facebook,
						</a>						
			
						<a href="https://plus.google.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}" title="Google Plus share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					  		Google+,
					  	</a>

						<a href="https://twitter.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Twitter share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							Twitter,
						</a>

						<a href="https://www.linkedin.com/shareArticle?mini=true&url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Linkedin Share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							Linkedin,
						</a>	

						<a href="https://www.pinterest.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Linkedin Share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
							Pinterest
						</a>		
					</div>

					<div class="next-prev-news">
						@if(isset($previous_vest))
						<div class="prev-div">
							<i class="fas fa-chevron-left"></i>
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($previous_vest->web_vest_b2c_id) }}">{{ $previous_vest->naslov }}</a>
						</div>
						@endif

						@if(isset($next_vest))
						<div class="next-div">	
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($next_vest->web_vest_b2c_id) }}">{{ $next_vest->naslov }}</a>
							<i class="fas fa-chevron-right"></i>
						</div>
						@endif
					</div>		
				</article> 
			</div>
		</div>
	
		<div class="row">
			<div class="more-post-div">
				<h2 class="more-post-heading">Još postova</h2> 
				
				<div class="single-news-slider row">

					@foreach(All::getShortListNews($id) as $new)
					<div class="more-post-content col-md-4 col-sm-6 col-xs-12">
						<a class="more-post-link" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}" style="background-image: url('{{ Options::domain() }}{{ $new->slika }}');">
						</a>
					
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}" class="more-post-title">
							{{ $new->naslov }}
						</a>
					</div>
					@endforeach

				</div>
			</div>
		</div>

	</div>
</div>


<script type="text/javascript">
	/* Single blog slider */
	$(document).ready(function () { 
		$('.single-news-slider').slick({
		  infinite: true,
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 2400,
		  speed: 1500,
		  responsive: [
			{
			  breakpoint: 1100,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 800,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				autoplaySpeed: 2000,
	  			speed: 1000
			  }
			},
			{
			  breakpoint: 580,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplaySpeed: 2000,
	  			speed: 1000
			  }
			}
		]
		});

	});

</script>

	
<script src="{{Options::domain()}}/js/slick.min.js" ></script>
		 
<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />

@endsection