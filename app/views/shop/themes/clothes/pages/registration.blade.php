@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
@if(Session::has('message'))
<script type="text/javascript">
    $(document).ready(function(){
		swal("{{ Language::trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') }}.", {
		  buttons: {
		    yes: {
		      text: "Zatvori",
		      value: true,
		    }
		  },
		}).then(function(value){ });
    });
</script>
@endif

<div class="registration-page">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div>
					<p class="title">
						{{ Language::trans('Registrujte se') }} <br>
						
						<span class="login-title">
							kao
							@if(Input::old('flag_vrsta_kupca') == 1)
							<span class="private-user">fizičko lice</span>, kao 
							<span class="active-user company-user">pravno lice</span>
							@else
							<span class="active-user private-user">fizičko lice</span>, kao 
							<span class="company-user">pravno lice</span>
							@endif
						</span>
					</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div>
					<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
						<div class="frm-control">
							<input type="hidden" name="flag_vrsta_kupca" value="0">
						</div>
						<div class="frm-control-div">		
							<div class="frm-control">
								<label for="ime">
									Ime
									<span class="label-star">*</span> 
								</label>
								<input name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('ime') ? $errors->first('ime') : "" }}
								</div>
							</div>

							<div class="frm-control">
								<label for="prezime">
									Prezime
									<span class="label-star">*</span> 
								</label>
								<input name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="naziv">
									Naziv firme
									<span class="label-star">*</span> 
								</label>
								<input name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}
								</div>
							</div>
							<div class="frm-control">
								<label for="pib">
									PIB
									<span class="label-star">*</span> 
								</label>
								<input name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('pib') ? $errors->first('pib') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">				
							<div class="frm-control">
								<label for="email">
									E-mail
									<span class="label-star">*</span> 
								</label>
								<input autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('email') ? $errors->first('email') : "" }}
								</div>
							</div>						
							<div class="frm-control">
								<label for="lozinka">
									Lozinka
									<span class="label-star">*</span> 
								</label>
								<input name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
								<div class="error red-dot-error">
									{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="telefon">
									Telefon 
								</label>
								<input name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}
								</div>
							</div>

							<div class="frm-control">
								<label for="adresa">
									Adresa 
								</label>
								<input name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}
								</div>
							</div>	
							<div class="frm-control">
								<label for="mesto">
									Mesto 
								</label>
								<input name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}
								</div>
							</div>
						</div>
						<div class="text-right"> 
							<button type="submit" class="orange-btn">
								{{ Language::trans('Registruj se') }}
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 