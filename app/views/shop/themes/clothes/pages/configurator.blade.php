@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="configurator-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="konfigurator table">
						<thead>
							<tr>
								<td>{{ Language::trans('Grupa') }}</td> 
								<td></td>
								<td>{{ Language::trans('Artikli') }}</td>
								<td></td>
								<td>{{ Language::trans('Količina') }}</td>
								<td>{{ Language::trans('Cena') }}</td>
							</tr>
						</thead>
						@foreach($grupe as $row)
						<tr class="JSUsefulRow">
							<td class="konfigurator-grupa">{{ $row->grupa }} </td>
							<td class="JSGrupaAdd" data-id="{{ $row->grupa_pr_id }}">{{ Language::trans('Dodaj') }}</td>
							<td>
								<select class="JSArtikal">
									<option value="">{{ Language::trans('Odaberite') }}...</option>
									@foreach(All::getKonfiguratorArticles($row->grupa_pr_id) as $row2) 
									<?php $cena = Product::get_price($row2->roba_id); ?>
									<option value="{{ $row2->roba_id }}" data-cena="{{ $cena }}">{{  Language::trans($row2->naziv_web) }} -> {{ $cena }}.din</option>
									@endforeach
								</select>
								<span class="JSArtDetails">{{ Language::trans('Vidi') }}</span>
							</td>
							<td>
								<span class="JSGrupaRemove"></span>
							</td>
							<td>
								<input type="text" class="JSKolicina" value="1" onkeypress="validate(event)">
							</td>
							<td>
								<div class="JSCenaKonf">0.00</div>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div> <!-- End of col -->
		</div> <!-- End of row -->

		<div class="row"> 
			<div class="col-md-12">
				<div class="amount-section">
					<h2 class="amount-heading">Iznos narudzbine</h2>
					<div class="amount-div">
						<div>
							<span class='description'>{{ Language::trans('Ukupno u konfiguratoru') }}</span>
							<span class="value" id="JSUkupnaCenaKonf">0.00</span>
						</div>
						<div>
							<span class="description">{{ Language::trans('Ukupno u korpi') }}</span>
							<span class="value" id="JSKorpaUkupnaCena">{{ $korpa_ukupno }}</span>
						</div>
						<div>		
							<span class="description">{{ Language::trans('Ukupno') }}</span>
							<span class="value" id="JSUkupno">{{ $korpa_ukupno }}</span>
						</div>
					</div>
				</div> <!-- End of amount-section -->
			</div>
		</div> <!-- End of row -->

		<div class="row text-center btn-container">
			<button id="JSKonfAdd" class="atractive-comment-btn">{{ Language::trans('Dodaj u korpu') }}</button>
		</div>
	</div>
</div>
@endsection