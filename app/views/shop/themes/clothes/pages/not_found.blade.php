@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="not-found-page">
	<div class="container">
		<div class="row">
			<div class="col-ld-12 col-md-12">
				<div>
					<p class="title">
						{{ Language::trans('404 - Žao nam je, stranica nije dostupna') }}
					</p>
					<p class="not-found-text">
						Žao nam je, tražena strana nije pronađena. Molimo Vas, proverite ispravnost upisane URL adrese.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection