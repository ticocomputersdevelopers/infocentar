@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="logged-user-page">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<div>
					<p class="title">
						Izmenite podatke naloga
					</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="loged-user-content">
				  <form action="{{ Options::base_url()}}korisnik-edit" method="post" class="login-form" autocomplete="off">
						<!-- PERSONAL INFO CHANGE -->
						@if($web_kupac->flag_vrsta_kupca == 0)
						<div class="frm-control-div">
							 <div class="frm-control">
								<label for="name">{{ Language::trans('Ime') }}</label>
								<input name="ime" type="text" value="{{ Input::old('ime') ? Input::old('ime') : $web_kupac->ime }}">
								<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
							</div>
							
							<div class="frm-control">
								<label for="surname">{{ Language::trans('Prezime') }}</label>
								<input name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : $web_kupac->prezime }}">
								<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
							</div>
						 </div>
						
						<!-- COMPANY INFO CHANGE -->
						@elseif($web_kupac->flag_vrsta_kupca == 1)
						<div class="frm-control-div">
							 <div class="frm-control">
								<label for="company-name">{{ Language::trans('Naziv firme') }}</label>
								<input name="naziv" type="text" value="{{ Input::old('naziv') ? Input::old('naziv') : $web_kupac->naziv }}">
								<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
							</div>
							
							<div class="frm-control">
								<label for="pib">{{ Language::trans('PIB') }}</label>
								<input name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : $web_kupac->pib }}">
								<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
							</div>
						 </div>
						@endif
				        <div class="frm-control-div">
				            <div class="frm-control">
				                <label for="e-mail">E-mail</label>
				                <input name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : $web_kupac->email }}">
				                <div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
				            </div>
				            <div class="frm-control">
				                <label for="lozinka_user">{{ Language::trans('Lozinka') }}</label>
				                <input name="lozinka" type="password" value="{{ Input::old('lozinka') ? Input::old('lozinka') : base64_decode($web_kupac->lozinka) }}">
				                <div class="error red-dot-error">{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}</div>
				            </div>
				        </div>

				        <div class="frm-control-div">
				            <div class="frm-control">
				                <label for="address">{{ Language::trans('Adresa') }}</label>
				                <input name="adresa" type="text" value="{{ Input::old('adresa') ? Input::old('adresa') : $web_kupac->adresa }}">
				                <div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
				            </div>
				            <div class="frm-control">
				                <label for="mesto">{{ Language::trans('Mesto') }}</label>
				                <input name="mesto" type="text" value="{{ Input::old('mesto') ? Input::old('mesto') : $web_kupac->mesto }}">
				                <div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
				            </div>
				        </div>
				      
						<div class="frm-control-div">
				            <div class="frm-control">
				                <label for="telefon">{{ Language::trans('Telefon') }}</label>
				                <input name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : $web_kupac->telefon }}">
				                <div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
				            </div>	
						</div>

						<div class="frm-control-div"> 
							<div class="frm-control"> 
								<input type="hidden" name="web_kupac_id" value="{{$web_kupac->web_kupac_id}}" />
								<input type="hidden" name="flag_vrsta_kupca" value="{{$web_kupac->flag_vrsta_kupca}}" />
								
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="orange-btn">{{ Language::trans('Izmeni') }}</button>
						</div>
					</form>

				</div>

				@if(Session::get('message'))	
				
					<script type="text/javascript">
						function alertSuccess(message) {
							swal(message, "", "success");
								
							setTimeout(function() { 
								swal.close();  
							}, 1800);
						}
						alertSuccess("{{ Session::get('message') }}");
					</script>
			    @endif

			</div>
		</div>

		
		

	@if(count(All::getNarudzbine($web_kupac->web_kupac_id)) > 0)
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive order-table-div">
				<table class="user-orders-table">
					<tr>
						<th>{{ Language::trans('Broj narudž') }}.</th>
						<th>{{ Language::trans('Datum') }}</th>
						<th>{{ Language::trans('Iznos') }}</th>
						<th>{{ Language::trans('Status') }}</th>
						<th>{{ Language::trans('Vidi narudž') }}.</th>
					</tr>
					<!-- thus repeat -->
					@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)
					<tr>	
						<td>{{ $row->broj_dokumenta }}</td>
						<td>{{ $row->datum_dokumenta }}</td>
						<td>{{Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id))}}</td>
						<td>{{Order::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</td>
						<td>
							<button type="button" class="user-order-button" data-toggle="modal" data-target="#order-modal{{ $row->web_b2c_narudzbina_id }}"><i class="fa fa-plus" aria-hidden="true"></i></button>
						</td>
					</tr>
					@endforeach
					<!-- end repeat -->
				</table>
			</div>
			<!-- Modal -->
			@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)
			<div class="modal fade" id="order-modal{{ $row->web_b2c_narudzbina_id }}" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title text-center">{{ Language::trans('Vaše narudžbine') }}</h4>
						</div>
						<div class="modal-body">
							<div class="table-responsive">
							 	 <table class="user-orders-table">
									<tr>
										<th class="cell-product-name">{{ Language::trans('Naziv proizvoda') }}:</th>
										<th class="cell">{{ Language::trans('Cena') }}</th>
										<th class="cell">{{ Language::trans('Količina') }}</th>
										<th class="cell">{{ Language::trans('Ukupna cena') }}:</th>
									</tr>
									<tr> 
										@foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->get() as $row2)
										<td class="cell-product-name">{{ Product::short_title($row2->roba_id) }} {{Product::getOsobineStrNonActv($row2->roba_id,$row2->osobina_vrednost_ids)}}</td>
										<td class="cell">{{ Cart::cena($row2->jm_cena) }}</td>
										<td class="cell">{{ (int)$row2->kolicina }}</td>
										<td class="cell">{{ Cart::cena($row2->kolicina*$row2->jm_cena) }}</td>
									</tr>
									@endforeach
									<tr>
										<td colspan="3"></td>
										<td class="summary"><b> {{ Language::trans('Ukupno') }}:{{ Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id)) }}</b></td>

									</tr>					 
								</table>
							</div>
						 </div>
						<div class="modal-footer">
							<button type="button" class="close-me-btn" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endif

	</div>
</div>
@endsection

