@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="login-page">
	<div class="container">

		<div class="row">

			<div class="col-md-12">
				<div>
					<p class="title">
						{{ Language::trans('Prijavite se') }} 
					</p>
				</div>

				<form action="{{ Options::base_url()}}login-post" method="post" class="login-form" autocomplete="off">

					<div class="frm-control">
	 					<label for="email_login">
	 						E-mail <span class="label-star">*</span>
	 					</label> 
						<input id="JSemailLogin" class="login-form__input" placeholder="E-mail adresa" name="email_login" type="text" value="{{ Input::old('email_login') }}" autocomplete="off">
						<div id="JSEmailError" class="error-message">{{ $errors->first('email_login') ? $errors->first('email_login') : "" }}</div>
					</div>
					
					<div class="frm-control">			
						<label for="lozinka_login">
							{{ Language::trans('Lozinka') }} <span class="label-star">*</span>
						</label>

					    <input class="login-form__input" placeholder="Lozinka" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') }}">
					    <div id="JSPasswordlError" class="error-message">{{ $errors->first('lozinka_login') ? $errors->first('lozinka_login') : "" }}</div>
					</div>

					<div class="text-right"> 
						<button type="submit" class="orange-btn">{{ Language::trans('Prijavite se') }}</button>	
					</div>
					<div id="JSForgotPassword">
						Zaboravili ste lozinku?
					</div>
				</form>


			</div>
	
		</div>
	</div>
</div>

@endsection