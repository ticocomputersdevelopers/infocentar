

@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="news-page">
	<div class="container">
		<div class="row">
			<h2 class="title">
				{{ Language::trans('Blog') }}
			</h2> 
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 blog-flex">
				@foreach(All::getNews() as $row)
					<div class="single-news-on-page">
						<a class="image-div" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ Options::domain() }}{{ $row->slika }}');">
							<!-- <img src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}" /> -->
						</a>

						<div class="single-news-content">
							
							<a class="title-new" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
								{{ $row->naslov }}
							</a>

							<span class="create-news-date">{{ Support::date_convert($row->datum) }}</span>
							
							<div class="short-description">{{ All::shortNewDesc($row->tekst) }}</div>

							<div class="more-news-div">
								<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
									{{ Language::trans('Pročitaj više') }} 
									<i class="fas fa-chevron-right"></i>
								</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection 