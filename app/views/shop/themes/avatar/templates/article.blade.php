﻿<!DOCTYPE html>
<html lang="sr">
    <head>
        @include('shop/themes/'.Support::theme_path().'partials/head')
        <script type="application/ld+json">
        {
        "@context" : "http://schema.org",
        "@type" : "Product",
        "name" : "<?php echo $title ?>",
        "image" : "<?php echo Options::domain()."".Product::web_slika_big($roba_id); ?>",
        "description" : "<?php echo Product::seo_description($roba_id) ?>",
        "brand" : {
        "@type" : "Brand",
        "name" : "<?php echo Product::get_proizvodjac_name($roba_id); ?>"
        },
        "offers" : {
        "@type" : "Offer",
        "price" : "<?php echo Cart::cena(Product::get_price($roba_id)); ?>"
        } 
        }  
        </script>
        <link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />
    </head>
    <body id="artical-page" 
    @if($strana == All::get_page_start()) id="start-page"  @endif 
    @if(Support::getBGimg() != null) 
        style="background-image: url({{Options::base_url()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif
    >
        
 
        <!-- HEADER -->
        @include('shop/themes/'.Support::theme_path().'partials/header')
        
        @if(Options::category_view()==0)
            @include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
        @endif  


        @yield('article_details')

        
        <!-- FOOTER -->
        @include('shop/themes/'.Support::theme_path().'partials/footer')

 
        <a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>

        <!-- LOGIN POPUP -->
        @include('shop/themes/'.Support::theme_path().'partials/popups')

        <!-- BASE REFACTORING -->
        <input type="hidden" id="base_url" value="{{Options::base_url()}}" />
        <input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
        
        <!-- js includes -->
        @if(Session::has('b2c_admin'.Options::server()))
        <script type="text/javascript" src="{{Options::domain()}}js/tinymce2/tinymce.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
        <script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
        @endif
        
        <script src="{{Options::domain()}}js/slick.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.min.js"></script>

        <script src="{{Options::domain()}}js/jquery.fancybox.pack.js"></script>
        @if(Session::has('b2c_admin'.Options::server()))
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        @endif
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
        @if(Options::header_type()==1)
        <!-- fixed navgation bar -->
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
        @endif
      </body>
</html>