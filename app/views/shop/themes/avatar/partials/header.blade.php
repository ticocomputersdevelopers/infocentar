@include('shop/themes/'.Support::theme_path().'partials/menu_top')

<header> 

 
  
      <div id="fixed_header" class="main-header"> 

        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 header-sub-div">

          


            <!-- LOGO AREA -->
            <div class="logo-div">
               <h1 class="for-seo"> 
                  <a class="logo" href="/" title="{{Options::company_name()}}">
                      <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                  </a>
               </h1>
             </div>

            <div class="resp-nav-btn">
              <i class="fas fa-bars"></i>
            </div>
           
          <!-- SEARCH BUTTON -->
            <div class="search-div-content">
              <div class="search-div row">
                <div class="col-md-4 col-sm-4 col-xs-4 no-padding"> 
                  {{ Groups::firstGropusSelect('2') }} 
                </div>

                <div class="JSsearchContent2 col-md-8 col-sm-8 col-xs-4 no-padding">
                    <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch2" placeholder="Pretraga" />            
                </div>
                
                <button onclick="search2()" class="JSsearch-button2"> <i class="fa fa-search"></i></button>
              </div>
            
              <div class="most-searched-div">
                <span>Najtraženije:</span>

                  <ul>     
                     @foreach(Articles::mostPopularArticlesSearch() as $row)
                        <li>
                          <a href="{{Options::base_url()}}artikal/{{ Url_mod::url_convert($row->naziv) }}">{{$row->naziv}}</a>
                        </li>
                      @endforeach
                  </ul> 
              </div>             

            </div> 
         

        <!-- CART AREA -->   
        @include('shop/themes/'.Support::theme_path().'partials/cart_top')
         
        </div> <!-- row 1 end-->
      </div>
     </div>
    </div>
 

  <div class="JSktgDarken"></div> <!-- categories hover black bgcolor-->
  <div class="menu-background">  <!-- row 2 -->
       <div class="container"> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div id="responsive-nav" >     
   
                    @if(Options::category_view()==1)
                        @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                    @endif   

                    <!-- PAGES IN MENU -->
                    <div class="pages-in-header-div">
                        <ul id="main-menu" class="pages-in-header">

                            <?php $first=DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                            @foreach($first as $row)
                                @if(All::broj_cerki($row->web_b2c_seo_id) > 0) 
                                  <li class="dropdown-nav">
                                    <div class="dropdown_div">
                                      <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">
                                          {{$row->title}}
                                      </a>
                                      <i class="fa fa-chevron-down JSDropdown-scroll"></i>
                                    </div>
                                      <?php $second=DB::table('web_b2c_seo')->where(array('parrent_id'=>$row->web_b2c_seo_id,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                                      <ul class="dropdown-ul">
                                        @foreach($second as $row2)
                                            <li> 
                                                <a href="{{ Options::base_url().Url_mod::url_convert($row2->naziv_stranice) }}">{{$row2->title}}</a>
                                            </li>
                                        @endforeach
                                      </ul>

                                       @else  

                                      <li>
                                          <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">
                                            {{$row->title}}
                                          </a>
                                      </li>
                                  </li>                     
                                @endif                    
                            @endforeach

                        <!-- <li>
                          <a href="#!">
                            <span><i class="fas fa-box"></i></span>
                            Free Shipping on Orders $100
                          </a>
                        </li> -->
                        </ul>   
                    </div> 
                    
                    <div class="decoration-div">
                      
                    </div>

                </div>

            </div>
         </div>
     </div>    

      <div class="body-overlay">
          
      </div>
   </div> <!-- row 2 end -->

   
 </header>
 

<!-- Login modal -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content" >

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <i class="fas fa-times"></i>
                </button>

                <div class="modal-heading">
                  <div>
                      {{ Language::trans('Dobrodošli') }}
                  </div>

                  <div>
                    {{ Language::trans('Za pristup Vašem nalogu unesite Vaš E-mail i lozinku') }}.
                  </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="error red-dot-error" id="JSLoginErrorMessage"></div>
                <div class="error red-dot-error" id="JSForgotSuccess"></div>

                <div class="frm-control">
                     <label for="JSemail_login">E-mail</label>
                     <input placeholder="E-mail adresa" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>

                <div class="frm-control">
                    <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                    <input placeholder="{{ Language::trans('Lozinka') }}" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>

            </div>

            <div class="modal-footer text-right">
                  
                  <div class="button-div">
                    <button type="submit" onclick="user_forgot_password()" class="comment-btn">
                       {{ Language::trans('Zaboravljena lozinka') }}
                    </button>
                    <a href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}" class="comment-btn">
                        {{ Language::trans('Registruj se') }}
                    </a>
                     <button type="submit" id="login" onclick="user_login()" class="comment-btn">
                        {{ Language::trans('Prijavi se') }}
                    </button>
                  </div>
            </div>

        </div>   
    </div>
</div>
 


  <!-- Modal for fompared articles -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              &times;
          </button>
            <h4 class="modal-title text-center">
              {{ Language::trans('Upoređeni artikli') }}
          </h4>
          </div>

          <div class="modal-body">
          <div class="compare-section">
            <div id="compare-article">
              <div id="JScompareTable" class="compare-table text-center table-responsive"></div>
            </div>
          </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="close-me-btn" data-dismiss="modal">
              {{ Language::trans('Zatvori') }}
          </button>
          </div>

      </div>    
    </div>
  </div>


  <!-- Quick view modal -->
  <div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

          <div class="modal-header">
            <button type="button" id="JSQuickViewCloseButton" class="close">
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="modal-body" id="JSQuickViewContent">
            <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
          </div>

      </div>    
    </div>
  </div>