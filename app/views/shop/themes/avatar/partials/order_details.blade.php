 
<div class="container">
    <div class="order-page bw">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="page-breadcrumb">
                    <ul>
                        <li>
                            <a href="pocetna">
                                {{ Language::trans('Početna / ') }}
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                {{ Language::trans('Kontakt') }}
                            </a>
                        </li>
                    </ul>
                </div>

                <h2 class="page-heading">
                    {{ Language::trans('Informacije o narudžbini') }}
                </h2>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="order-content">
                    
                     @if(!is_null($bank_result))
                        @if($bank_result->realized == 1)
                        <h3 class="order-heading">
                            {{ Language::trans('Račun platne kartice je zadužen') }}.
                        </h3>
                        @else
                        <h3 class="order-heading">
                            {{ Language::trans('Plaćanje nije uspešno, račun platne kartice nije zadužen. Najčešći uzrok je pogrešno unet broj kartice, datum isteka ili sigurnosni kod, pokušaje ponovo, u slučaju uzastopnih greški pozovite vašu banku') }}.
                        </h3>
                        @endif
                    @else
                        <h3 class="order-heading">
                            {{ Language::trans('Uspešno ste izvršili kupovinu') }} 
                        </h3>
                    @endif

                
                    <div class="table-responsive test">
                        <table>  
                            <th colspan="2">{{ Language::trans('Informacije o narudžbini') }}:</th>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Broj porudžbine') }}:</td>
                                <td>{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Datum porudžbine') }}:</td>
                                <td>{{Order::datum_porudzbine($web_b2c_narudzbina_id)}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Način isporuke') }}:</td>
                                <td>{{Order::n_i($web_b2c_narudzbina_id)}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Način plaćanja') }}:</td>
                                <td>{{Order::n_p($web_b2c_narudzbina_id)}}</td> 
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Napomena') }}:</td>
                                <td>{{Order::napomena_nar($web_b2c_narudzbina_id)}}</td> 
                            </tr>
                        </table>

                        <table>
                            <th colspan="2">{{ Language::trans('Informacije o kupcu') }}:</th>
                            <tr>
                            @if($kupac->flag_vrsta_kupca == 0)
                                <td class="td-desc">{{ Language::trans('Ime i Prezime') }}:</td>
                                <td>{{ $kupac->ime.' '.$kupac->prezime }}</td>
                            @else
                                <td class="td-desc">{{ Language::trans('Firma i PIB') }}:</td>
                                <td>{{ $kupac->naziv.' '.$kupac->pib }}</td>
                            @endif
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Adresa') }}:</td>
                                <td>{{ $kupac->adresa }}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Mesto') }}:</td>
                                <td>{{ Order::mesto_narudzbina($kupac->mesto) }}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Telefon') }}:</td>
                                <td>{{ $kupac->telefon }}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">Email:</td>
                                <td>{{ $kupac->email }}</td>
                            </tr>
                        </table>


                        <table>
                            <th colspan="2">{{ Language::trans('Informacije o prodavcu') }}:</th>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Naziv prodavca') }}:</td>
                                <td>{{Options::company_name()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Adresa') }}:</td>
                                <td>{{Options::company_adress()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Telefon') }}:</td>
                                <td>{{Options::company_phone()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Fax') }}:</td>
                                <td>{{Options::company_fax()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('PIB') }}:</td>
                                <td>{{Options::company_pib()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Šifra delatnosti') }}:</td>
                                <td>{{Options::company_delatnost_sifra()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">{{ Language::trans('Žiro račun') }}:</td>
                                <td>{{Options::company_ziro()}}</td>
                            </tr>
                            <tr>
                                <td class="td-desc">E-mail:</td>
                                <td>{{Options::company_email()}}</td>
                            </tr>
                        </table>

                            
                        @if(!is_null($bank_result))
                            <table>
                                <tr>
                                    <th colspan="2">{{ Language::trans('Informacije o transakciji') }}:</th>
                                </tr>
                                
                                <tr>
                                    <td>{{ Language::trans('Broj narudžbine') }}</td>
                                    <td> 
                                        {{ isset($bank_result->oid) ? $bank_result->oid : '' }}
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{ Language::trans('Autorizacioni kod') }}</td>
                                    <td> 
                                        {{ isset($bank_result->auth_code) ? $bank_result->auth_code : '' }}
                                    </td>
                                </tr>

                            
                                <tr>
                                    <td>{{ Language::trans('Status transakcije') }}</td>
                                    <td> 
                                        {{ isset($bank_result->response) ? $bank_result->response : '' }}
                                    </td>
                                </tr>

                                
                                <tr>
                                    <td>{{ Language::trans('Kod statusa transakcije') }}</td>
                                    <td> 
                                        {{ isset($bank_result->result_code) ? $bank_result->result_code : '' }}
                                    <td> 
                                </tr>

                                <tr>
                                    <td>{{ Language::trans('Broj transakcije') }}</td>
                                    <td> 
                                        {{ isset($bank_result->trans_id) ? $bank_result->trans_id : '' }}
                                    </td>
                                </tr>

                                <tr>
                                    <td>{{ Language::trans('Datum transakcije') }}</td>
                                    <td> 
                                        {{ isset($bank_result->post_date) ? $bank_result->post_date : '' }}
                                    <td> 
                                </tr>

                                <tr>
                                    <td>{{ Language::trans('Statusni kod 3D trans akcije') }}</td>
                                    <td> 
                                        {{ isset($bank_result->md_status) ? $bank_result->md_status : '' }}
                                    </td>
                                </tr>
                            </table>
                        @endif    
         
                        <table>
                            <tr>
                                <th colspan="5">{{ Language::trans('Informacije o narucenim proizvodima') }}:</th>
                            </tr>

                            <tr>
                                <td class="cell-product-name">{{ Language::trans('Naziv proizvoda') }}:</td>
                                <td class="cell">{{ Language::trans('Cena') }} :</td>
                                <td class="cell">{{ Language::trans('Količina') }}</td>
                                <td class="cell">{{ Language::trans('Ukupna cena') }}:</td>
                            </tr>


                            @foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row)
                            <tr>
                                <td class="cell-product-name">{{ Product::short_title($row->roba_id) }} {{ Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}</td>
                                <td class="cell">{{ Cart::cena($row->jm_cena) }}</td>
                                <td class="cell">{{ (int)$row->kolicina }}</td>
                                <td class="cell">{{ Cart::cena(($row->kolicina*$row->jm_cena)) }}</td>
                            </tr>
                            @endforeach
                         
                            @if(Options::checkTezina() == 1 AND Order::troskovi_isporuke($web_b2c_narudzbina_id)>0)
                            <tr>
                                <td colspan="4" class="summary">
                                    {{ Language::trans('Cena artikala') }}: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}}
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="summary">
                                    {{ Language::trans('Troškovi isporuke') }}: {{Cart::cena(Order::troskovi_isporuke($web_b2c_narudzbina_id))}}
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="summary">
                                    {{ Language::trans('Ukupno') }}: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+Order::troskovi_isporuke($web_b2c_narudzbina_id))}}
                                </td>
                            </tr>
                             @elseif(Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Order::cena_isporuke()>0)
                            <tr>
                                <td colspan="4" class="summary">{{ Language::trans('Cena artikala') }}: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="summary">{{ Language::trans('Troškovi isporuke') }}: {{Cart::cena(Order::cena_isporuke($web_b2c_narudzbina_id))}}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="summary">{{ Language::trans('Ukupno') }}: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+Order::cena_isporuke())}}</td>
                            </tr>
                            @else
                            <tr>
                                <td colspan="4" class="summary">
                                    {{ Language::trans('Ukupno') }}: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}}
                                </td>
                            </tr>
                            @endif
                            
                        </table>
                    </div>
                
                </div> <!-- End of order-content -->
            </div> <!-- End of columns -->
        </div>  <!-- End of row -->
    </div> <!-- End of container -->
</div> <!-- End of order page -->