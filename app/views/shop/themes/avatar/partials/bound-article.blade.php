@if(Options::web_options(118))

<div class="bound-section bw">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h2 class="section-heading">
                {{Language::trans('Vezani artikli')}}
            </h2>
        </div>
         
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding JSproduct-slider">
                 
                        @foreach($vezani_artikli as $row)
                            @if(Product::checkView($row->roba_id))

                            <div class="JSproduct col-md-3 col-sm-3 col-xs-12">
                                <div class="shop-product-card"> 

                                    <div class="multiple-function-div">
                                            @if(Cart::kupac_id() > 0)
                                                <div>
                                                    <button data-roba_id="{{$row->vezani_roba_id}}" data-toggle="tooltip" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
                                                        <i class="far fa-heart"></i>
                                                    </button>
                                                </div> 
                                            @else
                                                <div>
                                                    <button data-roba_id="{{$row->vezani_roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list JSnot-wish">
                                                        <i class="far fa-heart"></i>
                                                    </button>
                                                </div>  
                                            @endif  

                                        <div>
                                            <span class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
                                                <i title="{{ Language::trans('Uporedi') }}" class="fas fa-exchange-alt"></i>
                                            </span>
                                        </div>

                                        <div>
                                            <span type="button" class="JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <!-- SALE PRICE -->
                                    @if(All::provera_akcija($row->roba_id))
                                        <div class="sale-label">
                                            {{ Language::trans('Akcija') }}
                                            <!-- <span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} din</span> -->
                                        </div>
                                    @endif


                                    <div class="product-image-wrapper">
                                    <!-- PRODUCT IMAGE -->
                                        <a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">

                                            <img class="product-image img-responsive" 
                                            src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />

                                            <img class="second-product-image" src="{{ Options::domain() }}{{ Product::web_slika_second($row->vezani_roba_id) }}" >

                                        </a>
                                    </div>



                                    <div class="product-meta">

                                        <!-- PRODUCT TITLE -->
                                        <div>
                                            <a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                                                <h2> {{ Product::short_title($row->vezani_roba_id) }} </h2>
                                            </a>
                                        </div>

                                        <div class="price-holder">
                                            <span class="product-price"> {{ Cart::cena(Product::get_price($row->vezani_roba_id)) }} </span>
                                            @if(All::provera_akcija($row->roba_id))
                                            <span class="product-old-price">{{ Cart::cena(Product::old_price($row->vezani_roba_id)) }}</span>
                                            @endif     
                                        </div>   

                                        <div class="text-center"> 
                                             <span class="review"> 
                                                {{ Product::getRating($row->roba_id) }}
                                            </span>
                                        </div>
                <!-- 
                                        <a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                                        <img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />
                                        </a> -->
                                       
                                    

                                    
                                        @if($row->flag_cena == 1)
                                            <div class="add-to-cart-container">           
                                                @if(AdminSupport::getStatusArticle($row->roba_id) == 1)
                                                    @if(Cart::check_avaliable($row->vezani_roba_id) > 0)
                                                        <button class="JSadd-to-cart-vezani buy-btn" data-roba_id="{{ $row->vezani_roba_id }}">
                                                            {{Language::trans('Dodaj u korpu')}}
                                                        </button>

                                                        <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->

                                                    @else  
                                                        <button class="dodavnje not-available buy-btn">{{Language::trans('Nije dostupno')}}</button>
                                                    @endif

                                                    @else
                                                        <div class="dodavnje not-available buy-btn">
                                                            {{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($row->roba_id),'naziv') }}
                                                        </div>
                                                  @endif
                                            </div> <!-- End of add-to-cart-container -->
                                        @endif
                                    </div>

                        
                                    @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                                        <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->vezani_roba_id}}" href="#">
                                            {{Language::trans('IZMENI ARTIKAL')}}
                                        </a>
                                    @endif
                               
                            </div> <!-- End of JSproduct card -->

                         </div> <!-- End of JSproduct -->
                            @endif
                        @endforeach
         
           

       </div> <!-- End of col -->
    </div> <!-- End of row --><br>
</div>
@endif