   



    <div class="modal-img-wrapper">
        <img src="{{ Options::base_url().$image }}" >
  
        @if(All::provera_akcija($roba_id))
          <div class="sale-label">
            {{ Language::trans('Akcija') }}
          </div>
        @endif
    </div>

    <div class="modal-description">

        <h2 class="article-heading" itemprop="name">
           {{ Language::trans(Product::seo_title($roba_id)) }}
        </h2>

      <div class="short-desc">    
          @if(!is_null($proizvodjac) AND $proizvodjac->proizvodjac_id > 0)
          <p class="manufacturer-div">
            <span class="manufacturer-span">
              {{Language::trans('Proizvođač')}}: 
            </span>
            <span class="manufacturer-name">
              {{ Product::get_proizvodjac($roba_id) }}
            </span>
          </p>
          @endif

          <p>
            {{ Product::get_karakteristike_short_grupe($roba_id) }}
          </p>
      </div>

      <div class="modal-price">
          <span class="price-name">
            {{Language::trans('Cena')}}: 
          </span>

          <span class="price-amount"> 
            {{ Cart::cena(Product::get_price($roba_id)) }} 
          </span>

          @if(All::provera_akcija($roba_id))
            <span class="old-price">
              {{ Cart::cena(Product::old_price($roba_id)) }}
            </span> 
          @endif     
      </div>


      <div class="add-to-cart-area"> 

          <div class="button-container">

              @if(AdminSupport::getStatusArticle($roba_id) == 1) 
                @if(Cart::check_avaliable($roba_id) > 0)

                <input type="number" name="kolicina" id="JSQuickViewAmount" class="modal-amount" min="1" value="1">

                <button data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="JSAddToCartQuickView comment-btn">
                    {{ Language::trans('Dodaj u korpu') }}       
                </button>           

                @else    
                  <!-- Not available -->
                  <button class="not-available comment-btn" title="Nije dostupno"> 
                     Nije dostupno
                  </button>       
                @endif

                @else  
                <!-- Not available -->  
                <button class="not-available comment-btn">
                    {{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($roba_id),'naziv') }}  
                </button>      

              @endif

              <div class="like-it">
                <a href="#!" data-roba_id="{{$roba_id}}" title="Dodaj na listu želja" class="{{ Cart::kupac_id() > 0 ? 'JSadd-to-wish' : 'JSnot-wish' }}">
                  <i class="far fa-heart"></i>
                </a>
              </div> 

          </div>
    
      </div> 


  </div>
