@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
	<div class="container">
		<div class="contact-page bw">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="{{ Options::domain() }}">
								{{ Language::trans('Početna / ') }}
							</a>
						</li>
						<li>
							<a href="#!">
								{{ Language::trans('Kontakt') }}
							</a>
						</li>
					</ul>
				</div>
 
				<h2 class="page-heading">
					{{ Language::trans('Kontaktirajte nas') }}
				</h2>
			</div>
		</div>


		<div class="row contact-details">

				<div class="contact-form col-md-6 col-sm-12 col-xs-12">

					<h3 class="contact-heading">{{ Language::trans('Da li imate pitanja?') }}</h3> 

						<div class="frm-control col-md-6 no-padding">
							<label id="label_name">{{ Language::trans('Vaše ime') }} <span class="label-st">*</span></label>

							<input class="contact-name" id="JSkontakt-name" type="text" onchange="check_fileds('JSkontakt-name')" placeholder="Vaše ime*" >
						</div>

						<div class="frm-control col-md-6 no-padding">
							<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} <span class="label-st">*</span></label>

							<input class="contact-email" id="JSkontakt-email" onchange="check_fileds('JSkontakt-email')" type="text" placeholder="Vaša e-mail adresa*" >
						</div>		

						<div class="frm-control textarea-div col-md-12 col-sm-12 col-xs-12 no-padding">	
							<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>

							<textarea class="contact-message" rows="5" id="message" placeholder="Vaša poruka*"></textarea>
						</div>

					<div> 
						<button class="atractive-comment-btn" onclick="meil_send()">
							{{ Language::trans('Pošalji poruku') }}
						</button>
					</div>

				</div>

				<div class="contact-info col-md-6 col-sm-12 col-xs-12">

					<div class="col-md-8 no-padding">
					 	<h3 class="contact-heading">{{ Language::trans('Kontakt') }}</h3>
					 	@if(Options::company_name() != '')
							<div class="more-info-div">
								<span>{{ Language::trans('Firma') }} :</span>
								<span>{{ Options::company_name() }}</span>
							</div> 
						@endif
						@if(Options::company_adress() != '')
							<div class="more-info-div">
								<span>{{ Language::trans('Adresa') }} :</span>
								<span>{{ Options::company_adress() }}</span>
							</div>
						@endif
						@if(Options::company_city() != '')
							<div class="more-info-div">
								<span>{{ Language::trans('Grad') }} :</span>
								<span>{{ Options::company_city() }}</span>
							</div>
						@endif
						@if(Options::company_phone() != '')
							<div class="more-info-div">
						 		<span>{{ Language::trans('Telefon') }} :</span>
						 		<span>{{ Options::company_phone() }}</span>
						 	</div>
					 	@endif
					 	@if(Options::company_fax() != '')
						 	<div class="more-info-div">
						 		<span>{{ Language::trans('Fax') }} :</span>
						 		<span>{{ Options::company_fax() }}</span>
						 	</div>
					 	@endif
					 	@if(Options::company_email() != '')
							<div class="more-info-div">
								<span>E-mail :</span>
								<span><a href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a></span>
							</div>
						@endif
						@if(Options::company_pib() != '')
							<div class="more-info-div">
						 		<span>{{ Language::trans('PIB') }} :</span>
						 		<span>{{ Options::company_pib() }}</span>
						 	</div>
					 	@endif
					 	@if(Options::company_maticni() != '')
							<div class="more-info-div">
						 		<span>{{ Language::trans('Matični broj') }} :</span>
						 		<span>{{ Options::company_maticni() }}</span>
					 		</div>
					 	@endif
					</div>

				</div>
	
			</div>

			
			@if(Options::company_map() != '' && Options::company_map() != ';')
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">				
						<div id="bounce_map_canvas" class="map"></div>
					</div>			
				</div>
			@endif
		</div>
	
</div>


@endsection     