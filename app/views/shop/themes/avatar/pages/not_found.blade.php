@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="not-found-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="pocetna">
								Početna /
							</a>
						</li>
						<li>
							<a href="#!">
								Strana 404
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Greška') }}
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div>
					<p class="login-title">
						{{ Language::trans('Žao nam je, stranica nije dostupna') }}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection