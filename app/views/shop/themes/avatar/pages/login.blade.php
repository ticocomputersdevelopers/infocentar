@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

 
    <div class="container">
        <div class="login-page bw">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="page-breadcrumb">
                    <ul>
                        <li>
                            <a href="/pocetna">
                                Početna /
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                Moj nalog
                            </a>
                        </li>
                    </ul>
                </div>

                <h2 class="page-heading">
                    {{ Language::trans('Moj nalog') }}
                </h2>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div>
                    <p class="login-title"> 
                        {{ Language::trans('Prijavite se') }}
                    </p>
                </div>

                <form action="{{ Options::base_url()}}login-post" method="post" class="login-form" autocomplete="off">

                    <div class="frm-control">
                         <label for="email_login">
                             {{ Language::trans('E-mail') }} <span class="label-star">*</span>
                         </label>

                        <input id="JSemailLogin" class="login-form__input" placeholder="E-mail adresa" id="email_login" name="email_login" type="text" value="{{ Input::old('email_login') ? Input::old('email_login') : Input::old('email_fg') }}" autocomplete="off">

                        <div id="JSEmailError" class="error-message">
                            {{ $errors->first('email_login') ? $errors->first('email_login') : $errors->first('email_fg') }}
                        </div>
                    </div>

                    <div class="frm-control">
                        <label for="lozinka_login">
                            {{ Language::trans('Lozinka login') }} <span class="label-star">*</span>
                        </label>

                        <input class="login-form__input" placeholder="Lozinka" autocomplete="off" id="lozinka_login" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') }}">

                        <div id="JSPasswordlError" class="error-message">
                            {{ $errors->first('lozinka_login') }}
                        </div>
                    </div>
 
                    <div class="ForgotPassBtn">
                        <button type="submit" class="atractive-comment-btn">
                            {{ Language::trans('Prijavi se') }}
                        </button>

                        <div id="JSForgotPassword">
                            Zaboravili ste lozinku?
                        </div>
                    </div>
            
                </form>
                
                <div class="fusnote">
                    <span class="label-star">*</span>
                    Obavezna polja
                </div>
            </div>

        </div>
    </div>
</div>

@endsection



