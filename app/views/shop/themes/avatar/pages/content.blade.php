@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

 
<div class="container">
	<div class="new-content bw">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="{{ Options::domain() }}">
								{{ Language::trans('Početna / ') }}
							</a>
						</li>
						<li>
							<a href="#!">
								{{ $naziv }}
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ $naziv }}
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="content-div">
					{{ $content}}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection