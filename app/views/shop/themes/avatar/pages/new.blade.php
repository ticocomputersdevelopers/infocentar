
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

 
<div class="container">
	<div class="single-news-section bw">
		@include('shop/themes/'.Support::theme_path().'partials/news-breadcrumb')

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<article class="single-news">
			
					<!-- <a class="single-news-category" href="#!">
						Business
					</a> -->
						
					<div class="create-news-date">
						{{ Support::date_convert($datum) }}
					</div>

					<img class="img-responsive single-news-img" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
				
					{{ $sadrzaj }} 

					<!-- <div class="like-share-div">
						<a href="#!" class="like">
							<i class="far fa-heart"></i>
							10
						</a>
					
						<a href="#!" class="comment">
							<i class="far fa-comments"></i>
							0
						</a>
					</div> -->

					<div class="share-news-div">
						<span>Podelite ovaj post</span>
						<a href="https://www.facebook.com/sharer.php?u={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}" target="_blank" title="Facebook share">
							Facebook,
						</a>						
			
						<a href="https://plus.google.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}" title="Google Plus share" onclick="javascript:window.open(this.href,
					  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					  		Google+,
					  	</a>

						<a href="https://twitter.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Twitter share">
							Twitter,
						</a>

						<a href="https://www.linkedin.com/shareArticle?mini=true&url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Linkedin Share">
							Linkedin,
						</a>	

						<a href="https://www.pinterest.com/share?url={{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($id) }}"  title="Linkedin Share">
							Pinterest
						</a>		
					</div>

					<div class="next-prev-news">
						@if(isset($previous_vest))
						<div class="prev-div">
							<i class="fas fa-chevron-left"></i>
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($previous_vest->web_vest_b2c_id) }}">{{ $previous_vest->naslov }}</a>
						</div>
						@endif
						@if(isset($next_vest))
						<div class="next-div">	
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($next_vest->web_vest_b2c_id) }}">{{ $next_vest->naslov }}</a>
							<i class="fas fa-chevron-right"></i>
						</div>
						@endif
					</div>		
				</article> 
			</div>
		</div>
	
		<div class="row">
			<div class="more-post-div col-md-9 col-sm-12">
				<h2 class="more-post-heading">Još postova</h2> 
				
				<div class="single-news-slider row">
					@foreach(All::getShortListNews($id) as $new)
					<div class="more-post-content col-md-4 col-sm-6 col-xs-12">
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}" style="background-image: url('{{ Options::domain() }}{{ $new->slika }}');">
							
						 </a>
						 <a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}" class="more-post-title">
							{{ $new->naslov }}
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
			
<!-- 		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="leave-news-comment">
					<h2>Ostavite komentar</h2>
					<span>Vaša e-mail adresa neće biti javno prikazana</span>
					<form class="news-form">
						<div class="frm-control comment">
							<textarea placeholder="Ostavite komentar"></textarea>
						</div>
						
						<div class="frm-control author-name">
							<input type="name" name="" placeholder="Vaše ime*">
						</div>

						<div class="frm-control author-mail">
							<input type="e-mail" name="" placeholder="Vaše prezime*">
						</div>

						<div>
							<button class="comment-btn">Ostavite komentar</button>
						</div>
					</form>
				</div>
			</div>
		</div> -->
		

	</div>
</div>


<script type="text/javascript">

$(document).ready(function () {
	$('.single-news-slider').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  autoplay: true,
	  responsive: [
		{
		  breakpoint: 1100,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
		  }
		},
		{
		  breakpoint: 800,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 580,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	]
	});
});
</script>
 
@endsection