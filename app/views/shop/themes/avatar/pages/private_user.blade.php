@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row reg-page-padding">
	<div class="col-md-5 col-sm-12 col-xs-12">
		<div> 
			<h3 class="registracija-fizickog-lica">{{ Language::trans('Registracija za fizička lica') }}</h3>
		  	<span class="whats-registration"> {{ Language::trans('Prednosti registracije') }}</span>
		</div>
		<div> 
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Brza kupovina') }}.</span><br>
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Mogućnost skupljanja poena i ostvarivanja povremenih popusta') }}. </span><br>
			 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Istorija porudžbina') }}.</span><br>
	    </div>
	</div>	

<div class="col-md-5 col-sm-12 col-xs-12"> 
<form action="{{ Options::base_url() }}registracija-post" method="post" class="registration-form" autocomplete="off">
<input type="hidden" name="flag_vrsta_kupca" value="0"> 

	<div class="row form-group">
		<label for="ime">{{ Language::trans('Ime') }}</label>
		<input id="ime" class="form-control" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
		</div>
	</div>
 
 	<div class="row form-group">
		<label for="prezime">{{ Language::trans('Prezime') }}</label>
		<input id="prezime" class="form-control" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
	</div>

	<div class="row form-group">
		<label for="email">E-mail</label>
		<input id="email" class="form-control" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
		</div>
	</div>	

	<div class="row form-group">
		<label for="lozinka">{{ Language::trans('Lozinka') }}</label>
		<input id="lozinka" class="form-control" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
		<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
	</div>


	<div class="row form-group">
		<label for="telefon">{{ Language::trans('Telefon') }}</label>
		<input id="telefon" class="form-control" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
 	</div>

	<div class="row form-group">
		<label for="adresa">{{ Language::trans('Adresa') }}</label>
		<input id="adresa" class="form-control" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
	</div>

	<div class="row form-group">
		<label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label>
		<input id="mesto" type="text" class="form-control" name="mesto" />
		<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
	</div>

	<div class="row">
		<div class="text-right">   
			<button type="submit" class="login-form-button admin-login">{{ Language::trans('Registruj se') }}</button>
		</div>
	</div>

 </form>
	@if(Session::get('message'))
		<div class="row"> 
			<div class="success bg-success">{{ Language::trans('Potvrda registracije je poslata na vašu e-mail adresu') }}!</div>
	 	</div>
	@endif
 </div>
</div>
 
@endsection

 