<!DOCTYPE html>
<html lang="sr">
	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
	</head> 
	<body id="product-page" 
    @if($strana == All::get_page_start()) id="start-page"  @endif 
    @if(Support::getBGimg() != null) 
        style="background-image: url({{Options::base_url()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif
	> 
	 
		<!-- PREHEADER -->
		@include('shop/themes/'.Support::theme_path().'partials/menu_top')
		<!-- HEADER -->
		@include('shop/themes/'.Support::theme_path().'partials/header')
			
			
		@if(Options::category_view()==0 AND $strana != 'proizvodjac' AND $strana != 'akcija' AND $strana != 'tip')
		@include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
		@endif		
		<!-- MIDDLE AREA -->
	 
            <div id="middle-area" class="container"> 
              <div class="row bw">  <!-- BREADCRUMBS -->
				@if($strana!='akcija' and $strana!='tip')
				    <ul class="breadcrumb">
						@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
						{{ Url_mod::breadcrumbs2()}}
						@elseif($strana == 'proizvodjac')
						<li>
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('brendovi') }}">Brendovi</a>  
							@if($grupa)
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac') }}/{{ Url_mod::url_convert($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
						    {{ Groups::get_grupa_title($grupa) }}
						</li>
							@else
							{{ All::get_manofacture_name($proizvodjac_id) }} 
							@endif
						@else					
						<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
						<li>{{$title}}</li>
						@endif
				    </ul>
				  @endif  
				 
            <!-- SIDEBAR LEFT -->	 
  			 <aside id="sidebar-left" class="col-md-3 col-sm-12 col-xs-12 no-padding">  
          
        	<!-- FILTERI -->
				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif

			 	@if(Options::enable_filters()==1 AND $filter_prikazi)
					@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif
  
             	 &nbsp; <!-- FOR EMPTY SPACE NBSP-->
	        </aside>

	        <!-- MAIN CONTENT -->
 		 		<div id="main-content" class="col-md-9 col-sm-12 col-xs-12 product-page">
					   	@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
							@if(isset($sub_cats) and !empty($sub_cats))
							<div class="row group-parent-div sub_category_div">
								@foreach($sub_cats as $row)
								<a href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::url_convert($row->grupa) }}/0/0/0-0">
									<div class="col-md-3 col-sm-3 col-xs-12 sub_category_item sm-text-center">
										@if(isset($row->putanja_slika))
										<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class="col-md-3 col-sm-3 col-xs-3 img-responsive" />
										<p class="col-md-8 col-sm-8 col-xs-8 text-center">
											{{ $row->grupa }}
										</p>
										@else
										<img src="{{ Options::domain() }}images/no-image.jpg" alt="{{ $row->grupa }}" class="col-md-3 col-sm-3 col-xs-3 img-responsive"/>
										<p class="col-md-8 col-sm-8 col-xs-8 text-center">
											{{ $row->grupa }}
										</p>
										@endif
									</div>
								</a>
								@endforeach
							</div>
							@endif
						@endif
		 
		            @yield('products_list')
				</div>
		  </div>
		</div>
       
		
		<!-- FOOTER -->
		@include('shop/themes/'.Support::theme_path().'partials/footer')
 
 	<a class="JSscroll-top" href="javascript:void(0)"><i class="fa fa-long-arrow-up"></i></a>

	<!-- LOGIN POPUP -->
	@include('shop/themes/'.Support::theme_path().'partials/popups')

	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
	<input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
	
	<!-- js includes -->
	@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	@if(Session::has('b2c_admin'.Options::server()))
	<script type="text/javascript" src="{{Options::domain()}}js/tinymce2/tinymce.min.js"></script>
	<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
	@endif


	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

  	@if(Options::header_type()==1)
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif
</body>
</html>