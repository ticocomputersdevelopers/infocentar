<div class="row">    
    <div class="quick-view-wrapper col-md-6 col-sm-6 col-xs-12">
        <img class="img-responsive" src="{{ Options::base_url().$image }}" alt="articlal-image">
        @if(All::provera_akcija($roba_id))
            <div class="label label-red sale-label"> - {{ Product::getSale($roba_id) }} din</div>
        @endif
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <h2 class="article-heading" itemprop="name">{{ Language::trans(Product::seo_title($roba_id)) }}</h2>
        <ul>    
            @if(!is_null($proizvodjac) AND $proizvodjac->proizvodjac_id > 0)
            <li>{{Language::trans('Proizvođač')}}: {{ Product::get_proizvodjac($roba_id) }}</li>
            @endif
            <li>{{ Product::get_karakteristike_short_grupe($roba_id) }}</li> 
            <li class="quick-view-price"> {{Language::trans('Cena')}}: {{ Cart::cena(Product::get_price($roba_id)) }} </li>
            @if(All::provera_akcija($roba_id))
            <li><del> {{ Cart::cena(Product::old_price($roba_id)) }} </del></li> 
            @endif     
        </ul>
        <div class="add-to-cart-area"> 
            @if(AdminSupport::getStatusArticle($roba_id) == 1) 
                @if(Cart::check_avaliable($roba_id) > 0)
                <input type="number" name="kolicina" autocomplete="off" id="JSQuickViewAmount" class="modal-amount cart-amount" min="1" value="1">
                <button data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="JSAddToCartQuickView add-to-cart-artikal">
                    {{ Language::trans('Dodaj u korpu') }}       
                </button>           
                @else    
                <button class="not-available" title="Nije dostupno">Nije dostupno</button>       
                @endif
            @else   
            <button class="not-available">{{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($roba_id),'naziv') }}</button>      
            @endif
          
            @if(Cart::kupac_id() > 0)
            <button data-roba_id="{{$roba_id}}" class="JSadd-to-wish wish-list" title="Dodaj na listu želja">
                <i class="fa fa-heart-o"></i>
            </button> 
            @else
            <button data-roba_id="{{$roba_id}}" class="wish-list JSnot_logged" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima">
                <i class="fa fa-heart-o"></i>
            </button> 
            @endif                
        </div>
    </div>
</div>