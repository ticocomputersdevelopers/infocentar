<div class="row"> 
    @if(All::broj_akcije() > 0)
        <div id="JSshift_right">
         
            <div class="h2-container"> 
                <h2 class="section-heading"><span class="heading-background JSInlineShort" data-target='{"action":"sale","id":"34"}'>{{Language::trans(Support::title_akcija())}}</span></h2>
            </div>
        
            <div class="sale-list JSproduct-list row slider JSslide-articles relative">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-akcija">
                    <div class="loader"></div>
                </div>

                <button class="previous btn-slider slider-prev" id="JSakcija-previous" data-id="akcija" data-offset="1">
                    <span class="fa fa-long-arrow-left"></span>
                </button>

                <button class="next btn-slider slider-next" id="JSakcija-next" data-id="akcija" data-offset="1">
                    <span class="fa fa-long-arrow-right"></span>
                </button>

                <div class="ajax-content" id="JSakcija-content" data-tipid="akcija">
                    <!-- Product ajax content -->
                </div>
            </div>
        </div>
    @endif
    
    <?php $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('active',1)->orderBy('rbr','asc')->get(); ?>

    @if(count($tipovi) > 0) 
    @foreach($tipovi as $tip)
        @if(All::provera_tipa($tip->tip_artikla_id))
        
            <div class="h2-container"> 
                <h2 class="section-heading"><span class="heading-background JSInlineShort" data-target='{"action":"type","id":"{{$tip->tip_artikla_id}}"}'>{{ Language::trans($tip->naziv) }}</span></h2>
            </div>
       
            <div class="tip-list JSproduct-list row slider JSslide-articles relative">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-{{ $tip->tip_artikla_id }}">
                    <div class="loader"></div>
                </div>

                <button class="previous btn-slider slider-prev" id="JS{{ $tip->tip_artikla_id.'-' }}previous" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                    <span class="fa fa-long-arrow-left"></span>
                </button>
                
                <button class="next btn-slider slider-next" id="JS{{ $tip->tip_artikla_id.'-' }}next" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                      <span class="fa fa-long-arrow-right"></span>
                </button>

                <div class="ajax-content" id="JS{{ $tip->tip_artikla_id.'-' }}content" data-tipid="{{ $tip->tip_artikla_id }}">
                    <!-- Product ajax content -->
                </div>
            </div>
        @endif
    @endforeach
    @endif
</div>