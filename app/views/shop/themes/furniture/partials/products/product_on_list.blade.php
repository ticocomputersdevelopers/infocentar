<div class="shop-product-card-list col-md-12 col-sm-12 col-xs-12"> 
	 <!-- SALE PRICE -->
	@if(All::provera_akcija($row->roba_id))
		<span class="ribbon-sale"><span> {{ Language::trans('Akcija') }} </span></span>
	@endif
    <div class="row"> 

	<div class="col-md-3 col-sm-3 col-xs-12 product-image-wrapper">
		<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="">
		    <img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
	</div> 

	<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="row"> 
			<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				{{ Product::short_title($row->roba_id) }}
			</a>
		</div> 

		<div class="row"> 
			<span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
			@if(All::provera_akcija($row->roba_id))
			<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
			@endif
			<div> 
				{{ Product::getRating($row->roba_id) }}
			</div>
		</div>

		 <div class="add-to-cart-container"> 
        <!--  <div class="">Šifra artikla: {{ Product::get_sifra($row->roba_id) }}</div> -->
<!-- ADD TO CART BUTTON -->
			@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
		  		<div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
			 		 {{ Language::trans('Dodaj u korpu') }}			 
			    </div>	
<!-- COMPARE BUTTON -->
  				@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
					<div class="JScompare compare-me {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<i class="fa fa-exchange" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
					</div>
				@endif	
			 @else 		 
       		    <div class="dodavnje not-available buy-btn" title="{{ Language::trans('Nije dostupno') }}"> {{ Language::trans('Nije dostupno') }}</div>
       			 @if(Options::compare()==1)
				<div class="JScompare compare-me {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
					<i class="fa fa-exchange" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
				</div>
				@endif		 
		  @endif
			 @else	 
				<div class="dodavanje not-available buy-btn" title="{{ Language::trans('Nije dostupno') }}">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</div>
			 	@if(Options::compare()==1)
				<div class="JScompare compare-me {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
					<i class="fa fa-exchange" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
				</div>
				@endif		
			 @endif

   <!-- WISH LIST  -->	
			@if(Cart::kupac_id() > 0)
				<button data-roba_id="{{$row->roba_id}}" class="JSadd-to-wish wish-list">
					<i title="Dodaj na listu želja" class="fa fa-heart-o"></i>
				</button>
			@else
				<button data-roba_id="{{$row->roba_id}}" class="wish-list JSnot_logged">
					<i title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="fa fa-heart-o"></i>
				</button>
			@endif 
	 		<button class="quick-view JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
				<i title="Brzi pregled" class="glyphicon glyphicon-fullscreen"></i>
			</button> 
		</div>
	</div>
 
<!-- OPIS KARAKTERISTIKE -->
	<!-- <div class="col-md-5  ">
		{{ Product::get_karakteristike_short_grupe($row->roba_id) }}
	</div>  -->
 
  		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif
   </div>
</div>
    