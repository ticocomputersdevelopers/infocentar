<nav id="JScategories" class="category-nav relative inline-block">

	<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
    <h3 class="JScategories-titile relative text-center">{{ Language::trans('Kategorije')  }} <i class="fa fa-bars"></i><span class="glyphicon glyphicon-remove JSclose-nav"></span></h3> 
	 
	<!-- CATEGORIES LEVEL 1 -->

    <ul class="JSlevel-1" id="the_categories">
    @if(Options::category_type()==1) 
		@foreach ($query_category_first as $row1)
			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

			<li class="level-1-list">
				<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="">
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="level-1-img-container hidden-sm hidden-xs"> 
						<img class="level-1-img" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}"  />
				 	</span>
					@endif
					{{ Language::trans($row1->grupa)  }} 
				</a>
				 
				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="JSlevel-2 row">
			 		@foreach ($query_category_second->get() as $row2)
					<li class="col-md-4 col-sm-4 col-xs-12">  
						@if(Groups::check_image_grupa($row2->putanja_slika))
							<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" class="level-2-container-for-image hidden-sm hidden-xs col-md-2 col-sm-2 col-xs-12">
								<img class="level-2-img" src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
							</a>
						@endif
					 
						<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" 
						class="col-md-10 col-sm-10 col-xs-12 lvl-2-cat">
							{{ Language::trans($row2->grupa) }} 
						</a>
					 	@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
						<span class=""> 
							<i class="" aria-hidden="true"></i>
						</span>
						@endif  	
					 

						@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
						<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
						<ul class="JSlevel-3 row">
							@foreach($query_category_third as $row3)
							
							<li class="col-md-12 col-sm-12 col-xs-12">						 
								<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0" class="col-md-12 col-sm-12 col-xs-12">
									{{ Language::trans($row3->grupa) }}
								</a>

								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend">
									<i class="fa fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif  
 
								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
											
								<ul class="JSlevel-4 row">
									@foreach($query_category_forth as $row4)
									<li class="col-md-12 col-sm-12 col-xs-12">
										<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0" class="">{{ Language::trans($row4->grupa) }}</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
				</ul>
			</li>

			@else

			<li class="level-1-list">		 
				<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="">		 
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="level-1-img-container hidden-sm hidden-xs"> 
						<img class="level-1-img" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }}  				
				</a>			 
			</li>
			@endif
		@endforeach
	@else
		@foreach ($query_category_first as $row1)
			
			    
		@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
		  	<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">
				{{ Language::trans($row1->grupa)  }}
			</a>
		<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

		 

		@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0">
					{{ Language::trans($row2->grupa) }}
				</a>
			@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
			<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul class="">
				@foreach($query_category_third as $row3)
				  <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0">{{ Language::trans($row3->grupa) }}
				  </a>
				@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
				<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

				<ul class="">
					@foreach($query_category_forth as $row4)
					<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ Language::trans($row4->grupa) }}</a>
					@endforeach
				</ul>
				@endif	
				@endforeach
				</ul>
			@endif
			</li>
		@endforeach

		@else
			<li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">{{ Language::trans($row1->grupa) }}</a>
		@endif

		</li>
		@endforeach				
	@endif

   @if(Options::all_category()==1)
	<li>
		<a class="" href="{{ Options::base_url() }}{{ Url_mod::convert_url('sve-kategorije') }}"><i class=""></i>{{ Language::trans('Sve kategorije') }}</a>
	</li>
	@endif
 
</ul>
 
</nav>
