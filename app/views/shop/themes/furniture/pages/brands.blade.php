@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
	<div class="row">
	@foreach($brendovi as $brend)
	    <span class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
	        <a class="brend-item" href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac')}}/{{ Url_mod::url_convert($brend->naziv) }}">
	            @if($brend->slika != null OR $brend->slika != '')
	            <img class="img-responsive" src="{{ Options::domain() }}{{ $brend->slika }}" alt="{{ Language::trans($brend->naziv) }}" />
	            @else
	            {{ Language::trans($brend->naziv) }}
	            @endif
	        </a>
	    </span>	 
	@endforeach
	</div>
@endsection