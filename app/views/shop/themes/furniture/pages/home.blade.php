@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    @include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection 

@section('page')
    @include('shop/themes/'.Support::theme_path().'partials/products/action_type')
    @if(Options::web_options(136)==0)
        @if(Options::web_options(208)==1)
            <!-- ARTICLES AT FRONT PAGE -->
            <div class="h2-container">
                 <h2><span class="heading-background JSInlineShort" data-target='{"action":"front_admin_label","id":"1"}'>{{ Language::trans(Support::front_admin_label(1)) }}</span></h2>
            </div>
            <div class="JSMostPopularProducts JSproduct-slider row">
                @foreach(Articles::mostPopularArticles() as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
        
            @if(count(Articles::bestSeller()))
            <div class="h2-container">
               <h2><span class="heading-background JSInlineShort" data-target='{"action":"front_admin_label","id":"2"}'>{{ Language::trans(Support::front_admin_label(2)) }}</span></h2>
            </div>
            <div class="JSBestSellerProducts JSproduct-slider row">
                @foreach(Articles::bestSeller(4) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
            @endif
            <div class="h2-container">
                 <h2><span class="heading-background JSInlineShort" data-target='{"action":"front_admin_label","id":"3"}'>{{ Language::trans(Support::front_admin_label(3)) }}</span></h2>
            </div>
            <div class="JSMostPopularProducts JSproduct-slider row">
                @foreach(Articles::latestAdded() as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
        @endif
    @else
        <div class="h2-container">
            <h2><span class="heading-background JSInlineShort" data-target='{"action":"home_all_articles"}'>{{ Language::trans(Support::title_all_articles()) }}</span></h2>
        </div> 
        <div class="JSproduct-slider"> 
            @foreach($articles as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </div>
         
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            {{ Paginator::make($articles, $count_products, $limit)->links() }}
        </div>
    @endif


    <script type="text/javascript">
        $(document).ready(function(){
            @if(Session::has('login_success'))
            $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
            $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>{{ Session::get('login_success') }}</p>");
            @endif
    
            @if(Session::has('registration_success'))
            $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
            $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>{{ Session::get('registration_success') }}</p>");
            @endif

            @if(Session::has('loggout_succes'))
            $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
            $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>{{ Session::get('loggout_succes') }}</p>");
        </script>
            @endif

            @if(Session::has('confirm_registration_message'))
            $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
            $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>{{ Session::get('confirm_registration_message') }}</p>");
            @endif
        });
    </script>

@endsection