@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding">
	<div class="col-md-12 col-sm-12 col-xs-12 register-page text-center"> 
		<a href="{{ Options::base_url()}}{{ Url_mod::convert_url('fizicko_lice') }}">{{ Language::trans('Fizička lica') }}</a>
		<a href="{{ Options::base_url()}}{{ Url_mod::convert_url('pravno_lice') }}">{{ Language::trans('Pravna lica') }}</a>
	</div>
</div>  
@endsection 