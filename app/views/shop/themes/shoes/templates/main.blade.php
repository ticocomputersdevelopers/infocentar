<!DOCTYPE html>
<html lang="sr">

	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
		
		<script type="application/ld+json">
			{  
				"@context" : "http://schema.org",
				"@type" : "WebSite",
				"name" : "<?php echo Options::company_name(); ?>",
				"alternateName" : "<?php echo $title." | ".Options::company_name(); ?>",
				"url" : "<?php echo Options::domain(); ?>"
			}
		</script>
	</head>
<body> 
       
		<!-- Header -->
		@include('shop/themes/'.Support::theme_path().'partials/admin-menu')

		@if($strana != All::get_page_start())

			@include('shop/themes/'.Support::theme_path().'partials/header')

		@endif

 	  	
		@yield('baners_sliders')
     	  	  

        @yield('page')
            
        <!-- Footer -->
	    @include('shop/themes/'.Support::theme_path().'partials/footer')
	        
	  
	<a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>

	<!-- Login popup -->
	@include('shop/themes/'.Support::theme_path().'partials/popups')

	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
	<input type="hidden" id="admin_id" value="{{Session::has('b2c_admin'.Options::server()) ? Session::get('b2c_admin'.Options::server()) : ''}}" />
	<input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
	<input type="hidden" id="lat" value="{{ All::lat_long()[0] }}" />
	<input type="hidden" id="long" value="{{ All::lat_long()[1] }}" />
	

	<!-- js includes -->
	@if(Session::has('b2c_admin'.Options::server()))
	<script type="text/javascript" src="{{Options::domain()}}js/tinymce2/tinymce.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
	@endif
	
	@if(Session::has('b2c_admin'.Options::server()))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	@if($strana == All::get_page_start())
	<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />

	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}action_type.js"></script>
	<script src="{{Options::domain()}}js/slick.min.js" ></script>
	@endif
	 
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>  

    @if($strana==Seo::get_korpa())
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}order.js"></script>
    @endif
    <script src="{{Options::domain()}}js/shop/login_registration.js"></script>

	@if($strana=='konfigurator' AND Options::web_options(121))
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}configurator.js"></script>
	@endif

	@if($strana==Seo::get_kontakt())
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf05l1NhASLAtI_hy9k4Z2EowvME7JyoM&callback=initMap"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}/bounce_map_initalization.js"></script>
	@endif
	
	@if(Options::header_type()==1) 
 		<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif
</body>
</html>
