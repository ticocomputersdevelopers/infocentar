@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="contact-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"><h2 class="title">{{ Language::trans('Kontaktirajte nas') }}</h2></div>
		</div>

		<div class="row">
			<div class="contact-form col-md-6 col-sm-12 col-xs-12">
				<h3 class="contact-heading">{{ Language::trans('Da li imate pitanja?') }}</h3> 
				<div class="frm-control col-md-6 no-padding">
					<label id="label_name">{{ Language::trans('Vaše ime') }}</label>
					<input class="contact-name" id="JSkontakt-name" type="text" onchange="check_fileds('JSkontakt-name')" placeholder="Vaše ime*" >
				</div>
				<div class="frm-control col-md-6 no-padding">
					<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }}</label>
					<input class="contact-email" id="JSkontakt-email" onchange="check_fileds('JSkontakt-email')" type="text" placeholder="Vaša e-mail adresa*" >
				</div>		
				<div class="frm-control textarea-div col-md-12 no-padding">	
					<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
					<textarea class="contact-message" rows="5" id="message" placeholder="Vaša poruka*"></textarea>
				</div>
				<div> 
					<button class="orange-btn" onclick="meil_send()">{{ Language::trans('Pošalji poruku') }}</button>
				</div>
			</div>
			<div class="contact-info col-md-6 col-sm-12 col-xs-12">
				<div class="row">	
					<div class="contact-info col-md-12 col-sm-12 col-xs-12 no-padding contact-info-div">
						<h3 class="contact-heading">{{ Language::trans('Kontakt') }}</h3>
						@if(Options::company_name() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Firma') }} :</span>
								<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_name() }}</span>
							</div> 
						@endif
						@if(Options::company_adress() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Adresa') }} :</span>
								<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_adress() }}</span>
							</div>
						@endif
						@if(Options::company_city() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Grad') }} :</span>
								<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_city() }}</span>
							</div>
						@endif
						@if(Options::company_phone() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Telefon') }} :</span>
								<span class="col-md-9 col-md-9 col-xs-9">{{ Options::company_phone() }}</span>
							</div>
						@endif
						@if(Options::company_fax() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Fax') }} :</span>
								<span class="col-md-9 col-md-9 col-xs-9">{{ Options::company_fax() }}</span>
							</div>
						@endif
						@if(Options::company_email() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">E-mail :</span>
								<span class="col-md-9 col-sm-9 col-xs-9"><a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a></span>
							</div>
						@endif
						@if(Options::company_pib() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('PIB') }} :</span>
								<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_pib() }}</span>
							</div>
						@endif
						@if(Options::company_maticni() != '')
							<div class="more-info-div row">
								<span class="col-md-3 col-sm-3 col-xs-3">{{ Language::trans('Matični broj') }} :</span>
								<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_maticni() }}</span>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		
		@if(Options::company_map() != '' && Options::company_map() != ';') 
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12"><div id="bounce_map_canvas" class="map"></div></div>		
			</div>
		@endif
	</div>
</div>
@endsection     