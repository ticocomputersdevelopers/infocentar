
<?php // echo $kara; ?>

<div class="filters">
	
		<div class="all-articles">
		    <a href="{{ Options::base_url()}}{{Url_mod::url_convert('svi-artikli')}}" class="JSfilters-slide-toggle">
		    	{{ Language::trans('Svi artikli') }}
			</a>				 				 
		</div>		
		@if(Options::compare()==1)
		<div>
			<a type="button" id="JScompareArticles"  data-toggle="modal" data-target="#compared-articles" href="#!">
                {{ Language::trans('Upoređeni artikli') }}
            </a>
		</div>
		@endif
	
	<!-- Checked filters -->
	<div class="selected-filters-wrapper">

	    <div class="selected-filters-title">
		    Izabrani filteri:
		</div> 

		<ul class="selected-filters">
			<?php $br=0;
			foreach($niz_proiz as $row){
				$br+=1;
				if($row>0){
			?>
				<li>
				    {{All::get_manofacture_name($row)}}     
				    <a href="javascript:void(0)">
				        <i class="fa fa-close JSfilter-close" data-type="1" data-rb="{{$br}}" data-element="{{$row}}" ></i>
				    </a>
				</li>
			<?php }}

			$br=0;
			foreach($niz_karakteristike as $row){
				$br+=1;
				if($row>0){
			?>
				<li>
					{{All::get_fitures_name($row)}}
					<a href="javascript:void(0)">
				        <i class="fa fa-close JSfilter-close" data-type="2" data-rb="{{$br}}" data-element="{{$row}}" ></i>
				    </a>
				</li>
				<?php }}?>
		</ul>

		<input type="hidden" id="JSniz_proiz" value="{{$proizvodjac_ids}}"/>
		<input type="hidden" id="JSniz_kara" value="{{$kara}}"/>
	</div>	
	 
       
    <div class="filters-button-div">
	    <a class="JSreset-filters-button" role="button" href="javascript:void(0)">Poništi filtere</a>
    </div>

	<!-- End checked -->
	<ul class="">
		<!-- Filter item -->
		@if(count($manufacturers)>0)
		<li class="filter-box">
		    <a href="javascript:void(0)" class="filter-links JSfilters-slide-toggle">Proizvođač
		    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
			   <i class="fa fa-angle-down"></i>
			</a>

	 	 	<div class="JSfilters-slide-toggle-content">
				@foreach($manufacturers as $key => $value)
				@if(in_array($key,$niz_proiz))

					<label>
						<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>

						<span class="filter-text">
							{{All::get_manofacture_name($key)}} 
						</span>

						<span class="filter-count">
							@if(Options::filters_type()) {{ $value }} @endif
						</span>
					</label>

				@else
					<label>
						<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">

						<span class="filter-text">
							{{All::get_manofacture_name($key)}} 
						</span>

						<span class="filter-count">
							@if(Options::filters_type()) {{ $value }} @endif
						</span>
					</label>
				@endif
				@endforeach
			</div>				 				 
		</li>
		@endif

	@foreach($characteristics as $keys => $values)

		<li class="filter-box">
				<a  href="javascript:void(0)" class="filter-links JSfilters-slide-toggle">
					{{Language::trans($keys)}} 
					<!-- <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
					<i class="fa fa-angle-down"></i>  							
				</a>

			<div class="JSfilters-slide-toggle-content">
				@foreach($values as $key => $value)
				<?php if(in_array($key, $niz_karakteristike)){ ?>

				<label>
					<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>

					<span class="filter-text">
						{{All::get_fitures_name($key)}}
					</span>

					<span class="filter-count">
						@if(Options::filters_type()) {{ $value }} @endif
					</span>
				</label>

				<?php }else {?>

				<label>
					<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 

					<span class="filter-text">
						{{All::get_fitures_name($key)}}
					</span>

					<span class="filter-count">
						@if(Options::filters_type()) {{ $value }} @endif
					</span>
				</label>

				<?php } ?>
				@endforeach
			</div>
		</li>
	@endforeach			
	</ul>

	<input type="hidden" id="JSfilters-url" value="{{$url}}" />
 
	<!-- Slajder za cenu -->
	<div class="filter-price-div"> 
		<span id="JSamount" class="filter-price"></span>
		<div id="JSslider-range"></div>
	</div>

	<script>
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>
</div>

 
 