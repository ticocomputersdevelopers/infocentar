@if(Cart::broj_cart()>0)
<ul>
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<li>
	    <div class="header-cart-image-wrapper">
	    	<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
		   		<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
		    </a>
		</div>

		<div class="header-cart-content">
			<a class="name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				{{Product::short_title($row->roba_id)}}
				<div>
					{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}
				</div>
			</a>
		    <span class="header-cart-amount">{{round($row->kolicina)}}</span>
			<span class="multiplication-icon">x</span>
		    <span class="header-cart-price">{{Cart::cena($row->jm_cena)}}</span>
			
			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="JSDeleteStavka exit">
				<i class="fas fa-times"></i>
			</a>			
		</div>

	</li>
	@endforeach	
	<div class="toCart-div">
		<a href="{{Options::base_url()}}korpa">
			Vidi korpu
			<i class="fas fa-chevron-right"></i>
		</a>		
	</div>
</ul>

@endif
