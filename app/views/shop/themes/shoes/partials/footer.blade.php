<footer>
	<div class="footer-content">
		<div class="container">
			<div class="row"> 
 

				@foreach(All::footer_sections() as $footer_section)
					@if($footer_section->naziv == 'slika')
					<div class="col-md-3 col-sm-3 col-xs-12 min-height-div">				
						<div class="footer-desc-div first">
							@if(!is_null($footer_section->slika))
							<a class="logo" href="{{ $footer_section->link }}">
								 <img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
							</a>
							@else
							<a class="logo" href="/" title="{{Options::company_name()}}">
								 <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
							</a>
							@endif

							<p class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->sadrzaj }} 
							</p>
						</div>
					</div>
					@elseif($footer_section->naziv == 'text')
					<div class="col-md-3 col-sm-3 col-xs-12 min-height-div">
								
						<div class="footer-desc-div">
							<h2 class="footer-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->naslov }}
							</h2>

							<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->sadrzaj }} 
							</div>
						</div>
					</div>
					@elseif($footer_section->naziv == 'linkovi')
					<div class="col-md-3 col-sm-3 col-xs-12 min-height-div">

						<h2 class="footer-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->naslov }}
						</h2>

						<ul class="footer-links">
							@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
								<li>
									<a href="{{ Options::base_url().Url_mod::url_convert($page->naziv_stranice) }}">{{ Language::trans($page->title) }}</a>
								</li>
							@endforeach
						</ul>  
					</div>
					@elseif($footer_section->naziv == 'kontakt')
					<div class="col-md-3 col-sm-3 col-xs-12 contact-div min-height-div"> 
						<h2 class="footer-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->naslov }}
						</h2>

						@if(Options::company_adress() OR Options::company_city())
							<span>
								<i class="fas fa-map-marker-alt"></i>
								{{ Options::company_adress() }}, <br>
							 	{{ Options::company_city() }}
							</span>
						@endif
						@if(Options::company_phone())
							<span>
								<i class="fas fa-phone-volume"></i>
								<a href="tel:{{ Options::company_phone() }}">
									{{ Options::company_phone() }}
								</a>
							</span>
						@endif
						@if(Options::company_email())
							<span>
								<i class="far fa-envelope-open"></i>
								<a class="mailto" href="mailto:{{ Options::company_email() }}">
									{{ Options::company_email() }}
								</a>
							</span>
						@endif
					</div>

					@elseif($footer_section->naziv == 'drustvene_mreze')
					<div class="col-md-3 col-sm-3 col-xs-12 min-height-div">
						<div class="social-icons">
					    	<h2 class="footer-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
						    	{{ $footer_section->naslov }}
						    </h2>
					 		{{Options::social_icon()}}
					 	</div>
					</div>						
					@endif

				@endforeach

			</div> 
		</div>
	</div>

	<div class="below-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p>{{ Options::company_name() }} &copy; {{ date('Y') }}. Sva prava su zadržana. - <a href="https://www.selltico.com/">Izrada internet prodavnice</a> - 
						<a href="https://www.selltico.com/"> Selltico. </a>
					</p>
				</div>
			</div>
		</div>
	</div>
    @if(Support::banca_intesa())
	<div class="after-footer"> 
		<div class="container"> 
			<div class="row banks">
		 		<div class="col-md-6 col-sm-6 col-xs-12">
		 			<ul class="list-inline sm-text-center">
		 				<li>
		 					<a href="https://www.bancaintesa.rs" target="_blank">
		 						<img alt="bank-logo" src="{{ Options::domain() }}images/cards/banca-intesa.png">
			 				</a>
			 			</li> 
		 				<li>
		 					<a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank">
		 						<img alt="bank-logo" src="{{ Options::domain() }}images/cards/verified-by-visa.jpg">
			 				</a>
			 			</li>
		 				<li>
		 					<a href="http://www.mastercard.com/rs/consumer/credit-cards.html" target="_blank">
			 					<img alt="bank-logo"src="{{ Options::domain() }}images/cards/master-card-secure.gif">
			 				</a>
			 			</li>
		 			</ul>
		 		</div>	
		 		<div class="col-md-6 col-sm-6 col-xs-12">
		 			<ul class="list-inline sm-text-center text-right">
		 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/visa-card.png"></li> 
		 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/american-express.png"></li>
		 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/master-card.png"></li>
		 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/maestro-card.png"></li>
		 			</ul>
		 		</div>
		 	</div>
		</div>
	</div>
	@endif
</footer>
  