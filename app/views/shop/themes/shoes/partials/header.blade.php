

<header id="fixed_header"> 
 
    <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-6">
            <div>
               <a class="logo" href="/" title="{{Options::company_name()}}">
                    <img class='img-responsive' src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
                </a>
            </div>
          </div>

          <div class="col-md-10 col-sm-9 col-xs-6 text-right">
            <ul class="site-navigation">

          
              <span class="exit">
                  <i class="fas fa-times"></i>
              </span>


              <?php $first=DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>
 

              @foreach($first as $row)
                  @if(All::broj_cerki($row->web_b2c_seo_id) > 0) 
                    <li class="dropdown-nav">
                        <div class="dropdown_div">
                            <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">
                                {{$row->title}}
                            </a>
                            <i class="fa fa-chevron-down JSDropdown-scroll"></i>
                        </div>
                        <?php $second=DB::table('web_b2c_seo')->where(array('parrent_id'=>$row->web_b2c_seo_id,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                        <ul class="dropdown-ul">
                          @foreach($second as $row2)
                              <li> 
                                  <a href="{{ Options::base_url().Url_mod::url_convert($row2->naziv_stranice) }}">{{$row2->title}}</a>
                              </li>
                          @endforeach
                        </ul>

                         @else  

                        <li>
                            <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">
                              {{$row->title}}
                            </a>
                        </li>
                    </li>                     
                  @endif
              @endforeach

              @if(Options::checkB2B())
                  <li>
                      <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="confirm">B2B</a>
                  </li> 
              @endif 
 

              <li class="dropdown-nav">
                  <div class="dropdown_div">
                      <a href="#!">
                          {{ Language::trans('Korisnik') }}
                      </a>
                      <i class="fa fa-chevron-down JSDropdown-scroll"></i>
                  </div>

                  <ul class="dropdown-ul">
                      @if(Session::has('b2c_kupac'))
                        <li>
                          <a href="{{Options::base_url()}}{{Url_mod::convert_url('lista-zelja')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                            {{ Language::trans('Lista želja') }}
                          </a>
                        </li>   
                        @if(trim(WebKupac::get_user_name()))
                        <li> 
                            <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                                {{ WebKupac::get_user_name() }}
                            </a>
                        </li>
                        @endif 

                        @if(trim(WebKupac::get_company_name()))
                        <li>
                            <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                            {{ WebKupac::get_company_name() }}
                            </a>     
                        </li>
                        @endif 

                        <li>
                            <a id="logout-button" href="{{Options::base_url()}}logout">
                            {{ Language::trans('Odjavi se') }}
                            </a>
                        </li>

                        @else 

                        <li>
                            <a class="registration-btn" id="login-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('prijava') }}">
                                {{ Language::trans('Prijava') }}
                            </a>
                        </li>
                        <li>
                            <a class="registration-btn" id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}">
                                {{ Language::trans('Registracija') }}
                            </a>
                        </li>
                      @endif  
                  </ul>

              </li>   

              
            </ul>


            <!-- CART AREA -->   
            @include('shop/themes/'.Support::theme_path().'partials/cart_top')

            

            <span id="navigationToggle">
              <i class="fas fa-bars"></i>
            </span>
          </div>
        </div>

        <div class="body-overlay">
          
        </div>
    </div>
   
 </header>
 

<!-- Login modal -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content" >

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <i class="fas fa-times"></i>
                </button>

                <div class="modal-heading">
                  <div>
                      {{ Language::trans('Dobrodošli') }}
                  </div>

                  <div>
                    {{ Language::trans('Za pristup Vašem nalogu unesite Vaš E-mail i lozinku') }}.
                  </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="frm-control">
                     <label for="JSemail_login">E-mail</label>
                     <input placeholder="E-mail adresa" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>

                <div class="frm-control">
                    <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                    <input placeholder="{{ Language::trans('Lozinka') }}" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>

                <div class="error red-dot-error" id="JSLoginErrorMessage"></div>
                <div class="error red-dot-error" id="JSForgotSuccess"></div>

            </div>

            <div class="modal-footer text-right">
                  
                  <div class="button-div">
                    <button type="submit" id="login" onclick="user_login()" class="orange-btn">
                        {{ Language::trans('Prijavi se') }}
                    </button>
                    <a href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}">Registruj se</a>

                    <button type="submit" onclick="user_forgot_password()" class="forgot-psw pull-left">
                       {{ Language::trans('Zaboravljena lozinka') }}
                    </button>
                  </div>
            </div>

        </div>   
    </div>
</div>
 


  <!-- Modal for fompared articles -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <i class="fas fa-times"></i>
          </button>
            <h4 class="modal-title text-center">
              {{ Language::trans('Upoređeni artikli') }}
          </h4>
          </div>

          <div class="modal-body">
          <div class="compare-section">
            <div id="compare-article">
              <div id="JScompareTable" class="compare-table text-center table-responsive"></div>
            </div>
          </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="close-me-btn" data-dismiss="modal">
              {{ Language::trans('Zatvori') }}
          </button>
          </div>

      </div>    
    </div>
  </div>

