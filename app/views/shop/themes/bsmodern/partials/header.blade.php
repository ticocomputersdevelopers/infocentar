<header>   
    <div id="JSfixed_header"> 
      <div class="div-in-header container-fluid sm-text-center"> 

        <!-- LOGO AREA -->
        <div id="top-head-logo" class="col-md-3 col-sm-3 col-xs-12 test">
           <h1 class="for-seo"> 
              <a class="logo inline-block" href="/" title="{{Options::company_name()}}">
                  <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive" style="padding-left: 12%;" />
              </a>
           </h1>
         </div>
         
      <!-- CATEGORIES  -->
        <div class="col-md-1 col-sm-3 col-xs-12" id="top-head-nav">
          <div>     
            @if(Options::category_view()==1)
              <nav id="JScategories" class="category-nav relative inline-block top-nav-cat-2">
                <?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                  <h3 class="JScategories-titile relative"><i class="fas fa-bars"></i><span class="JSclose-nav">&times;</span></h3> 
                 
                <!-- CATEGORIES LEVEL 1 -->

                  <ul class="JSlevel-1 top-nav-lvl-2" id="the_categories">
                  @if(Options::category_type()==1) 
                  @foreach ($query_category_first as $row1)
                    @if(Groups::broj_cerki($row1->grupa_pr_id) >0)

                    <li class="level-1-list">
                      <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="">
                        @if(Groups::check_image_grupa($row1->putanja_slika))
                        <span class="level-1-img-container hidden-sm hidden-xs"> 
                          <img class="level-1-img" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
                        </span>
                        @endif
                        {{ Language::trans($row1->grupa)  }} 
                      </a>
                       
                      <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                      <ul class="JSlevel-2 row">
                        @foreach ($query_category_second->get() as $row2)
                        <li class="col-md-6 col-sm-6 col-xs-12">  
                          @if(Groups::check_image_grupa($row2->putanja_slika))
                            <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" class="level-2-container-for-image hidden-sm hidden-xs col-md-2 col-sm-2 col-xs-12">
                              <img class="level-2-img" src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
                            </a>
                          @endif
                         
                          <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" 
                          class="col-md-10 col-sm-10 col-xs-12">
                            {{ Language::trans($row2->grupa) }} 
                          </a>
                          @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                          <span class=""> 
                            <i class="" aria-hidden="true"></i>
                          </span>
                          @endif    
                         

                          @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                          <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                          <ul class="JSlevel-3 row">
                            @foreach($query_category_third as $row3)
                            
                            <li class="col-md-12 col-sm-12 col-xs-12">             
                              <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0" class="col-md-12 col-sm-12 col-xs-12">
                                {{ Language::trans($row3->grupa) }}
                              </a>

                              @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                              <span class="JSCategoryLinkExpend">
                                <i class="fas fa-chevron-down" aria-hidden="true"></i>
                              </span>
                              @endif  
               
                              @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                              <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                    
                              <ul class="JSlevel-4 row">
                                @foreach($query_category_forth as $row4)
                                <li class="col-md-12 col-sm-12 col-xs-12">
                                  <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0" class="">{{ Language::trans($row4->grupa) }}</a>
                                </li>
                                @endforeach
                              </ul>
                              @endif    
                            </li>           
                            @endforeach
                          </ul>
                          @endif
                        </li>
                        @endforeach
                      </ul>
                    </li>
                   @else

                    <li class="level-1-list">    
                      <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="">     
                        @if(Groups::check_image_grupa($row1->putanja_slika))
                        <span class="level-1-img-container hidden-sm hidden-xs"> 
                          <img class="level-1-img" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
                        </span>
                        @endif
                        {{ Language::trans($row1->grupa)  }}          
                      </a>       
                    </li>
                    @endif
                  @endforeach
                @else
                  @foreach ($query_category_first as $row1)
                    
                        
                  @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                      <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">
                      {{ Language::trans($row1->grupa)  }}
                    </a>
                  <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                   

                  @foreach ($query_category_second->get() as $row2)
                    <li>
                      <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0">
                        {{ Language::trans($row2->grupa) }}
                      </a>
                    @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                    <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                      <ul class="">
                      @foreach($query_category_third as $row3)
                        <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0">{{ Language::trans($row3->grupa) }}
                        </a>
                      @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                      <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

                      <ul class="">
                        @foreach($query_category_forth as $row4)
                        <a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ Language::trans($row4->grupa) }}</a>
                        @endforeach
                      </ul>
                      @endif  
                      @endforeach
                      </ul>
                    @endif
                    </li>
                  @endforeach

                  @else
                    <li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">{{ Language::trans($row1->grupa) }}</a>
                  @endif

                  </li>
                  @endforeach       
                @endif

                 @if(Options::all_category()==1)
                <li>
                  <a class="" href="{{ Options::base_url() }}{{ Url_mod::convert_url('sve-kategorije') }}"><i class=""></i>{{ Language::trans('Sve kategorije') }}</a>
                </li>
                @endif            
              </ul>           
              </nav>
            @endif               
          </div>
        </div>

      <!-- SEARCH BUTTON -->
        <div class="col-md-5 col-sm-7 col-xs-12 header-search">
            <div class="col-md-2 col-sm-3 col-xs-3 no-padding">  
                {{ Groups::firstGropusSelect('2') }} 
            </div> 

      <!-- SEARCH AREA -->
            <div class="JSsearchContent2 col-md-8 col-sm-7 col-xs-7 no-padding">
                <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch2" placeholder="Pretraga" />            
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 no-padding"> 
                <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
            </div>       
        </div>

    <!-- LOGIN AND REGISTRATION -->
        <div class="col-md-3 col-sm-6 col-xs-6 text-center">
            @if(Session::has('b2c_kupac'))
             <span class="JSbroj_wish glyphicon glyphicon-heart" title="Omiljeni artikli"><span> {{ Cart::broj_wish() }} </span></span> 
              <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                  <span>{{ WebKupac::get_user_name() }}</span>
              </a>

              <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                  <span>{{ WebKupac::get_company_name() }}</span>
              </a>
              <a id="logout-button" href="{{Options::base_url()}}logout"><span>{{ Language::trans('Odjavi se') }}</span></a>
              @else 
              <div class="dropdown inline-block">
                  <button class="dropdown-toggle login-btn" type="button" data-toggle="dropdown">
                     <span class="fas fa-sign-in-alt"></span> &nbsp;{{ Language::trans('Moj nalog') }} <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu login-dropdown">
       <!-- ====== LOGIN MODAL TRIGGER ========== -->
                      <li><a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal"><span>
                         <i class="fas fa-user"></i> {{ Language::trans('Prijavi se') }}</span></a>
                      </li>
                      <li><a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}"><span>
                          <i class="fas fa-user-plus"></i> {{ Language::trans('Registracija') }}</span></a>
                       </li>
                  </ul>
              </div> 
            @endif
        </div>
     <!-- CART AREA -->   
         @include('shop/themes/'.Support::theme_path().'partials/cart_top')

        <div class="resp-nav-btn"><span class="fas fa-bars"></span></div>
        
      </div> 
    </div>  
</header>

   <div class="menu-background">   
      <div class="container clearfix"> 
         <div id="responsive-nav" class="resp-nav-class">
        
                @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                @endif               
         
            <nav class="navbar navbar-header inline-block col-md-8 col-lg-9 col-sm-12">
                 <ul id="main-menu" class="nav navbar-nav pages-in-header">
<!-- PAGES IN MENU -->
                <?php $first=DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                @foreach($first as $row)
                  <li class="header-dropdown">
                     @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                      <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">{{$row->title}}</a>
                          <?php $second=DB::table('web_b2c_seo')->where(array('parrent_id'=>$row->web_b2c_seo_id))->orderBy('rb_strane','asc')->get(); ?>

                          <ul class="header-dropdown-content">
                            @foreach($second as $row2)
                              <li> 
                                <a href="{{ Options::base_url().Url_mod::url_convert($row2->naziv_stranice) }}">{{$row2->title}}</a>
                              </li>
                            @endforeach
                          </ul>
                       @else  
                       
                           <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">{{$row->title}}</a>
                     
                     @endif                    
                    </li>                     
                  @endforeach
             
 
                 @if(Options::web_options(121)==1)
                        <?php $konfiguratori = All::getKonfiguratos(); ?>
                        @if(count($konfiguratori) > 0)
                            @if(count($konfiguratori) > 1)
                                <li class="konf-dropdown">
                                    <a href="#!" class="konf-dropbtn">
                                      Konfiguratori
                                    </a>
                                    <ul class="konf-dropdown-content">
                                        @foreach($konfiguratori as $row)
                                        <li>
                                           <a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li>
                                   <a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">
                                      Konfigurator
                                   </a>
                                </li>
                            @endif
                        @endif
                    @endif
              </ul>   
           </nav> 
         </div>
     </div>    
  </div> 
 
 <!-- MODAL ZA LOGIN -->
 
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><span class="welcome-login"><span>{{ Language::trans('Dobrodošli') }}</span>{{ Language::trans('Za pristup Vašem nalogu unesite Vaš E-mail i lozinku INIT') }}.</span></h4>
            </div>
                <div class="modal-body">
                 <div class="form-group">
                     <label for="JSemail_login">E-mail</label>
                     <input class="form-control" placeholder="E-mail adresa" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                    <input class="form-control" placeholder="{{ Language::trans('Lozinka') }}" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>
            </div>

            <div class="modal-footer text-right">
                 <a class="create-acc inline-block text-center" href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}">Registruj se</a>
                 <button type="submit" id="login" onclick="user_login()" class="login-form-button admin-login">{{ Language::trans('Prijavi se') }}</button>
                 <button type="submit" onclick="user_forgot_password()" class="forgot-psw admin-login pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>
                 <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi.') }}
                </div> 
            </div>
        </div>   
    </div>
</div>
 