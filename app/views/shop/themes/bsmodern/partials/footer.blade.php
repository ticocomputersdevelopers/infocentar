 <footer>
  <div class="container sm-text-center">
  	<div class="row"> 
		<div class="col-md-9 col-sm-9 col-xs-12 footer-cols no-padding">
			@foreach(All::footer_sections() as $footer_section)
				@if($footer_section->naziv == 'slika')
					<div class="col-md-4 col-sm-4 col-xs-12">				
						<div class="footer-desc-div">
							@if(!is_null($footer_section->slika))
							<a class="footer-logo" href="{{ $footer_section->link }}">
								 <img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
							</a>
							@else
							<a class="footer-logo" href="/" title="{{Options::company_name()}}">
								 <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
							</a>
							@endif

							<p class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->sadrzaj }} 
							</p>
						</div>
					</div>
				@elseif($footer_section->naziv == 'text')
					<div class="col-md-4 col-sm-4 col-xs-12">
								
						<div class="footer-desc-div">
							<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->naslov }}
							</h5>

							<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
								{{ $footer_section->sadrzaj }} 
							</div>
						</div>
					</div>
				@elseif($footer_section->naziv == 'linkovi')
					<div class="col-md-4 col-sm-4 col-xs-12">

						<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->naslov }}
						</h5>

						<ul class="footer-links">
							@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
								<li>
									<a href="{{ Options::base_url().Url_mod::url_convert($page->naziv_stranice) }}">{{ Language::trans($page->title) }}</a>
								</li>
							@endforeach
						</ul>  
					</div>
				@elseif($footer_section->naziv == 'kontakt')
					<div class="col-md-4 col-sm-4 col-xs-12 contact-div"> 
						<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->naslov }}
						</h5>

						<ul>
						@if(Options::company_adress() OR Options::company_city())
							<li>
								<i class="fas fa-map-marker-alt"></i>
								{{ Options::company_adress() }}, <br>
							 	{{ Options::company_city() }}
							</li>
						@endif
						@if(Options::company_phone())
							<li>
								<a class="mailto" href="tel:{{ Options::company_phone() }}">
									{{ Options::company_phone() }}
								</a>
							</li>
						@endif
						@if(Options::company_phone())
							<li>
								<a class="mailto" href="tel:{{ Options::company_fax() }}">
									{{ Options::company_fax() }}
								</a>
							</li>
						@endif
						@if(Options::company_email())
							<li>
								<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
							</li>
						@endif
						</ul>
					</div>
				@elseif($footer_section->naziv == 'drustvene_mreze')
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="social-icons"> 
					    	<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
						    	{{ $footer_section->naslov }}
						    </h5>
					 		<div class="social-icons">
						  		{{Options::social_icon()}}
						 	</div> 
					 	</div><br>
					</div>						
				@endif
			@endforeach
 		</div>

 		<?php $newslatter_description = All::newslatter_description(); ?>
		@if(Options::newsletter()==1)
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"newslatter_label"}'>
					{{ $newslatter_description->naslov }}
				</h5> 
				<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
					{{ $newslatter_description->sadrzaj }}
				</p>

				<div class="newsletter text-right">				 
	 				<div class="form-group">
						<input type="text" class="form-control" placeholder="E-mail" id="newsletter" />
					</div>
					<button onclick="newsletter()">
						{{ Language::trans('Prijavi se') }}
					</button>
				</div>
		  	</div>
	  	@endif
	</div> 
 	 
 	<br>

	<div class="row text-center foot-note">
		<p>{{ Language::trans('Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.') }}
		</p>
		<p>{{ Options::company_name() }} &copy; {{ date('Y') }}. Sva prava zadržana. - <a href="https://www.selltico.com/">Izrada internet prodavnice</a> - 
			<a href="https://www.selltico.com/"> Selltico. </a>
		</p>
	</div>
 
 </div>
  <a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>
</footer>
@if(Support::banca_intesa())
<div class="after-footer"> 
	<div class="container"> 
		<div class="row banks">
	 		<div class="col-md-6 col-sm-6 col-xs-12">
	 			<ul class="list-inline sm-text-center">
	 				<li>
	 					<a href="https://www.bancaintesa.rs" target="_blank">
	 						<img alt="bank-logo" src="{{ Options::domain() }}images/cards/banca-intesa.png">
		 				</a>
		 			</li> 
	 				<li>
	 					<a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank">
	 						<img alt="bank-logo" src="{{ Options::domain() }}images/cards/verified-by-visa.jpg">
		 				</a>
		 			</li>
	 				<li>
	 					<a href="http://www.mastercard.com/rs/consumer/credit-cards.html" target="_blank">
		 					<img alt="bank-logo"src="{{ Options::domain() }}images/cards/master-card-secure.gif">
		 				</a>
		 			</li>
	 			</ul>
	 		</div>	
	 		<div class="col-md-6 col-sm-6 col-xs-12">
	 			<ul class="list-inline sm-text-center text-right">
	 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/visa-card.png"></li> 
	 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/american-express.png"></li>
	 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/master-card.png"></li>
	 				<li><img alt="bank-logo" src="{{ Options::domain() }}images/cards/maestro-card.png"></li>
	 			</ul>
	 		</div>
	 	</div>
	</div>
</div>
@endif