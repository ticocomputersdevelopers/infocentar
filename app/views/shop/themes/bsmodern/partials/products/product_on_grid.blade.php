<div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding"> 
	<div class="shop-product-card relative"> 
 <!-- SALE PRICE -->
		@if(All::provera_akcija($row->roba_id))
		<div class="label label-red sale-label"><span class="for-sale">{{ Language::trans('Akcija') }}</span><span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} %</span></div>
		@endif
		 <div class="product-image-wrapper relative">
 <!-- PRODUCT IMAGE -->
			<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
			<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="article-details hidden-xs hidden-sm">
				<span class="fas fa-search-plus"></span> {{ Language::trans('Detaljnije') }}</a>
		 </div>
   
	<div class="product-meta"> 
		 <div> 
		 <!-- <div class="text-right">  -->
             <span class="review">
				{{ Product::getRating($row->roba_id) }}
            </span>
         </div>
  <!-- PRODUCT PRICE -->
		<div class="price-holder">
		    <span class="product-price"> {{ Cart::cena(Product::get_price($row->roba_id)) }} </span>
            @if(All::provera_akcija($row->roba_id))
            <span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
            @endif     
        </div>	 
<!-- PRODUCT TITLE -->
		<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			<h2> {{ Product::short_title($row->roba_id) }} </h2>
		</a>
 
	<div class="add-to-cart-container">  
	<!-- WISH LIST  -->	
		@if(Cart::kupac_id() > 0)
			  <div class="like-it"><button data-roba_id="{{$row->roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish far fa-heart wish-list"></button></div> 
		@else
			  <div class="like-it JSnot_logged"><button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="far fa-heart wish-list"></button>
		     </div> 
		@endif							

 <!-- ADD TO CART BUTTON -->
	     	@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)	 
					<div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
						  {{ Language::trans('U korpu') }}			 
			        </div>	
  <!-- COMPARE BUTTON -->		      
			          @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
						<div class="JScompare like-it {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
							<i class="fas fa-exchange-alt" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
						</div>
			   	      @endif				    
				@else  	 <!-- NOT AVAILABLE -->
	                <div class="dodavnje not-available buy-btn" title="Nije dostupno"> Nije dostupno</div>	               	 
	                @if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
		                <div class="JScompare like-it {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
		                	<i class="fas fa-exchange-alt" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
		                </div>
	                @endif	                     
				@endif
				@else  <!-- NOT AVAILABLE -->	 
					<div class="dodavanje not-available buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }} 	</div>	
					@if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
						<div class="JScompare like-it {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Upored') }}i">
							<i class="fas fa-exchange-alt" aria-hidden="true" title="{{ Language::trans('Uporedi') }}"></i>
						</div>
					@endif						 
					 	 	
			  @endif 	
		  </div>             
	 </div>
<!-- ADMIN BUTTON -->
	 @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">{{ Language::trans('IZMENI ARTIKAL') }}</a>
	 @endif
	</div>
</div>
 
               
  
  