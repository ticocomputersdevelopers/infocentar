<nav class="manufacturer-categories"> 
      <h3 class="text-center">{{$title}}</h3>
      <ul class="row">
        @foreach(Support::tip_categories($tip_id) as $key => $value)
            <li class="col-md-12 col-sm-12 col-xs-12">
                <a  class="" href="{{ Options::base_url()}}{{ Url_mod::convert_url('tip') }}/{{ Url_mod::url_convert($tip) }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ Language::trans($key) }}</span>
                    <span class="">{{ $value }}</span>
                </a>
            </li>
        @endforeach 
    </ul>
</nav> 
 