@if(Cart::broj_cart()>0)
<ul> 
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<li>
	    <span class="header-cart-image-wrapper">
		    <img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
		</span>
		<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}
		</a>
	    <span class="header-cart-amount">{{round($row->kolicina)}}</span>
		@if(AdminOptions::web_options(152)==1)
		<span class="header-cart-price">{{Cart::cena(ceil(($row->jm_cena)/10)*10 )}}</span>
		@else
		<span class="header-cart-price">{{Cart::cena($row->jm_cena)}}</span>
		@endif
		<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="close JSDeleteStavka">X</a>
	</li>
	@endforeach	
 </ul>
@if(Options::checkTroskoskovi_isporuke() == 1 AND Cart::cart_ukupno() < Cart::cena_do())
<span class="header-cart-summary">{{ Language::trans('Cena artikala') }}: <span>{{Cart::cena(Cart::cart_ukupno())}}</span></span>
<span class="header-cart-summary">{{ Language::trans('Troškovi isporuke') }}: <span>{{Cart::cena(Cart::cena_dostave())}} </span></span>
<span class="header-cart-summary">{{ Language::trans('Ukupno') }}: <span> {{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}} </span></span>

@else
<span class="header-cart-summary">{{ Language::trans('Ukupno') }}: <span>{{ Cart::cena(Cart::cart_ukupno()) }}</span></span> 
@endif
<div class="text-right"> 
	<a class="inline-block" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">Završi kupovinu</a>
</div>
@endif
 