<div class="col-md-1 col-sm-2 col-xs-3 header-cart-container"> 
	<a class="header-cart relative" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
		<span class="fas fa-shopping-cart cart-me"></span>		
		<span class="JSbroj_cart badge"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div> 