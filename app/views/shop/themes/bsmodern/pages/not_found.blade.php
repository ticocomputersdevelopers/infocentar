@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="container">
	 <div class="h2-container row">
	    <h2><span class="heading-background">{{ Language::trans('Greška') }}</span></h2>
	</div>
    <p class="no-articles">{{ Language::trans('Žao nam je, stranica nije dostupna') }}.</p>
</div>
@endsection