@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding">
	<div class="col-md-5 col-sm-12 col-xs-12 log-in">
		<h3>{{ Language::trans('Registracija za pravna lica') }}</h3><br>
		<span><b>  {{ Language::trans('Prednosti registracije') }} </b></span><br>
		<span>
			<i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Brza kupovina') }}.
		</span><br>
		<span>
			<i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Mogućnost skupljanja poena i ostvarivanja povremenih popusta') }}. 
		</span><br>
		<span>
			<i class="fa fa-angle-double-right"></i>&nbsp; {{ Language::trans('Ostvarivanje partnerskih popusta i mogućnost dobijanja dilerskih cena') }}.
		</span>
	</div>	

<div class="col-md-5 col-sm-12 col-xs-12"> 
<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
	<input type="hidden" name="flag_vrsta_kupca" value="1">

	<div class="row form-group">
		<label for="naziv">{{ Language::trans('Naziv firme') }}:</label>
		<input id="naziv" class="form-control" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
		</div>
	</div>

	<div class="row form-group">
		<label for="pib">{{ Language::trans('PIB') }}</label>
		<input id="pib" class="form-control" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
	</div>	

	<div class="row form-group">
		<label for="email">E-mail</label>
		<input id="email" class="form-control" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
		</div>
	</div>	

	<div class="row form-group">
		<label for="lozinka">{{ Language::trans('Lozinka') }}</label>
		<input id="lozinka" class="form-control" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
		<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
	</div>

	<div class="row form-group">
		<label for="telefon">{{ Language::trans('Telefon') }}</label>
		<input id="telefon" class="form-control" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
	</div>

	<div class="row form-group">
		<label for="adresa">{{ Language::trans('Adresa') }}</label>
		<input id="adresa" class="form-control" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
	</div>

	<div class="row form-group">
		<label for="mesto">{{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>
		<input id="mesto" type="text" class="form-control" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
	</div>
 
	<div class="row">
		<div class="text-right">   
			<button type="submit" class="login-form-button admin-login">{{ Language::trans('Registruj se') }}</button>
		</div>
	</div>
 </form>
 
	@if(Session::get('message'))
 	<script type="text/javascript">
		bootbox.alert({
			message: "<p>{{ Language::trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') }}.</p>"
		});
 	</script>
	@endif
</div>
</div>

@endsection