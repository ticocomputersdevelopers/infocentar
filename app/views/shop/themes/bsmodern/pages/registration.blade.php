@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding">
	<div class="col-md-6 col-sm-6 col-xs-12 overlay-container"> 
		<img width='260' height='260' src="{{ Options::domain() }}images/private-person.png"  class="log-and-reg-image img-responsive center-block" alt="login_and_registration_image">
		<div class="overlay-log-and-reg">
			<div class="text-log-and-reg"><a class="legal-person-link-button inline-block" href="{{ Options::base_url()}}{{ Url_mod::convert_url('fizicko-lice') }}">{{ Language::trans('Fizička lica') }}</a></div>
		</div>
 	</div>

	<div class="col-md-6 col-sm-6 col-xs-12 overlay-container"> 
		<img width='260' height='260' src="{{ Options::domain() }}images/legal-person.png" class="log-and-reg-image img-responsive center-block" alt="login_and_registration_image">
		<div class="overlay-log-and-reg">
			<div class="text-log-and-reg"><a class="legal-person-link-button inline-block" href="{{ Options::base_url()}}{{ Url_mod::convert_url('pravno-lice') }}">{{ Language::trans('Pravna lica') }}</a></div>
		</div>
	</div>
</div>
@endsection 