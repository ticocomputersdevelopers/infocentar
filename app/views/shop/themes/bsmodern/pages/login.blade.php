@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding">
	<div class="col-md-5 login-page-image">
		<img src="{{ Options::domain() }}images/login.jpg" alt="login-image">
 	</div>

	<div class="col-md-4 login-form-wrapper">
		 <form action="{{ Options::base_url()}}login" method="post" class="login-form" autocomplete="off">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<p class="welcome-login"><span>Dobrodošli</span>Za pristup Vašem nalogu unesite E-mail i lozinku.</p>
				</div>
			</div>
			<div class="field-group row">
				<div class="col-md-3"> 
 					<label for="email_login">E-mail</label> 
				</div>

				<div class="col-md-9">
					<?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
					<input class="login-form__input" placeholder="E-mail adresa" name="email_login" type="text" value="{{ $old_mail }}" autocomplete="off">
				</div>
			</div>
			
			<div class="field-group row">
				<div class="col-md-3"> 
					<label for="lozinka_login">Lozinka</label>
				</div>
				<div class="col-md-9"> 
				    <input class="login-form__input" placeholder="Lozinka" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
				</div>
			 </div>

			 <div class="row"> 
			 	<div class="col-md-2"> 
					<button type="submit" class="login-form-button admin-login">Login</button>
				</div>
			</div>
		</form>

		<div class="field-group error-login">
			<div class="row"> 
				<div class="col-md-9 wrong-email"> 
			<?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

			@if(Session::get('confirm'))
				Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
			@endif

			@if(Session::get('message'))
				Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
			@endif
				</div>
			</div>
		</div>

		<div class="row"> 
			<div class="col-md-8"> 
				<form class="forgot_pass" action="{{ Options::base_url()}}zaboravljena-lozinka" method="post" autocomplete="off">
					<div class="error">{{ $errors->first('email_fg') ? $errors->first('email_fg') : "" }}</div>
					<?php if(Input::old('email_fg')){ $old_mail_fg = Input::old('email_login'); }else{ if(Input::old('email_login')){ $old_mail_fg = Input::old('email_login'); }else{$old_mail_fg ='';} } ?>
					<input name="email_fg" type="hidden" value="{{ $old_mail_fg }}" >
					<button class="btn-forgot-pass" type="submit">Zaboravili ste lozinku?</button>
				</form>
			</div>
		</div>
	</div> <!-- end of .login-form-wrapper -->
	<!-- <div class="large-1 large-push-6 form-spacer">&nbsp;</div> -->
</div>
@endsection