@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row">
	<div class="medium-6 large-4 large-offset-1 columns login-form-wrapper">
		<h2><span class="title-for-articles title-for-articles-login">Ulogujte se</span></h2>
		<form action="{{ Options::base_url()}}login-post" method="post" class="login-form login-i-registration" autocomplete="off">
			
			<div class="field-group">
				<label for="email">E-mail</label>
				<?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
				<input class="login-form__input" name="email_login" type="text" value="{{ $old_mail }}" autocomplete="off">
			</div>
			
			<div class="field-group">
				<label for="lozinka">Lozinka</label>
				<input class="login-form__input" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
			</div>
			<button type="submit" class="login-form-button admin-login">Prijavi se</button>
		</form>
		<div class="field-group error-login">
			<?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

			@if(Session::get('confirm'))
				Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
			@endif

			@if(Session::get('message'))
				Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
			@endif
		</div>
		
		<form class="forgot_pass" action="{{ Options::base_url()}}zaboravljena-lozinka" method="post" autocomplete="off">
				<div class="error">{{ $errors->first('email_fg') ? $errors->first('email_fg') : "" }}</div>
				<?php if(Input::old('email_fg')){ $old_mail_fg = Input::old('email_login'); }else{ if(Input::old('email_login')){ $old_mail_fg = Input::old('email_login'); }else{$old_mail_fg ='';} } ?>
				<input name="email_fg" type="hidden" value="{{ $old_mail_fg }}" >
			<button class="btn-forgot-pass" type="submit">Zaboravljena lozinka</button>
		</form>
	</div> <!-- end of .login-form-wrapper -->
	
	<div class="medium-6 large-4 large-push-2 columns end registration-form-wrapper">
		<h2><span class="title-for-articles title-for-articles-login">Registracija</span></h2>
		<form action="{{ Options::base_url()}}registracija-post" method="post"class="registration-form" autocomplete="off">
			<div class="field-group registracija-lica">
				@if(Input::old('flag_vrsta_kupca') == 1)
				<input class="radio" type="radio" name="flag_vrsta_kupca" value="0"><label>Fizičko lice</label>
				<input class="radio" type="radio" name="flag_vrsta_kupca" value="1" checked><label>Pravno lice</label>
				@else
				<input class="radio" type="radio" name="flag_vrsta_kupca" value="0" checked><label>Fizičko lice</label>
				<input class="radio" type="radio" name="flag_vrsta_kupca" value="1"><label>Pravno lice</label>
				@endif
			</div>					
			<div class="field-group">
				<label for="ime">Ime</label>
				<input class="login-form__input" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
				<div class="error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="prezime">Prezime</label>
				<input class="login-form__input" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
				<div class="error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="naziv">Naziv firme</label>
				<input class="login-form__input" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
				<div class="error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="pib">PIB</label>
				<input class="login-form__input" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
				<div class="error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
			</div>		

			<div class="field-group">
				<label for="email">E-mail</label>
				<input class="login-form__input" autocomplete="false" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
				<div class="error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}</div>
			</div>		

			<div class="field-group">
				<label for="lozinka">Lozinka</label>
				<input class="login-form__input" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
				<div class="error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="telefon">Telefon</label>
				<input class="login-form__input" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
				<div class="error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="adresa">Adresa</label>
				<input class="login-form__input" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
				<div class="error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
			</div>

			<div class="field-group">
				<label for="mesto_id">Mesto</label>
				<input class="login-form__input" name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
				<div class="error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
			</div>
			<button type="submit" class="login-form-button admin-login">Registruj se</button>
		</form>
		@if(Session::get('message'))
			<div>Potvrda registracije je poslata na vašu e-mail adresu!</div>
		@endif
	</div> <!-- end of .registration-form-wrapper -->
	<div class="large-1 large-push-6 form-spacer">&nbsp;</div>
</div>
@endsection
