@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

    <!-- MIDDLE AREA -->
    <div class="main-wrapper clearfix">
        <section id="middle-area" class="row">
            <!-- MAIN CONTENT -->
            <section id="main-content" class="columns">
                <!-- PRODUCT -->
                <ul class="breadcrumbs clearfix">
                    {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
                </ul>
                <section id="product-preview" itemscope itemtype="http://schema.org/Product" class="clearfix">
                    <div class="row">
                        <!-- PRODUCT PREVIEW IMAGE -->
                        <section class="JSproduct-preview-image medium-6 columns">
                            <a class="fancybox" rel="gallery_01" href="{{ Options::base_url() }}{{ Product::web_slika_big($roba_id) }}">
                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::base_url() }}{{ Product::web_slika_big($roba_id) }}" >
                                    <img  itemprop="image" class="JSzoom_03" src="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" alt="{{ Product::seo_title($roba_id)}}"/>
                                </span>
                            </a>
                            <div id="gallery_01" class="clearfix">
                                {{ Product::get_list_images($roba_id) }}
                            </div>
                        </section>

                        <section class="product-preview-info medium-6 columns">
                            <h1 class="article-heading" itemprop="name">{{ Product::seo_title($roba_id)}}</h1>
                            @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                                <a class="article-edit-btn article-edit-btn2" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $roba_id }}">IZMENI ARTIKAL</a>
                            @endif
                            <div>
                                {{ Product::getAverageRating($roba_id) }}
                            </div>
                            <ul>
                                <li class="product-group">Proizvod iz grupe:{{ Product::get_grupa($roba_id) }}</li>
                                <li class="product-manufacturer" itemprop="brand">Proizvodjač: 
                                    @if(Support::checkBrand($roba_id))
                                    <a href="{{ Options::base_url() }}proizvodjac/{{ Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                    {{Product::get_proizvodjac($roba_id)}}
                                    </a>
                                    @else
                                    <span><strong>{{Product::get_proizvodjac($roba_id)}}</strong></span>
                                    @endif
                                </li>
                                @if(Options::vodjenje_lagera() == 1)
                                <li class="product-available-amount">Dostupna količina: <a href="#"> {{Cart::check_avaliable($roba_id)}}</a></li>
                                @endif
                                @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                                <li class="product-available-amount">Težina artikla: <a href="#"> {{Product::tezina_proizvoda($roba_id)/1000}} kg</a></li>
                                @endif
                                
                                @if(Options::checkTags() == 1)
                                    @if(Product::tags($roba_id) != '')
                                    <li class="product-available-amount">
                                        Tagovi: {{ Product::tags($roba_id) }}
                                    </li>
                                    @else
                                    <li class="product-available-amount">
                                        Tagovi: Nema tagova
                                    </li>
                                    @endif
                                @endif
                            </ul>
                            <section class="product-preview-price">

                                @if(All::provera_akcija($roba_id))
                                    <span class="product-preview-price-old">{{ Cart::cena(Product::old_price($roba_id)) }}</span>
                                @endif
                                    <span class="product-preview-price-new" itemprop="price">{{ Cart::cena(Product::get_price($roba_id)) }}</span>

                            </section>
                            <section class="facebook-btn-share">
                                <div class="fb-share-button" data-href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($roba_id))}}" data-layout="button"></div>
                            </section>
                            <section class="add-to-cart-area clearfix">
                                @if(Product::getStatusArticle($roba_id) == 1)
                                    @if(Cart::check_avaliable($roba_id) > 0)
                                    <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm">
                                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                                        @if(Product::check_osobine($roba_id))
                                            <div class="osobine">
                                            @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                                <div class="osobina">
                                                    <div class="osobina_naziv">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>
                                                    <div class="karakteristike_osobine">
                                                        @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id)->get() as $row2)
                                                        <label class="osobina_karakteristika" style="background-color: {{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'boja_css') }}">
                                                            <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $row2->osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$row2->osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                                            <span title="{{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'vrednost') }}">{{ Product::osobina_vrednost_checked($osobina_naziv_id, $row2->osobina_vrednost_id) }}</span>
                                                        </label>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                            </div>
                                        @endif

                                        <div class="input_kolicina">Količina</div>
                                        <input type="text" name="kolicina" class="JScart-amount" id="not-colored" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                                        <div style="margin: 10px 0;">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>
                                        <button id="JSAddCartSubmit" class="JSadd-to-cart add-to-cart-artikal"> <i class="fa fa-cart-plus"></i> U korpu</button>
                                    </form>
                                    @else
                                    <button class="dodavanje not-available">NEDOSTUPNO</button>
                                    @endif
                                @else
                                <button class="dodavanje not-available" data-roba-id="{{$roba_id}}">{{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}</button>
                                @endif
                            </section>
                        </section>                      
                    </div>
                    <!-- PRODUCT PREVIEW TABS-->
                    <section class="product-preview-tabs">
                        <ul class="JStab-titles clearfix">
                            <li class="JStab-title-description active">Opis</li>
                            <li class="tab-title-tehnicaldoc">Tehnička dokumentacija</li>
                            <li class="JStab-title-comments">Komentari</li>
                        </ul>
                        <section class="tab-content JSdescription-content active clearfix" >
                            <p itemprop="description"> {{ Product::get_opis($roba_id) }} </p>
                            {{ Product::get_karakteristike($roba_id) }}
                        </section>
                        
                        <section class="tab-content tehnicaldoc-content">
                            @if(Options::web_options(120))
                                @if(count($fajlovi) > 0)
                                <section class="">
                                   <!--  <h2 class="h2-margin"><span class="title-for-articles">Dodatni fajlovi</span></h2> -->
                                    @foreach($fajlovi as $row)
                                    <div class="files-list-item">
                                        <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                            <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                            <div class="files-list-item">
                                                <div class="files-name">{{ $row->naziv }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                </section>
                                @endif
                            @endif
                        </section>

                        <section class="tab-content JScomments-content row">
                            <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                            if($query_komentary->count() > 0){?>
                            <ul class="comments medium-6 small-12 columns">
                                <?php foreach($query_komentary->get() as $row)
                                { ?>
                                <li class="comment">
                                    <span class="comment-content">
                                        <span class="comment-name">{{$row->ime_osobe}}</span>
                                        <span class="comment-date">{{$row->datum}}</span>
                                        <p class="comment-rating">{{Product::getRatingStars($row->ocena)}}</p>
                                        <p class="comment-text">{{ $row->pitanje }}</p>
                                    </span>
                                    <!-- REPLIES -->
                                    @if($row->odgovoreno == 1)
                                    <ul class="replies">
                                        <li class="comment">
                                            <span class="comment-content">
                                                <span class="comment-name">{{ Options::company_name() }}</span>
                                                <p class="comment-text">{{ $row->odgovor }}</p>
                                            </span>
                                        </li>
                                    </ul>
                                            @endif
                                        </li>
                                        <?php }?>
                                    </ul>
                                    <?php }?>
                                    <div class="medium-6 small-12 columns">
                                        <label>Vaše ime</label>
                                        <input id="JScomment_name" onchange="check_fileds('JScomment_name')" type="text" >
                                        <label>Komentar</label>
                                        <textarea class="comment-message" id="JScomment_message" onchange="check_fileds('JScomment_message')"  ></textarea>
                                        <input type="hidden" value="{{ $roba_id }}" id="JSroba_id" />
                                        <span class="review">
                                            <span>Ocena:</span>
                                            <i id="JSstar1" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                            <i id="JSstar2" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                            <i id="JSstar3" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                            <i id="JSstar4" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                            <i id="JSstar5" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                            <input id="JSreview-number" value="0" type="hidden"/>
                                        </span>
                                        <button class="submit JScomment_add">Pošalji</button>
                                    </div>
                                </section>
                            </section>

                            @if(Options::web_options(118))
                            <section class="vezani_artikli">
                                <h2 class="h2-margin JSLinked"><span class="title-for-articles">Vezani artikli</span></h2>
                                <section class="JSproduct">
                                    @foreach($vezani_artikli as $row)
                                        @if(Product::checkView($row->roba_id))
                                        <div class="product-content clearfix">
                                            <section class="image-holder medium-3 columns">
                                                <a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}" class="product-image-wrapper clearfix">
                                                    <img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />
                                                </a>
                                            </section>

                                            <section class="description-holder medium-5 columns">
                                                <br>
                                                <a class="product-title small-only-text-center" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">{{ Product::short_title($row->vezani_roba_id) }}</a>
                                            </section>

                                            @if($row->flag_cena == 1)
                                                <section class="action-holder medium-4 columns clearfix">
                                                    <br>
                                                    <div class="row"> 
                                                        <span class="product-price">{{ Cart::cena(Product::get_price($row->vezani_roba_id)) }}</span>
                                                    </div>
                                                    @if(Product::getStatusArticle($row->roba_id) == 1)
                                                        @if(Cart::check_avaliable($row->vezani_roba_id) > 0)
                                                        <div class="row">
                                                            <div class="btn-container"> 
                                                                <input type="text" class="JSkolicina vezani_input" value="1" onkeypress="validate(event)">&nbsp;
                                                                <button class="JSadd-to-cart-vezani" data-roba_id="{{ $row->vezani_roba_id }}">
                                                                     <i class="fa fa-cart-plus"></i> U korpu
                                                                </button>
                                                            </div>
                                                        </div>
                                                        @else
                                                        <div class="row">
                                                             <div class="btn-container"> 
                                                                <button title="Proizvod nije dostupan" class="dodavnje not-available">Nije dostupno</button>
                                                             </div>
                                                        </div>
                                                        @endif
                                                    @else
                                                    <div class="row">
                                                         <div class="btn-container"> 
                                                            <button title="Proizvod nije dostupan" class="dodavnje not-available">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </section>
                                            @endif
                                        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                                            <a class="article-edit-btn article-edit-btn2" href="{{ Options::base_url() }}admin/product/{{ $roba_id }}">IZMENI ARTIKAL</a>
                                        @endif

                                        </div>
                                        @endif
                                    @endforeach
                                  </section>
                            </section>
                            @endif
 
                            <!-- TAGS -->
    <!-- 						@if(Product::tags_count($roba_id)>1)
                            {{ Product::get_tags($roba_id) }}
                            @endif -->
                            <!-- RELATED PRODUCTS -->
                            <h2 class="h2-margin JSRelated"><span class="title-for-articles">Srodni proizvodi</span></h2>
                            <section class="JSrelated-products JSproduct-slider row">
                            @foreach(Product::get_related($roba_id) as $row)
                                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                            @endforeach
                            </section>
                        </section>
                    </section>
                </section>
        </div>

@endsection