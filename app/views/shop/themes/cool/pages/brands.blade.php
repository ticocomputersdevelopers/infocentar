@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row">
@foreach($brendovi as $brend)
    <span class="columns large-2 medium-2 small-6 end">
	        <a class="brend-item" href="{{ Options::base_url() }}proizvodjac/{{ Url_mod::url_convert($brend->naziv) }}" class="">
	            @if($brend->slika != null OR $brend->slika != '')
	            <img src="{{ Options::domain() }}{{ $brend->slika }}" alt="{{ $brend->naziv }}">
	            @else
	            {{ $brend->naziv }}
	            @endif
	        </a>
    </span>	
@endforeach
</div>
@endsection