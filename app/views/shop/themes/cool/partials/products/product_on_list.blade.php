
<section class="JSproduct">
  <div class="product-content clearfix">
	<section class="image-holder medium-3 columns">
		<a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="product-image-wrapper clearfix">
		    <img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
	</section>

	<section class="description-holder medium-6 columns">
		<a class="product-title" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			{{ Product::short_title($row->roba_id) }}
		</a>
	</section>

	<section class="action-holder medium-3 columns clearfix">
		<div class="price-holder">
			<span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
			@if(All::provera_akcija($row->roba_id))
			<span class="old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
			@endif
		</div>
		@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
				<div class="btn-container">
                    <button title="Dodaj u korpu" data-roba_id="{{$row->roba_id}}" class="dodavanje JSadd-to-cart"  > <i class="fa fa-cart-plus"></i> U korpu</button>
					@if(Options::compare()==1)
					<button title="Uporedi" class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi"><i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i></button>
					@endif
				</div>
			@else 
				<div class="btn-container">
                    <button title="Proizvod nije dostupan" class="dodavanje not-available">NEDOSTUPNO</button>
					@if(Options::compare()==1)
					<button title="Uporedi" class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
						<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
					</button>
					@endif										 
				</div> <!-- end of btn-container -->
			@endif
		@else
			<div class="btn-container">
				<button title="Proizvod nije dostupan" class="dodavanje not-available">
					{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}
				</button>
				@if(Options::compare()==1)
				<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
					<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
				</button>
				@endif
			</div>		
		@endif	
	</section>
		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn article-edit-btn2" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
		@endif
	</div>
</section>
