@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
	<section class="row" id="admin-menu">
		<div class="columns medium-6 large-6">
		</div>
		<div class="columns large-6">	
			<span class="fr ms admin-links"><a href="{{ Options::domain() }}admin-logout">Odjavi se</a></span>
			<span class="fr ms admin-links"><a target="_blank" href="{{ Options::base_url() }}admin">Admin Panel</a> | </span>
            <span class="fr">Prijavljeni ste kao administrator | </span>
		</div>		
	</section>
@endif

<section id="JSpreheader" class="row">
    <div class="social-icons show-for-medium-up">
        {{Options::social_icon()}}
    </div>
 
	<!-- LOGIN & REGISTRATION ICONS -->
    <section class="main-wrapper clearfix">
        
        @if(Options::user_registration()==1)
            <section class="preheader-icons">
            <div class="medium-7 columns small-only-text-center">
                <span class="top-menu-links">
                    @foreach(All::menu_top_pages() as $row)
                        @if($row->grupa_pr_id != -1 and $row->grupa_pr_id != 0)
                        <li>
                          <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($row->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($row->title) }}</a>
                        </li>
                        @elseif($row->tip_artikla_id == -1)
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                        @else
                            @if($row->tip_artikla_id==0)
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            @else
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            @endif
                        @endif
                    @endforeach
                </span>
            </div>
            <div class="medium-5 columns text-right small-only-text-center">                   
               @if(Session::has('b2c_kupac'))
                <div class="logout-wrapper clearfix">
                    <span class="logged-user">Ulogovan korisnik:</span>
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                        <span>{{ WebKupac::get_user_name() }}</span>
                    </a>
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                        <span>{{ WebKupac::get_company_name() }}</span>
                    </a>
                    <a id="logout-button" href="{{Options::base_url()}}logout"><span>Odjavi se</span></a>
                </div>
               @else 
                    <a id="registration-icon" href="{{Options::base_url()}}prijava"><i class="fa fa-user-plus" aria-hidden="true"></i><span>Registracija</span></a>
                    <a id="login-icon" href="{{Options::base_url()}}prijava"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Uloguj se</span></a>
         
               @endif

                @if(Options::checkB2B())
                    <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="btn confirm global-bradius">B2B Portal</a>
                @endif

            </div>
           </section>
        @endif
    </section>
    <span class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></span>
</section>
