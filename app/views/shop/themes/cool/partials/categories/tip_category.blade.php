<nav class="manufacturer-categories"> 
    <h3>{{$title}}</h3>  
 
    <!-- CATEGORIES LEVEL 1 -->
    <ul>
        @foreach(Support::tip_categories($tip_id) as $key => $value)
            <li>
                <a href="{{ Options::base_url()}}tip/{{ Url_mod::url_convert($tip) }}/{{ Url_mod::url_convert($key) }}">
                    <span>{{ $key }}</span>
                    <span>{{ $value }}</span>
                </a>
            </li>
        @endforeach 
    </ul>
</nav>
