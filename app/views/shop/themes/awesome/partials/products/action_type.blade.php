<div class="container">

    @if(All::broj_akcije() > 0)
        <div id="JSshift_right">
            <h2 class="section-heading"><span class="heading-background">Na akciji</span></h2>
            <section class="sale-list JSproduct-list row grid-5 JSproduct-slider">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-akcija">
                    <div class="loader"></div>
                </div>
                <button class="previous btn-slider slider-prev" id="JSakcija-previous" data-id="akcija" data-offset="1">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </button>

                <button class="next btn-slider slider-next" id="JSakcija-next" data-id="akcija" data-offset="1">
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </button>

                <div class="ajax-content" id="JSakcija-content" data-tipid="akcija">
                    <!-- Product ajax content -->
                </div>
            </section>
        </div>
    @endif
    
    <?php

    $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('active',1)->orderBy('rbr','asc')->get();

    ?>
    @if(count($tipovi) > 0)
    @foreach($tipovi as $tip)
        @if(All::provera_tipa($tip->tip_artikla_id))
            <h2 class="section-heading"><span class="heading-background">{{ $tip->naziv }}</span></h2>
            <section class="tip-list JSproduct-list row grid-5 JSproduct-slider">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-{{ $tip->tip_artikla_id }}">
                    <div class="loader"></div>
                </div>
                <button class="previous btn-slider slider-prev" id="JS{{ $tip->tip_artikla_id.'-' }}previous" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </button>
                
                <button class="next btn-slider slider-next" id="JS{{ $tip->tip_artikla_id.'-' }}next" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </button>

                <div class="ajax-content" id="JS{{ $tip->tip_artikla_id.'-' }}content" data-tipid="{{ $tip->tip_artikla_id }}">
                    <!-- Product ajax content -->
                </div>
            </section>
        @endif
    @endforeach
    @endif
</div>