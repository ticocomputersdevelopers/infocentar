@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<h2><span class="heading-background">Izmeni podatke naloga</span></h2>

<section class="user-info-form clearfix">
	<form action="{{ Options::base_url()}}korisnik-edit" method="post" class="login-form login-form--no-overflow" autocomplete="off">
		<!-- PERSONAL INFO CHANGE -->
		@if($web_kupac->flag_vrsta_kupca == 0)
		<div class="person-fields clearfix">
			
			<div class="medium-6 columns">
				<label for="name">Ime</label>
				<input name="ime" type="text" value="{{ Input::old('ime') ? Input::old('ime') : $web_kupac->ime }}">
				<div class="error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
			</div>
			
			<div class="medium-6 columns">
				<label for="surname">Prezime</label>
				<input name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : $web_kupac->prezime }}">
				<div class="error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
			</div>
			
		</div>
		
		<!-- COMPANY INFO CHANGE -->
		@elseif($web_kupac->flag_vrsta_kupca == 1)
		<div class="company-fields clearfix">
			
			<div class="medium-6 columns">
				<label for="company-name">Naziv firme</label>
				<input name="naziv" type="text" value="{{ Input::old('naziv') ? Input::old('naziv') : $web_kupac->naziv }}">
				<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
			</div>
			
			<div class="medium-6 columns">
				<label for="pib">PIB</label>
				<input name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : $web_kupac->pib }}">
				<div class="error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
			</div>
			
		</div>
		@endif
        <div class="person-fields clearfix">
            <div class="medium-6 columns">
                <label for="e-mail">E-mail</label>
                <input name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : $web_kupac->email }}">
                <div class="error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
            </div>
            <div class="medium-6 columns">
                <label for="lozinka_user">Lozinka</label>
                <input name="lozinka" type="password" value="{{ Input::old('lozinka') ? Input::old('lozinka') : base64_decode($web_kupac->lozinka) }}">
                <div class="error">{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}</div>
            </div>
        </div>

        <div class="person-fields clearfix">
            <div class="medium-6 columns">
                <label for="address">Adresa</label>
                <input name="adresa" type="text" value="{{ Input::old('adresa') ? Input::old('adresa') : $web_kupac->adresa }}">
                <div class="error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
            </div>
            <div class="medium-6 columns">
				<label for="mesto">Mesto</label>
                <input name="mesto" type="text" value="{{ Input::old('mesto') ? Input::old('mesto') : $web_kupac->mesto }}">
                <div class="error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
			</div>
        </div>
		<div class="row">
            <div class="medium-6 columns">
                <label for="telefon">Telefon</label>
                <input name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : $web_kupac->telefon }}">
                <div class="error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
            </div>	
		</div>
		<input type="hidden" name="web_kupac_id" value="{{$web_kupac->web_kupac_id}}" />
		<input type="hidden" name="flag_vrsta_kupca" value="{{$web_kupac->flag_vrsta_kupca}}" />
		<input type="submit" class="submit-btn-edit" value="Izmeni">
	</form>
	@if(Session::get('message'))
		<div class="success">Vaši podaci su uspešno sačuvani!</div>
	@endif
</section>

	

@if(count(All::getNarudzbine($web_kupac->web_kupac_id)) > 0)
<section class="user-orders">
	<div class="row">
		<div class="large-12 columns">
			<table class="user-orders-table">
				<tr>
					<th>Broj p.</th>
					<th>Datum</th>
					<th>Iznos</th>
					<th>Status</th>
					<th></th>
				</tr>

				<!-- thus repeat -->
				@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)
				<tr>	
					<td>{{ $row->broj_dokumenta }}</td>
					<td>{{ $row->datum_dokumenta }}</td>
					<td>{{Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id))}}</td>
					<td>{{Order::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</td>
					<td><div data-reveal-id="userOrdersModal-{{ $row->web_b2c_narudzbina_id }}"><i class="fa fa-plus" aria-hidden="true"></i>
</div></td>
				</tr>
				
				@endforeach
				<!-- end repeat -->
			</table>
	@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)		
	<div id="userOrdersModal-{{ $row->web_b2c_narudzbina_id }}"  class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<table class="user-orders-table">
					<tr>
						<table>
				        <tr>
				            <td class="cell-product-name">Naziv proizvoda:</td>
				            <td class="cell">Cena :</td>
				            <td class="cell">Količina</td>
				            <td class="cell">Ukupna cena:</td>
				        </tr>
				        @foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->get() as $row2)
				            <tr>
				                <td class="cell-product-name">{{ Product::short_title($row2->roba_id) }} {{Product::getOsobineStrNonActv($row2->roba_id,$row2->osobina_vrednost_ids)}}</td>
				                <td class="cell">{{ Cart::cena($row2->jm_cena) }}</td>
				                <td class="cell">{{ (int)$row2->kolicina }}</td>
				                <td class="cell">{{ Cart::cena($row2->kolicina*$row2->jm_cena) }}</td>
				            </tr>
				        @endforeach
				        </table>
				        <table>
				            <tr class="text-right">
				                <td class="summary text-right">Ukupno:{{ Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id)) }}</td>
				            </tr>
				        </table>
					</tr>
				</table>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			@endforeach
		</div>
	</div>
</section>
@endif
@endsection