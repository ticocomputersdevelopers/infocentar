@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<section class="news-section">
	<article class="single-news">
		<div class="row">
			<div class="columns medium-12 large-12">
				<img class="vest-slika" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
			</div>
			<div class="columns medium-12 large-12">
				<h2 class="news-title-list">{{ $naslov }}</h2>
				<p>{{ $sadrzaj }}</p>

			</div>
		</div> <!--  end of .row -->
	</article>
</section>
@endsection