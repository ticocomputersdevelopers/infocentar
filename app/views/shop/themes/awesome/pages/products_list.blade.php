@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')    
<section class="product-list-options clearfix flex-horizontal-center">			
    <div class="medium-4 small-12 columns numbers_of_pages">
		<span class="product-number">Ukupno: {{ $count_products }}</span>
        @if(Options::product_number()==1)
		<div class="per-page JSselect-field">		
			<span class="per-page-active">
				@if(Session::has('limit'))
				{{Session::get('limit')}}
				@else
				20
				@endif
			</span>
			<i class="fa fa-caret-down" aria-hidden="true"></i>
			<ul>		
				<li><a href="{{ Options::base_url() }}limit/20">20</a></li>
				<li><a href="{{ Options::base_url() }}limit/30">30</a></li>
				<li><a href="{{ Options::base_url() }}limit/50">50</a></li>		
			</ul>		
		</div>
		@endif	
	</div>

	<div class="medium-8 small-12 columns buttons_and_else">
		@if(Options::compare()==1)
		<div id="JScompareArticles" class="show-compered" data-reveal-id="compare-article">Upoređeni artikli</div>
		@endif
        <div class="currencyANDsort clearfix">
        @if(Options::product_currency()==1)
            <div class="currency JSselect-field">
                <span class="currency-active">{{Articles::get_valuta()}}</span>
                <ul>
                    <li><a href="{{ Options::base_url() }}valuta/1">Din</a></li>
                    <li><a href="{{ Options::base_url() }}valuta/2">Eur</a></li>
                </ul>
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </div>

            @endif
            @if(Options::product_sort()==1)
            <div class="sort-products JSselect-field">
                <span class="sort-active">{{Articles::get_sort()}}</span>
                <ul>
                    <li><a href="{{ Options::base_url() }}sortiranje/price_asc">Cena - Rastuće</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/price_desc">Cena - Opadajuće</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/news">Najnovije</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/name">Prema nazivu</a></li>
                </ul>
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </div>
            @endif
        </div>    

        @if(Options::product_view()==1)
		@if(Session::has('list'))

		<div class="view-buttons">		
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button "><i class="fa fa-th" aria-hidden="true"></i></span></a>
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button active"><i class="fa fa-navicon" aria-hidden="true"></i></span></a>		
		</div>

		@else
		<div class="view-buttons">		
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button active"><i class="fa fa-th" aria-hidden="true"></i></span></a>
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button"><i class="fa fa-navicon" aria-hidden="true"></i></span></a>		
		</div>
		@endif
        @endif	
	</div>
</section>

	<section class="pagination_section clearfix">
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</section>

	<section class="compare-section">
		<div id="compare-article" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<div id="JScompareTable" class="table-scroll"></div>
			<a class="close-reveal-modal close" aria-label="Close"><i class="fa fa-close"></i></a>
		</div>
	</section>

	<!-- PRODUCTS -->
	@if(Session::has('list') or Options::product_view()==3)
	<!-- LIST PRODUCTS -->
	<section class="JSproduct-list list-view row">
		@foreach($articles as $row)
			@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
		@endforeach
	</section>

	@else
	<!-- Grid proucts -->
	<section class="JSproduct-list row">
		@foreach($articles as $row)
			@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
		@endforeach
	</section>		
	@endif

	@if($count_products == 0)
	<span class="no-articles">Trenutno nema artikla za date kategorije</span>
	@endif

	<section class="pagination_section clearfix down-pagination">
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</section>				
					
@endsection