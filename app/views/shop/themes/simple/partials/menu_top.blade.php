@if(Session::has('b2c_admin'.Options::server()) AND Support::check_admin(array(200)))
	<section class="row" id="admin-menu">
		<div class="columns medium-6 large-6">
		</div>
		<div class="columns medium-6 large-6 text-right">
			<span class="ms">Prijavljeni ste kao administrator</span>
			<span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">Odjavi se</a></span>
			<span class="ms admin-links"><a target="_blank" href="{{ Options::base_url() }}admin">Admin Panel</a></span>

		</div>		
	</section>
@endif

<section id="JSpreheader" class="row">
	
	   <!-- MAIN MENU -->
		
	   <nav class="medium-7 small-12 columns">
		
		    <ul id="main-menu" class="clearfix">
				    
			   @foreach(All::menu_top_pages() as $row)
					@if($row->grupa_pr_id != -1 and $row->grupa_pr_id != 0)
					<li>
					  <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($row->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($row->title) }}</a>
					</li>
					@elseif($row->tip_artikla_id == -1)
                    <li> 
                        <a href="{{ Options::base_url().$row->naziv_stranice }}">
                            {{ ($row->title) }}
                        </a>
                     </li>
                    @else
                        @if($row->tip_artikla_id==0)
                        <li> 
                            <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                {{ ($row->title) }}
                            </a>
                        </li>
                        @else
                        <li> 
                            <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                {{ ($row->title) }}
                            </a>
                        </li>
                        @endif
                    @endif
                @endforeach 

				@if(Options::web_options(121)==1)
					<?php $konfiguratori = All::getKonfiguratos(); ?>
					@if(count($konfiguratori) > 0)
						@if(count($konfiguratori) > 1)
							<li class="dropdown">
								<a href="#" class="dropbtn">Konfiguratori</a>
								<ul class="dropdown-content">
									@foreach($konfiguratori as $row)
									<li><a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a></li>
									@endforeach
								</ul>
							</li>
						@else
							<li><a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">Konfigurator</a></li>
						@endif
					@endif
				@endif
			</ul>
		
	   </nav>
	
	<span class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></span>
	
	<!-- LOGIN & REGISTRATION ICONS -->
	@if(Options::user_registration()==1)
	   <section class="medium-5 small-12 columns preheader-icons">
			
			<div class="social-icons">
				{{Options::social_icon()}}
			</div>
 
	       @if(Session::has('b2c_kupac'))
	        <div class="logout-wrapper clearfix">
	            <span class="logged-user">Ulogovan korisnik:</span>

	            <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_user_name())}}"><span>{{ WebKupac::get_user_name() }}</span></a>
				<a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_company_name())}}"><span>{{ WebKupac::get_company_name() }}</span></a>
				<a id="logout-button" href="{{Options::base_url()}}logout"><span>Odjavi se</span></a>
			
			</div>
           @else 
           <div class="login-wrapper">
	   
				<a id="registration-icon" href="{{Options::base_url()}}prijava"><i class="fa fa-user-plus" aria-hidden="true"></i><span>Registracija</span></a>
				<a id="login-icon" href="{{Options::base_url()}}prijava"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Uloguj se</span></a>
				 
			</div>
           @endif

            @if(Options::checkB2B())
			<a id="b2b-login-icon" href="{{Options::base_url()}}b2b/login" class="btn confirm global-bradius">B2B Portal</a>
			@endif
	   </section>
	@endif
</section>
