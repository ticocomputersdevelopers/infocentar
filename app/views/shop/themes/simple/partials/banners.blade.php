<section class="banners text-center">
    <?php foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->get() as $row){ ?>
            <div class=" text-center">
                <a href="<?php echo $row->link; ?>">
                    <img src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
                </a>
            </div>
        <?php } ?>
</section>