<nav id="JScategories">

	<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
    <h3 class="JScategories-titile">Kategorije </h3>
	
	<span class="JStoggler"><i class="fa fa-bars"></i></span>
	
	<!-- CATEGORIES LEVEL 1 -->

    <ul class="JSlevel-1">
	    @if(Options::category_type()==1)
		@foreach ($query_category_first as $row1)
		
			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
			<li>
				<div class="category__link-group">
					<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="category__link-group__link">
						<div class="category__link-group__link__text">
							@if(Groups::check_image_grupa($row1->putanja_slika))
								<img class="cat-img-1" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa  }}" />
							@endif
							{{ $row1->grupa  }} 
						</div>
						<span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span>
					</a>
					
						<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>
				</div>
				<ul class="JSlevel-2 clearfix">
				
					@foreach ($query_category_second->get() as $row2)
						<li>

							@if(Groups::check_image_grupa($row2->putanja_slika))
							<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" class="hide-for-small-only">
								<img class="cat-img-2" src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
							</a>
							@endif
							<div class="category__link-group">
								<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0" class="category__link-group__link">
									{{ $row2->grupa }} 
									<span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}}</span>
								</a>
								@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
								<span class="category__item__link__expend JSCategoryLinkExpend">
									<i class="fa fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif
							</div>
							
							@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
							<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
							
							
							    <ul class="level-3 clearfix">
									@foreach($query_category_third as $row3)
									 <li>
									 	<div class="category__link-group">
										 	<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0" class="test">
										 		{{ $row3->grupa }}
										 		<span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}}</span>
										 	</a>

							 				@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
										 	<span class="category__item__link__expend JSCategoryLinkExpend">
												<i class="fa fa-chevron-down" aria-hidden="true"></i>
											</span>
											@endif
										</div>
										@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
											<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
																		
											    <ul class="level-4 clearfix">
													@foreach($query_category_forth as $row4)
													 <li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ $row4->grupa }} <span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}}</span></a>
													@endforeach
												</ul>
										@endif								 	
									@endforeach
								</ul>
								@endif
						</li>
					@endforeach
				</ul>
			</li>
			@else
			<li>
				<div class="category__link-group">
					<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0" class="category__link-group__link">
						<div class="category__link-group__link__text">
							@if(Groups::check_image_grupa($row1->putanja_slika))
								<img class="cat-img-1" src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa  }}" />
							@endif
							{{ $row1->grupa  }} 
						</div>
						<span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span>
					</a>
				</div>
			</li>
			@endif
		@endforeach
		@else
		@foreach ($query_category_first as $row1)
		
		    
				@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
				<li>
					<a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">
						{{ $row1->grupa  }}
						<span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span>
					</a>
				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>
				<ul class="level-2-category clearfix level-3-open">
				
					@foreach ($query_category_second->get() as $row2)
						 <li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/0/0/0-0">{{ $row2->grupa }} <span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}}</span></a>
						 @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
							<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
							
							
							    <ul class="level-3 clearfix">
									@foreach($query_category_third as $row3)
									 <li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/0/0/0-0">{{ $row3->grupa }} <span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}}</span></a>
										 @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
											<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
																		
											    <ul class="level-4 clearfix">
													@foreach($query_category_forth as $row4)
													 <li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/{{ Url_mod::url_convert($row2->grupa) }}/{{ Url_mod::url_convert($row3->grupa) }}/{{ Url_mod::url_convert($row4->grupa) }}/0/0/0-0">{{ $row4->grupa }} <span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}}</span></a>
													@endforeach
												</ul>
										@endif	
									@endforeach
								</ul>
								@endif
						 </li>
					@endforeach
				</ul>
				@else
				<li><a href="{{ Options::base_url()}}{{ Url_mod::url_convert($row1->grupa) }}/0/0/0-0">{{ $row1->grupa  }} <span class="category__item__link__span">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span></a>
				
				@endif
				
				</li>
		@endforeach				
		@endif

		@if(Options::all_category()==1)
		<li class="category-item-accent">
			<a class="all-category-link" href="{{ Options::base_url() }}sve-kategorije">Sve Kategorije</a>
		</li>
		@endif
	
	</ul>

</nav>
