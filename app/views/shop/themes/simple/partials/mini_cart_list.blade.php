@if(Cart::broj_cart()>0)
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<li>
	    <div class="header-cart-image-wrapper">
		    <img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}" />
		</div>
		<a href="{{Options::base_url()}}{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}</a>
	    <span class="header-cart-amount">{{round($row->kolicina)}}</span>
		<span class="header-cart-price">{{Cart::cena($row->jm_cena)}}</span>
		<a href="#" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="Ukloni artikal" class="close JSDeleteStavka" >X</a>					
	</li>
	@endforeach	
<span class="header-cart-summary">Ukupno: <span>{{ Cart::cena(Cart::cart_ukupno()) }}</span></span>
@endif