<!DOCTYPE html>
<html>
	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
	</head>
	<body>
		<div id="main-wrapper">
		<!-- PREHEADER -->
		@include('shop/themes/'.Support::theme_path().'partials/menu_top')
		<!-- HEADER -->
		@include('shop/themes/'.Support::theme_path().'partials/header')
			
			
		@if(Options::category_view()==0 AND $strana != 'proizvodjac' AND $strana != 'akcija' AND $strana != 'tip')
		@include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
		@endif
		<!-- MIDDLE AREA -->
		<section id="middle-area" class="row bw">
			
			<!-- SIDEBAR LEFT -->		
			<!-- BREADCRUMBS -->
			<aside id="sidebar-left" class="medium-3 columns">
				@if($strana!='akcija' and $strana!='tip')
				<ul class="breadcrumbs clearfix">
					@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
					{{ Url_mod::breadcrumbs2()}}
					@elseif($strana == 'proizvodjac')
					<li><a href="{{ Options::base_url() }}brendovi">Brendovi</a> > 
						@if($grupa)
						<a href="{{ Options::base_url() }}proizvodjac/{{ Url_mod::url_convert($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
						  {{ Groups::get_grupa_title($grupa) }}</li>
						@else
						{{ All::get_manofacture_name($proizvodjac_id) }} 
						@endif
					@else					
					<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
					<li><h1>{{$title}}</h1></li>
					@endif
				</ul>
				@endif
				
				@if(Options::category_view()==1 AND $strana != 'proizvodjac' AND $strana != 'akcija' AND $strana != 'tip')
					@include('shop/themes/'.Support::theme_path().'partials/categories/category')
				@endif
				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif
				

				@if(Options::enable_filters()==1 AND $filter_prikazi)
				@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif
				
				<!-- @include('shop/themes/'.Support::theme_path().'partials/banners') -->
			</aside>
			<!-- MAIN CONTENT -->


			<section id="main-content" class="medium-9 columns">
				
				
				@yield('products_list')
				
				
			</section>
			
		</section>
		
		<!-- FOOTER -->
		@include('shop/themes/'.Support::theme_path().'partials/footer')
	</div>
	<a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>
	<!-- LOGIN POPUP -->
	@include('shop/themes/'.Support::theme_path().'partials/popups')

	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
	<input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
	
	<!-- js includes -->
	<script src="{{Options::domain()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
	@if(Session::has('b2c_admin'.Options::server()))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	<script src="{{Options::domain()}}js/foundation.min.js" type="text/javascript" ></script>
	<script src="{{Options::domain()}}js/jquery.accordion.js" type="text/javascript" ></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js" type="text/javascript" ></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js" type="text/javascript" ></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js" type="text/javascript" ></script>

	 
</body>
</html>