@extends('shop/themes/'.Support::theme_path().'templates/main')


@section('page')
<div class="container">
    <h2><span class="heading-background">Greška</span></h2>
    <p style="padding:10px;"><b>Žao nam je, stranica nije dostupna.</b></p>
</div>
@endsection