@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<h2>Kontaktirajte nas</h2>
			
	@if(Options::company_map() != '' && Options::company_map() != ';')
		<div id="map_canvas" class="map"></div>
	@endif
	
<section class="contact-info medium-4 columns">

    <h3>Kontakt informacije</h3>

    <ul>
 		@if(Options::company_name() != '')
			<li class="medium-4 small-5 columns">Firma:</li>
			<li class="medium-8  small-7 columns"> {{ Options::company_name() }} &nbsp;</li>
		@endif
		@if(Options::company_adress() != '')
		    <li class="medium-4 small-5 columns">Adresa:</li>
		    <li class="medium-8 small-7 columns"> {{ Options::company_adress() }} &nbsp;</li>
		@endif
		@if(Options::company_city() != '')
			<li class="medium-4 small-5 columns">Grad:</li>
			<li class="medium-8 small-7 columns"> {{ Options::company_city() }} &nbsp;</li>
		@endif
		@if(Options::company_phone() != '')
			<li class="medium-4 small-5 columns">Telefon:</li>
			<li class="medium-8 small-7 columns"> {{ Options::company_phone() }} &nbsp;</li>
		@endif
		@if(Options::company_fax() != '')
			<li class="medium-4 small-5 columns">Fax:</li>
			<li class="medium-8 small-7 columns"> {{ Options::company_fax() }} &nbsp;</li>
		@endif
		@if(Options::company_pib() != '')
			<li class="medium-4 small-5 columns">PIB:</li>
			<li class="medium-8 small-7 columns"> {{ Options::company_pib() }} &nbsp;</li>
		@endif
		@if(Options::company_maticni() != '')
			<li class="medium-4 small-5 columns">Matični broj:</li>
			<li class="medium-8 small-7 columns"> {{ Options::company_maticni() }} &nbsp;</li>
		@endif
		@if(Options::company_email() != '')
			<li class="medium-4 small-5 columns">E-mail:</li>
			<li class="medium-8 small-7 columns">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</li>
		@endif
	</ul>
</section>

<section class="contact-form medium-8 columns">

    <h3>Pošaljite poruku</h3>

    <label id="label_name">Vaše ime *</label>
	<input class="contact-name" id="JSkontakt-name" type="text" onchange="check_fileds('JSkontakt-name')" >
	
	<label id="label_email">Vaša e-mail adresa *</label>
	<input class="contact-email" id="JSkontakt-email" onchange="check_fileds('JSkontakt-email')" type="text" >
					
	<label id="label_message">Vaša poruka </label>
	<textarea class="contact-message" id="message" ></textarea>
					
	<button class="submit" onclick="meil_send()" >Pošalji</button>

</section>
@endsection