@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<table class="konfigurator table">
	<thead>
		<tr>
			<td>Grupa</td>
			<td></td>
			<td>Artikli</td>
			<td></td>
			<td>Količina</td>
			<td>Cena</td>
		</tr>
	</thead>
	@foreach($grupe as $row)
	<tr class="JSUsefulRow">
		<td class="konfigurator-grupa">{{ $row->grupa }} </td>
		<td class="JSGrupaAdd" data-id="{{ $row->grupa_pr_id }}">Dodaj</td>
		<td>
			<select class="JSArtikal">
				<option value="">Odaberite...</option>
				@foreach(All::getKonfiguratorArticles($row->grupa_pr_id) as $row2)
				<?php $cena = Product::get_price($row2->roba_id); ?>
				<option value="{{ $row2->roba_id }}" data-cena="{{ $cena }}">{{  $row2->naziv_web }} -> {{ $cena }}.din</option>
				@endforeach
			</select>
			<span class="JSArtDetails">Vidi</span>
		</td>
		<td>
			<span class="JSGrupaRemove"></span>
		</td>
		<td>
			<input type="text" class="JSKolicina" value="1" onkeypress="validate(event)">
		</td>
		<td>
			<div class="JSCenaKonf">0.00</div>
		</td>
	</tr>
	@endforeach
</table>

<div class="ukupno-konf">
	<div class="ukupno-konf-div">
		<p>Ukupno u konfiguratoru</p>
		<p id="JSUkupnaCenaKonf">0.00</p>
	</div>
	<div class="ukupno-konf-div">
		<p>Ukupno u korpi</p>
		<p id="JSKorpaUkupnaCena">{{ $korpa_ukupno }}</p>
	</div>
	<div class="ukupno-konf-div">		
		<p>Ukupno</p>
		<p id="JSUkupno">{{ $korpa_ukupno }}</p>
	</div>
</div>

<button id="JSKonfAdd" class="dodaj-konf">Dodaj u korpu</button>
@endsection