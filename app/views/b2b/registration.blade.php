<!DOCTYPE html>

<html>

<head>

    <title>{{ B2bOptions::company_name()}} Admin</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{{ B2bOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ B2bOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />



</head>
<body class="body-login">
    <div class="color-overlay-image"></div>
    <div class="color-overlay"></div>
<!-- LOGIN FORM -->

    <div class="registration-form-wrapper form-wrapper">

        <section class="registration-form clearfix form-wrapper__form b2b-registration-form">

            @if(Session::has('wings_message'))
            <div class="center">Uskoro će vam stići email poruka o potvrdi registracije.</div>
            @endif

            <div class="center">
                <a class="logo" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}"><img src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt=""></a>
                <span class="logo-text">B2B</span>
            </div>
            <form action="{{route('b2b.registration.store')}}" method="post"> 
                <h2>Registracija </h2>
                <div class="field-group">
                    <label for="naziv">Naziv firme*</label>
                    <input id="naziv" name="naziv" type="text" class="login-form__input" required>
                </div>

                <div class="field-group">
                    <label for="pib">PIB*</label>
                    <input id="pib" name="pib" type="text" class="login-form__input" required>
                </div>
                
                <div class="field-group">
                    <label for="broj_maticni">Matični broj*</label>
                    <input id="broj_maticni" name="broj_maticni" class="login-form__input" type="text" required>
                </div>
                
                <div class="field-group">
                    <label for="adresa">Adresa*</label>
                    <input id="adresa" name="adresa" type="text" class="login-form__input" required>
                </div>

                <div class="field-group">
                    <label for="mesto">Mesto*</label>
                    <input id="mesto" name="mesto" type="text" class="login-form__input" required>
                </div>
                <h2>Kontakt osoba </h2>

                <div class="field-group">
                    <label for="komercijalista">Ime i prezime*</label>
                    <input id="komercijalista" name="komercijalista" type="text" class="login-form__input" required>
                </div>
                
                <div class="field-group">
                    <label for="telefon">Telefon*</label>
                    <input id="telefon" name="telefon" type="text" class="login-form__input" required>
                </div>
                
                <div class="field-group">
                    <label for="mail">Email*</label>
                    <input id="mail" name="mail" type="email" class="login-form__input" required>
                </div>
                
                <h2>Pristupni podaci </h2>
                <div class="field-group">
                    <label for="login">Korisnik*</label>
                    <input id="login" name="login" type="text" class="login-form__input" required>
                </div>
    
                <div class="field-group">
                    <label for="password">Lozinka*</label>
                    <input id="password" name="password" type="password" class="login-form__input" required>
                </div>


                <div class="btn-container center">
                    <button class="submit admin-login btn btn-primary btn-round btn-large">Registracija</button>
                </div>
                <div class="center">
                    <a class="submit became-partner btn-clear btn btn-secondary btn-round btn-small" href="login">Otkaži</a>
                </div>

            </form>


        </section>

    </div>




</body>

</html>