@extends('b2b.layouts.b2bpage')
@section('content')
    <section class="cart">

        <h2><span class="heading-background">Vaša Kartica</span></h2>
        <div class="medium-12 columns">
            <span class="medium-2 columns filter-text">Filtriraj: </span>
                <div class="date-filter">
                    <label>Datum od:</label>
                    <input type="text" value="{{ !is_null($date_start) ? $date_start : '' }}" id="date_start">
                </div>
                <div class="date-filter">
                    <label>Datum do:</label>
					<input type="text" id="date_end" value="{{ !is_null($date_end) ? $date_end : '' }}">
                 </div>
                <div class="button-filter">
                <button id="JSFilterDate">Potvrdi</button>
					<a href="{{B2bOptions::base_url()}}b2b/user/card">Poništi filtere</a>
                     
                </div>
                @if(B2bOptions::info_sys('calculus'))
                <div class="button-filter">
                    <form method="POST" action="{{B2bOptions::base_url()}}b2b/user/card-refresh">
                        <button onClick="this.form.submit(); this.disabled=true;">Osveži karticu</button>
                    </form>
                     
                </div>
                @endif
        </div>
        <ul class="medium-12 columns cart-labels clearfix">
            <li class="medium-3 columns">Vrsta dokumenta</li>
            <li class="medium-3 columns">Datum dokumenta</li> 
            <li class="medium-2 columns">Datum dospeća</li>
            <li class="medium-2 columns">Duguje</li>
            <li class="medium-2 columns">Potražuje</li>
        </ul>
        @foreach($cart_items as $item)
         <ul class="cart-item clearfix">
            <li class="medium-3 columns">{{$item->vrsta_dokumenta}}</li>
            <li class="medium-3 columns">{{date("d-m-Y",strtotime($item->datum_dokumenta))}}</li>
            <li class="medium-2 columns">{{date("d-m-Y",strtotime($item->datum_dospeca))}}</li>
            <li class="medium-2 columns">{{number_format($item->duguje,2)}}</li>
            <li class="medium-2 columns">{{number_format($item->potrazuje,2)}}</li>
        </ul>
        @endforeach

        <div class="medium-6 columns zero-padding">
            <ul class="cart-item clearfix">

                <li><span class="medium-6 columns">Ukupno dugovanje :</span><span class="medium-6 columns">{{number_format($sumDuguje,2)}}</span></li>
                <li><span class="medium-6 columns">Ukupno potraživanje :</span><span class="medium-6 columns">{{number_format($sumPotrazuje,2)}}</span></li>
                <li><span class="medium-6 columns">Saldo :</span><span class="medium-6 columns">{{number_format($saldo,2)}}</span></li>

            </ul>
        </div>
        <div class="medium-6 columns zero-padding">
            {{ $cart_items->links() }}
        </div>

    </section>
@endsection

@section('footer')
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#date_start, #date_end").datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });

        $('#date_start, #date_end').keydown(false);

        $('#JSFilterDate').click(function(){
            var datum_start = $('#date_start').val();
            var datum_end = $('#date_end').val();

            if(datum_start == ''){
                datum_start = 'null';
            }
            if(datum_end == ''){
                datum_end = 'null';               
            }

            location.href = "{{ B2bOptions::base_url() }}b2b/user/card/"+datum_start+"/"+datum_end;

        });
    </script>

@endsection