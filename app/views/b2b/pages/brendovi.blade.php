@extends('b2b.templates.main')
@section('content') 
<div class="main-content">
	<div class="row">
	@foreach($brendovi as $brend)
	    <span class="col-md-2 col-sm-6 col-xs-6">
	        <a class="brend-item" href="{{ B2bOptions::base_url() }}b2b/proizvodjac/{{ B2bUrl::url_convert($brend->naziv) }}">
	            @if($brend->slika != null OR $brend->slika != '')
	            <img class="img-responsive" src="{{ B2bOptions::domain() }}{{ $brend->slika }}" alt="{{ $brend->naziv }}" />
	            @else
	            {{ $brend->naziv }}
	            @endif
	        </a>
	    </span>	
	@endforeach
	</div> 
</div>
@endsection