@extends('b2b.templates.login')
@section('content')
<div class="b2bLoginContainer">
    <div class="b2b-login row b2b-flex">
        <div class="col-md-5 col-sm-5 col-xs-12 center"> 
            <a class="logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                <img class="b2b-login-img" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="">
            </a>
        </div> 
      <div class="col-md-7 col-sm-7 col-xs-12 left-border"> 
        <div class="b2bForm"> 
            <form action="{{route('b2b.login.store')}}" method="post">
                <div class="row">
                    <div class="col-md-4 col-sm-4 no-padd text-center"> 
                        <p class="center no-margin">PRIJAVA</p>
                    </div>
                    <div class="col-md-8 col-sm-8 no-padd text-center"> 
                        @if(!B2bOptions::info_sys('calculus'))
                        <a class="submit b2bRegisterBtn center right" href="{{route('b2b.registration')}}">REGISTRACIJA</a>
                        @endif
                    </div>
                </div><br>
                <div class="field-group">
                    <label for="username">E-mail</label>
                    <input id="username" name="username" type="text" class="login-form__input form-control"> 
                    @if($errors->first('username'))
                        <span class="error">{{ $errors->first('username') }}</span>
                    @endif
                </div>
                 
                <div class="field-group">
                    <label for="password">Lozinka</label>
                    <input id="password" name="password" type="password" class="login-form__input form-control"> 
                     @if($errors->first('password'))
                        <span class="error">{{ $errors->first('password') }}</span>
                     @endif
                </div> 
                <div class="field-group">
                    <button class="submit btn" onclick="login()">PRIJAVI SE</button>&nbsp;
                    @if(!B2bOptions::info_sys('wings') AND !B2bOptions::info_sys('calculus'))
                        <a class="forgotten-psw" href="{{ route('b2b.forgot_password') }}">Zaboravljena lozinka</a>
                    @endif 
                </div> 
            </form> 
            <span class="text-right">B2B ENGINE BY: 
                 <a href="https://www.selltico.com/" target="_blank"> <img src="{{ Options::base_url()}}images/logo-selltico.png">     </a>
            </span> 
        </div>
    </div>
</div>
</div>
@endsection