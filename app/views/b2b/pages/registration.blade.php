@extends('b2b.templates.login')
@section('content')  
<div class="b2bLoginContainer">
    <div class="row b2b-flex">
        <div class="col-md-5 col-sm-5 col-xs-12 center"> 
            <a class="logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                <img class="b2b-login-img" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="">
            </a>
        </div> 
        <div class="col-md-7 col-sm-7 col-xs-12 left-border"> 
            <div class="b2bForm"> 
                @if(Session::has('wings_message'))
                <div class="center">Uskoro će vam stići email poruka o potvrdi registracije.</div>
                @endif 
                <form action="{{route('b2b.registration.store')}}" method="post"> 
                    <div class="row">
                        <div class="col-md-7 col-sm-7 no-padd center"> 
                            <p class="center no-margin">REGISTRACIJA</p>
                        </div>
                        <div class="col-md-5 col-sm-5 no-padd center"> 
                            <a class="submit b2bRegisterBtn center right" href="login">PRIJAVA</a>
                        </div>
                    </div><br> 
                    <div class="field-group">
                        <label for="naziv">Naziv firme*</label>
                        <input id="naziv" name="naziv" type="text" class="login-form__input form-control" value="{{ Input::old('naziv') }}">
                        <div class="error">{{ $errors->first('naziv') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="pib">PIB*</label>
                        <input id="pib" name="pib" type="text" class="login-form__input form-control" value="{{ Input::old('pib') }}">
                        <div class="error">{{ $errors->first('pib') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="broj_maticni">Matični broj</label>
                        <input id="broj_maticni" name="broj_maticni" class="login-form__input form-control" type="text" value="{{ Input::old('broj_maticni') }}">
                        <div class="error">{{ $errors->first('broj_maticni') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="adresa">Adresa*</label>
                        <input id="adresa" name="adresa" type="text" class="login-form__input form-control" value="{{ Input::old('adresa') }}">
                        <div class="error">{{ $errors->first('adresa') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="mesto">Mesto*</label>
                        <input id="mesto" name="mesto" type="text" class="login-form__input form-control" value="{{ Input::old('mesto') }}">
                        <div class="error">{{ $errors->first('mesto') }}</div>
                    </div>
                    <p class="autoWidth">Kontakt osoba </p> 
                    <div class="field-group">
                        <label for="kontakt_osoba">Ime i prezime*</label>
                        <input id="kontakt_osoba" name="kontakt_osoba" type="text" class="login-form__input form-control" value="{{ Input::old('kontakt_osoba') }}">
                        <div class="error">{{ $errors->first('kontakt_osoba') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="telefon">Telefon*</label>
                        <input id="telefon" name="telefon" type="text" class="login-form__input form-control" value="{{ Input::old('telefon') }}">
                        <div class="error">{{ $errors->first('telefon') }}</div>
                    </div> 
                    <div class="field-group">
                        <label for="mail">Email*</label>
                        <input id="mail" name="mail" type="text" class="login-form__input form-control" value="{{ Input::old('mail') }}">
                        <div class="error">{{ $errors->first('mail') }}</div>
                    </div>
                    <div class="btn-container center">
                        <button class="submit admin-login btn">REGISTRUJ SE</button> 
                    </div> 
                </form> 
                <span class="text-right">B2B ENGINE BY: 
                    <a href="https://www.selltico.com/" target="_blank"> <img src="{{ Options::base_url()}}images/logo-selltico.png">     </a>
                </span> 
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    @if(Session::has('registration_message'))
    bootbox.alert({
        message: "Uspešno ste uneli podateke. Uskoro ćete dobiti pristupne podatke."
    });
    @endif
</script>
@endsection