
<!-- <div class="popup JSinfo-popup">
    <div class="popup-wrapper">	
	    <div class="JSpopup-inner">
		 
	    </div>	
	</div>
</div> -->

<div class="popup info-confirm-popup info-popup">
    <div class="popup-wrapper">
        <div class="popup-inner"></div>
    </div> 
</div>

@foreach(B2bOptions::popup_banners() as $popup_banner) 
	@if($popup_banner->tip_prikaza == 9)
	<div class="JSfirst-popup first-popup">
		<div class="first-popup-inner relative">  
			 <a href="{{ $popup_banner->link }}"" class="relative inline-block" target="_blank">  
			 	<img class="popup-img" src="{{AdminB2BOptions::base_url()}}{{$popup_banner->img}}">	
			 </a> 
			 <span class='JSclose-me-please'>&times;</span>
	 	</div> 
	</div>
	@endif
@endforeach

@foreach(B2bOptions::pozadinska_slika() as $slika)
	<a href="{{ $slika->link }}"  class="JSleft-body-link inline-block"></a> 
	<a href="{{ $slika->link2 }}" class="JSright-body-link inline-block"></a>
@endforeach

