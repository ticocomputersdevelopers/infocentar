<div id="preheader"> 
    <div class="container">
        <div class="navbar-header navyBar">
            <button type="button" class="navbar-toggle menu-top-toggle" data-toggle="collapse" data-target="#menu-top-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
        </div>
        <div class="collapse navbar-collapse no-padding" id="menu-top-collapse">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <ul class="list-inline">
                    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/user/card">Kartica</a>
                    </li>
                    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/blog">Blog</a>
                    </li> 
                 <!--    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/servis">Servis</a>
                    </li>  -->
                    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/rma">RMA</a>
                    </li> 
                    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/kontakt">Kontakt</a>
                    </li> 
                    <li>
                        <a href="{{B2bOptions::base_url()}}b2b/brendovi">Brendovi</a>
                    </li> 
                    @foreach(B2bCommon::menu_top_items() as $row)
                    <li>
                        <a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-right"> 
                <a id="b2b-login-icon" href="/b2b/logout" class="confirm"><span>Odjavi se</span></a>&nbsp;|   
                @if(Session::has('b2b_user_'.B2bOptions::server()))
                    <a id="user_edit" href="{{route('b2b.user_edit')}}"><i class="fa fa-user"></i><span> {{ B2bPartner::getPartnerName() }}</span></a>
                @endif
                @if(Session::has('b2c_admin'.B2bOptions::server()))
                &nbsp;| <a target="_blank" href="{{ Options::base_url() }}admin"> Admin Panel</a>
                @endif
            </div>
        </div>
    </div>
</div>
