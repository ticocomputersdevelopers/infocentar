<footer class="sm-text-center">
    <div class="container">
       <div class="row pre-footer"> 
           <div class="col-md-3 col-sm-3 col-xs-12 newsletter-text">
               <i class="glyphicon glyphicon-envelope"></i>
               <p>Newsletter</p>
           </div>
            <div class="col-md-5 col-sm-5 col-xs-12 newsletter-input">
                <div class="relative"> 
                    @if(Options::newsletter()==1)
                        <input type="text" class="" placeholder="E-mail" id="newsletter" />
                        <button onclick="newsletter()">Prijavi se</button>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 social-icons text-center"> 
                {{Options::social_icon()}}
            </div>  
        </div>
 
        <div class="row footer-lists"> 
            <div class="col-md-3 col-sm-3 col-xs-12">
                <ul>
                    @foreach (B2bCommon::menu_footer() as $row)
                    <li>
                        <a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
  <!--           <div class="col-md-6 col-sm-6 col-xs-12 partners">
                <ul class="list-inline text-center">
                    <li><img class="img-responsive" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/Bosch-logo.png"></li>
                    <li><img class="img-responsive" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/Siemens_AG_logo.svg.png"></li>
                    <li><img class="img-responsive" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/1000px-Intel-logo.svg.png"></li>
                    <li><img class="img-responsive" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/amd-elite.png"></li>
                 
                </ul>
            </div> -->
            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-6">
                <ul> 
                    <li>{{ Options::company_adress() }} {{ Options::company_city() }}</li>
                    @if(Options::company_phone() != '')
                    <li><i class="fa fa-phone"></i> {{ Options::company_phone() }} </li>
                    @endif
                    @if(Options::company_fax() != '')
                    <li><i class="fa fa-fax"></i> {{ Options::company_fax() }}</li>
                    @endif
                    @if(Options::company_email() != '')
                    <li><i class="glyphicon glyphicon-envelope"></i><a href="mailto:{{ Options::company_email() }}"> {{ Options::company_email() }} </a></li>
                    @endif
                </ul>
            </div>
        </div>
 
        <div class="row foot-note">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                 <p>{{B2bOptions::company_name() }} &copy; {{ date('Y') }}. Sva prava zadržana. - <a href="https://www.selltico.com/">Izrada internet prodavnice</a> - 
                    <a href="https://www.selltico.com/"> Selltico. </a></p>
            </div> 
        </div>
    </div>
 
    <a class="JSscroll-top" href="javascript:void(0)">
        <i class="fa fa-angle-up"></i>
    </a>

</footer>
 

