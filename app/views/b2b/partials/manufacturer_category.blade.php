<nav class="manufacturer-categories"> 
    <h3 class="text-center">Proizvođač - {{$proizvodjac}}</h3>
    <ul class="row">
        @foreach(B2bCommon::manufacturer_categories($proizvodjac_id) as $key => $value)
            <li class="col-md-12 col-sm-12 col-xs-12">
                <a  class="" href="{{ Options::base_url()}}b2b/proizvodjac/{{ Url_mod::url_convert($proizvodjac) }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ $key }}</span>
                    <span class="">{{ $value }}</span>
                </a>
            </li>
        @endforeach  
    </ul>
</nav>
