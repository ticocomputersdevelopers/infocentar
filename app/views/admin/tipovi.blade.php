<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi tip</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/tip/0">Dodaj novi</option>
						@foreach(AdminSupport::getTipovi(true) as $row)
							<option value="{{ AdminOptions::base_url() }}admin/tip/{{ $row->tip_artikla_id }}" @if($row->tip_artikla_id == $tip_artikla_id) {{ 'selected' }} @endif>({{ $row->tip_artikla_id }}) {{ $row->naziv }} {{$row->rbr}}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/tip-edit" enctype="multipart/form-data">
					  	<input type="hidden" name="tip_artikla_id" value="{{ $tip_artikla_id }}">

					  	<div class="row">
							<div class="columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">Naziv tipa</label>
								<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus">
							</div>
						</div>

					    <div class="row">
							<div class="columns medium-6 field-group">
								<label>Aktivan</label>
								<select name="active">
									@if(Input::old('active'))
										@if(Input::old('active'))
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@else
										@if($active)
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@endif
								</select>
							</div>
							<div class="columns medium-6 field-group{{ $errors->first('rbr') ? ' error' : '' }}">
								<label for="rbr">Redni broj</label>
								<input type="text" name="rbr" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}">
							</div>
						</div>

						<div class="row"> 
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								@if($tip_artikla_id != 0)
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/tip-delete/{{ $tip_artikla_id }}">Obriši</button>
			 					@endif
							</div>
						</div>
					 
				</form>
			</div>
	</section>
</div> <!-- end of .flat-box -->
				
 