<html>
	<head>
		<title>Export Shopmania</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
       
        <style>

* { box-sizing: border-box; }

.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.text-center { text-align: center; }

header { background-color: #f2f2f2; padding: 30px 0; }

.custom-col { padding: 20px 0; }

.custom-btn { background: #a6a6a6; padding: 10px 15px; }

.select2-container--default .select2-search--inline .select2-search__field { margin: 0; }

.select2-results__option {  padding: 0 5px !important; font-size: 14px; }

        </style>
	</head>
	<body>

		<form method="POST" action="{{AdminOptions::base_url()}}export/shopmania-config/{{ $key }}">
			<input type="hidden" name="export_id" value="{{ $export_id }}">
			<input type="hidden" name="kind" value="{{ $kind }}">

			<header class="text-center">
			  	<h1>Export - Shopmania</h1>
			</header>

			<div class="row">
			 
				 
				<div class="col-5 custom-col">
						<div><input name="name" type="checkbox" checked> Naziv</div>
						<div><input name="category" type="checkbox" checked> Kategorije</div>
						<div><input name="category_photo" type="checkbox"> Slika kategorije</div>
						<div><input name="manufacturer" type="checkbox" checked> Proizvođač</div>
						<div><input name="description_meta" type="checkbox" checked> Meta opis</div>
						<div><input name="keywords" type="checkbox" checked> Ključne reči</div>
						<div><input name="price" type="checkbox" checked> Web Cena</div>
						<div><input name="mp_price" type="checkbox" checked> MP Cena</div>
						<div><input name="currency" type="checkbox" checked> Valuta</div>
					</div>

				 <div class="col-5 custom-col">
						<div><input name="photo" type="checkbox" checked> Slika</div>
						<div><input name="url" type="checkbox" checked> URL proizvoda</div>
						<div><input name="availability" type="checkbox" checked> Dostupnost</div>
						<div><input name="stock" type="checkbox" checked> Zalihe</div>
						<div><input name="weight" type="checkbox" checked> Tezina</div>
						<div><input name="tags" type="checkbox" checked> Tagovi</div>
						<div><input name="additional_file" type="checkbox"> Dodatni fajl</div>
						<div><input name="product_video" type="checkbox"> Video proizvoda</div>
						<div><input name="description" type="checkbox" checked> Opis</div>
						<div>			
						<select id="grupa_pr_ids" name="grupa_pr_ids[]" multiple="multiple">						
							{{ AdminSupport::select2Groups() }}
						</select>
						</div>
						 
						<input type="checkbox" name="auto_export"> Auto
					</div>

				 	<div class="col-12 text-center">
				 		<button class="custom-btn" type="submit">Eksportuj</button>
				 	</div>
				 
			</div>
			
		</form>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
	</body>
	<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
	<script type="text/javascript">
	  	$("#grupa_pr_ids").select2({
	  		placeholder: 'Izaberi grupe'
	  	});
	</script>
</html>