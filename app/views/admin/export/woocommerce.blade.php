<html>
	<head>
		<title>Export Woocommerce</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
       
        <style>

* { box-sizing: border-box; }

.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.text-center { text-align: center; }

header { background-color: #f2f2f2; padding: 30px 0; }

.custom-col { padding: 20px 0; }

.custom-btn { background: #a6a6a6; padding: 10px 15px; }

        </style>
	</head>
	<body>

		<form method="POST" action="{{AdminOptions::base_url()}}export/woocommerce-config/{{ $key }}">
			<input type="hidden" name="export_id" value="{{ $export_id }}">
			<input type="hidden" name="kind" value="{{ $kind }}">

		    <header class="text-center">
				<h1>Export - Woocommerce</h1>
			</header>

			<div class="row">
		 
					<div class="col-5 custom-col">
						<div><input name="name" type="checkbox" checked> Naziv</div>
						<div><input name="category" type="checkbox" checked> Kategorije</div>
						<div><input name="keywords" type="checkbox" checked> Ključne reči</div>
						<div><input name="price" type="checkbox" checked> Web Cena</div>
						<div><input name="mp_price" type="checkbox" checked> MP Cena</div>
						<div><input name="description" type="checkbox" checked> Opis</div>
						<div><input name="short_description" type="checkbox" checked> Kratak opis</div>
						<div><input name="tags" type="checkbox"> Tagovi</div>
					</div>

					<div class="col-5 custom-col">
						<div><input name="image" type="checkbox" checked> Slika</div>
						<div><input name="small_image" type="checkbox"> Mala slika</div>
						<div><input name="weight" type="checkbox"> Tezina</div>
						<div><input name="brand" type="checkbox" checked> Brend</div>
						<div><input name="brand_link" type="checkbox"> Brend Link</div>
						<div><input name="brand_image" type="checkbox"> Brend Image</div>
						<div><input name="link" type="checkbox" checked> Link artikla</div>
						<div><input name="reviews" type="checkbox"> Broj pregleda</div>
						<div><input name="variants" type="checkbox"> Varijante artikla</div> 
					</div>

					<div class="col-12 text-center">
						<button class="custom-btn" type="submit" value="Eksportuj">Eksportuj</button>
					</div>
			</div>
			
		</form>
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
</body>
</html>