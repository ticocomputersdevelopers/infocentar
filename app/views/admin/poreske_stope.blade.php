<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi poresku stopu</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/poreske-stope">Dodaj novi</option>
					@foreach(AdminSupport::getPoreskeGrupe(false) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/poreske-stope/{{ $row->tarifna_grupa_id }}" {{ ($tarifna_grupa_id != NULL && ($row->tarifna_grupa_id == $tarifna_grupa_id)) ? 'selected' : '' }} >({{ $row->tarifna_grupa_id }}) {{ $row->naziv }} -> {{$row->porez}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->			
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/poreske-stope-edit" enctype="multipart/form-data">
					  <input type="hidden" name="tarifna_grupa_id" value="{{ $tarifna_grupa_id }}"> 

						<div class="row">
							<div class="columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">Naziv poreske stope</label>
								<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
							</div>
						</div>

						<div class="row">
							<div class="columns medium-6 field-group{{ $errors->first('porez') ? ' error' : '' }}">
								<label for="porez">Porez %</label>
								<input type="text" name="porez" value="{{ Input::old('porez') ? Input::old('porez') : $porez }}"> 
							</div>

							<!-- <div class="columns medium-6 field-group{{ $errors->first('sifra_connect') ? ' error' : '' }}">
								<label for="sifra_connect">Šifra konekt</label>
								<input type="text" name="sifra_connect" value="{{ Input::old('sifra_connect') ? Input::old('sifra_connect') : $sifra_connect }}"> 
							</div> -->

							<div class="columns medium-6 field-group{{ $errors->first('tip') ? ' error' : '' }}">
								<label for="tip">Tip</label>
								<input type="text" name="tip" value="{{ Input::old('tip') ? Input::old('tip') : $tip }}"> 
							</div>
						</div>

						<div class="row">
							<div class="columns medium-12 field-group">
								<input name="active" type="checkbox" @if($active == 1) checked @endif> Aktivan
							  	<input name="default_tarifna_grupa" type="checkbox"  @if($default_tarifna_grupa == 1) checked @endif> Default
							</div>
						</div>
						
						<div class="row">
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								@if($tarifna_grupa_id != null)
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/poreske-stope-delete/{{ $tarifna_grupa_id }}">Obriši
									</button>
								@endif
							</div>
						</div>
				 </form>
				</div>
		    </section>
		 </div>  
	  </div> <!-- end of .row -->
 