<div id="main-content" >
	@if(Admin_model::check_admin(array(200)))
    @include('admin/partials/product-tabs')
    @endif
    
	<div class="row">
		<div class="large-6 medium-6 small-6 columns large-centered medium-centered">
			<div class="flat-box">
			<h3 class="title-med">Dodaj novi fajl</h3>
			<form method="POST" action="{{AdminOptions::base_url()}}admin/dodatni_fajlovi_upload" enctype="multipart/form-data">
				@if(Session::has('mess'))
				<div class="row"> 
					<div class="column medium-12 field-group error">
						Fajl ne sme biti veci od 1MB!
					</div>
				</div>
				@endif
				<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">

				<div class="row"> 
					<div class="column medium-12 field-group {{ $errors->first('naziv') ? 'error' : '' }}">
						<label>Naziv</label>
						<input type="text" name="naziv" value="{{ Input::old('naziv') != null ?  Input::old('naziv') : null }}">
					</div>
				</div>

				<div class="row"> 
					<div class="column medium-6 field-group">
						<label>Link</label>
						<select name="check_link" id="check_link">
						{{ AdminSupport::selectCheck(Input::old('check_link') != 0 ?  1 : 0) }}
						</select>
					</div>
					 <div class="column medium-6 field-group">
						<label>Vrsta fajla</label>
						<select name="vrsta_fajla">
							@foreach(DB::table('vrsta_fajla')->orderBy('vrsta_fajla_id','asc')->get() as $row)
								@if($row->vrsta_fajla_id == Input::old('vrsta_fajla'))
								<option value="{{ $row->vrsta_fajla_id }}" selected>{{ $row->ekstenzija }}</option>
								@else
								<option value="{{ $row->vrsta_fajla_id }}">{{ $row->ekstenzija }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>

				<div class="row"> 
					<div class="column"> 
						<div id="filePutanja" class="field-group {{ $errors->first('putanja_etaz') ? 'error' : '' }}" {{ Input::old('check_link') == 0 || Input::old('check_link') == null ? 'hidden' : '' }}>
							<label>Putanja</label>
							<input type="text" name="putanja" value="{{ Input::old('putanja') != null ?  Input::old('putanja') : null }}">
						</div>

						<div id="fileFile" class="field-group {{ $errors->first('file') ? 'error' : '' }}" {{ Input::old('check_link') == 0 || Input::old('check_link') == null ? '' : 'hidden' }}>
							<div class="bg-image-file"> 
								<input type="file" name="upl_file"> 
							</div>
						</div>
					</div>
				</div>
				<div class="row"> 
					<div class="btn-container text-center"> 
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
				</div>
			</form>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="large-6 medium-6 small-12 columns large-centered medium-centered">
			@if(count($vrste_fajlova) > 0)
			<div class="filter-files">
				@foreach($vrste_fajlova as $row)
				<input type="checkbox" class="JSCheckFilter" data-action='{"roba_id":"{{ $roba_id }}", "vrsta_id":"{{ $row->vrsta_fajla_id }}"}' {{ $extension_id == $row->vrsta_fajla_id ? 'checked' : '' }}>{{ $row->ext }}
				@endforeach
			</div>
			@endif
		</div>
	</div>

	<div class="row">

		<div class="large-6 medium-6 small-12 columns large-centered medium-centered">
			@foreach($fajlovi as $row)
			<div class="flat-box files-list">
				Naziv: {{ $row->naziv }} | Vrsta: {{ AdminSupport::getExtension($row->vrsta_fajla_id) }}
				<span class="files-list-items">
					<a href="{{ AdminOptions::base_url().'admin/dodatni_fajlovi_delete/'.$roba_id.'/'.$row->web_file_id }}">Obriši</a> | 
					<a href="{{ $row->putanja != null ? AdminOptions::base_url().$row->putanja : $row->putanja_etaz }}" target="_blank">Vidi</a>
				</span>
			</div>
			@endforeach
		</div>
	</div>

</div>