<div id="main-content" class="kurs-page">
	@include('admin/partials/tabs')
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@if(Session::has('alert'))
		<script>
			alertify.error('{{ Session::get('alert') }}');
		</script>
	@endif
		<div class="row">
		<section class="small-12 medium-12 large-4 columns">
			<div class="flat-box ">	
			<h3 class="title-med">Unos kursa <i class="fa fa-eur"></i></h3>
			<form method="POST" action="{{ AdminOptions::base_url() }}admin/kurs/edit">
				<div class="row"> 
					<div class="column medium-12 after-select-margin"> 
						<select name="valuta_id" id="JSkursSelect">
							@foreach($valute as $row)
								<option value="{{ $row->valuta_id }}" {{ $row->valuta_id == $valuta_id ? 'selected' : '' }}>{{ $row->valuta_naziv }}</option>
							@endforeach
						</select> 
					</div>
				</div>
		    </div>
				<div class="flat-box clearfix">	

					<h3 class="title-med">Današnji kurs</h3>

					<section class="small-6 medium-6 large-6 columns">
						<div class="form-group {{ $errors->first('kupovni') ? 'error' : '' }}">
							<label for="">Kupovni</label>
							<input type="text" name="kupovni" value="{{ Input::old('kupovni')!=null ? Input::old('kupovni') : $tekuca_valuta->kupovni }}">
						</div>
						<div class="form-group {{ $errors->first('srednji') ? 'error' : '' }}">
							<label for="">Srednji</label>
							<input type="text" name="srednji" value="{{ Input::old('srednji')!=null ? Input::old('srednji') : $tekuca_valuta->srednji }}">
						</div>
						<div class="form-group {{ $errors->first('prodajni') ? 'error' : '' }}">
							<label for="">Prodajni</label>
							<input type="text" name="prodajni" value="{{ Input::old('prodajni')!=null ? Input::old('prodajni') : $tekuca_valuta->prodajni }}">
						</div>
					</section>
					<section class="small-6 medium-6 large-6 columns">
						<div class="form-group {{ $errors->first('ziralni') ? 'error' : '' }}">
							<label for="">Kurs za import</label>
							<input type="text" name="ziralni" value="{{ Input::old('ziralni')!=null ? Input::old('ziralni') : $tekuca_valuta->ziralni }}">
						</div>
						<div class="form-group {{ $errors->first('web') ? 'error' : '' }}">
							<label for="">Web</label>
							<input type="text" name="web" value="{{ Input::old('web')!=null ? Input::old('web') : $tekuca_valuta->web }}">
						</div>
					</section>
					<section class="small-12 medium-12 large-12 columns">
						<div class="btn-container"> 
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							<a href="{{ AdminOptions::base_url() }}admin/kurs/{{ $valuta_id }}/nbs" class="btn btn-primary">Preuzmi od NBS</a>
							<a href="{{ AdminOptions::base_url() }}admin/kurs/{{ $valuta_id }}" class="fr btn btn-danger">Otkaži</a>
						</div>
						 
					</section>
				</div>
			<form>
		</section>
		<section class="small-12 medium-12 large-8 columns">
			<div class="flat-box">
				<h3 class="title-med">Kursna lista po danima</h3>
				<div class="scroll-tb">
					<table>
						<thead>
							<td>Kurs ID</td>
							<td>Valuta</td>
							<td>Datum</td>
							<td>Kupovni</td>
							<td>Srednji</td>
							<td>Prodajni</td>
							<td>Žiralni</td>
							<td>Web</td>
						</thead>
						<tbody class="">
						@foreach($tabela_valuta as $row)
							<tr>
								<td>{{ $row->kursna_lista_id }}</td>
								<td>{{ AdminSupport::getCurrencyName($row->valuta_id) }}</td>
								<td>{{ $row->datum }}</td>
								<td>{{ $row->kupovni }}</td>
								<td>{{ $row->srednji }}</td>
								<td>{{ $row->prodajni }}</td>
								<td>{{ $row->ziralni }}</td>
								<td>{{ $row->web }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</section>
		</div>

</div>