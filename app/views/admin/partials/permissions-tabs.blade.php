	<div class="m-tabs clearfix">
		@if(Admin_model::check_admin(array(5600,5800)))
		<div class="m-tabs__tab{{ $strana=='administratori' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/administratori">Korisnici administracije</a></div>
		@endif
		@if(Admin_model::check_admin(array(6000)))
		<div class="m-tabs__tab{{ $strana=='grupa_modula' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/grupa-modula">Nivoi pristupa i moduli</a></div>
		@endif
	</div>