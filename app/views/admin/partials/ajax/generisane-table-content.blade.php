<table class="modal-karak">

	<thead>
		<td>Karakteristika</td>
		<td>Vrednost</td>
		<td>Dodeli</td>
		<td>Obriši</td>
	</thead>

	<tbody>
		@foreach($nazivi as $row)			
		<tr>
			<td>
				{{ $row->naziv }}
			</td>
			<td>
				<select class="JSGenerisaneVrednost">
					@foreach(AdminSupport::getVrednostKarak($row->grupa_pr_naziv_id) as $row1)
					<option value="{{ $row1->grupa_pr_vrednost_id }}">{{ $row1->naziv }}</option>
					@endforeach
				</select>
			</td>
			<td>
				<button class="btn btn-primary m-input-and-button__btn JSAddGenerisane" data-naziv_id="{{ $row->grupa_pr_naziv_id }}"><i class="fa fa-plus" aria-hidden="true"></i></button>
			</td>
			<td>
				<button class="btn btn-danger m-input-and-button__btn JSDeleteGenerisane" data-naziv_id="{{ $row->grupa_pr_naziv_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
			</td>
		</tr>
		@endforeach
	</tbody>
	
</table>