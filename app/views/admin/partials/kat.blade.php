
<?php
$query_gr=AdminSupport::query_gr_level(0);
?>

<!-- LEVEL 1 -->
<div id="jstree" class="categories large-12 medium-12 columns {{Session::get('adminCategories')}}">
	<ul>
	<li class="jstree-open">
		<a class="li-item__link__text" href="{{AdminOptions::base_url()}}admin/artikli/0">GRUPE</a>
		<ul class="">
			@foreach($query_gr as $row)
				<li class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row->grupa_pr_id) }}">

					<a class="li-item__link__text" href="{{AdminOptions::base_url()}}admin/artikli/{{ $row->grupa_pr_id }}">{{ $row->grupa }}</a>
					<?php $query_gr1=AdminSupport::query_gr_level($row->grupa_pr_id); ?>
					<!-- LEVEL 2 -->
					<ul class="">
						@foreach($query_gr1 as $row1)
						<li class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row1->grupa_pr_id) }}">
							<a class="li-item__link__text" href="{{AdminOptions::base_url()}}admin/artikli/{{ $row1->grupa_pr_id }}">{{ $row1->grupa }}</a>
							<?php $query_gr2=AdminSupport::query_gr_level($row1->grupa_pr_id); ?>
							<!-- LEVEL 3 -->
							<ul class="">
								@foreach($query_gr2 as $row2)
									<li class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row2->grupa_pr_id) }}">
										<a class="li-item__link__text" href="{{AdminOptions::base_url()}}admin/artikli/{{ $row2->grupa_pr_id }}">{{ $row2->grupa }}</a>
									<?php $query_gr3=AdminSupport::query_gr_level($row2->grupa_pr_id); ?>
									<!-- LEVEL 4 -->
										<ul class="">
											@foreach($query_gr3 as $row3)
												<li class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row3->grupa_pr_id) }}">
													<a class="li-item__link__text" href="{{AdminOptions::base_url()}}admin/artikli/{{ $row3->grupa_pr_id }}">{{ $row3->grupa }}</a>
												</li>
											@endforeach
										</ul>
									</li>
								@endforeach
							</ul>
						</li>
						@endforeach
					</ul>
				</li>
			@endforeach
		</ul>
	</li>
	</ul>
</div>










@include('admin.partials.custom_menu')






















