<div class="m-tabs clearfix">
	<div class="m-tabs__tab{{ $strana == 'product' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product/{{ $roba_id }}">ARTIKAL </a></div>
	@if(Admin_model::check_admin(array(200)))
	<div class="m-tabs__tab{{ $strana == 'slike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_slike/{{ $roba_id }}">SLIKE </a></div>
	@endif
	@if(Admin_model::check_admin(array(220)))
	<div class="m-tabs__tab{{ $strana == 'opis' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_opis/{{ $roba_id }}">OPIS </a></div>
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'karakteristike' || $strana == 'generisane' || $strana == 'dobavljac_karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_karakteristike/{{ $roba_id }}">KARAKTERISTIKE </a></div>
	@endif
	<div class="m-tabs__tab{{ $strana == 'osobine' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_osobine/{{ $roba_id }}/0">OSOBINE </a></div>
	@if(Admin_model::check_admin(array(5219)))
	<div class="m-tabs__tab{{ $strana == 'vezani_artikli' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/vezani_artikli/{{ $roba_id }}">VEZANI ARTIKLI </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'product_akcija' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija/{{ $roba_id }}">AKCIJA </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'dodatni_fajlovi' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/dodatni_fajlovi/{{ $roba_id }}">FAJLOVI </a></div>
	@endif
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'seo' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_seo/{{ $roba_id }}">SEO I TAGOVI </a></div>
	@endif
	@if(Admin_model::check_admin(array(200)))
	<div class="m-tabs__tab"><a href="{{AdminArticles::article_link($roba_id)}}" target="_blank">VIDI ARTIKAL </a></div>
	@endif
</div>