
	<div class="m-tabs clearfix">
		@if(Admin_model::check_admin(array(256)))
		<div class="m-tabs__tab{{ $strana=='karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_karakteristike/{{ $roba_id }}">HTML KARAKTERISTIKE </a></div>
		@endif
		@if(Admin_model::check_admin(array(257)))
		<div class="m-tabs__tab{{ $strana=='generisane' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_generisane/{{ $roba_id }}">GENERISANE KARAKTERISTIKE</a></div>
		@endif
		@if(Admin_model::check_admin(array(258)) AND Admin_model::check_admin(array(400)))
		<div class="m-tabs__tab{{ $strana=='dobavljac_karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/dobavljac_karakteristike/{{ $roba_id }}">OD DOBAVLJAČA</a></div>
		@endif
	</div>