<?php
	if($datum_od==0 and $datum_do==0){
	   $dat1='';
       $dat2='';
	}else 
    if($datum_od!=0 and $datum_do==0){
	   $dat1=$datum_od;
       $dat2='';
	}else 
    if($datum_od==0 and $datum_do!=0){
	   $dat1='';
       $dat2=$datum_do;
	}else 
    {
	   $dat1=$datum_od;
       $dat2=$datum_do;
	}
?>

<section id="main-content" class="orders-page row">
	<!-- ORDER LIST -->
	<section class="articles-listing medium-12 columns orders-listing">


		<!--=============================
		=            Filters            =
		==============================-->
		<div class="row"> 
			<div class="column medium-4"> 
				<h1 class="title-filters-h1">Narudžbine</h1>
	  			<!-- ====================== -->
				<a style="margin: 0 0 3px 0;" href="#" class="video-manual" data-reveal-id="orders-manual">Uputstvo <i class="fa fa-film"></i></a>
			 		<div id="orders-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
						<div class="video-manual-container"> 
							<p><span class="video-manual-title">Narudžbine</span></p>
							<iframe src="https://player.vimeo.com/video/271250335" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<a class="close-reveal-modal" aria-label="Close">&#215;</a>
					</div>
			  	<!-- ====================== -->
			</div>

			<div class="orders-input column medium-4"> 
				<input type="text" class="search-field" id="search" autocomplete="on" placeholder="Pretraži narudžbine...">
				<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</div>
			<div class="columns medium-4 text-right"><a href="/admin/narudzbina/0" class="btn btn-create btn-small">Kreiraj narudžbinu</a></div>
		 </div>
  
		<ul class="o-filters row"> 
			<span class="o-filters-span-1 column medium-5">
			<label>Filteri kupca:</label> 
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="registrovani" {{  in_array('registrovani',$statusi) ? 'checked' : '' }}>
					<span class="label-text">Registrovani</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="neregistrovani" {{ in_array('neregistrovani',$statusi) ? 'checked' : '' }}>
					<span class="label-text">Neregistrovani</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="privatna" {{ in_array('privatna',$statusi) ? 'checked' : '' }}>
					<span class="label-text private">Fizička Lica</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="pravna" {{ in_array('pravna',$statusi) ? 'checked' : '' }}>
					<span class="label-text legal-person">Pravna Lica</span>
				</label>
			</li>
		</span>

		<span class="o-filters-span-1 column medium-4"> 
	   		<label>Filteri narudžbina:</label> 
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
					<span class="label-text nova">Nove</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
					<span class="label-text prihvacena">Prihvaćene</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
					<span class="label-text">Realizovane</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
					<span class="label-text stornirana">Stornirane</span>
				</label>
			</li>
		</span>

		<span class="o-filters-span-1 column medium-3">
		   	@if(AdminOptions::checkNarudzbine() == 1)
		   	<div class="column medium-9"> 
		   	<label>Filteri dodatnih statusa:</label>
				<select class="m-input-and-button__input import-select " id="narudzbina_status_id" >
					<option value="0">Svi dodatni statusi</option>
					@foreach(AdminNarudzbine::dodatni_statusi() as $statusi)
						@if($statusi->narudzbina_status_id == $narudzbina_status_id )	
							<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
						@else
							<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
						@endif				
					@endforeach
				</select>	
			</div>
			@endif

			<a href="#" class="m-input-and-button__btn btn btn-danger erase-btn">
				<span class="fa fa-times tooltipz-left" aria-label="Poništi filtere" id="JSFilterClear" aria-hidden="true"></span>
			</a>
		 </span>
 
	  </ul>

		<div class="row">
			<span class="o-filters-span-2 column medium-12"> 
				<span class="datepicker-col">
					<div class="datepicker-col__box clearfix">
						<label>Od:</label>
						<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
					</div>

					<div class="datepicker-col__box clearfix">
						<label>Do:</label>
						<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
					</div>
				</span>
			</span>
		</div>


	 
		<!--====  End of Filters  ====-->
		
	@if(AdminOptions::checkNarudzbine() == 1)

		<!-- DODATNI STATUSI -->
		<ul class="admin-list order-list-titles row hide-for-small-only">
			<li class="medium-1 columns admin-li table-head"><a href="#">Ime i prezime</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Broj</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Datum</a></li>
			<li class="medium-2 columns admin-li table-head"><a href="#">Artikli</a></li>
			<li class="medium-1 columns text-right table-head"><a href="#">Iznos</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Telefon</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Mesto</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Adresa kupca</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Dodatni- status</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Status</a></li>
			<li class="small-1 columns admin-li table-head"><a href="#">Obriši</a></li>
			<!-- <li class="medium-1 columns admin-li table-head"><a href="#">Više</a></li> -->
		</ul>
		@foreach($query as $row) 
			
			<ul>
				<li class="admin-list order-list-item row porudzbina-flex {{AdminNarudzbine::narudzbina_status_css($row->web_b2c_narudzbina_id)}}">
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Ime i prezime:</span>
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{Admin_model::narudzbina_kupac($row->web_b2c_narudzbina_id)}}</span>

					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Broj n.:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{$row->broj_dokumenta}}</span>

					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Datum:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id)}}</span>

	                <div class="medium-2 columns">
	                @foreach(AdminNarudzbine::narudzbina_stavke($row->web_b2c_narudzbina_id) as $stavka)
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Artikli:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew on-hover-visible text-left">{{ AdminArticles::find($stavka->roba_id, 'naziv_web') }}</span>
	                @endforeach
	                </div>
					
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Iznos:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li text-right">{{AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($row->web_b2c_narudzbina_id))}}</span>
	                
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Telefoni:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{AdminNarudzbine::kupacTelefoni($row->web_kupac_id)}}</span>
	            	
 					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Mesto:</span>
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{AdminNarudzbine::kupacMesto($row->web_kupac_id)}} &nbsp;</span>    
				
	                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Adresa:</span>
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns columns admin-li">{{AdminNarudzbine::kupacAdresa($row->web_kupac_id)}}
					</span>
					
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Dodatni- status:</span>
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{AdminNarudzbine::status_narudzbine($row->web_b2c_narudzbina_id)}} &nbsp;</span>
					 
					<span class="medium-1 columns admin-li right">
					
						<section class="order-status"> 
							<select class="order-status__select">
								<option class="order-status-active">{{AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</option>
								{{AdminNarudzbine::narudzbina_status_posible(AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id),$row->web_b2c_narudzbina_id)}}
							</select>
						</section>
						
					</span>

					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Obriši:</span>
					<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSDeleting medium-1 columns admin-li"><i class="fa fa-times"></i>
					</span>
			</li>
		</ul>
        <hr class="show-for-small-only">
		
@endforeach
	
		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}
		
		<!-- OBICAN PREGLED NARUDZBINA	 -->
	@else
		<ul class="admin-list order-list-titles row hide-for-small-only">
			<li class="medium-2 columns admin-li table-head"><a href="#">Ime i prezime</a></li>
			<li class="medium-2 columns admin-li table-head"><a href="#">Broj narudžbine</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Datum</a></li>
			<li class="medium-2 columns admin-li table-head"><a href="#">Iznos</a></li>
			<li class="medium-2 columns admin-li table-head"><a href="#">Adresa kupca</a></li>
			<li class="medium-2 columns admin-li table-head"><a href="#">Telefoni</a></li>
			<li class="medium-1 columns admin-li table-head"><a href="#">Status</a></li>

			<!-- <li class="medium-1 columns admin-li table-head"><a href="#">Više</a></li> -->
		</ul>
		@foreach($query as $row) 
		<ul>
			<li class="admin-list order-list-item row porudzbina-flex {{AdminNarudzbine::narudzbina_status_css($row->web_b2c_narudzbina_id)}}">
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Ime i prezime:</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-2 columns admin-li">{{Admin_model::narudzbina_kupac($row->web_b2c_narudzbina_id)}}</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Broj n.:</span>
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-2 columns admin-li">{{$row->broj_dokumenta}}</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Datum:</span>
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-1 columns admin-li">{{AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id)}}</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Iznos:</span>
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-2 columns admin-li">{{AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($row->web_b2c_narudzbina_id))}}</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Adresa:</span>
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-2 columns admin-li">{{AdminNarudzbine::kupacAdresa($row->web_kupac_id)}}</span>
				<span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Telefoni:</span>
                <span data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew medium-2 columns admin-li">{{AdminNarudzbine::kupacTelefoni($row->web_kupac_id)}}</span>

				<span class="medium-1 columns admin-li">
					<section class="order-status">
						<select class="order-status__select">
							<option class="order-status-active">{{AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</option>
							{{AdminNarudzbine::narudzbina_status_posible(AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id),$row->web_b2c_narudzbina_id)}}
						</select>
					</section>
				</span>
				<!-- <span class="medium-1 columns admin-li">
					<div class="btn-container center no-margin">
						<span class="JSMoreWiew btn btn-primary btn-xm btn-circle small" data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}"><i class="fa fa-info" aria-hidden="true"></i></span>
					</div>
				</span> -->
			</li>
		</ul>
        <hr class="show-for-small-only">
		@endforeach

		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}
		@endif
	</section>
	
</section> <!-- end of #main-content -->

<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="ordersDitailsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="content"></div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->