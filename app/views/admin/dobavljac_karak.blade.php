<div id="main-content" class="">
@include('admin/partials/product-tabs')
	@include('admin/partials/karak-tabs')

	<input id="roba_id_obj" type="hidden" value="{{ $roba_id }}">

	<div class="row"> 
		<div class="columns medium-6 large-centered medium-centered">
			<div class="flat-box">
				<h2 class="title-med">Karakteristike</h2>
				<table role="grid" class="karakteristike-tabela">
					@if(count($grupe_karakteristika))
						@foreach($grupe_karakteristika as $row)
							<tr class="JSGrupaNazivData inline-list">
								<td><input type="text" class="JSGrupaRbr ordered-number" value="{{ $row->redni_br_grupe }}"></td>
								<td><input type="text" class="JSGrupaNaziv" value="{{ $row->karakteristika_grupa }}"></td>
								<td>
									<button class="JSGrupa name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-old="{{ $row->karakteristika_grupa }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
								</td>
								<td>
									<button class="JSGrupaDelete name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-old="{{ $row->karakteristika_grupa }}"><i class="fa fa-times" aria-hidden="true"></i></button>
								</td>
								<td>
									<table>
										@foreach(AdminSupport::getGrupeNazivKarak($roba_id,$row->karakteristika_grupa) as $row2)
										<tr>
											<td><input type="text" class="JSKarakNaziv" value="{{ $row2->karakteristika_naziv }}"></td>
											<td><input type="text" class="JSKarakVrednost" value="{{ $row2->karakteristika_vrednost }}"></td>
											<td>
												<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="{{ $row2->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</td>
											<td>
												<button class="JSKarakDelete name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-id="{{ $row2->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
											</td>
										</tr>
										@endforeach
										<tr>
											<td><input type="text" class="JSKarakNaziv"></td>
											<td><input type="text" class="JSKarakVrednost"></td>
											<td>
												<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						@endforeach
							<tr class="inline-list">
								<td></td>
								<td><input type="text" class="JSGrupaNaziv"></td>
								<td>
									<button class="JSGrupa name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-old="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
								</td>
								<td></td>
								<td></td>
							</tr>
					@else
						@foreach(AdminSupport::getDobavljKarak($roba_id) as $row)
						<tr>
							<td><input type="text" class="JSKarakNaziv" value="{{ $row->karakteristika_naziv }}"></td>
							<td><input type="text" class="JSKarakVrednost" value="{{ $row->karakteristika_vrednost }}"></td>
							<td>
								<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="{{ $row->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
							</td>
							<td>
								<button class="JSKarakDelete name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-id="{{ $row->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
							</td>
						</tr>
						@endforeach
						@if(count(AdminSupport::getDobavljKarak($roba_id)) > 0)
						<tr>
							<td><input type="text" class="JSKarakNaziv"></td>
							<td><input type="text" class="JSKarakVrednost"></td>
							<td>
								<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
							</td>
						</tr>
						@endif
					@endif
				</table>
			</div>	
		</div>
	</div>
</div>