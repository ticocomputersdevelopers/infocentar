<div id="main-content" class="article-edit">
@if(Session::has('message_success'))
	<script>
		alertify.success('{{ Session::get('message_success') }}');
	</script>
@endif
@if(Session::has('message_delete'))
	<script>
		alertify.success('{{ Session::get('message_delete') }}');
	</script>
@endif

	<?php
	$old = count(Input::old()) ? 1 : 0;
	// echo '<pre>';
	// var_dump(Input::old()); die();
	?>

	<div class="row">
		@if(Session::has('message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">Artikal ima istoriju!</div> 
			<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}/1">Ipak obriši!</a>
		</div>
		@endif
		@if(Session::has('limit_message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">Unošenje novih artikala nije dozvoljeno!</div>
		</div>
		@endif
		<form method="POST" action="{{AdminOptions::base_url()}}admin/product-edit-short" enctype="multipart/form-data" id="JSArticleForm">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
			<input type="hidden" name="clone_id" value="{{ $clone_id ? $clone_id : 0 }}">
			<input type="hidden" name="orgj_id" value="{{ $orgj_id }}">
			<input type="hidden" name="jedinica_mere_id" value="{{ $jedinica_mere_id }}">
			<input type="hidden" name="tarifna_grupa_id" value="{{ $tarifna_grupa_id }}">

			<div class="clearfix floating-field-group">

			<div class="columns medium-10 cut-padding medium-centered">
				<div class="row article-edit-box">
					<div class="columns medium-10">
						<div class="naziv-web-big field-group">
							<label>Naziv na web-u</label>
							<input type="text" name="naziv_web" value="{{ htmlentities($old ? Input::old('naziv_web') : $naziv_web) }}">
							<div class="error">{{ $errors->first('naziv_web') }}</div>
						</div>
					</div>
					<div class="columns medium-2 no-padd">
						<label>&nbsp;</label>
					  	@if($roba_id!=0)
						<a href="{{AdminArticles::article_link($roba_id)}}" target="_blank" class="btn btn-secondary btn-small">Vidi artikal </a>
					 	@endif
					</div>
				</div>

			 <div class="article-edit-box">
			    <div class="row"> 
					<div class="columns medium-2">
						<div class="field-group field-group">
							<label>SKU</label>
							<input type="text" name="sku" value="{{ htmlentities($old ? Input::old('sku') : $sku) }}">
							<div class="error">{{ $errors->first('sku') }}</div>
						</div>
					</div>
					<div class="columns medium-5">
						<div class="field-group">
							<label>Grupa</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="grupa_pr_grupa" value="{{Input::old('grupa_pr_grupa') ? Input::old('grupa_pr_grupa') : $grupa_pr_grupa }}" autocomplete="off">
							</div>
							<div class="short-group-container"> 
								{{ AdminSupport::listGroups() }}
							</div>
							<div class="error">{{ $errors->first('grupa_pr_grupa') }}</div>
						</div>
					</div>
	   		  		<div class="columns medium-3">
						<div class="field-group">
							<label>Stanje artikla</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="roba_flag_cene" value="{{Input::old('roba_flag_cene') ? Input::old('roba_flag_cene') : $roba_flag_cene }}" autocomplete="off">
							</div>
							<div class="short-group-container"> 
								<ul id="JSListaFlagCene" hidden="hidden">
									@foreach(AdminSupport::getFlagCene() as $row)
									<li class="JSListaFlagCena" data-roba_flag_cene="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('roba_flag_cene') }}</div>
						</div>
					</div>
					<div class="columns medium-2">
						<div class="field-group">
							<label>Količina</label>
							<input type="text" name="kolicina" value="{{ $old ? Input::old('kolicina') : $kolicina }}">
							<div class="error">{{ $errors->first('kolicina') }}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="article-edit-box">
				<div class="row">
					<div class="columns medium-2">					
						<div class="field-group">
							<label>Na Web-u </label>
							<select name="flag_prikazi_u_cenovniku">
							@if($old)
								@if(Input::old('flag_prikazi_u_cenovniku'))
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@else
								@if($flag_prikazi_u_cenovniku)
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@endif
							</select>
						</div>	
					</div>

					<div class="columns medium-2">
						<div class="field-group">
							<label>Akcija</label>
							<select name="akcija_flag_primeni" id="JSAkcijaFlagPrimeni">
							@if($old)
								@if(Input::old('akcija_flag_primeni'))
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@else
								@if($akcija_flag_primeni)
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@endif
							</select>
						</div>
					</div>

					<div class="columns medium-4">
						<div class="field-group">
							<label>Tip artikla</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="tip_artikla" value="{{Input::old('tip_artikla') ? Input::old('tip_artikla') : $tip_artikla }}" autocomplete="off">
							</div>
							<div class="short-group-container"> 
								<ul id="JSListaTipaArtikla" hidden="hidden">
									<li class="JSListaTipArtikla" data-tip_artikla="">Bez tipa</li>
									@foreach(AdminSupport::getTipovi() as $row)
									<li class="JSListaTipArtikla" data-tip_artikla="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('tip_artikla') }}</div>
						</div>
					</div>

			 
					<div class="columns medium-2">
						<div class="field-group columns large-5">
						<label>Web cena</label>
						<input type="text" name="web_cena" class="JSCenaChange" value="{{ $old ? Input::old('web_cena') : $web_cena }}" {{ AdminArticles::vrsteCena('WEB') ? '' : 'disabled' }}>
						<div class="error">{{ $errors->first('web_cena') }}</div>
						</div>
					 </div>

					 <div class="columns medium-2">
						<div class="field-group columns large-6 medium-6 {{ $errors->first('akcijska_cena') ? 'error' : '' }}">
						<label>Akcijska cena</label>
						<input type="text" name="akcijska_cena" id="JSAkcijskaCena" value="{{ $old ? Input::old('akcijska_cena') : $akcijska_cena }}" {{ AdminArticles::vrsteCena('NC') ? '' : 'disabled' }}>
						</div>
					</div>

					<div class="columns medium-12 no-padd">
						<div class="short-product-action" id="JSAkcijaDatum" {{ (Input::old('akcija_flag_primeni') ? Input::old('akcija_flag_primeni') : $akcija_flag_primeni) == 0 ? 'hidden' : '' }}>
							<div class="column medium-2">
								<label>Datum od</label>
								<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_od') ? Input::old('datum_akcije_od') : $datum_akcije_od }}">
								<span id="datum_od_delete">&times;</span>
							</div>
							<div class="column medium-2">
								<label>Datum do</label>
								<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_do') ? Input::old('datum_akcije_do') : $datum_akcije_do }}">
								<span id="datum_do_delete">&times;</span>
							</div>
					 	</div>
					 </div>
				</div>
			</div>
	 		
			 <div class="article-edit-box">
				 <div class="row">
				 	<div class="column medium-12">
						<h3 class="title-med">Slike</h3>
					</div>
					@foreach(AdminArticles::getSlike($roba_id) as $row)
					<div class="column medium-2 col-slika">
						<div class="col-slika-ads"> 
							<div class="radio-button-field">
								<span class="tooltipz" aria-label="Glavna slika">
								<input type="radio" name="akcija" class="akcija" value="{{ $row->web_slika_id }}" @if($row->akcija) checked @endif></span>	
								<a data-link="{{AdminOptions::base_url()}}admin/slika-delete/{{ $roba_id }}/{{$row->web_slika_id}}/true" class="tooltipz right JSSlikaDeleteShort" aria-label="Obriši"> 
								&times;
								</a> 
							</div>
							<img src="{{ AdminOptions::base_url().$row->putanja }}">
						</div>
					</div>
					@endforeach
	 				<div id="JSUploadImages" class="column medium-12 short-article-img">				 
						<div class="field-group {{ $errors->first('slike') ? 'error' : '' }}">
							<div class="bg-image-file"> 
								<input type="file" name="slike[]" multiple>
							</div>
						</div>
						<div class="error">{{ $errors->first('slike') ? $errors->first('slike') : '' }}</div>			 
					</div>
				 </div>
			</div>

			 <div class="article-edit-box">
				 <div class="row">
<!-- 					<div class="columns medium-3">
						<div class="">
							<h3 class="title-med">Karakteristike</h3>
							<div id="JSGrupaKarakteristike" class="GrupaKarakteristike">
							include('admin/partials/ajax/grupa_generisane')
							</div>
						</div>
					</div> -->
					<div class="columns medium-12"> 
						<h3 class="title-med">Opis</h3>
						<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
						<textarea class="special-textareas" id="mc" name="web_opis">{{ Input::old('web_opis') ? Input::old('web_opis') : $web_opis }}</textarea>
					</div>
				</div>
			</div>

			<div class="row article-edit-box">
				<div class="columns large-12 medium-12">
					<div class="btn-container center">
						@if(isset($clone_id))
						<input type="checkbox" name="slike_clone" checked>Kloniraj slike
						@endif
						<button type="submit" id="JSArticleSubmit" class="btn btn-primary save-it-btn">Sačuvaj</button> 
						 
						@if($roba_id != 0)
						<a class="Deleting1 btn btn-danger" >Obriši</a>
							@if(Admin_model::check_admin(array(2400)))
							<a class="btn-secondary btn" href="{{AdminOptions::base_url()}}admin/product/0/{{ $roba_id }}">Kloniraj artikal</a>
							@endif
						@endif

						<a href="#" id="JSArtikalRefresh" class="btn btn-primary">Poništi izmene</a>
						<a href="{{AdminOptions::base_url()}}admin/product/{{$roba_id}}" class="btn btn-primary">Detaljnija izmena </a>
					</div>
				</div>
			</div>
		</div> 
		</div>
		</form>
	</div>  
</div>
 


