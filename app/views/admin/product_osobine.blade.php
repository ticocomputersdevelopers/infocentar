<div id="main-content" class="article-edit">
	
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif

	@if(Session::has('alert'))
		<script>
			alertify.error('{{ Session::get('alert') }}');
		</script>
	@endif

	<div class="row m-subnav">	
		@include('admin/partials/product-tabs')
	</div>
	
	<div class="row">
		<div class="columns medium-6 large-centered medium-centered">
			<div class="flat-box">
				<h1 class="title-med">Dodavanje osobine</h1>
				
			  	<form method="POST" action="{{ AdminOptions::base_url() }}admin/product_osobine/{{$roba_id}}">
				     <div class="row">
						<div class="columns medium-4 after-select-margin">							
							<select class="JSosobine" name="osobina_naziv_id">
								<option data-roba="{{ $roba_id }}">Izaberite osobinu</option>
								@foreach($naziv as $row)
								<option data-roba="{{ $roba_id }}" data-id="{{ $row->osobina_naziv_id }}" @if($row->osobina_naziv_id == $osobina_naziv_id) selected @endif>{{ $row->naziv }}</option>
								@endforeach
							</select>
						</div>
						<div class="column medium-8 after-select-margin no-padd">
<!-- 							<button type="submit" class="btn-primary btn btn-small"><i class="fa fa-plus" aria-hidden="true"></i> Dodaj</button>
 -->						<a href="{{ AdminOptions::base_url() }}admin/product_osobine/{{$roba_id}}/{{$osobina_naziv_id}}/all" class="btn-secondary btn btn-small">                                   <i class="fa fa-plus-square-o" aria-hidden="true"></i> Dodaj </a>
 									<a href="{{ AdminOptions::base_url() }}admin/product_osobine/{{ $roba_id }}/{{ $osobina_naziv_id }}/0/deletegroup" class="btn-secondary btn btn-small"><i class="fa fa-trash-o" aria-hidden="true"></i> Obriši izabrane osobine</a>
							<a href="{{ AdminOptions::base_url() }}admin/product_osobine/{{ $roba_id }}/{{ $osobina_naziv_id }}/0/deleteall" class="btn-secondary btn btn-small"><i class="fa fa-trash-o" aria-hidden="true"></i> Obriši sve</a>
						</div>

					  </div>
				 </form>
				
			 </div>

			<div class="flat-box">
				<h1 class="title-med">Osobine artikla</h1>
				<div class="row">

				@foreach($osobine_artikla as $row)
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/product_osobine/{{$roba_id}}/{{$row->osobina_naziv_id}}">
					<div class="row property">
						<div class="columns medium-9 no-padd"> 
						<span class="columns medium-3"><strong>{{ AdminOsobine::findProperty($row->osobina_naziv_id, 'naziv') }}</strong></span>
							<span class="columns medium-3 no-padd">
								<select name="osobina_vrednost_id">
									<option value="0">Izaberite</option>
									@foreach(AdminOsobine::getPropertyValues($row->osobina_naziv_id) as $row2)
										<option value="{{$row2->osobina_vrednost_id}}" {{ $row->osobina_vrednost_id == $row2->osobina_vrednost_id ? 'selected' : '' }}>{{ $row2->vrednost }}</option>
									@endforeach
								</select>
								 <input type="hidden" name="old_vrednost_id" value="{{ $row->osobina_vrednost_id }}" />
							</span>

						  	<span class="columns medium-3 no-padd">
								<input name="aktivna" type="checkbox" @if($row->aktivna == 1) checked @endif /> Aktivno
							</span>

							<span class="columns medium-3 no-padd">
								<label>Redni broj</label>
								<input class="ordered-number" name="rbr" type="text" value="{{ $row->rbr }}" />
							</span>
						</div>

						<div class="columns medium-3 btn-container no-padd">
							<button type="submit" class="btn btn-primary save-it-btn"> Sačuvaj</button>
							<a href="{{ AdminOptions::base_url() }}admin/product_osobine/{{ $roba_id }}/{{ $row->osobina_naziv_id }}/{{ $row->osobina_vrednost_id }}/delete" class="btn btn-danger"> Obriši</a>
						</div>
					</div>
				</form>
				@endforeach
				
				</div>
			</div>
		</div>
	</div>
</div>