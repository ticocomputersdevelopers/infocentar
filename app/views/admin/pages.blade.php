@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
@endif
<section id="main-content" class="pages-page">
	<section class="medium-3 columns">
		<div class="flat-box">
			<h3 class="text-center h3-margin">Strane:</h3>
			<ul class="page-list lista" id="JSPagesSortable">
				<li class="new-page new-elem" id="0">
					<a href="{{AdminOptions::base_url()}}admin/stranice/0">Nova strana</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($query_list_pages as $row)
				<li class="{{  (AdminSupport::parrent_strana($row->web_b2c_seo_id) == 1 )   ? 'parrent' : '' }} ui-state-default @if($row->web_b2c_seo_id == $web_b2c_seo_id) active @endif" id="{{$row->web_b2c_seo_id}}" {{$row->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
					<a href="{{AdminOptions::base_url()}}admin/stranice/{{$row->web_b2c_seo_id}}">{{$row->title}}</a>
				</li>
				@endforeach
			</ul>
		</div>
			<!-- uredi footer -->
<!-- 	<form class="pages-form" method="post" action="{{AdminOptions::base_url()}}admin/footer">
		<input type="submit" value="Uredi footer" class="btn btn-danger">
	</form> -->
	</section>
	<section class="page-edit medium-9 columns">
		<div class="flat-box">
			<h3 class="text-center h3-margin">Sadržaj:</h3>
			
			<!-- PAGE EDIT TOP -->
			
			<form name="form_pages" action="{{AdminOptions::base_url()}}admin/pages" method="post">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">
				<section class="page-edit-top">
					<div class="row">
						<div class="row">
							<div class="medium-5 columns page-edit-name">
								<label>Naziv strane:</label>
								<input type="text" id="page_name" name="page_name" value="{{Input::old('page_name') ? Input::old('page_name') : $naziv}}" {{$disable==1?'disabled="disabled"':''}} onchange="check_fileds('page_name')">
							</div>
							<div class="medium-7 columns include-in-menu">

							<label>Prikaži u:</label>
							@if(!in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(7,8,9)))
							<label class="checkbox-label medium-4 columns">                 
	                            <input type="checkbox" id="top_menu" name="top_menu" {{ Input::old('top_menu') ? 'checked="checked"' : ($menu_top ? 'checked="checked"' : '') }}>
	                            <span class="label-text">Top meniju</span>
	                        </label>
	                        @endif
	                        <label class="checkbox-label medium-4 columns">					
								<input type="checkbox" id="header_menu" name="header_menu" {{ Input::old('header_menu') ? 'checked="checked"' : ($header_menu ? 'checked="checked"' : '') }}>
	                            <span class="label-text">Header meniju</span>
							</label>
			<!-- 				<label class="checkbox-label medium-4 columns">	
								<input type="checkbox" id="footer_menu" name="footer_menu" {{ Input::old('footer_menu') ? 'checked="checked"' : ($footer ? 'checked="checked"' : '') }}>
	                            <span class="label-text">Footer meniju</span>
							</label> -->
								</div>
						 </div>
						 <br>
						<div class="row">
							@if($disable==0 AND AdminOptions::is_shop())
							<div class="medium-3 columns include-in-menu">
								<label>Listaj artikle grupe:</label>  
								<select name="grupa_pr_id" class="medium-12 columns seo-button-wrapper">
									<option value="-1"></option>
									@foreach(DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get() as $grupa)
										@if(Input::old('grupa_pr_id') == $grupa->grupa_pr_id)
										<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
										@else
											@if($grupa_pr_id == $grupa->grupa_pr_id)
											<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
											@else
											<option value="{{ $grupa->grupa_pr_id }}">{{ $grupa->grupa }}</option>
											@endif
										@endif
									@endforeach
								</select>
							</div>
							<div class="medium-3 columns include-in-menu">
								<label>Listaj artikle akcije ili tipova:</label>  
								<select name="tip_artikla_id" class="medium-12 columns seo-button-wrapper">
									<option value="-1"></option>
									@if(!is_null(Input::old('tip_artikla_id')) AND Input::old('tip_artikla_id') == 0)
									<option value="0" selected>Akcija</option>
									@else
										@if($tip_artikla_id == 0)
										<option value="0" selected>Akcija</option>
										@else
										<option value="0">Akcija</option>
										@endif
									@endif
									@foreach(AdminSupport::getTipovi(1) as $tip)
										@if(Input::old('tip_artikla_id') == $tip->tip_artikla_id)
										<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
										@else
											@if($tip_artikla_id == $tip->tip_artikla_id)
											<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
											@else
											<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
											@endif
										@endif
									@endforeach
								</select>
							</div>
							@endif
							<div class="medium-4 columns include-in-menu">
								<label>Osnovna stranica </label>  
								<select name="parrent_id" class="medium-12 columns seo-button-wrapper">
									<option value="0">Izaberite stranicu ispod koje će se listati stranica</option>
									@foreach(DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get() as $strana)
						
										@if(Input::old('web_b2c_seo_id') == $strana->web_b2c_seo_id)
										<option value="{{ $strana->web_b2c_seo_id }}" selected>{{ $strana->title }}</option>
										@else
											@if($parrent_id == $strana->web_b2c_seo_id)
											<option value="{{ $strana->web_b2c_seo_id }}" selected>{{ $strana->title }}</option>
											@else
											<option value="{{ $strana->web_b2c_seo_id }}">{{ $strana->title }}</option>
											@endif
										@endif
								
									@endforeach
								</select>
							</div>
<!-- 							@if($web_b2c_seo_id == 1)
							<div class="medium-5 columns page-edit-name">
								<label>Tekst na početnoj:</label>
								<textarea type="text" id="tekst" name="tekst" > {{Input::old('tekst') ? Input::old('tekst') : $tekst}}								
								</textarea>
							</div>
							@endif -->

							<div class="medium-4 columns seo-button-wrapper">
							 	<br>
								<span class="seo-button-wrapper">
									<button class="page-edit-seo-button btn btn-secondary btn-small">SEO podešavanja</button>
								</span>
							</div>
						</div>
						<br>
						<div class="row">
							@if(count($jezici) > 1)
							<div class="languages">
								<ul>	
								@foreach($jezici as $jezik)
									<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/stranice/{{ $web_b2c_seo_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
								</ul>
							</div>
							@endif
						</div>
						
						<!-- SEO SETTINGS -->

						<section class="seo-settings row">
							<div class="medium-6 columns field-group {{ $errors->first('seo_title') ? 'error' : '' }}">
								<label>Naslov (title) do 60 karaktera:</label>
								<input id="seo-title" type="text" value="{{Input::old('seo_title') ? Input::old('seo_title') : $seo_title}}"  name="seo_title">
							</div>
							<div class="medium-6 columns field-group {{ $errors->first('keywords') ? 'error' : '' }}">
								<label>Ključne reči (keywords)</label>
								<input id="seo-keywords" type="text" value="{{Input::old('keywords') ? Input::old('keywords') : $keywords}}" name="keywords">
							</div>
							
							<div class="medium-6 columns field-group {{ $errors->first('description') ? 'error' : '' }}">
								<label>Opis (description) do 158 karaktera:</label>
								<input id="seo-description" type="text" value="{{Input::old('description') ? Input::old('description') : $desription}}" name="description">
							</div>

						</section>
					</div>
				</section>
				@if($disable==0)
				<!-- PAGE EDIT TEXT EDITOR -->
				<div class="row"> 
					<textarea class="special-textareas" name="content" id="content"  style="width:100%">
						{{$content}}
					</textarea>
				</div>
				@endif
				<input type="hidden" name="web_b2c_seo_id" value="{{$web_b2c_seo_id}}" />
				<input type="hidden" name="status" value="{{$status}}" />
				<input type="hidden" name="flag_page" id="flag_page" value="{{$flag_page}}" />
				<input type="hidden" name="flag_b2b_show" id="flag_b2b_show" value="{{$flag_b2b}}" />
				
				<div class="btn-container center">
					<a href="#" class="page-edit-save btn btn-primary save-it-btn">Sačuvaj</a>
				</div>
				
			</form>
			@if($disable==0)
			 
			@foreach($stranice as $row)
			<a href="{{ Options::base_url().$row->naziv_stranice }}" target="_blank" class="btn btn-primary" value="{{$web_b2c_seo_id}}">Vidi stranicu</a>
			@endforeach

			<form class="pages-form" method="post" action="{{AdminOptions::base_url()}}admin/delete_page">
				<input type="hidden" name="web_b2c_seo_id" value="{{$web_b2c_seo_id}}" />				
				<input type="submit" value="Obriši stranicu" class="btn btn-danger">
			</form>

			<div class="image-upload-area clearfix">	
				<form method="post" enctype="multipart/form-data" action="{{AdminOptions::base_url()}}admin/image_upload" >
					<div class="row"> 
						<div class="column"> 
							<label>Dodajte sliku na strani:</label>
				  			<div class="bg-image-file"> 
								<input type="file" id="img" name="img" class="file-input">
								<button class="file-upload btn btn-secondary btn-small" type="submit">Učitaj</button>
							</div>
							 
						</div>
				    </div>
 				</form>
				<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
					{{Admin_model::upload_directory()}}
				</div>
			</div>
	
			@endif
		</div> <!-- end of .flat-box-->
 	<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="pages-manual">Uputstvo <i class="fa fa-film"></i></a>
		   	<div id="pages-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
					<p><span class="video-manual-title">Stranice</span></p>
					<iframe src="https://player.vimeo.com/video/271253202" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		</div>
 <!-- ====================== -->
	</section>
</section>

