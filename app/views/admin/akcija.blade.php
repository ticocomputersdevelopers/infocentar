<div id="main-content" class="">
@include('admin/partials/product-tabs')

<div class="column large-6 medium-12 large-centered">	
<div class="flat-box">
<form method="POST" action="{{AdminOptions::base_url()}}admin/akcija_edit">
	<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
	 
		<div class="row">
			<div class="column medium-6 akcija-column">
				<h2 class="title-med">Akcija aktivna</h2>
				<select name="akcija_flag_primeni">
					@if(Input::old('akcija_flag_primeni') ? Input::old('akcija_flag_primeni') : $akcija_primeni)
					<option value="1" selected>DA</option>
					<option value="0" >NE</option>
					@else
					<option value="1" >DA</option>
					<option value="0" selected>NE</option>
					@endif
				</select>
			</div>
			<div class="column medium-6 akcija-column">
				<h2 class="title-med">Redni broj</h2>
				<input type="text" name="akcija_redni_broj" class="{{ $errors->first('akcija_redni_broj') ? 'error' : '' }}" value="{{ Input::old('akcija_redni_broj') ? Input::old('akcija_redni_broj') : $redni_broj }}">
			</div>
		</div>

	 <div class="row">
		<div class="column medium-6 akcija-column">
			<h2 class="title-med">Datum od</h2>
			<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_od') ? Input::old('datum_akcije_od') : $datum_akcije_od }}">
			<span id="datum_od_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
		</div>

		<div class="column medium-6 akcija-column">
			<h2 class="title-med">Datum do</h2>
			<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_do') ? Input::old('datum_akcije_do') : $datum_akcije_do }}">
			<span id="datum_do_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
		</div>
	</div>

 	<div class="row">
		<div class="column medium-6 akcija-column">
			<h2 class="title-med">{{ AdminOptions::web_options(132)==1?'Web cena':'Maloprodajna cena' }}</h2>
			<input class="<?php if($errors->first('web_cena')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="web_cena" value="{{ Input::old('web_cena') ? Input::old('web_cena') : AdminOptions::web_options(132)==1?$web_cena:$mpcena }}" {{ AdminArticles::vrsteCena('WEB') ? '' : 'disabled' }}>
		</div>
  		<div class="column medium-6 akcija-column">
			<h2 class="title-med">Akcijski popust (%)</h2>
			<input class="<?php if($errors->first('akcija_popust')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="akcija_popust" value="{{ Input::old('akcija_popust') ? Input::old('akcija_popust') : $akcija_popust }}">
		</div>
	</div>

 	<div class="row">
		<div class="column medium-6 akcija-column">
		    <h2 class="title-med">Akcijska cena</h2>
			<input class="<?php if($errors->first('akcijska_cena')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="akcijska_cena" value="{{ Input::old('akcijska_cena') ? Input::old('akcijska_cena') : $akcijska_cena }}">
		</div>
    </div>

 	<div class="row">
		<div class="btn-container center">
			<button type="submit" class="setting-button btn btn-primary save-it-btn">Sačuvaj</button>
		</div>			
	</div>
	
</form>	
</div>	
</div>
</div>
