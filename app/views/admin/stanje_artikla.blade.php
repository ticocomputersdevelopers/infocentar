<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi stanje</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/stanje-artikla">Dodaj novi</option>
					@foreach(AdminSupport::getFlagCene(true) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/stanje-artikla/{{ $row->roba_flag_cene_id }}" @if($row->roba_flag_cene_id == $roba_flag_cene_id) {{ 'selected' }} @endif>({{ $row->roba_flag_cene_id }}) {{ $row->naziv }} </option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">
 				<h1 class="title-med">{{ $title }} {{ $roba_flag_cene_id == 1 ? '(Podrazumevani)' : '' }}</h1>
				<!-- <h1 id="info"></h1> -->
 					<form method="POST" action="{{ AdminOptions::base_url() }}admin/stanje-artikla-edit" enctype="multipart/form-data">
					  <input type="hidden" name="roba_flag_cene_id" value="{{ $roba_flag_cene_id }}">
						<div class="row">
							<div class="columns medium-6 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">Naziv stanja</label>
								<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus">
							</div>
							<div class="columns medium-6 field-group">
							  	<label>Aktivno</label>
								<select name="selected">
									{{ AdminSupport::selectCheck(Input::old('selected') ? Input::old('selected') : $selected) }}
								</select>
							</div>
					 </div>
					 	<div class="row">
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								@if($roba_flag_cene_id != null AND $roba_flag_cene_id != 1)	
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/stanje-artikla-delete/{{ $roba_flag_cene_id }}">Obriši</button>
								@endif
							</div>
						</div>
				 </form>
			  </div> <!-- end of .flat-box -->
		 </section>
	</div> <!-- end of .row -->
 </div>