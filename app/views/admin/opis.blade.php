<div id="main-content" class="">
@include('admin/partials/product-tabs')

	<div class="row">
		<div class="column large-6 large-centered">
			<div class="flat-box">
				<form method="POST" action="{{AdminOptions::base_url()}}admin/opis_edit">

					<div class="row">
						<h3 class="title-med">Opis artikla</h3>
						<div class="column large-12 medium-12">
							<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
							<textarea class="special-textareas" id="mc" name="web_opis">{{ $web_opis }}</textarea>
						</div>
					</div> <!-- end of .row -->
					
					<div class="row">
						<div class="column large-12 medium-12">
							<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
							<div class="btn-container center">
								<a class="setting-button btn btn-primary" href="#" id="JSsablon">Unesi šablon</a>
								<button type="submit" class="setting-button btn btn-primary save-it-btn">Sačuvaj</button>
							</div>
						</div>	
					</div> <!-- end of .row -->
				</form>	
				 
			</div>
			<div class="image-upload-area clearfix">
				<form method="post" enctype="multipart/form-data" action="{{AdminOptions::base_url()}}admin/image_upload_opis" >
					<label>Dodajte sliku u opisu:</label>

					<input type="file" id="img" name="img" class="file-input">
					<button class="file-upload btn btn-secondary" type="submit">Učitaj</button>
				</form>
				<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
					{{AdminOsobine::upload_directory_opis()}}
				</div>
			</div>		
		</div>
	</div>
</div>
