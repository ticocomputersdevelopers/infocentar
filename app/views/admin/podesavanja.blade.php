<section class="settings settings-page" id="main-content">
@if(Admin_model::check_admin())
 <div class="row">  
	<div class="flat-box">
		<h3 class="text-center no-nomargin">Konfigurisanje SWIFT mailer-a</h3>
		<div class="row"> 
			<div class="columns medium-12 columns"> 
				Aktivan: 
				<input class="check_swift" type="checkbox" {{ AdminOptions::gnrl_options(3003) ? 'checked' : '' }}>
			</div>
		</div>
		<form action="save_email_options" method="post" class="">
			@if(Session::has('error_msg'))
				{{ Session::get('error_msg') }}
			@endif
			<div class="row">
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_host">Host:</label>
					<input type="text" name="email_host" id="email_host" class="JSMailInput" value="{{ AdminOptions::email_server() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_port">Port:</label>
					<input type="text" name="email_port" id="email_port" class="JSMailInput" value="{{ AdminOptions::port() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_username">Username:</label>
					<input type="text" name="email_username" id="email_username" class="JSMailInput" value="{{ AdminOptions::username() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_password">Password:</label>
					<input type="password" name="email_password" id="email_password" class="JSMailInput" value="{{ AdminOptions::password() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_name">Name:</label>
					<input type="text" name="email_name" id="email_name" class="JSMailInput" value="{{ AdminOptions::name() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
				<div class="small-12 medium-2 large-2 columns">
					<label for="email_subject">Subject:</label>
					<input type="text" name="email_subject" id="email_subject" class="JSMailInput" value="{{ AdminOptions::subject() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
				</div>
			</div>
			<div class="btn-container center">
				<button class="JSWebOptions setting-button active btn btn-primary JSMailInput" type="submit" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>Sačuvaj</button>
		 </div>
		</form>
	</div> <!-- end of .flat-box -->
</div>
@endif
@if(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id == 5)
<div class="row">
	<div class="flat-box columns medium-12">
		<h3 class="text-center"><button class="click_me_toggle btn btn-secondary">Boje prodavnice <i class="fa fa-chevron-down" style="font-size: 11px;"></i></button></h3>
	    <div class="slide_me_toggle"> 
		<form action="save-color" method="post" class="">
			<div class="row">
			  	<div class="medium-12 columns small-12">	 
					<label for="active">Aktivan:</label>&nbsp;
					<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3021) == 1 ? 'checked="checked"' : '' }}>
				 </div>
				 <br>
				@foreach(DB::table('prodavnica_boje')->where('aktivan',1)->get() as $boja)
				<div class="custom_colors medium-3 columns">
		  			<label class="small-10 medium-10 large-10 columns manual-padding" >{{ $boja->title }}:</label>
					<div class="medium-2 small-2 columns manual-padding">	 
						<input type="color" name="{{ $boja->prodavnica_boje_id }}" value="{{ isset($boja->kod) ? $boja->kod : $boja->default_kod }}">
					</div>
			 	</div>
				@endforeach
			</div>
			<div class="btn-container center">
				<button class="JSWebOptions setting-button active btn btn-primary" type="submit">Sačuvaj</button>
		  	</div>
		</form>
		</div>
	</div>
</div>
@endif
@if(Admin_model::check_admin())
	@if(DB::table('prodavnica_stil')->where('zakljucana',1)->count() == 0)
		<div class="row">
			<div class="small-12 medium-12 large-12 columns flat-box text-center">
				<h3 class="text-center h3-margin">Izbor teme:</h3> 
				<ul class="flex">
					@foreach(DB::table('prodavnica_tema')->where('aktivna',1)->orderBy('prodavnica_tema_id','asc')->get() as $row1)
					<li class="text-left theme_choose"><span> &nbsp; {{ $row1->naziv }}</span>
						<ul>
							@foreach(DB::table('prodavnica_stil')->where('prodavnica_tema_id',$row1->prodavnica_tema_id)->where('aktivna',1)->orderBy('prodavnica_stil_id','asc')->get() as $row2)
							<li class="clearfix">
								<label class="">{{ $row2->naziv }}</label>
								<input type="radio" name="JSTheme" class="JSTheme" data-id="{{ $row2->prodavnica_stil_id }}" {{ $row2->izabrana == 1 ? 'checked' : '' }}> 		
							</li>
							@endforeach
						</ul>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif
@endif
 
@if(AdminOptions::is_shop())
<div class="row">
	<div class="flat-box">
		<h3 class="text-center h3-margin"><button class="click_me_toggle btn btn-secondary">Podešavanja <i class="fa fa-chevron-down" style="font-size: 11px;"></i></button></h3> 
		<div class="slide_me_toggle"> 
		<ul class="row">
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Dozvoli registraciju korisnika :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::user_registration()==1)
						<button class="JSWebOptions setting-button active" data-id="0" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="0" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="0" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="0" data-status="0">Ne</button>
					@endif
				</div> 		
			</li>

			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Dozvoli poručivanje neregistrovanim korisnicima :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::neregistrovani_korisnici()==1)
						<button class="JSWebOptions setting-button active" data-id="1" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="1" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="1" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="1" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Uključi potvrdu registracije korisnika :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::user_cnfirm_registration()==1)
						<button class="JSWebOptions setting-button active" data-id="100" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="100" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="100" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="100" data-status="0">Ne</button>
					@endif
				</div>
			</li>

			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Vodjenje lagera :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::vodjenje_lagera()==1)
						<button class="JSWebOptions setting-button active" data-id="103" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="103" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="103" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="103" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			@endif
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz akcije na početnoj :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(98)==1)
						<button class="JSWebOptions setting-button active" data-id="98" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="98" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="98" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="98" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz blogova na početnoj :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(99)==1)
						<button class="JSWebOptions setting-button active" data-id="99" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="99" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="99" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="99" data-status="0">Ne</button>
					@endif
				</div>
			</li>

			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz sifre na webu :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::prikaz_sifre()==1)

						<button class="JSWebOptions setting-button active" data-id="203" data-status="1" id="JSPrikazDa">Da</button>
						<button class="JSWebOptions setting-button" data-id="203" data-status="0" id="JSPrikazNe">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="203" data-status="1" id="JSPrikazDa">Da</button>
						<button class="JSWebOptions setting-button active" data-id="203" data-status="0" id="JSPrikazNe">Ne</button>
					@endif
				</div>
			</li>

			@if(Admin_model::check_admin()) 
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Vrsta prikaza liste proizvoda :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::product_view()==1)
						<button class="JSWebOptions setting-button" data-id="106" data-status="2">Grid</button>
						<button class="JSWebOptions setting-button" data-id="106" data-status="3">List</button>
						<button class="JSWebOptions setting-button active" data-id="106" data-status="1">Oba prikaza</button>
					@elseif(AdminOptions::product_view()==2)
						<button class="JSWebOptions setting-button active" data-id="106" data-status="2">Grid</button>
						<button class="JSWebOptions setting-button" data-id="106" data-status="3">List</button>
						<button class="JSWebOptions setting-button " data-id="106" data-status="1">Oba prikaza</button>
					@else
						<button class="JSWebOptions setting-button" data-id="106" data-status="2">Grid</button>
						<button class="JSWebOptions setting-button active" data-id="106" data-status="3">List</button>
						<button class="JSWebOptions setting-button" data-id="106" data-status="1">Oba prikaza</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz kolone u tabeli Artikli (ROBA_ID,SIFRA IS, ŠIFRA DOB, SKU) :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::sifra_view()==1)
						<button class="JSWebOptions setting-button" data-id="200" data-status="2">Šifra IS</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="3">SKU</button>
						<button class="JSWebOptions setting-button active" data-id="200" data-status="1">Roba_id</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view()==2)
						<button class="JSWebOptions setting-button active" data-id="200" data-status="2">Šifra IS</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="3">SKU</button>
						<button class="JSWebOptions setting-button " data-id="200" data-status="1">Roba_id</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view()==3)
						<button class="JSWebOptions setting-button" data-id="200" data-status="2">Šifra IS</button>
						<button class="JSWebOptions setting-button active" data-id="200" data-status="3">SKU</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="1">Roba_id</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view()==4)
						<button class="JSWebOptions setting-button" data-id="200" data-status="2">Šifra IS</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="3">SKU</button>
						<button class="JSWebOptions setting-button" data-id="200" data-status="1">Roba_id</button>
						<button class="JSWebOptions setting-button active" data-id="200" data-status="4">Šifra Dob</button>
					@endif
				</div>
			</li>
			@endif
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Uključi Newsletter :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::newsletter()==1)
						<button class="JSWebOptions setting-button active" data-id="104" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="104" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="104" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="104" data-status="0">Ne</button>
					@endif
				</div>
			</li>

			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz vrste sifre na webu :</label>
				</div>
				@if(AdminOptions::prikaz_sifre()==1)
				<div class="option-col__option">
					@if(AdminOptions::sifra_view_web()==1)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==2)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button " data-id="201" data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==3)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==4)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="4">Šifra Dob</button>
					@endif
				</div>
				@else
				<div class="option-col__option" >
					@if(AdminOptions::sifra_view_web()==1)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2" >Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==2)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button " data-id="201" disabled data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==3)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">Šifra Dob</button>
					@elseif(AdminOptions::sifra_view_web()==4)
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2">Šifra IS</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">SKU</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="1">Roba_id</button>
						<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="4">Šifra Dob</button>
					@endif
				</div>
				@endif
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Uključi opciju broj proizvoda po strani :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::product_number()==1)
						<button class="JSWebOptions setting-button active" data-id="107" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="107" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="107" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="107" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Sortiranje proizvoda po min - max ceni , najnoviji artikli i po nazivu artikla :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::product_sort()==1)
						<button class="JSWebOptions setting-button active" data-id="108" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="108" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="108" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="108" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Uključi opciju promena valute :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::product_currency()==1)
						<button class="JSWebOptions setting-button active" data-id="109" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="109" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="109" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="109" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Uključi opciju filtriranja artikala :</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::enable_filters()==1)
						<button class="JSWebOptions setting-button active" data-id="110" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="110" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="110" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="110" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Filtriranje atikla, sužavajući filter:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::filters_type()==1)
						<button class="JSWebOptions setting-button active" data-id="111" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="111" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="111" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="111" data-status="0">Ne</button>
					@endif
				</div>
			</li>
			@endif
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Fiksiran Heder:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::header_type()==1)
						<button class="JSWebOptions setting-button active" data-id="114" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="114" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="114" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="114" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
<!-- 			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Pozicija Kategorija:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::category_view()==1)
						<button class="JSWebOptions setting-button" data-id="115" data-status="0">Horizontalno</button>
						<button class="JSWebOptions setting-button active" data-id="115" data-status="1">Vertikalno</button>
					@else
						<button class="JSWebOptions setting-button active" data-id="115" data-status="0">Horizontalno</button>
						<button class="JSWebOptions setting-button" data-id="115" data-status="1">Vertikalno</button>
					@endif	
				</div>
			</li>
			@if(AdminOptions::category_view()==1)
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Način Listanja kategorija:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::category_type()==1)
						<button class="JSWebOptions setting-button" data-id="112" data-status="0">Listanje</button>
						<button class="JSWebOptions setting-button active" data-id="112" data-status="1">Padajući meni</button>
					@else
						<button class="JSWebOptions setting-button active" data-id="112" data-status="0">Listanje</button>
						<button class="JSWebOptions setting-button" data-id="112" data-status="1">Padajući meni</button>
					@endif	
				</div>
			</li>
			@endif -->
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Stranica sa svim kategorijama:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::all_category()==1)
						<button class="JSWebOptions setting-button active" data-id="116" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="116" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="116" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="116" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Upoređivanje artikala:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::compare()==1)
						<button class="JSWebOptions setting-button active" data-id="117" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="117" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="117" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="117" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			@if(Admin_model::check_admin(array(5219)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz vezanih artikala:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(118)==1)
						<button class="JSWebOptions setting-button active" data-id="118" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="118" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="118" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="118" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
<!-- 			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Default-ni flag za cenu kod vezanih artikala:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(119)==1)
						<button class="JSWebOptions setting-button active" data-id="119" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="119" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="119" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="119" data-status="0">Ne</button>
					@endif	
				</div>
			</li> -->
			@endif
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz dodatnih fajlova:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(120)==1)
						<button class="JSWebOptions setting-button active" data-id="120" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="120" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="120" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="120" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Konfigurator:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(121)==1)
						<button class="JSWebOptions setting-button active" data-id="121" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="121" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="121" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="121" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			@if(Admin_model::check_admin())
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Cena u prodavnici:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(132)==1)
						<button class="JSWebOptions setting-button active" data-id="132" data-status="1">Web Cena</button>
						<button class="JSWebOptions setting-button" data-id="132" data-status="0">MP cena</button>
					@else
						<button class="JSWebOptions setting-button" data-id="132" data-status="1">Web Cena</button>
						<button class="JSWebOptions setting-button active" data-id="132" data-status="0">MP cena</button>
					@endif	
				</div>
			</li>
			@endif
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Zaokruživanje cena na front-panelu:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(152)==1)
						<button class="JSWebOptions setting-button active" data-id="152" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="152" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="152" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="152" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Troškovi isporuke po težini:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(133)==1)
						<button class="JSWebOptions setting-button active" data-id="133" data-status="1" id="JSTezina1">Da</button>
						<button class="JSWebOptions setting-button" data-id="133" data-status="0" id="JSTezina2">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="133" data-status="1" id="JSTezina1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="133" data-status="0" id="JSTezina2">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Dodatni statusi za narudžbinu:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(135)==1)
						<button class="JSWebOptions setting-button active" data-id="135" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="135" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="135" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="135" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			
			@if(Admin_model::check_admin(array(200)))
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Troškovi isporuke po vrednosti:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(150)==1)
						<button class="JSWebOptions setting-button active JSCena" data-id="150" data-status="1" id="JSCenaDostave1">Da</button>
						<button class="JSWebOptions setting-button JSCena" data-id="150" data-status="0" id="JSCenaDostave2">Ne</button>
					@else
						<button class="JSWebOptions setting-button JSCena" data-id="150" data-status="1" id="JSCenaDostave1">Da</button>
						<button class="JSWebOptions setting-button active JSCena" data-id="150" data-status="0" id="JSCenaDostave2">Ne</button>
					@endif	
				</div>
			</li>
			@endif
			
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Tagovi:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(134)==1)
						<button class="JSWebOptions setting-button active" data-id="134" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="134" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="134" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="134" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz svih artikala na početnoj:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(136)==1)
						<button class="JSWebOptions setting-button active" data-id="136" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="136" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="136" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="136" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz najnovijih, najpopularnijih i najprodavanijih artikala na početnoj:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(208)==1)
						<button class="JSWebOptions setting-button active" data-id="208" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="208" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="208" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="208" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">
					<label>Prikaz najnovijih, najpopularnijih i najprodavanijih artikala na B2B-u:</label>
				</div>
				<div class="option-col__option">
					@if(AdminOptions::web_options(151)==1)
						<button class="JSWebOptions setting-button active" data-id="151" data-status="1">Da</button>
						<button class="JSWebOptions setting-button" data-id="151" data-status="0">Ne</button>
					@else
						<button class="JSWebOptions setting-button" data-id="151" data-status="1">Da</button>
						<button class="JSWebOptions setting-button active" data-id="151" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@foreach(DB::table('options')->whereIn('options_id',(Admin_model::check_admin() ? array(3029,3033,3035) : array(3029,3035)))->orderBy('options_id','asc')->get() as $row)
			<li class="small-12 medium-12 large-6 columns option-col">
				<div class="option-col__label">  
					<label>{{ $row->naziv }}:</label>
				</div>
				<div class="option-col__option">
					@if($row->int_data == 1 )
						<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="1">Da</button>
						<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="0">Ne</button>
					@else
						<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="1">Da</button>
						<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="0">Ne</button>
					@endif	
				</div>
			</li>
			@endforeach
		</ul> <!-- end of .row -->
		</div>
	</div> <!-- end of .flat-box -->
</div>
@endif
<div class="row flex">
	<div class="flat-box large-2 medium-2 small-12 columns">
		<h3 class="text-center no-nomargin">Google analitika</h3>
		<form action="save-google-analytics" method="post" class="">
			<div class="row">
				<div class="small-12 medium-8 large-8 columns">
					<label for="google_id">ID:</label>
					<input type="text" name="google_id" id="google_id" value="{{ AdminOptions::gnrl_options(3019,'str_data') }}">
					<div>{{ $errors->first('google_id') ? $errors->first('google_id') : '' }}</div>
				</div>
				<div class="small-12 medium-4 columns">
					<label for="active">Aktivan:</label>
					<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3019) == 1 ? 'checked="checked"' : '' }}>
				</div> 
				<div class="small-12 medium-12 columns text-center">
					<button class="JSWebOptions setting-button active btn btn-primary" type="submit">Sačuvaj</button>
				</div>
			</div>
		</form>
	</div> <!-- end of .flat-box -->

	<div class="flat-box large-2 medium-2 small-12 columns">
		<h3 class="text-center no-nomargin">Čet</h3>
		<form action="save-chat" method="post" class="">
			<div class="row">
				<div class="small-12 medium-8 large-8 columns">
					<label for="key">Link:</label>
					<input type="text" name="key" id="key" value="{{ AdminOptions::gnrl_options(3020,'str_data') }}">
					<div>{{ $errors->first('key') ? $errors->first('key') : '' }}</div>
				</div>
				<div class="small-12 medium-4 columns">
					<label for="active">Aktivan:</label>
					<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3020) == 1 ? 'checked="checked"' : '' }}>
				</div> 
				<div class="small-12 medium-12 columns text-center">
					<button class="JSWebOptions setting-button active btn btn-primary" type="submit">Sačuvaj</button>
				</div>
			</div>
		</form>
	</div> <!-- end of .flat-box -->

	<div class="flat-box large-2 medium-2 small-12 columns">
		<h3 class="text-center no-nomargin">Sitemap</h3>
		<div class="row">
			<div class="columns medium-12 text-center"> 
				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/sitemap-generate">Generiši</a>
			</div>
		</div>
	</div> <!-- end of .flat-box -->
	
	@if(Admin_model::check_admin() && AdminOptions::is_shop())
	<div class="flat-box large-3 medium-3 small-12 columns">
		<h3 class="text-center no-nomargin">Intesa podešavanja</h3>
		<form action="save-intesa-keys" method="post" class="">
			<div class="row">
				<div class="small-12 medium-6 large-6 columns">
					<label for="client_id">Klijent ID:</label>
					<input type="text" name="client_id" value="{{ AdminOptions::gnrl_options(3023,'str_data') }}">
					<div>{{ $errors->first('client_id') ? $errors->first('client_id') : '' }}</div>
				</div>
				<div class="small-12 medium-6 large-6 columns">
					<label for="store_key">Store Key</label>
					<input type="text" name="store_key" value="{{ AdminOptions::gnrl_options(3024,'str_data') }}">
					<div>{{ $errors->first('store_key') ? $errors->first('store_key') : '' }}</div>
				</div> 
				<div class="small-12 medium-12 columns text-center">
					<button class="JSWebOptions setting-button active btn btn-primary" type="submit">Sačuvaj</button>
				</div>
			</div>
		</form>
	</div> <!-- end of .flat-box -->

	<div class="flat-box large-2 medium-2 small-12 columns">
		<h3 class="text-center no-nomargin">Backup</h3>
		<div class="row">
			<div class="columns medium-12 text-center">
				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}backup/articles/xml/{{ AdminOptions::gnrl_options(3002,'str_data') }}">Artikli</a>
			    <a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}backup/images/zip/{{ AdminOptions::gnrl_options(3002,'str_data') }}">Slike artikala</a>
			 </div>
		</div>
	</div> <!-- end of .flat-box -->
	@endif
 </div>
 
@if(Admin_model::check_admin() && AdminOptions::is_shop()) 
	<div class="row flex">
		<div class="small-12 medium-1 large-1 columns box-sett">
			<h3 class="text-center h3-margin">B2C i B2B portal:</h3> 	
			<ul class="double-Options">
				<li>
					<label> B2C </label>
					<input name="JSPortal" value="0" data-id="130" type="radio" {{ !AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 0) ? 'checked' : '' }}> 
				</li>
				<li>
					<label>B2C i B2B </label>
					<input name="JSPortal" value="1" data-id="130" type="radio" {{ AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 1) ? 'checked' : '' }}> 
				</li>
				<li>
					<label>B2B </label>
					<input name="JSPortal" value="2" data-id="130" type="radio" {{ AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 2) ? 'checked' : '' }}> 
				</li>
			</ul>
		</div>
		<div class="small-12 medium-3 large-2 columns box-sett text-center">
			<h3 class="text-center h3-margin">Izbor teme:</h3> 	
			@if(DB::table('prodavnica_stil')->where('zakljucana',1)->count() == 0)
			<button id="JSThemeConfirm" class="btn btn-primary btn-small">Zaključaj temu</button>
			@else
			<?php $stil = DB::table('prodavnica_stil')->where('zakljucana',1)->first(); ?>
			<div>Izabrana tema: {{ DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('naziv') }} {{ $stil->naziv }}</div>
			@endif
		</div>
	
		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Prikaz artikala na web-u:</h3> 	
			<ul class="">
				<li class="">
					<label class="">Bez cene</label>
					<input class="options_active" data-id="1307" type="checkbox" {{ AdminOptions::gnrl_options(1307) == 1 ? 'checked' : '' }}> 		
				</li>
				<li class="">
					<label class="">Bez slike</label>
					<input class="options_active" data-id="1308" type="checkbox" {{ AdminOptions::gnrl_options(1308) == 1 ? 'checked' : '' }}> 		
				</li>
				<li class="">
					<label class="">Bez opisa</label>
					<input class="options_active" data-id="1305" type="checkbox" {{ AdminOptions::gnrl_options(1305) == 1 ? 'checked' : '' }}> 		
				</li>
				<li class="">
					<label class="">Bez karakteristika</label>
					<input class="options_active" data-id="1306" type="checkbox" {{ AdminOptions::gnrl_options(1306) == 1 ? 'checked' : '' }}> 		
				</li>
			</ul>
		</div>
		
		<div class="small-12 medium-2 large-2 columns box-sett">
		<h3 class="text-center h3-margin">Jezici:</h3> 	
		<ul class="">
			@foreach(DB::table('jezik')->orderBy('jezik_id','asc')->get() as $row)
			<li class="">
				<label class="option-col__label">{{ $row->naziv }}</label>
				<input class="JSJezikActive" data-id="{{ $row->jezik_id }}" type="checkbox" {{ $row->aktivan == 1 ? 'checked' : '' }} {{ $row->izabrani == 1 ? 'disabled' : '' }}>
				<input name="jezik_primary" class="JSJezikPrimary" data-id="{{ $row->jezik_id }}" type="radio" {{ $row->izabrani == 1 ? 'checked' : '' }} {{ $row->aktivan == 1 ? '' : 'disabled' }}>		
			</li>
			@endforeach
		</ul>
	</div>

	</div>

	<div class="row flex">
		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Vrste cena:</h3> 	
			<ul class="">
				@foreach(DB::table('vrsta_cena')->where('vrsta_cena_id','<>',-1)->orderBy('vrsta_cena_id','asc')->get() as $row)
				<li class="">
					<span class="sett-name">{{ $row->naziv }}</span>
					<select class="JSVrstaCena" data-id="{{ $row->vrsta_cena_id }}">
						@foreach(DB::table('valuta')->where('ukljuceno',1)->where('valuta_id','<>',-1)->orderBy('valuta_id','asc')->get() as $row2)
							@if($row->valuta_id == $row2->valuta_id)
							<option value="{{ $row2->valuta_id }}" selected>{{ $row2->valuta_sl }}</option>
							@else
							<option value="{{ $row2->valuta_id }}">{{ $row2->valuta_sl }}</option>
							@endif
						@endforeach
					</select>
					<input class="JSVrstaCenaSelect" data-id="{{ $row->vrsta_cena_id }}" type="checkbox" {{ $row->selected == 1 ? 'checked' : '' }}>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Magacini:</h3> 	
			<ul class="">
				@foreach(DB::table('orgj')->where('orgj_id','<>',-1)->orderBy('orgj_id','asc')->get() as $row)
				<li class="">
					<label class="option-col__label">{{ $row->naziv }}</label>
					<input class="JSOrgjEnable" data-id="{{ $row->orgj_id }}" type="checkbox" {{ $row->b2c == 1 ? 'checked' : '' }} {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>10,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'disabled' : '' }}>
					<input name="orgj_primary" class="JSOrgjPrimary" data-id="{{ $row->orgj_id }}" type="radio" {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>10,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'checked' : '' }} {{ $row->b2c == 1 ? '' : 'disabled' }}>		
				</li>
				@endforeach
			</ul>
		</div>

		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Eksporti:</h3> 	
			<ul class="">
				@foreach(DB::table('export')->orderBy('export_id','asc')->get() as $row)
				<li class="">
					<label class="option-col__label">{{ $row->naziv }}</label>
					<input class="JSExportEnable" data-id="{{ $row->export_id }}" type="checkbox" {{ $row->dozvoljen == 1 ? 'checked' : '' }}>
				</li>
				@endforeach
			</ul>
		</div>
		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Automatski import:</h3> 	
			<ul >
				<li>
					<label class="">Aktivan</label>
					<input class="auto_import_active" data-id="3002" type="checkbox" {{ AdminOptions::gnrl_options(3002) ? 'checked' : '' }}>
				</li>
				<label class="">
				<li>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}">Kratki import lagera i cena</a>
				</li>
				<li>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}/cene">Kratki import cena</a>
				</li>
				<li>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}/lager">Kratki import lagera</a>
				</li>
				<li>
					<a href="{{AdminOptions::base_url()}}auto-full-import/{{AdminOptions::sifra()}}">Kompletni import</a>
				</li>
				</label>
			</ul>
		</div>
		<div class="small-12 medium-2 large-2 columns box-sett">
			<h3 class="text-center h3-margin">Auto import report</h3> 	
			<ul >
	  			<li class="text-center">
					<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}files/logs/full_import.txt">Kompletni import</a>
				</li>
				<li class="text-center">
					<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}files/logs/short_import.txt">Kratki import</a>
				</li>
	  		</ul>
		</div>
	</div> 
	</div>

@endif
</section>