<div id="main-content" class="kupci-page">
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@if(Session::has('alert'))
		<script>
			alertify.error('{{ Session::get('alert') }}');
		</script>
	@endif
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="medium-12 large-6 large-centered columns">
		<h1>Upload file-a</h1>
			<div class="flat-box">
			
				<form action="{{AdminOptions::base_url()}}admin/file-upload-save" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="field-group column medium-12 large-6">
							<label for="">Podržani importi</label>
							<select type="text" name="podrzan_import_id">
								{{ AdminKupci::podrzaniImportiUpload() }}
							</select>
						</div>
						<div class="field-group column medium-12 large-6">
							<label for="">File</label>
							<input type="file" name="import_file">
						</div>
					</div>

					<div class="btn-container center no-margin">
						<button type="submit" id="submit" class="btn btn-primary">Upload</button>
						<a href="{{AdminOptions::base_url()}}admin/file-upload" class="btn btn-secondary">Otkaži</a>
					</div>
				</form>
				
			</div>
		</div>
	</div>

</div>