<div id="main-content" class="">
@include('admin/partials/product-tabs')

<div class="row">
	<div class="column large-6 large-centered">
		<div class="flat-box">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/seo_edit">
				@if(count($jezici) > 1)
				<div class="row"> 
					<div class="languages">
						<ul>	
						@foreach($jezici as $jezik)
							<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/product_seo/{{ $roba_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
						@endforeach
						</ul>
					</div>
				</div>
				@endif                
                <div class="row">
				<h2 class="title-med">Naslov (title) do 60 karaktera</h2>
					<div class="column large-12 medium-12">
                        <input type="text" class="{{ $errors->first('seo_title') ? 'error' : '' }}" name="seo_title" value="{{ Input::old('seo_title') ? Input::old('seo_title') : $seo_title }}">
					</div>	
				</div> <!-- end of .row -->
                
				<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id ? $jezik_id : 0 }}">
				<div class="row">
				<h2 class="title-med">Opis (description) do 158 karaktera </h2>
					<div class="column large-12 medium-12">
						<textarea name="description" class="{{ $errors->first('description') ? 'error' : '' }}">{{ Input::old('description') ? Input::old('description') : $description }}</textarea>
					</div>
				</div> <!-- end of .row -->
				
				<div class="row">
				<h2 class="title-med">Ključne reči (keywords)</h2>
					<div class="column large-12 medium-12">
						<textarea name="keywords" class="{{ $errors->first('keywords') ? 'error' : '' }}">{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}</textarea>
					</div>	
				</div> <!-- end of .row -->
				<div class="row">
				<h2 class="title-med">Tagovi (Default-ni jezik)</h2>
					<div class="column large-12 medium-12">
						<textarea name="tags" class="{{ $errors->first('tags') ? 'error' : '' }}">{{ Input::old('tags') ? Input::old('tags') : $tags }}</textarea>
						<div class="btn-container center">
							<button type="submit" class="setting-button btn btn-primary save-it-btn">Sačuvaj</button>
						</div>
					</div>	
				</div> <!-- end of .row -->                                
			</form>		
		</div>
	</div>
</div>
</div>