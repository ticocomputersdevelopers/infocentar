<div class="m-tabs clearfix">

	<div class="m-tabs__tab{{ $strana=='b2b_artikli' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/article-list/0">Rabat grupa i artikala</a></div>

	<div class="m-tabs__tab{{ $strana=='proizvodjaci' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/proizvodjaci">Rabat proizvođača</a></div>

	<div class="m-tabs__tab{{ $strana=='partneri' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/partneri">Rabat partnera</a></div>

	<div class="m-tabs__tab{{ $strana=='rabat_kombinacije' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/rabat_kombinacije">Kombinacije rabata</a></div>
	
	<div class="m-tabs__tab{{ $strana=='partner_kategorija' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/partner_kategorija">Kategorizacija partnera</a></div>

</div>