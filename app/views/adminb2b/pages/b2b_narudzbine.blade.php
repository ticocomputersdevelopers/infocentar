@extends('adminb2b.defaultlayout')
@section('content')
<?php
	if($datum_od==0 and $datum_do==0){
	   $dat1='';
       $dat2='';
	}else 
    if($datum_od!=0 and $datum_do==0){
	   $dat1=$datum_od;
       $dat2='';
	}else 
    if($datum_od==0 and $datum_do!=0){
	   $dat1='';
       $dat2=$datum_do;
	}else 
    {
	   $dat1=$datum_od;
       $dat2=$datum_do;
	}
?>

<section id="main-content" class="orders-page row b2b-orders">
	<!-- ORDER LIST -->
	<section class="articles-listing medium-12 columns orders-listing">
	<div class="row">
		<div class="column medium-4"> 
		<h1 class="title-filters-h1">B2B Narudžbine</h1>
		</div>
	<!-- pretraga narudzbina -->
 
		<div class="orders-input column medium-4"> 
			<input type="text" class="search-field" id="search" autocomplete="on" placeholder="Pretraži narudžbine...">
			<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
			<i class="fa fa-search" aria-hidden="true"></i>
			</button>
		</div>
		<div class="columns medium-4 text-right"><a href="/admin/b2b/narudzbina/0" class="btn btn-create btn-small">Kreiraj narudžbinu</a></div>
	</div>

	<!-- filteri narudzbina -->
	<ul class="o-filters row"> 
		<span class="o-filters-span-1 column medium-9"> 
	   		<label>Filteri narudžbina:</label> 
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
					<span class="label-text nova">Nove</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
					<span class="label-text prihvacena">Prihvaćene</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
					<span class="label-text">Realizovane</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
					<span class="label-text stornirana">Stornirane</span>
				</label>
			</li>
		</span>
		<span class="o-filters-span-1 column medium-3">
		   	@if(AdminB2BOptions::checkB2B() == 1)
		   	<div class="column medium-9"> 
		   	<label>Filteri dodatnih statusa:</label>
				<select class="m-input-and-button__input import-select " id="narudzbina_status_id" >
					<option value="0">Svi dodatni statusi</option>
					@foreach(AdminB2BNarudzbina::dodatni_statusi() as $statusi)
						@if($statusi->narudzbina_status_id == $narudzbina_status_id )	
							<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
						@else
							<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
						@endif				
					@endforeach
				</select>	
			</div>
			@endif

			<a href="#" class="m-input-and-button__btn btn btn-danger erase-btn">
				<span class="fa fa-times tooltipz-left" aria-label="Poništi filtere" id="JSFilterClear" aria-hidden="true"></span>
			</a>

		</span>
	</ul>

	<div class="row">
			<span class="o-filters-span-2 column medium-12"> 
				<span class="datepicker-col">
					<div class="datepicker-col__box clearfix">
						<label>Od:</label>
						<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
					</div>

					<div class="datepicker-col__box clearfix">
						<label>Do:</label>
						<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
					</div>
				</span>
			</span>
		</div>

		<ul class="admin-list order-list-titles row b2b-porudzbina-flex">
			<li class="order-firm columns medium-1 admin-li table-head"><a href="#">Firma</a></li>
			<li class="order-num columns medium-1 admin-li table-head"><a href="#">Broj n.</a></li>
			<li class="order-dates columns medium-1 admin-li table-head"><a href="#">Datum</a></li>
			<li class="order-product columns medium-1 admin-li table-head"><a href="#">Artikli</a></li>
			<li class="order-price columns medium-1 admin-li table-head"><a href="#">Iznos</a></li>
			<li class="order-phone columns medium-1 admin-li table-head"><a href="#">Telefon</a></li>
			<li class="order-place columns medium-1 admin-li table-head"><a href="#">Mesto</a></li>
			<li class="order-payment columns medium-1 admin-li table-head"><a href="#">Način plaćanja</a></li>
			<li class="order-shipment columns medium-1 admin-li table-head"><a href="#">Način isporuke</a></li>
			<li class="order-additional columns medium-1 admin-li table-head"><a href="#">Dodatni stat.</a></li>
			<li class="order-stat columns medium-1 admin-li table-head"><a href="#">Status</a></li>
			<li class="order-more columns medium-1 admin-li table-head"><a href="#">Više</a></li>
		</ul>
		<ul>
		@foreach($query as $row)  
			<li class="admin-list order-list-item row b2b-porudzbina-flex {{AdminB2BNarudzbina::narudzbina_status_css($row->web_b2b_narudzbina_id)}}">
				<span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="columns medium-1 admin-li JSMoreWiew order-firm">&nbsp;{{AdminB2BNarudzbina::narudzbina_partner($row->web_b2b_narudzbina_id)}}</span>
				<span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="order-num columns medium-1 admin-li JSMoreWiew">{{$row->broj_dokumenta}}</span>
				<span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="order-dates columns medium-1 admin-li JSMoreWiew">{{AdminB2BNarudzbina::datum_narudzbine($row->web_b2b_narudzbina_id)}}</span>

                <div class="order-product columns medium-1 admin-li">
                @foreach(AdminB2BNarudzbina::narudzbina_stavke($row->web_b2b_narudzbina_id) as $stavka)
                <span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="JSMoreWiew show-for-small-only porudzbine_li_title">Artikli:</span>
                <span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="JSMoreWiew on-hover-visible text-left">{{ AdminB2BArticles::find($stavka->roba_id, 'naziv_web') }}</span>
                @endforeach
                </div>

				<span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="order-price columns medium-1 admin-li JSMoreWiew">{{AdminB2BArticles::cena(AdminB2BNarudzbina::narudzbina_iznos_ukupno($row->web_b2b_narudzbina_id))}}</span>
				<span data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}" class="order-phone columns medium-1 admin-li JSMoreWiew">{{AdminB2BNarudzbina::partner_telefon($row->web_b2b_narudzbina_id)}}</span>
				<span class="order-place columns medium-1 admin-li">{{AdminB2BNarudzbina::partner_mesto($row->web_b2b_narudzbina_id)}}</span>
				<span class="order-payment columns medium-1 admin-li">{{AdminB2BNarudzbina::narudzbina_np($row->web_b2b_narudzbina_id)}}</span>
				<span class="order-shipment columns medium-1 admin-li">{{AdminB2BNarudzbina::narudzbina_ni($row->web_b2b_narudzbina_id)}}</span>
				<span class="order-additional columns medium-1 admin-li">{{AdminB2BNarudzbina::status_narudzbine($row->web_b2b_narudzbina_id)}}</span>
				<span class="order-stat columns medium-1 admin-li">
					<section class="order-status">
						<select class="order-status__select">
							<option class="order-status-active">{{AdminB2BNarudzbina::narudzbina_status_active($row->web_b2b_narudzbina_id)}}</option>
							{{AdminB2BNarudzbina::narudzbina_status_posible(AdminB2BNarudzbina::narudzbina_status_active($row->web_b2b_narudzbina_id),$row->web_b2b_narudzbina_id)}}
						</select>
					</section>
				</span>
				<span class="order-more columns medium-1 admin-li">
					<div class="btn-container center no-margin">
						<span class="JSMoreWiew btn btn-secondary btn-xm btn-circle small" data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}"><i class="fa fa-info" aria-hidden="true"></i></span>
					</div>
				</span>
			</li> 
		@endforeach
		</ul>
		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}
	</section>
	
</section> <!-- end of #main-content -->

<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="ordersDitailsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="content"></div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->

@endsection