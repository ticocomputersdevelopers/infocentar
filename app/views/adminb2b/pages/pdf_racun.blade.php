<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
<title>PDF</title>
<style>

.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* {margin: 0; font-family: DejaVu Sans; box-sizing: border-box;}

body {font-size: 16px;}

table {border-collapse: collapse; font-size: 12px; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}

tr:nth-child(even) {background-color: #f5f6ff;}

h2 {margin-bottom: 15px;}

.container {width: 90%;	margin: 42px auto;}

.logo img {	max-width: 150px; width: 100%;}

.text-right {text-align: right;}

.signature {background: none; color: #808080;}

.signature td, th {border: none; background: none;}
 
.company-info {font-size: 11px;}

.comp-name {font-size: 18px; font-weight: bold;}
 
.kupac-info td {border: none;}

.kupac-info { border: 1px solid #ddd; }

thead {background: #eee;}

.ziro {font-weight: bold; font-size: 14px; margin: 10px 0;}
 
.napomena p {margin: 10px 0; font-size: 13px;}

.info-1 {margin-bottom: 30px;}

.rbr {width: 50px;}

.artikli td {text-align: center;}

.cell-product-name {text-align: left !important;}

.sum-span {margin-right: 20px;}

</style>
</head>
<body>
	 
	<div class="container">
		<div class="row"> 
			<div class="logo col-3">
				<img src="{{ AdminOptions::base_url()}}{{Options::company_logo()}}" alt="logo">
			</div>
			<div class="col-4 company-info">
				<p class="comp-name">{{AdminOptions::company_name()}}</p>
				<p>{{AdminOptions::company_adress()}}</p>
				<p>Telefon: {{AdminOptions::company_phone()}}, Fax: {{AdminOptions::company_fax()}}</p>
				<p>PIB: {{AdminOptions::company_pib()}}</p>
			    <p>E-mail: {{AdminOptions::company_email()}}</p>
			</div>
			
			<div class="col-4 kupac-info">
				{{AdminB2BSupport::narudzbina_kupac_pdf($web_b2b_narudzbina_id)}}
			</div>
	 	</div>		

	 	<div class="row"> 
			<p class="ziro">Žiro racun: {{AdminOptions::company_ziro()}}</p>
		 </div>

	 	<div class="row"> 
	 		<br>
			<h4 class="racun-br">Racun broj: {{AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'broj_dokumenta')}}</h4>
		 </div>

		<div class="row"> 
			<table class="info-1">
				<thead>
					<tr>
						<td>Datum izdavanja racuna</td>
						<td>Nacin isporuke</td>
						<td>Nacin placanja</td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>{{AdminB2BNarudzbina::formatDate(AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'datum_dokumenta'))}}</td>
						<td>{{AdminCommon::n_i_b2b($web_b2b_narudzbina_id)}}</td>
						<td>{{AdminCommon::n_p_b2b($web_b2b_narudzbina_id)}}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row"> 
		<table class="artikli">
	        <tr>
	        	<td class="cell">Rbr</td>
	            <td class="cell-product-name">Naziv proizvoda</td>
	            <td class="cell">Kol.</td>
	            <td class="cell">Cena</td>
	            <td class="cell">Poreska osnovica</td>
	            <td class="cell">PDV</td>
	            <td class="cell">Iznos PDV</td>
	            <td class="cell">Vrednost sa PDV</td>
	        </tr>
	        <?php $rbr = 1; ?>
	        @foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row)
	        	<tr>
	        		<td class="cell"><?php echo $rbr; $rbr++; ?></td>
	                <td class="cell-product-name">{{ AdminCommon::short_title($row->roba_id)}}</td>
	                <td class="cell">{{ (int)$row->kolicina }}</td>
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena / AdminB2BNarudzbina::tarifnaGrupaPdv($row->tarifna_grupa_id))) }}</td>
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena / AdminB2BNarudzbina::tarifnaGrupaPdv($row->tarifna_grupa_id)) * $row->kolicina) }}</td>
	                <td class="cell">{{ AdminB2BNarudzbina::pdv($row->tarifna_grupa_id) }}</td>
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena - ($row->jm_cena / AdminB2BNarudzbina::tarifnaGrupaPdv($row->tarifna_grupa_id))) * $row->kolicina) }}</td>
	                <td class="cell">{{ AdminCommon::cena($row->kolicina * $row->jm_cena) }}</td>
	            </tr>
	        @endforeach
        </table>
        </div>

        <div class="row"> 
        	<br>
			<h4>Rekapitulacija</h4>
		</div>

		<div class="row"> 
			<div class="col-8"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>Osnovica</td>
							<td>PDV</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>Oslobodjeno poreza</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po posebnoj stopi</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po opstoj stopi</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno_b2b($web_b2b_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
						</tr>
						<tr>
							<td><strong>Ukupno</strong></td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno_b2b($web_b2b_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
		                 {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno_b2b($web_b2b_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2b_narudzbina_id))}}</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">Ukupno:</span> 
		                {{AdminCommon::cena(AdminCommon::narudzbina_ukupno_b2b($web_b2b_narudzbina_id))}}</td>
		            </tr>
		        </table>
	        </div>
		</div>

		<div class="row"> 
			<div class="napomena">
				<p>Kod placanja - poziv na broj: {{AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'broj_dokumenta')}}</p>
				<p><strong>Napomena o poreskom oslobadjanju:</strong></p>
				<p>{{AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'napomena')}}</p>
			</div>
		</div>

		<div class="row"> 
			<table class="signature">
				<tr>
					<td class=""></td>
					<td class="text-right"><span class="robu_izdao">Racun izdao</span> ___________________________</td>
				</tr>
			</table>
		</div>
	</div>
 
</body>
</html>
