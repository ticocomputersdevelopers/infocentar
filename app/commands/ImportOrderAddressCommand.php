<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class ImportOrderAddressCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'add:order-address';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$cities = simplexml_load_file(__DIR__.'/../../files/posta/dexpress_cities.xml');
		$towns = simplexml_load_file(__DIR__.'/../../files/posta/dexpress_towns.xml');
		$streets = simplexml_load_file(__DIR__.'/../../files/posta/dexpress_streets.xml');

		$data_body = "";
		foreach($cities as $city) {
			$cityId = $city->CityId;
			$name = $city->CityName;
			$ptt = $city->PttNo;

			$data_body .= "(".$cityId.",'".$name."','".$ptt."'),";
		}

		if($data_body != ''){
		    DB::statement("INSERT INTO narudzbina_opstina (code,naziv,ptt) SELECT * FROM (VALUES ".substr($data_body,0,-1).")
			redovi(code,naziv,ptt) 
		    WHERE code NOT IN (SELECT code FROM narudzbina_opstina)");
		}


		$data_body = '';
		foreach($towns as $town) {
			$townId = $town->TownId;
			$cityId = $town->CityId;
			$name = $town->TownName;
			$ptt = $town->PttNo;

			$data_body .= "(".$townId.",".$cityId.",'".$name."','".$ptt."'),";
		}
		if($data_body != ''){
		    DB::statement("INSERT INTO narudzbina_mesto (code,narudzbina_opstina_code,naziv,ptt) SELECT * FROM (VALUES ".substr($data_body,0,-1).")
			redovi(code,narudzbina_opstina_code,naziv,ptt) 
		    WHERE code NOT IN (SELECT code FROM narudzbina_mesto)");
		}


		$data_body = '';
		foreach($streets as $street) {
			$streetId = $street->StreetId;
			$townId = $street->TownId;
			$name = $street->StreetName;

			$data_body .= "(".$streetId.",".$townId.",'".$name."',NULL),";
		}
		if($data_body != ''){
		    DB::statement("INSERT INTO narudzbina_ulica (code,narudzbina_mesto_code,naziv,ptt) SELECT * FROM (VALUES ".substr($data_body,0,-1).")
			redovi(code,narudzbina_mesto_code,naziv,ptt) 
		    WHERE code NOT IN (SELECT code FROM narudzbina_ulica)");
		}

		DB::statement("INSERT INTO narudzbina_ulica (code,narudzbina_mesto_code,naziv) SELECT CONCAT('1236',currval('narudzbina_ulica_narudzbina_ulica_id_seq'))::integer, code, naziv FROM narudzbina_mesto nm WHERE nm.code NOT IN (SELECT narudzbina_mesto_code FROM narudzbina_ulica)");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
