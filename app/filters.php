<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/



App::before(function($request)
{
	if(Options::gnrl_options(3013) == 1 && Options::gnrl_options(3022) == 0){
		header("Location: http://www.selltico.com");
		die();
	}	
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (! Session::has('b2b_user_'.Options::server()))
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('lang',function($route, $request){
	if(Options::web_options(130) == 2){
		return Redirect::to(Options::base_url());
	}

   $lang = $route->parameter('lang');
   if($lang && $lang!=Language::lang() && !is_null(DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->first())){
   		Session::put('lang',$lang);
   }
});


Route::filter('b2b',function($route, $request){
$input = $request->all();    $pureInput = htmlPurify($input);    $request->replace($pureInput);
	if(!B2bOptions::checkB2B()){
		return Redirect::to(Options::base_url());
	}

	if(!Session::has('b2b_user_'.Options::server()) ){
		return Redirect::to(Options::base_url().'b2b/login');
	}

	if(B2bOptions::info_sys('infograf') && !(Session::has('b2b_user_'.B2bOptions::server().'_discounts') && is_array(Session::get('b2b_user_'.B2bOptions::server().'_discounts')))){
		return Redirect::to(Options::base_url().'b2b/login');
	}
});


Route::filter('user_filter',function($route, $request){
   if(Session::has('b2c_kupac')){
    //return Redirect::back();
	   
   }
   else {
	   return Redirect::to(Options::base_url());
   }
});

Route::filter('b2c_admin',function($route, $request){
$input = $request->all();    $pureInput = htmlPurify($input);    $request->replace($pureInput);
   if(!Session::has('b2c_admin'.AdminOptions::server())){
		return Redirect::to(AdminOptions::base_url().'admin-login');
	}
});

Route::filter('hash_protect',function($route,$request){
	
	if($route->parameter('key') !== DB::table('options')->where('options_id',3002)->pluck('str_data') || DB::table('options')->where('options_id',3002)->pluck('int_data') == 0){
		return Redirect::to(AdminOptions::base_url());
	}

});
Route::filter('hash_protect_export',function($route,$request){
	if($route->parameter('key') !== DB::table('options')->where('options_id',3002)->pluck('str_data')){
		return Redirect::to(AdminOptions::base_url());
	}

});

Route::filter('b2b_admin',function($route, $request){
$input = $request->all();    $pureInput = htmlPurify($input);    $request->replace($pureInput);
   if(!(Session::has('b2c_admin'.AdminOptions::server()) && Admin_model::check_admin(array(10000)) && AdminOptions::checkB2B())){
	   return Redirect::to('admin-login');
   }
});

Route::filter('wings',function($route, $request){
   if(!AdminB2BOptions::info_sys('wings')){
	   return Redirect::to('admin/b2b');
   }
});

Route::filter('calculus',function($route, $request){
   if(!AdminB2BOptions::info_sys('calculus')){
	   return Redirect::to('admin/b2b');
   }
});

Route::filter('infograf',function($route, $request){
   if(!AdminB2BOptions::info_sys('infograf')){
	   return Redirect::to('admin/b2b');
   }
});

Route::filter('logik',function($route, $request){
   if(!AdminB2BOptions::info_sys('logik')){
	   return Redirect::to('admin/b2b');
   }
});

Route::filter('xml_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('xml')){
	   return Redirect::to('admin/b2b');
   }
});
function htmlPurify($data) {
    if (is_array($data)) {
        foreach ($data as &$element) {
            $element = htmlPurify($element);
        }
    } elseif (is_string($data)) {
        $pattern = '/<script[^>]*>.*?<\/script>/is';
        $data = preg_replace($pattern, '', $data);
        $data = str_replace('<script></script>', '', $data);
    }
    return $data;
}
