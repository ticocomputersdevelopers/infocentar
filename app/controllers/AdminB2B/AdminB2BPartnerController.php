<?php

class AdminB2BPartnerController extends Controller {

	function proizvodjaci(){
		$proizvodjaci = AdminB2BProizvodjac::getManufacturers();

		$data=array(
			"strana" => 'proizvodjaci',
			"title" => 'Rabat partnera i proizvođača',
			"proizvodjaci" =>$proizvodjaci
		);
		return View::make('adminb2b/pages/partneri_proizvodjaci', $data);
	}

	function partneri(){
		$partneri = AdminB2BProizvodjac::getPartners();

		$data=array(
            "strana"=>'partneri',
            "title"=> 'Rabat partnera',
            "partneri"=>$partneri
        );
        return View::make('adminb2b/pages/partneri', $data);
	}

	function ajax() {
		$action = Input::get('action');
		
		if($action == 'rabat_proizvodjac_edit') {
			$proizvodjac_id = Input::get('proizvodjac_id');
			$rabat = Input::get('rabat');

			$query = DB::table('proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->update(['rabat' => $rabat]);
			
			if($query == true) {
				return json_encode(1);
			} else {
				return json_encode(0);
			}
		} 

		
	}

	function rabat_partner_edit() {
            $partner_ids = Input::get('partner_ids');
            $rabat = Input::get('rabat');

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET rabat = ". $rabat ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner')->where('partner_id',$partner_id)->pluck('rabat');
            }
            return json_encode($response);

	}

	function kategorija_partner_edit() {
		    $partner_ids = Input::get('partner_ids');
            $kat = Input::get('kategorija');

            if($kat != 'Bez kategorije'){
            $kategorija = DB::table('partner_kategorija')->where('naziv',$kat)->pluck('id_kategorije');

            $response = array();
 
            foreach($partner_ids as $partner_id){

                DB::statement("UPDATE partner SET id_kategorije = ". $kategorija ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner_kategorija')->where('id_kategorije',$kategorija)->pluck('naziv');
            }
            return json_encode($response);
        }
        else if($kat == 'Bez kategorije'){

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET id_kategorije = 0 WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = "Bez kategorije";
            }
            return json_encode($response);
        }

	}


	public function partneri_search(){
		$word = trim(Input::get('search'));

		$data=array(
	                "strana"=>'partneri',
	                "title"=> 'Partneri',
	                "partneri" => AdminB2BSupport::searchPartneri($word),
	                "word" => $word
	            );
			return View::make('adminb2b/pages/partneri', $data);
	}

	function rabat_kombinacije() {
		$kombinacije = AdminB2BProizvodjac::getPartnerRabatGrupa();
		$data=array(
			"strana" => 'rabat_kombinacije',
			"title" => 'Rabat kombinacije',
			"kombinacije" => $kombinacije,
			"proizvodjaci" => AdminB2BSupport::getProizvodjaci(),
			"partneri" => AdminB2BSupport::getDobavljaci()
			
		);
		return View::make('adminb2b/pages/rabat_kombinacije', $data);
	}

	function rabat_kombinacije_edit() {
		$input = Input::get();

		$query = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->count();
		if($query == 0) {
			$insert = DB::table('partner_rabat_grupa')->insert(['partner_id' => $input['partner'], 'grupa_pr_id' => $input['grupa'], 'proizvodjac_id' => $input['proizvodjac'], 'rabat' => $input['rabat']]);
		} else {
			$update = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->update(['rabat' => $input['rabat']]);
		}

		return Redirect::to(AdminOptions::base_url().'admin/b2b/rabat_kombinacije');
	}

	function rabat_kombinacije_delete($id) {
		$query = DB::table('partner_rabat_grupa')->where('id', $id)->delete();
		if($query == true) {
		    return json_encode(1);
		} else {
		    return json_encode(0);
		}
	}

	 //partner_kategorije
    function partner_kategorija($id_kategorije=null){
        $data=array(
            "strana"=>'partner_kategorija',
            "title"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : 'Nova kategorija',
            "id_kategorije"=>$id_kategorije,
            "naziv"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : null,
            "rabat"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'rabat') : null,
            "active"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'active') : null 
        );
   
        return View::make('adminb2b/pages/partner_kategorija', $data);
    }

     //izmena kategorije
     function kategorije_edit()
    {   
        $inputs = Input::get();
        // die('test');
        $validator = Validator::make($inputs, array(
            'naziv' => 'required|max:100', 
            'rabat' => 'required|numeric' 
            ));
        if($validator->fails()){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$inputs['id_kategorije'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['id_kategorije'] == 0){                
                $general_data['id_kategorije'] = DB::select("SELECT MAX(id_kategorije) AS max FROM partner_kategorija")[0]->max + 1;
            }

            if(isset($general_data['active'])){
                $general_data['active'] = 1;
            }else{
                $general_data['active'] = 0; 
            }

            if($inputs['id_kategorije'] != 0){
                DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->update($general_data);
            }else{
                DB::table('partner_kategorija')->insert($general_data);
            }

            AdminSupport::saveLog('Partner kategorije, INSERT/EDIT id_kategorije -> '.$general_data['id_kategorije']);
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$general_data['id_kategorije'])->with('message',$message);
        }
    }

    //brisanje kategorije
    function kategorije_delete($id_kategorije)
    {   

        DB::table('partner_kategorija')->where('id_kategorije',$id_kategorije)->delete();
        
        AdminSupport::saveLog('Partner kategorije, DELETE id_kategorije -> '.$id_kategorije);
        return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija')->with('message','Uspešno ste obrisali sadržaj.');
    } 
    

}