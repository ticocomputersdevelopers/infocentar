<?php
use ISCustom\AdminB2BISCustom;

class AdminB2BISController extends Controller {
    function is(){
        $data=array(
            "strana"=>'informacioni_sistem',
            "title"=>'Informacioni sistem',
        );
        return View::make('adminb2b/pages/informacioni-sistem', $data);     
    }

    function load(){
        $kind = Input::get('kind');

        if($kind == 'xlsx'){
            $messages = AdminB2BIS::xlsx_exe();
        }
        elseif($kind == 'xml'){
            $messages = AdminB2BIS::xml_exe();
        }
        elseif($kind == 'csv'){
            $messages = AdminB2BIS::csv_exe();
        }
        elseif($kind == 'sql'){
            $messages = AdminB2BIS::sql_exe();
        }

        if(count($messages)>0){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_messages',$messages);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Podaci iz informacionog sistema su uspešno učitani!');
        }    
    }

    function custom_load(){

        $messages = AdminB2BISCustom::xlsx_exe();

        if(count($messages)>0){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_messages',$messages);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Podaci iz informacionog sistema su uspešno učitani!');
        }    
    }

    function file_upload(){
        $file = Input::file('import_file');
        $table = Input::get('table');
        $kind = Input::get('kind');

        if(Input::hasFile('import_file') && $file->isValid() && $file->getClientOriginalExtension() == $kind){
            if($kind == 'xml'){
                $suffix = "xml";
            }
            elseif($kind == 'csv'){
                $suffix = "csv";
            }else{
                $suffix = "excel";
            }            
            $file->move("files/IS/".$suffix,$table.".".$kind);

            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Fajl je uspešno upload-ovan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_message','Fajl nije upload-ovan! Proverite strukturu fajla!');
        }
        
    }

    function wings_post(){
        $result = AdminISWings::execute();
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    
    function calculus_post(){
        $result = AdminISCalculus::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function infograf_post(){
        $result = AdminIsInfograf::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function logik_post(){
        $result = AdminIsLogik::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function xml_post(){
        $result = AdminIsXml::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function auto_import_is($key){
        if(AdminB2BOptions::info_sys('excel')){

        }else if(AdminB2BOptions::info_sys('xml')){
            $result = AdminIsXml::execute();
        }else if(AdminB2BOptions::info_sys('wings')){
            $result = AdminISWings::execute();
        }else if(AdminB2BOptions::info_sys('calculus')){
            $result = AdminISCalculus::execute();
        }else if(AdminB2BOptions::info_sys('infograf')){
            $result = AdminIsInfograf::execute();
        }else if(AdminB2BOptions::info_sys('logik')){
            $result = AdminIsLogik::execute();
        }

        if(!$result->success){
            echo $result->message;
        }       
    }

}