<?php

class ArticleController extends Controller {

	public function article(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $article = Request::segment(2+$offset);

        $roba_id=Product::get_product_id($article);
        if($roba_id != 0){
           if($roba_id>0 && Product::checkView($roba_id)){
            All::articleView($roba_id);
            $seo = Seo::article($roba_id);
            $image = DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->orderBy('akcija','desc')->first();
           $data=array(
            "strana"=>'artikal',
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "og_image"=> !is_null($image) ? $image->putanja : null,
            "roba_id"=>$roba_id,
            "grupa_pr_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'),
            "proizvodjac_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'),
            "articles"=> Product::get_related($roba_id),
            "vezani_artikli"=>DB::table('vezani_artikli')->where('roba_id',$roba_id)->orderBy('vezani_roba_id','asc')->get(),
            "fajlovi" => DB::select("SELECT * FROM web_files WHERE roba_id = ".$roba_id." ORDER BY vrsta_fajla_id ASC")
            );
            return View::make('shop/themes/'.Support::theme_path().'pages/article_details',$data);
           }
        }
        
        if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
            return Redirect::to($new_link);
        }
        $data=array(
            "strana"=>'Not found',
            "title"=>'Not found',
            "description"=>'Not found',
            "keywords"=>'Not found'
        );
        $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
        return Response::make($content, 404);
	}

    public function article_link(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        return Url_mod::convert_url('artikal').'/'.Url_mod::url_convert(Product::seo_title($roba_id));
    }
    
    public function quick_view(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
        $image = Product::web_slika($roba_id);
        $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$roba->proizvodjac_id)->first();

        $data = array(
            'roba_id' => $roba_id,
            'image' => $image,
            'proizvodjac' => $proizvodjac,
            );
        return View::make('shop/themes/'.Support::theme_path().'partials/products/ajax/quick_view_contant',$data)->render();
    }

}