 <?php

class IntesaBankingController extends BaseController{

    public function intesa(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url());
        }
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return Redirect::to(Options::base_url());
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return Redirect::to(Options::base_url());
        }

        $orgClientId  =  Options::gnrl_options(3023,'str_data');
        $orgOid = $web_b2c_narudzbina_id;

        $orgAmount = DB::select("SELECT round(SUM(jm_cena*kolicina), 2) AS suma from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id=".$orgOid)[0]->suma;
        $orgOkUrl =  Options::base_url()."intesa-response";
        $orgFailUrl = Options::base_url()."intesa-response";
        $orgCancelUrl = Options::base_url()."korpa";
        $orgTransactionType = "PreAuth";
        $orgRnd =  microtime();
        $orgCurrency = "941";

        $clientId = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgClientId));
        $oid = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOid)); 
        $amount = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgAmount)); 
        $okUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOkUrl)); 
        $failUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgFailUrl)); 
        $cancelUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgCancelUrl));
        $transactionType = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgTransactionType)); 
        // $installment = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgInstallment)); 
        $rnd = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgRnd ));
        $currency = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgCurrency));
        $storeKey = str_replace("|", "\\|", str_replace("\\", "\\\\", Options::gnrl_options(3024,'str_data')));

        $plainText = $clientId . "|" . $oid . "|" . $amount . "|" . $okUrl . "|" . $failUrl . "|" . $transactionType . "||" . $rnd . "||||" . $currency . "|" . $storeKey;
        $hashValue = hash('sha512', $plainText);
        $hash = base64_encode (pack('H*',$hashValue));

        $data = array(
            'clientId' => $clientId,
            'oid' => $oid,
            'amount' => $amount,
            'okUrl' => $okUrl,
            'failUrl' => $failUrl,
            'cancelUrl' => $cancelUrl,
            'transactionType' => $transactionType,
            // 'installment' => $installment,
            'rnd' => $rnd,
            'currency' => $currency,
            'storeKey' => $storeKey,
            'hash' => $hash,

            'web_kupac' => $web_kupac,
            'preduzece' => $preduzece,
	    	'title' => 'Intesa',
	    	'description' => 'Intesa',
	    	'keywords' => 'intesa',
	    	'strana' => 'intesa'
            );

    	return View::make('shop/themes/'.Support::theme_path().'pages/intesa',$data);
    }

    public function intesa_response(){
        $request = Request::all();

        if(isset($request['ProcReturnCode']) && isset($request['ReturnOid'])){

            $web_b2c_narudzbina_id=$request['ReturnOid'];
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::convert_url('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }

            $bank_response = json_encode(array(
                'realized' => $request['ProcReturnCode'] == "00" ? 1 : 0,
                'payment_id' => isset($request['xid']) ? $request['xid'] : '',
                'oid' => isset($request['ReturnOid']) ? $request['ReturnOid'] : '',
                'trans_id' => isset($request['TransId']) ? $request['TransId'] : '',
                'track_id' => isset($request['TRANID']) ? $request['TRANID'] : '',
                'post_date' => isset($request['EXTRA_TRXDATE']) ? $request['EXTRA_TRXDATE'] : '',
                'result_code' => isset($request['ProcReturnCode']) ? $request['ProcReturnCode'] : '',
                'auth_code' => isset($request['AuthCode']) ? $request['AuthCode'] : '',
                'response' => isset($request['Response']) ? $request['Response'] : '',
                'md_status' => isset($request['mdStatus']) ? $request['mdStatus'] : ''
                ));

            $data = array('result_code'=> $bank_response);
            if($request['ProcReturnCode'] == "00"){
                $data['stornirano'] = 0;
                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
                return Redirect::to(Options::base_url().Url_mod::convert_url('narudzbina').'/'.$web_b2c_narudzbina_id);
            }
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
        }else{
            $web_b2c_narudzbina_id=$request['oid'];
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::convert_url('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }           
            $bank_response = json_encode(array(
                'realized' => 0,
                'payment_id' => isset($request['xid']) ? $request['xid'] : '',
                'oid' => isset($request['oid']) ? $request['oid'] : '',
                'trans_id' => '',
                'track_id' => isset($request['TRANID']) ? $request['TRANID'] : '',
                'post_date' => '',
                'result_code' => '',
                'auth_code' => '',
                'response' => '',
                'md_status' => isset($request['mdStatus']) ? $request['mdStatus'] : '',
                'bank_response' => $request
                ));
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('result_code'=> $bank_response));
        }
        return Redirect::to(Options::base_url().Url_mod::convert_url('intesa-odgovor').'/'.$web_b2c_narudzbina_id);
    }

    public function intesa_failure(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3 && $web_b2c_narudzbina->stornirano!=0){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('Intesa greška')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

}