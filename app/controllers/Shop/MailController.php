<?php 
use Service\Mailer;


class MailController extends Controller {

	public function meil_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

    	if(Options::gnrl_options(3003)){   
    	 		
	    	$data = array(
	    		'name' => Input::get('name'), 
	    		'email' => Input::get('email'),
	    		'msg' => Input::get('message'),
	    		'url' => Options::base_url()
	    	);

	    	Mail::send('shop.email.contact', $data, function($message){
	    	    $message->from(Input::get('email'), Input::get('name'));

	    	    $message->to(Options::company_email())->subject(EmailOption::subject());
	    	});
    	}else{
 		
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('name')." <br />
	        <b>Email:</b> ".Input::get('email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('message')."</p>";
	        $subject="Poruka sa ".Options::server();
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);
    	}

    } 


}