<?php 

class MainController extends Controller {
    public function index(){
        if(Options::web_options(130) == 2){
            if(Session::has('b2b_user_'.Options::server())){
                return Redirect::to(Options::base_url().'b2b/pocetna');
            }
            $strana='login';
            $seo=array(
                "title"=>'Prijava',
                "description"=>'Prijava',
                "keywords"=>'prijava',
            );
            return View::make('b2b.pages.login',compact('seo'));            
        }

        $strana=All::get_page_start();
        $seo = Seo::page($strana);
        $data=array(
            "strana"=>$strana,
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords
        );

        if(Options::web_options(136)==1){
            $limit = Session::has('limit') ? Session::get('limit') : 20;
            $pageNo = Input::get('page') ? Input::get('page') : 1;
            $offset = ($pageNo-1)*$limit;                
            $main_query = "SELECT DISTINCT r.roba_id, r.akcija_flag_primeni FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."";
            $paginate_query = " ORDER BY r.akcija_flag_primeni DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."";

            $count = count(DB::select($main_query));
            $data["articles"]=DB::select($main_query.$paginate_query);
            $data["limit"]=$limit;
            $data["count_products"]=$count;
        }
        return View::make('shop/themes/'.Support::theme_path().'pages/home',$data);     
    }
    
	public function lang(){
        $lang = Language::multi() ? Request::segment(1) : null;
		return Redirect::to('/');
			
	}
	public function page(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $strana = Request::segment(1+$offset);

        $strana = Language::slug_convert($strana);

        if($strana==""){
            $strana=All::get_page_start();
        }
        
        if(Seo::get_page_id($strana) != 0){
            $seo = Seo::page($strana);
            $data=array(
                "strana"=>$strana,
                "naziv"=>str_replace(' - '.Options::company_name(),'',$seo->title),
                "title"=>$seo->title,
                "description"=>$seo->description,
                "keywords"=>$seo->keywords
            );
        }else{
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }

            $data=array(
                "strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
        }


        if($strana == All::get_page_start()){
            if(Options::web_options(136)==1){
            $limit = Session::has('limit') ? Session::get('limit') : 20;
            $pageNo = Input::get('page') ? Input::get('page') : 1;
            $offset = ($pageNo-1)*$limit;                
            $main_query = "SELECT DISTINCT r.roba_id FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."";
            $paginate_query = " ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."";
            
            $count = count(DB::select($main_query));
            $data["articles"]=DB::select($main_query.$paginate_query);
            $data["limit"]=$limit;
            $data["count_products"]=$count;
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/home'; 
        }elseif($strana == Seo::get_kontakt()){
            $path = 'shop/themes/'.Support::theme_path().'pages/contact';
        }elseif($strana==Seo::get_korpa()){
        	$path = 'shop/themes/'.Support::theme_path().'pages/cart_content';
        }elseif($strana=='konfigurator' AND Options::web_options(121)){
            $konfigurator = DB::table('konfigurator')->where(array('dozvoljen'=>1))->first();
            if(!is_null($konfigurator) && Options::web_options(121)==1){
            	return Redirect::to(Options::base_url().Url_mod::convert_url('konfigurator').'/'.$konfigurator->konfigurator_id);
            }else{
                return Redirect::to(Options::base_url());
            }
        }elseif($strana=='prijava'){
            if(Session::has('b2c_kupac') || !Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(2,5,9,10))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/login';
        }elseif($strana=='registracija'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/registration';
        }elseif($strana=='pravno-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,7,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/company';
        }elseif($strana=='fizicko-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,7,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/private_user';
        }
        elseif($strana=='sve-kategorije'){
            $path = 'shop/themes/'.Support::theme_path().'pages/all_categories';
        }else{
            $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>Seo::get_page_id($strana)))->first();
            if($stranica->grupa_pr_id > 0){
                $redirect = Options::base_url() . Url_mod::url_convert(Groups::getGrupa($stranica->grupa_pr_id)).'/0/0/0-0';
                return Redirect::to($redirect);
            }elseif($stranica->tip_artikla_id != -1){
                $redirect = Options::base_url();
                if($stranica->tip_artikla_id == 0){
                    $redirect .= Url_mod::convert_url('akcija');
                }else{
                    $redirect .= Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($stranica->tip_artikla_id));
                }
                return Redirect::to($redirect);
            }

            $data['content'] = '';
            $stranica_jezik=DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>Seo::get_page_id($strana), 'jezik_id'=>Language::lang_id()))->first();
            if(!is_null($stranica_jezik)){
                $data['content'] = $stranica_jezik->sadrzaj;
            }
        	$path = 'shop/themes/'.Support::theme_path().'pages/content';
        }
      
		return View::make($path,$data);
	} 

    function coment_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
        $ime=Input::get('ime');
        $pitanje=Input::get('pitanje');
        $ocena=Input::get('ocena');
        
        $ip=All::ip_adress();
        $data=array(
        'roba_id'=>$roba_id,
        'ip_adresa'=>$ip,
        'ime_osobe'=>$ime,
        'pitanje'=>$pitanje,
        'komentar_odobren'=>0,
        'datum'=>date("Y-m-d"),
        'odgovoreno'=>0,
        'ocena'=>$ocena
        );
        DB::table('web_b2c_komentari')->insert($data);
		echo "Ok";
    }
    
    public function newsletter_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

	        $email=Input::get('email');
	        
	        $broj_korisnika_istih=DB::table('web_b2c_newsletter')->where('email',$email)->count();
	        if($broj_korisnika_istih > 0){
	            echo "E-mail je već registrovan.";
	        }
	        else {
                $kod = Support::custom_encrypt($email);

                 $body="Poštovani,<br><br>Prijavili ste se za prijem novosti iz naše prodavnice.<br>
                       Da biste aktivirali prijem novosti, molimo Vas da <a href='".Options::base_url().Url_mod::convert_url('newsletter-potvrda')."/".$kod."' target='_blank'>KLIKNETE OVDE<a> kako biste potvrdili Vašu adresu.<br><br>
                       Ukoliko se niste Vi prijavili i ovu poruku poruku ste primili greškom, molimo Vas da je zanemarite.<br><br>
                       S poštovanjem,<br>
                       ". Options::company_name()   ."<br>"
                        . Options::company_adress() .", ". Options::company_city() ."<br>"    
                        ."mob: ". Options::company_phone()  ."<br>"
                        ."fax: ".   Options::company_fax();

                $subject="Prijava za newsletter na ".Options::company_name();
                if(!DB::table('web_b2c_newsletter')->where('email',$email)->first()) {
                    WebKupac::send_email_to_client($body,$email,$subject);
                    DB::table('web_b2c_newsletter')->insert(['email'=>$email,'aktivan'=>0]);
                }
                echo "Potvrdite prijavu za newsletter preko vašeg mail-a.";

	        }
	        
	}

    public function newsletter_confirm(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $kod = Request::segment(2+$offset);

        $email = Support::custom_decrypt($kod);
        $validator = Validator::make(array('email'=>$email),array('email' => 'required|email'),Language::validator_messages());

        if(!$validator->fails()){
            $data=array('aktivan'=>1);
            DB::table('web_b2c_newsletter')->where('email',$email)->update($data);
        }
        
        return Redirect::to('/');
    }    
}


 