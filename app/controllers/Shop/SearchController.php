<?php 

class SearchController extends Controller {

	public function search(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $search = Request::segment(2+$offset);
        $grupa_pr_id = Request::segment(3+$offset);

		$search = strtolower(urldecode($search));

		$a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М'); 
  		$b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm'); 

  		$search = str_replace($a, $b, $search); 

		if(Session::has('limit')){
			$limit=Session::get('limit');
		}
		else { 
			$limit=20;
		}
	    if(Input::get('page')){
	    	$pageNo = Input::get('page');
	    }else{
	    	$pageNo = 1; 
	    }
	    $offset = ($pageNo-1)*$limit;

		$reci = explode(" ", $search);
		
			$nazivFormatiran0 = '';
		// Za svaku pretrazenu rec dodaje deo query-ja
		foreach ($reci as $rec) {
			$nazivFormatiran0 .= "AND naziv_web ILIKE '%" . $rec . "%' ";
			// Brise AND samo na pocetku querija, a na ostalim recim ostaje
			$nazivFormatiran = substr($nazivFormatiran0, 4);
		}

		$naziv_char_translate0 = '';
		// Za svaku pretrazenu rec dodaje deo query-ja
		foreach ($reci as $rec) {
			$naziv_char_translate0 .= "AND translate (naziv_web, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			// Brise AND samo na pocetku querija, a na ostalim recim ostaje
			$naziv_char_translate = substr($naziv_char_translate0, 4);
		}

		$tagsFormatiran0 = '';
		foreach ($reci as $rec) {
			$tagsFormatiran0 .= "AND tags ILIKE '%" . $rec . "%' ";
			$tagsFormatiran = substr($tagsFormatiran0, 4);
		}

		$tags_char_translate0 = '';
		foreach ($reci as $rec) {
			$tags_char_translate0 .= "AND translate (tags, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$tags_char_translate = substr($tags_char_translate0, 4);
		}

		$opisFormatiran0 = '';
		foreach ($reci as $rec) {
			$opisFormatiran0 .= "AND web_opis ILIKE '%" . $rec . "%' ";
			$opisFormatiran = substr($opisFormatiran0, 4);
		}

		$opis_char_translate0 = '';
		foreach ($reci as $rec) {
			$opis_char_translate0 .= "AND translate (web_opis, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$opis_char_translate = substr($opis_char_translate0, 4);
		}

		$karakFormatiran0 = '';
		foreach ($reci as $rec) {
			$karakFormatiran0 .= "AND karakteristika_naziv ILIKE '%" . $rec . "%' ";
			$karakFormatiran = substr($karakFormatiran0, 4);
		}

		$karak_char_translate0 = '';
		foreach ($reci as $rec) {
			$karak_char_translate0 .= "AND translate (karakteristika_naziv, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$karak_char_translate = substr($karak_char_translate0, 4);
		}

		$karakVrednostFormatiran0 = '';
		foreach ($reci as $rec) {
			$karakVrednostFormatiran0 .= "AND karakteristika_vrednost ILIKE '%" . $rec . "%' ";
			$karakVrednostFormatiran = substr($karakVrednostFormatiran0, 4);
		}

		$karakVrednost_char_translate0 = '';
		foreach ($reci as $rec) {
			$karakVrednost_char_translate0 .= "AND translate (karakteristika_vrednost, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$karakVrednost_char_translate = substr($karakVrednost_char_translate0, 4);
		}

		$dopunski_nazivFormatiran0 = '';
		foreach ($reci as $rec) {
			$dopunski_nazivFormatiran0 .= "AND naziv_dopunski ILIKE '%" . $rec . "%' ";
			$dopunski_nazivFormatiran = substr($dopunski_nazivFormatiran0, 4);
		}

		$dopunski_naziv_char_translate0 = '';
		foreach ($reci as $rec) {
			$dopunski_naziv_char_translate0 .= "AND translate (naziv_dopunski, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$dopunski_naziv_char_translate = substr($dopunski_naziv_char_translate0, 4);
		}

		$sifraisFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifraisFormatiran0 .= "AND sifra_is ILIKE '%" . $rec . "%' ";
				$sifraisFormatiran = substr($sifraisFormatiran0, 4);
		}

		$SKUFormatiran0 = '';
		foreach ($reci as $rec) {
			$SKUFormatiran0 .= "AND sku ILIKE '%" . $rec . "%' ";
			$SKUFormatiran = substr($SKUFormatiran0, 4);
		}

		$sifradFormatiran0 = '';
		foreach ($reci as $rec) {
			$sifradFormatiran0 .= "AND sifra_d ILIKE '%" . $rec . "%' ";
			$sifradFormatiran = substr($sifradFormatiran0, 4);
		}

	    $grupa_pr_ids=array();
	    $grupe_query = "";
	    if($grupa_pr_id){
		    Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
		    $grupe_query = "grupa_pr_id IN (".implode(",",$grupa_pr_ids).") AND ";
	    }



		//NOVI QUERY SA KARAKTERISTIKAMA
		$query = "SELECT roba_id, naziv_web, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa FROM roba WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$grupe_query."(" . $nazivFormatiran . " OR " . $naziv_char_translate . " OR " . $tagsFormatiran . " OR " . $tags_char_translate . " OR " . $opisFormatiran . " OR " . $opis_char_translate . " OR " . $dopunski_nazivFormatiran . " OR " . $dopunski_naziv_char_translate . " OR " . $sifraisFormatiran . " OR " .$SKUFormatiran. " OR " .$sifradFormatiran. " OR roba_id IN (SELECT roba_id FROM dobavljac_cenovnik_karakteristike WHERE roba_id <> -1 AND (" . $karakFormatiran . " OR " . $karak_char_translate . " OR " . $karakVrednostFormatiran . " OR " . $karakVrednost_char_translate . ")))";

		
		$query_products = DB::select($query." ORDER BY ".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");

		$data=array(
			"strana"=>'pretraga',
			"title"=>"Pretraga",
			"description"=>"",
			"keywords"=>"",
			"articles"=>$query_products,
			"count_products"=>count(DB::select($query)),
			"filter_prikazi"=>0,
			"limit"=>$limit
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	
	
	}	


	public function livesearch() {
        $lang = Language::multi() ? Request::segment(1) : null;

			$search = Input::get('search');
			$grupa_pr_id = Input::get('grupa_pr_id');

			$search = strtolower($search);
			$a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М'); 
	  		$b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm'); 
	  		$search = str_replace($a, $b, $search); 


			$reci = explode(" ", $search);

			$nazivFormatiran0 = '';

			// Za svaku pretrazenu rec dodaje deo query-ja
			foreach ($reci as $rec) {
				$nazivFormatiran0 .= "AND naziv_web ILIKE '%" . $rec . "%' ";
				// Brise AND samo na pocetku querija, a na ostalim recim ostaje
				$nazivFormatiran = substr($nazivFormatiran0, 4);
			}

			$naziv_char_translate0 = '';
			// Za svaku pretrazenu rec dodaje deo query-ja
			foreach ($reci as $rec) {
				$naziv_char_translate0 .= "AND translate (naziv_web, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				// Brise AND samo na pocetku querija, a na ostalim recim ostaje
				$naziv_char_translate = substr($naziv_char_translate0, 4);
			}

			$tagsFormatiran0 = '';
			foreach ($reci as $rec) {
				$tagsFormatiran0 .= "AND tags ILIKE '%" . $rec . "%' ";
				$tagsFormatiran = substr($tagsFormatiran0, 4);
			}

			$tags_char_translate0 = '';
			foreach ($reci as $rec) {
				$tags_char_translate0 .= "AND translate (tags, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$tags_char_translate = substr($tags_char_translate0, 4);
			}

			$opisFormatiran0 = '';
			foreach ($reci as $rec) {
				$opisFormatiran0 .= "AND web_opis ILIKE '%" . $rec . "%' ";
				$opisFormatiran = substr($opisFormatiran0, 4);
			}

			$opis_char_translate0 = '';
			foreach ($reci as $rec) {
				$opis_char_translate0 .= "AND translate (web_opis, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$opis_char_translate = substr($opis_char_translate0, 4);
			}

			$karakFormatiran0 = '';
			foreach ($reci as $rec) {
				$karakFormatiran0 .= "AND karakteristika_naziv ILIKE '%" . $rec . "%' ";
				$karakFormatiran = substr($karakFormatiran0, 4);
			}

			$karak_char_translate0 = '';
			foreach ($reci as $rec) {
				$karak_char_translate0 .= "AND translate (karakteristika_naziv, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$karak_char_translate = substr($karak_char_translate0, 4);
			}

			$karakVrednostFormatiran0 = '';
			foreach ($reci as $rec) {
				$karakVrednostFormatiran0 .= "AND karakteristika_vrednost ILIKE '%" . $rec . "%' ";
				$karakVrednostFormatiran = substr($karakVrednostFormatiran0, 4);
			}

			$karakVrednost_char_translate0 = '';
			foreach ($reci as $rec) {
				$karakVrednost_char_translate0 .= "AND translate (karakteristika_vrednost, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$karakVrednost_char_translate = substr($karakVrednost_char_translate0, 4);
			}

			$dopunski_nazivFormatiran0 = '';
			foreach ($reci as $rec) {
				$dopunski_nazivFormatiran0 .= "AND naziv_dopunski ILIKE '%" . $rec . "%' ";
				$dopunski_nazivFormatiran = substr($dopunski_nazivFormatiran0, 4);
			}

			$dopunski_naziv_char_translate0 = '';
			foreach ($reci as $rec) {
				$dopunski_naziv_char_translate0 .= "AND translate (naziv_dopunski, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$dopunski_naziv_char_translate = substr($dopunski_naziv_char_translate0, 4);
			}

			$sifraisFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifraisFormatiran0 .= "AND sifra_is ILIKE '%" . $rec . "%' ";
				$sifraisFormatiran = substr($sifraisFormatiran0, 4);
			}

			$SKUFormatiran0 = '';
			foreach ($reci as $rec) {
				$SKUFormatiran0 .= "AND sku ILIKE '%" . $rec . "%' ";
				$SKUFormatiran = substr($SKUFormatiran0, 4);
			}

			$sifradFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifradFormatiran0 .= "AND sifra_d ILIKE '%" . $rec . "%' ";
				$sifradFormatiran = substr($sifradFormatiran0, 4);
			}

		    $grupa_pr_ids=array();
		    $grupe_query = "";
		    if($grupa_pr_id){
			    Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
			    $grupe_query = "grupa_pr_id IN (".implode(",",$grupa_pr_ids).") AND ";
		    }

			$articles = DB::select("SELECT roba_id, naziv_web, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa FROM roba WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$grupe_query."(" . $nazivFormatiran . " OR " . $naziv_char_translate . " OR " . $tagsFormatiran . " OR " . $tags_char_translate . " OR ". $opisFormatiran . " OR " . $opis_char_translate . " OR " . $dopunski_nazivFormatiran . " OR " . $dopunski_naziv_char_translate . " OR " . $sifraisFormatiran . " OR " .$SKUFormatiran. " OR " .$sifradFormatiran. " OR roba_id IN (SELECT roba_id FROM dobavljac_cenovnik_karakteristike WHERE roba_id <> -1 AND (" . $karakFormatiran . " OR " . $karak_char_translate . " OR " . $karakVrednostFormatiran . " OR " . $karakVrednost_char_translate . "))) ORDER BY grupa ASC LIMIT 10");
			header('Content-type: text/plain; charset=utf-8');			
			$list = "<ul class='JSsearch_list'>";
			foreach ($articles as $article) {
				$list .= "
					<li class='search_list__item'>
						<a class='search_list__item__link' href='" . Options::base_url() .Url_mod::convert_url('artikal').'/'. Url_mod::url_convert(Product::seo_title($article->roba_id)) . "'>"
							."<span class='search_list__item__link__text'>" . $article->naziv_web . "</span>"
							."<span class='search_list__item__link__cat'>" . $article->grupa . "</span>
						</a>
					</li>";
			}

			$list .= "</ul>";
			echo $list;

	}



} 