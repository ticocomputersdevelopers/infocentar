<?php

class BrendoviController extends Controller {
	public function brend_list(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;

		$strana= 'brendovi';
		$check_page = DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->count();
		if($check_page==0){
			return Redirect::to(Options::base_url());
		}
		$brendovi = DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->orderBy('rbr','asc')->orderBy('naziv','asc')->get();

		$seo = Seo::page($strana);
		$data=array(
			"strana"=>$strana,
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
			"brendovi"=>$brendovi
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/brands',$data);		
	}

	public function proizvodjac(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$naziv = Request::segment(2+$offset);
		$grupa = Request::segment(3+$offset);

		$brendovi = DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->get();

		$naziv = Language::slug_convert($naziv);
		foreach($brendovi as $brend){
			if(Url_mod::url_convert($brend->naziv) == $naziv){
				$proizvodjac = $brend;
				break;
			}
		}
		if(!isset($proizvodjac)){
			return Redirect::to(Options::base_url());
		}
		if($grupa==null){
			$description = $proizvodjac->opis;
			$keywords = $proizvodjac->keywords;
			$check_grupa = "";
		}else{
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->get() as $gr){

		    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." 
		    	WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND r.grupa_pr_id = ".$gr->grupa_pr_id." AND r.proizvodjac_id = ".$proizvodjac->proizvodjac_id."")[0]->count;
		    
				if(Url_mod::url_convert($gr->grupa) == $grupa && $count > 0){
					$grupa_pr_id = $gr->grupa_pr_id;
					break;
				}
			}
			$description = Seo::grupa_description($grupa_pr_id);
			$keywords = Seo::grupa_keywods($grupa_pr_id);
			$level2=Groups::vratiSveGrupe($grupa_pr_id);

			$check_grupa = "AND r.grupa_pr_id IN(".implode(",",$level2).")";
		}

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND proizvodjac_id = ".$proizvodjac->proizvodjac_id." ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."".$check_grupa."";

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");
				}

		$seo = Seo::proizvodjac($proizvodjac->proizvodjac_id);
		$data=array(
			"strana"=>"proizvodjac",
			"title"=>$seo->title,
			"description"=>$seo->description,
			"keywords"=>$seo->keywords,
			"proizvodjac_id"=>$proizvodjac->proizvodjac_id,
			"proizvodjac"=>$proizvodjac->naziv,
			"grupa"=>isset($grupa) ? $grupa : '',
			"articles"=>$artikli,
			"limit"=>$limit,
			"count_products"=>$count,
			"filter_prikazi" => 0
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);		
	}
}