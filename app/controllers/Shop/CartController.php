<?php

class CartController extends Controller {

	public function list_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		Cart::add_to_cart($kupac_id,$korpa_id,$roba_id,1,array());

		echo json_encode(array('check_available'=>Cart::check_avaliable($roba_id),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart(),'cart_ukupno' => Cart::cena(Cart::cart_ukupno())));
	}

	public function quick_view_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
		$kolicina=Input::get('kolicina');
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		$available = Cart::check_avaliable($roba_id);

		$added = false;
		if($available >= $kolicina){
			Cart::add_to_cart($kupac_id,$korpa_id,$roba_id,intval($kolicina),array());
			$added = true;
		}

		echo json_encode(array('added'=>$added,'check_available'=>Cart::check_avaliable($roba_id),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart(),'cart_ukupno' => Cart::cena(Cart::cart_ukupno())));
	}

	public function wish_list_add(){
		$lang = Language::multi() ? Request::segment(1) : null;  

		$roba_id=Input::get('roba_id');
		$kupac_id = Cart::kupac_id();

 
		if(Cart::add_to_wish($kupac_id,$roba_id)){
			$message = Language::trans('Artikal je dodat na listu želja.');
		}else{
			$message = Language::trans('Artikal je već dodat na listu želja.');
		}
		
		echo json_encode(array("broj_wish"=>Cart::broj_wish(),"message"=>$message));
	}

	public function wish_list_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');		
		

		Cart::delete_wish($roba_id);

	}

	public function product_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$inputs=Input::get();
		$available = Cart::check_avaliable($inputs['roba_id']);

        $validator = Validator::make($inputs, array('kolicina' => 'numeric|min:1|max:'.$available.''), array('numeric' => Language::trans('Polje za količinu moze sadržati samo brojeve!'),'min' => Language::trans('Minimalna kolicina je 1!'),'max' => Language::trans('Maksimalna kolicina je '.$available.'!')));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{ 
        	$osobine_arr = array();
        	foreach($inputs as $key => $val){
        		if(strpos($key, 'osobine') !== false){
        			$osobine_arr[] = $val;
        		}
        	}

			$kupac_id = Cart::kupac_id();
			$korpa_id = Cart::korpa_id();
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$inputs['kolicina'],$osobine_arr);
			return Redirect::back()->withInput()->with('success_add_to_cart',true);
        }
	}
	public function konfigurator_cart_add(){
	    $lang = Language::multi() ? Request::segment(1) : null;

	    // Validacija ulaznih podataka
	    $validator = Validator::make(Input::all(), [
	        'roba_id' => 'required|integer',
	        'kolicina' => 'required|numeric'
	    ]);

	    // Provera da li je validacija neuspešna
	    if ($validator->fails()) {
	         return Redirect::back();
	    } else {
	        // Dobijanje validiranih podataka
	        $inputs = Input::all();
	        
	        $kupac_id = Cart::kupac_id();
	        $korpa_id = Cart::korpa_id();
	        $available = Cart::check_avaliable($inputs['roba_id']);

	        if ($available >= $inputs['kolicina']) {
	            Cart::add_to_cart($kupac_id, $korpa_id, $inputs['roba_id'], $inputs['kolicina'], array());
	           return Response::json(['message' => 'available'], 200);
	        } else {
	            Cart::add_to_cart($kupac_id, $korpa_id, $inputs['roba_id'], $available, array());
	           return Response::json(['message' => 'not-available'], 200);
	        }
	    }
	}
	public function vezani_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$inputs=Input::get();
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		$available = Cart::check_avaliable($inputs['roba_id']);
		if($available >= $inputs['kolicina']){
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$inputs['kolicina'],array());
		}else{
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$available,array());
		}
		echo json_encode(array('check_available'=>Cart::check_avaliable($inputs['roba_id']),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart()));

	}

	public function cart_add_sub(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$stavka_id=Input::get('stavka_id');
		$kolicina = Input::get('kolicina');

		$roba_id = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('roba_id');
		$old_kolicina = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('kolicina');;
		$diff_kolicina = intval($kolicina) - $old_kolicina;
		$available = Cart::check_avaliable($roba_id);

		if($available >= $diff_kolicina){
			$new_kolicina = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('kolicina') + $diff_kolicina;
			DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kolicina));
			Cart::rezervacija($roba_id,$diff_kolicina);
			echo 'kolicina-changed';
		}else{
			echo 'not-available';
		}
	}
	public function cart_stavka_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;
		
		$stavka_id=Input::get('stavka_id');
		$stavka = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->first();
		Cart::rezervacija($stavka->roba_id,$stavka->kolicina);
		DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->delete();
	}
	public function cart_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;

		foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->get() as $row){
			Cart::rezervacija($row->roba_id,$row->kolicina);
		}
		DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->delete();
	}

}