<?php
use Service\DExpress;
use Service\Mailer;

class AdminNarudzbineController extends Controller {

    
    function porudzbine($status, $narudzbina_status_id, $datum_od, $datum_do, $search)
    {   

        $statusi = explode('-',$status);
        $query_osnovni=0;
        
        $select = "SELECT * FROM web_b2c_narudzbina LEFT JOIN web_kupac ON web_kupac.web_kupac_id = web_b2c_narudzbina.web_kupac_id ";
        $where="";
        $where_registracija= "";
        $where_vrsta= "";
        $where_status= "";
        
        if (count($statusi)>1 && in_array('sve', $statusi)) {
            unset($statusi[array_search('sve', $statusi)]);
        }

        if (!in_array('sve', $statusi)) {
         
            if (in_array('registrovani', $statusi)) {
            $where_registracija.= "OR status_registracije=1 ";
            }
            if (in_array('neregistrovani', $statusi)) {
            $where_registracija.= "OR status_registracije=0 ";
            }
            if (in_array('nove', $statusi)) {
            $where_status.= "OR (prihvaceno=0 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('prihvacene', $statusi)) {
            $where_status.= "OR (prihvaceno=1 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('realizovane', $statusi)) {
            $where_status.= "OR (realizovano=1 AND stornirano=0) ";
            }
            if (in_array('stornirane', $statusi)) {
            $where_status.= "OR stornirano=1 ";
            }
            if (in_array('privatna', $statusi)) {
            $where_vrsta.= "OR flag_vrsta_kupca=0 ";
            }
            if (in_array('pravna', $statusi)) {
            $where_vrsta.= "OR flag_vrsta_kupca=1 ";
            }
           
        }

        if ($where_registracija != '') {
            $where.="AND (".substr($where_registracija, 3).") ";
        }
        if ($where_status != '') {
            $where.="AND (".substr($where_status, 3).") ";
        }
        if ($where_vrsta != '') {
            $where.="AND (".substr($where_vrsta, 3).") ";
        }
// echo $where; die;
        if($datum_od==0 and $datum_do==0){
        }
        else if($datum_od!=0 and $datum_do==0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' ";
        }
        else if($datum_od==0 and $datum_do!=0){
            $where.= "AND datum_dokumenta <= '".$datum_do."' ";
        }
        else if($datum_od!=0 and $datum_do!=0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' AND datum_dokumenta <= '".$datum_do."' ";
        }

        // filter dodatnih statusa
        if($narudzbina_status_id==0){
        }
        else if($narudzbina_status_id !=0){
            $where.="AND narudzbina_status_id ='".$narudzbina_status_id."' ";
        }

        //search

        if($search=='' ){
            $where.="AND web_b2c_narudzbina_id != -1 "; 
        }
        else if($search !=''){
            $where_search="";
            foreach (explode(' ',$search) as $word) {                   
                      $where_search.= "broj_dokumenta ILIKE '%" . strtoupper($word) . "%' OR adresa ILIKE '%" . $word . "%' OR ime ILIKE '%" . $word . "%' OR prezime ILIKE '%" . $word . "%' OR telefon ILIKE '%" . $word . "%' OR ";
                       }           
            $where.="AND (". substr($where_search, 0,-3) .") ";
        }
 
        if(Input::get('page')){
            $pageNo = Input::get('page');
        }else{
            $pageNo = 1;
        }

        $limit = 20;
        $offset = ($pageNo-1)*$limit;

        $pagination = " ORDER BY web_b2c_narudzbina_id DESC LIMIT ".$limit." OFFSET ".$offset."";

        $query_basic = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3));
        $query = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3).$pagination);

        $data=array(
		"strana"=>'pocetna',
		"title"=>"Administratorski panel",
		"query"=>$query,
        "count"=>count($query_basic),
        "limit"=>$limit,
        "narudzbina_status_id"=>$narudzbina_status_id,
        "statusi"=>$statusi,
        "search"=>$search,
        "datum_od"=>$datum_od,
        "datum_do"=>$datum_do
		);
    return View::make('admin/page',$data);

    }

    function narudzbina($web_b2c_narudzbina_id, $roba_id = null)
    {   
        
        if(isset($roba_id)){
            $check_roba_id = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,'roba_id'=>$roba_id))->count();
            if($check_roba_id == 0){            
                $stavka_data = DB::table('roba')->select('tarifna_grupa_id','racunska_cena_nc')->where('roba_id',$roba_id)->first();
                $insert_array = array(
                    'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
                    'broj_stavke' => 1,
                    'roba_id' => $roba_id,
                    'kolicina' => 1,
                    'jm_cena' => AdminCommon::get_price($roba_id),
                    'tarifna_grupa_id' => $stavka_data->tarifna_grupa_id,
                    'racunska_cena_nc' => $stavka_data->racunska_cena_nc
                    );
                if(AdminOptions::vodjenje_lagera() == 1){
                    $new_lager = intval(AdminSupport::lager($roba_id)) - 1;
                    DB::table('lager')->where('roba_id',$roba_id)->update(array('kolicina'=>$new_lager));
                }

                DB::table('web_b2c_narudzbina_stavka')->insert($insert_array);

                AdminSupport::saveLog('Narudzbina stavke, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id.' INSERT web_b2c_narudzbina_stavka_id -> '.DB::select("SELECT currval('web_b2c_narudzbina_stavka_web_b2c_narudzbina_stavka_id_seq')")[0]->currval);
            }
        }

        $web_b2c_narudzbina_id == 0 ? $check_old = false : $check_old = true;

        $data=array(
        "strana"=>'narudzbina',
        "title"=>"Narudzbina",
        "web_b2c_narudzbina_id" => $web_b2c_narudzbina_id,
        "web_kupac_id"=> $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_kupac_id') : '',
        "broj_dokumenta" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta') : AdminNarudzbine::brojNarudzbine(),
        "datum_dokumenta" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta') : date('Y-m-d'),
        "web_nacin_placanja_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_nacin_placanja_id') : '',
        "web_nacin_isporuke_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_nacin_isporuke_id') : '',
        "ip_adresa" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'ip_adresa') : '',
        "napomena" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena') : '',
        "posta_slanje_broj_posiljke" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'posta_slanje_broj_posiljke') : '',
        "posta_slanje_poslato" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'posta_slanje_poslato') : 0,
        "narudzbina_stavke" => $check_old ? DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->orderBy('web_b2c_narudzbina_stavka_id', 'asc')->get() : array(),
        "ukupna_cena" => $check_old ? AdminNarudzbine::ukupnaCena($web_b2c_narudzbina_id) : '0.00',
        "troskovi_isporuke" => $check_old ? AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id) : '0.00',
        "cena_dostave" => $check_old ? DB::table('cena_isporuke')->pluck('cena') : '0.00',
        "cena_do" => $check_old ? DB::table('cena_isporuke')->pluck('cena_do') : '0.00',
        "prihvaceno" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'prihvaceno') : '',
        "realizovano" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'realizovano') : '',
        "stornirano" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'stornirano') : '',
        "posta_slanje_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'posta_slanje_id') : 1,
        "narudzbina_status_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'narudzbina_status_id') : '',
        "narudzbina_poste_slanja" => $check_old ? AdminNarudzbine::narudzbina_poste_slanja($web_b2c_narudzbina_id) : ''
        );

    return View::make('admin/page',$data);

    }

    public function updateNarudzbinu()
    {
        $data = Input::get();
        if(AdminNarudzbine::find($data['web_b2c_narudzbina_id'], 'realizovano') || AdminNarudzbine::find($data['web_b2c_narudzbina_id'], 'stornirano')){
            $data_rel_stor = (array) DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$data['web_b2c_narudzbina_id'])->first();
            $data_rel_stor['napomena'] = $data['napomena'];
            $data = $data_rel_stor;
        }

        $rules = array(
            'web_kupac_id' => 'not_in:0',
            'broj_dokumenta' => 'required|between:2,10|unique:web_b2c_narudzbina,broj_dokumenta,'.$data['web_b2c_narudzbina_id'].',web_b2c_narudzbina_id',
            'posta_slanje_broj_posiljke' => 'max:15',
            'ip_adresa' => 'max:20',
            'napomena' => 'max:1000'
        );

        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$data['web_b2c_narudzbina_id'])->withInput()->withErrors($validator->messages());
        }else{
            
            $narudzbina = AdminNarudzbine::save($data);

            $data['web_b2c_narudzbina_id'] == 0 ? $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval : $web_b2c_narudzbina_id = $data['web_b2c_narudzbina_id'];

            AdminSupport::saveLog('Narudzbine, INSERT/EDIT web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id);
            $message = 'Uspešno ste sačuvali podatke';
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message', $message);
        }
    }

    // RACUN, PONUDA... PDFs
    public function pdf($web_b2c_narudzbina_id)
    {
        $data = [
            'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
        ];

        $pdf = App::make('dompdf');
        
        $pdf->loadView('admin.pdf_narudzbina', $data);
        AdminSupport::saveLog('Narudzbine PDF, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id); 
        return $pdf->stream();

        // AdminNarudzbine::createPdf($web_b2c_narudzbina_id);

    }

    public function pdf_racun($web_b2c_narudzbina_id)
    {
         $data = [
             'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_racun', $data);

        AdminSupport::saveLog('Racun PDF, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id);
        return $pdf->stream();    

    }

    public function pdf_ponuda($web_b2c_narudzbina_id)
    {
         $data = [
             'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_ponuda', $data);

        AdminSupport::saveLog('Ponuda PDF, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id);
        return $pdf->stream();
    }

    public function pdf_predracun($web_b2c_narudzbina_id)
    {
        $data = [
             'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
        ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_predracun', $data);

        AdminSupport::saveLog('Predracun PDF, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id);
        return $pdf->stream();
    }



    // AJAX
    public function narudzbineSearch() {

        $rec = trim(Input::get('articles'));
        $narudzbina_id = Input::get('narudzbina_id');

        $not_ids = 'r.roba_id NOT IN (';
        $artikli = DB::table('web_b2c_narudzbina_stavka')->select('roba_id')->where('web_b2c_narudzbina_id',$narudzbina_id)->get();
        
        if(count($artikli) > 0){            
            foreach ($artikli as $row) {
                $not_ids .= $row->roba_id.',';

            }
            $not_ids = substr($not_ids, 0, -1).') AND ';
        }else{
             $not_ids = '';
        }

        if(AdminOptions::vodjenje_lagera() == 1){
            $lager_join = 'INNER JOIN lager l ON r.roba_id = l.roba_id ';
            $lager_where = 'l.kolicina <> 0 AND ';
        }else{
            $lager_join = '';
            $lager_where = '';
        }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT r.roba_id, naziv_web, grupa FROM roba r ".$lager_join."INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE ".$lager_where."flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$not_ids."(" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . ") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "admin/narudzbina/".$narudzbina_id."/" . $article->roba_id . "'>"
                        ."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }

        $list .= "</ul>";
        echo $list; 
    }


    public function promena_statusa_narudzbine(){
        
        $status=Input::get('status_target');
        $web_b2c_narudzbina_id=Input::get('web_b2c_narudzbina_id');
                        
        switch($status){
            case "1":
                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('prihvaceno'=>1));
                AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id.', PRIHVACENA');

                if(AdminOptions::gnrl_options(3035) == 1){
                    AdminNarudzbine::mailOrderNotificationToClient($web_b2c_narudzbina_id, 'prihvacena');
                }
                echo '1';

            break;
            
             case "2":
                foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
                    AdminNarudzbine::realizovna_rezervacija($row->roba_id,$row->kolicina);
                }
                
                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('realizovano'=>1));
                AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id.', REALIZOVANA');

                if(AdminOptions::gnrl_options(3035) == 1){
                    AdminNarudzbine::mailOrderNotificationToClient($web_b2c_narudzbina_id, 'realizovana');
                }
                echo '2';


            break;
            case "3":
                $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('realizovano');
                foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
                   
                    if($realizovna==1){
                        $status2=2;
                    }else {
                        $status2=1;
                    }
                    AdminNarudzbine::stornirano_rezervacija($row->roba_id,$row->kolicina,$status2);
                }
                

                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('stornirano'=>1,'realizovano'=>0));
                AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$web_b2c_narudzbina_id.', STORNIRANA');
                echo '3';

            break;
        }


     
    }

    public function porudzbina_vise(){
        $web_b2c_narudzbina_id=Input::get('web_b2c_narudzbina_id');
        
        foreach(DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            echo '  <ul class="order-more-details-list">
                
                    <li class="row">
                        <span class="medium-6 columns">Broj porudžbine:</span>
                        <span class="medium-6 columns">'.$row->broj_dokumenta.'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Datum porudžbine:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Nacin isporuke:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::narudzbina_ni($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Način plaćanja:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::narudzbina_np($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Napomena:</span>
                        <span class="medium-6 columns">'.$row->napomena.'</span>
                    </li>
            
                </ul>';
        }
        echo '
        <hr>
        <ul class="order-more-product-list">
            <li class="row titles">
            <span class="medium-7 columns">Naziv proizvoda:</span>
            <span class="medium-2 columns">Cena:</span>
            <span class="medium-1 columns">Količina:</span>
            <span class="medium-2 columns">Ukupna cena:</span>
        </li>
        ';
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row2){
            echo '<li class="row">
                        <span class="medium-7 columns">'.AdminNarudzbine::artikal_narudzbina($row2->web_b2c_narudzbina_stavka_id).'</span>
                        <span class="medium-2 columns">'.AdminNarudzbine::artikal_cena_naruzbina($row2->web_b2c_narudzbina_stavka_id).'</span>
                        <span class="medium-1 columns">'.$row2->kolicina.'</span>
                        <span class="medium-2 columns">'.AdminNarudzbine::artikal_cena_stavka($row2->web_b2c_narudzbina_stavka_id).'</span>
                    </li>           
                ';
        }
        echo '
        <hr>
        <li class="row">
            <span class="medium-9 columns"><b>Ukupno:</b></span>
            <span class="medium-3 columns">'.AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($web_b2c_narudzbina_id)).'</span>
        </li>
        </ul>';
    }

    public function narudzbina_kupac(){
        $data = Input::get();
        $kupac = AdminKupci::kupacPodaci($data['web_kupac_id']);
        return View::make('admin/partials/ajax/narudzbina_kupac',array('kupac' => $kupac))->render();
    }

    public function deleteStavka() {

        $stavka_id = Input::get('stavka_id');
        if(AdminOptions::vodjenje_lagera() == 1){
            $stavka = DB::table('web_b2c_narudzbina_stavka')->select('roba_id','web_b2c_narudzbina_id')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->first();
            $roba_id = $stavka->roba_id;
            $web_b2c_narudzbina_id = $stavka->web_b2c_narudzbina_id;
            $old_stavka_kolicina = AdminNarudzbine::kolicina($web_b2c_narudzbina_id,$roba_id);
            $lager_kolicina = AdminSupport::lager($roba_id);
            DB::table('lager')->where('roba_id', $roba_id)->update(array('kolicina'=>$lager_kolicina+$old_stavka_kolicina));
        }

        AdminSupport::saveLog('Narudzbina stavke, web_b2c_narudzbina_id -> '.DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$stavka_id)->pluck('web_b2c_narudzbina_id').' DELETE web_b2c_narudzbina_stavka_id -> '.$stavka_id);
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->delete();

    }

    public function updateKolicina() {

        $stavka_id = Input::get('stavka_id');
        $kolicina = Input::get('kolicina');
        $narudzbina_id = Input::get('narudzbina_id');
        $results = array(
            'cena_artikla' => 0,
            'troskovi_isporuke' => 0,
            'ukupna_cena' => 0,
            'lager' => 0
            );

        $roba_id = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->pluck('roba_id');
        $results['lager'] = AdminSupport::lager($roba_id);

        if(AdminOptions::vodjenje_lagera() == 1){

            $old_stavka_kolicina = AdminNarudzbine::kolicina($narudzbina_id,$roba_id);
            $kolicinaOld = intval($results['lager']);
            $kolicinaDec = intval($kolicina);

            $kolicinaNew = $kolicinaOld - ($kolicinaDec - $old_stavka_kolicina);
            DB::table('lager')->where('roba_id',$roba_id)->update(array('kolicina'=>$kolicinaNew));
            $results['lager'] = $kolicinaNew;
        }

        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->update(['kolicina' => $kolicina]);
        AdminSupport::saveLog('Narudzbina stavke, web_b2c_narudzbina_id -> '.DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$stavka_id)->pluck('web_b2c_narudzbina_id').' UPDATE KOLICINA web_b2c_narudzbina_stavka_id -> '.$stavka_id);

        $cena_artikala = AdminNarudzbine::ukupnaCena($narudzbina_id);
        if(AdminOptions::web_options(133) == 1 AND AdminCommon::troskovi_isporuke($narudzbina_id)>0){
            $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['cena_artikla'] = $cena_artikala;
        }else{
            $results['ukupna_cena'] = $cena_artikala;
        }
        return json_encode($results);
    }

    public function prihvati(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['prihvaceno' => 1]);
        AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$narudzbina_id.', PRIHVACENA');

        if(AdminOptions::gnrl_options(3035) == 1){
            AdminNarudzbine::mailOrderNotificationToClient($narudzbina_id,'prihvacena');
        }
    }

    public function realizuj(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['realizovano' => 1]);
        AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$narudzbina_id.', REALIZOVANA');

        if(AdminOptions::gnrl_options(3035) == 1){
            AdminNarudzbine::mailOrderNotificationToClient($narudzbina_id,'realizovana');
        }
    }

    public function storniraj(){
        $narudzbina_id = Input::get('narudzbina_id');

        if(AdminOptions::vodjenje_lagera()==1){
            $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id'); 
            $magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();
            foreach($stavke as $stavka){
                $lager_query = DB::table('lager')->where(array('roba_id'=>$stavka->roba_id,'orgj_id'=>$magacin_id,'poslovna_godina_id'=>$godina_id));
                if(!is_null($lager_query->first())){
                    $kolicina = $lager_query->first()->kolicina + $stavka->kolicina;
                    $lager_query->update(array('kolicina'=>$kolicina));
                }else{
                    DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, nabavna_po, valuta_id, poslovna_godina_id) VALUES (".$stavka->roba_id.", ". $magacin_id .", ". $stavka->kolicina .", 0, 1, ". $godina_id .")");
                }
            }
        }
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['stornirano' => 1]);
        AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$narudzbina_id.', STORNIRANA');
    }

    public function nestorniraj(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['stornirano' => 0]);
        AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$narudzbina_id.', NESTORNIRANA');
    }
    public function editCena(){
            $narudzbina_id = Input::get('narudzbina_id');  
            $stavka_id = Input::get('stavka_id');         
            $val = Input::get('val');   
                       
            DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_stavka_id'=> $stavka_id ))->update(['jm_cena' => $val]); 

        $cena_artikala = AdminNarudzbine::ukupnaCena($narudzbina_id);
        if(AdminOptions::web_options(133) == 1 AND AdminCommon::troskovi_isporuke($narudzbina_id)>0){
            $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['cena_artikla'] = $cena_artikala;
        }else{
            $results['ukupna_cena'] = $cena_artikala;
        }
        return json_encode($results);      
    }

    public function obrisi(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->delete();
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->delete();
        AdminSupport::saveLog('Narudzbine, web_b2c_narudzbina_id -> '.$narudzbina_id.', OBRISANA');
    }

    public function narudzbina_stavka($narudzbina_stavka_id){
        $narudzbina_stavka = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_stavka_id'=> $narudzbina_stavka_id ))->first();

        if(is_null($narudzbina_stavka) || (!is_null($narudzbina_stavka) && $narudzbina_stavka->posta_zahtev == 1)){
            return Redirect::back();
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_stavka->web_b2c_narudzbina_id)->first();
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$narudzbina->web_kupac_id)->first();
        $narudzbina_stavka_upd = (array) $narudzbina_stavka;

        if(is_null($narudzbina_stavka->ulica_id)){
            $narudzbina_stavka_upd['ulica_id'] = $web_kupac->ulica_id;
        }
        if(is_null($narudzbina_stavka->broj)){
            $narudzbina_stavka_upd['broj'] = $web_kupac->broj;
        }
        if(is_null($narudzbina_stavka->posta_slanje_id)){
            $narudzbina_stavka_upd['posta_slanje_id'] = $narudzbina->posta_slanje_id;
        }
        if(is_null($narudzbina_stavka->web_nacin_placanja_id)){
            $narudzbina_stavka_upd['web_nacin_placanja_id'] = $narudzbina->web_nacin_placanja_id;
        }
        if(is_null($narudzbina_stavka->vrednost) || $narudzbina_stavka->vrednost == 0){
            $narudzbina_stavka_upd['vrednost'] = $narudzbina_stavka->kolicina * $narudzbina_stavka->jm_cena * 10;
        }
        if(is_null($narudzbina_stavka->masa) || $narudzbina_stavka->masa == 0){
            $narudzbina_stavka_upd['masa'] = DB::table('roba')->where('roba_id',$narudzbina_stavka->roba_id)->pluck('tezinski_faktor');
        }
        if(is_null($narudzbina_stavka->opis) || $narudzbina_stavka->opis == ''){
            $narudzbina_stavka_upd['opis'] = 'Tehnika';
        }

        $narudzbina_stavka = (object) $narudzbina_stavka_upd;

// All::dd($narudzbina_stavka);
        $data = array(
            'strana'=>'narudzbina_stavka',
            'title'=>"Narudzbina stavka",
            'narudzbina_stavka' => $narudzbina_stavka,
            'ime_stavke' => DB::table('roba')->where('roba_id',$narudzbina_stavka->roba_id)->pluck('naziv_web')
            );
        return View::make('admin/page',$data);

    }

    public function narudzbina_stavka_update(){ 
        $data = Input::get();
        $rules = array(
            'broj_paketa' => 'numeric|digits_between:0,10',
            'broj' => 'required|max:50',
            'cena_otkupa' => 'required|numeric|digits_between:0,20',
            'vrednost' => 'numeric|digits_between:0,20',
            'masa' => 'numeric|digits_between:0,20',
            'napomena' => 'max:250',
            'opis' => 'required|max:50'
        );
        if($data['otkup_prima'] != -1){
            $rules['racun_otkupa'] = 'required|max:20';
        }

        $messages = array(
            'numeric' => 'Polje sme da sadrži samo brojeve.',
            'required' => 'Niste popunili polje.',
            'digits_between' => 'Dužina sadržaja je neodgovarajuća.',
            'max' => 'Dužina sadržaja je neodgovarajuća.'
        );
        $validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka/'.$data['web_b2c_narudzbina_stavka_id'])->withInput()->withErrors($validator->messages());
        }else{
            
            $web_b2c_narudzbina_stavka_id = $data['web_b2c_narudzbina_stavka_id'];
            unset($data['web_b2c_narudzbina_stavka_id']);
            unset($data['opstina']);
            unset($data['mesto']);


            DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->update($data);
            unset($data['broj_paketa']);
            unset($data['vrednost']);
            unset($data['masa']);


            $web_b2c_narudzbina_id = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->pluck('web_b2c_narudzbina_id');
            $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$data['posta_slanje_id']);

            $stavke = $grouped_by_posiljalac[$data['posiljalac_id']];
            if(count($stavke) > 1){
                DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id', array_map('current',$stavke))->update($data);
            }

            AdminSupport::saveLog('Narudzbina stavka, EDIT web_b2c_narudzbina_stavka_id -> '.$web_b2c_narudzbina_stavka_id);
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka/'.$web_b2c_narudzbina_stavka_id);
        }
    }

    public function posta_posalji($web_b2c_narudzbina_id,$posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna'=>1))->first();
        if(is_null($posta_slanje)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($narudzbina) || $narudzbina->posta_slanje_poslato == 1){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }

        $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$posta_slanje_id);

        $message = '';
        $data = array();
        $data['dexpress'] = array();
        foreach($grouped_by_posiljalac as $posiljalac_id => $stavke){
            if(count($stavke) > 0){
                if($posiljalac_id != 1){
                    $posiljalac = AdminPartneri::partner($posiljalac_id);
                    if(is_null($posiljalac)){
                        continue;
                    }
                    // else{
                    //     if(is_null($posiljalac->mail) || $posiljalac->mail == ''){
                    //         continue;
                    //     }
                    // }
                    //send to dobavljac
                }

                if($posta_slanje_id == 3){
                    $response = DExpress::generate_data($stavke,$posiljalac_id,$narudzbina->web_kupac_id);
                    if($response->success){
                        $data['dexpress'][] = $response->data;
                    }else{
                        $message = $response->message;
                        break;
                    }
                }
            }
        }

        if($posta_slanje_id == 3 && count($data['dexpress']) > 0 && $message == ''){
            // $response = DExpress::convertToCsv($data['dexpress']);
            $response = DExpress::convertToString($data['dexpress']);
           
            if($response->success){
                // //old, send on mail
                // Mailer::dexpress_send('sasa.zivkovic@tico.rs',$response->path);
                // if(!is_null($response->path)){
                //     File::delete($response->path);
                // }

                // $client = new SOAPClient('http://callc.dailyexpress.rs/DePreCalls.asmx?WSDL');
                // $soap_response = $client->AddPrecalls(array(
                //     'authCode' => '419AFE8D-15D9-4750-B4FC-9BE3851560CA',
                //     'PreCallData' => $response->result
                // ));
                // if($soap_response->AddPrecallsResult == "OK"){
                if(true){
                    foreach($grouped_by_posiljalac as $stavke){
                        if(count($stavke) > 0){
                            DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',array_map('current',$stavke))->update(array('posta_zahtev'=>1));
                        }
                    }
                    $maticnaFirma = DB::table('preduzece')->where('preduzece_id',1)->first();
                    Mailer::send($maticnaFirma->email,$maticnaFirma->email,'DExpress zahtev ('.$maticnaFirma->naziv.')',$response->result);
                    $message = 'Zahtev je poslat kurirskoj službi.';
                }else{
                    $message = 'Greška DExpress servisa.';
                }

            }
        }
        if(!is_null($message) && $message != ''){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$message);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
    }

    public function posta_resetuj($web_b2c_narudzbina_id,$posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna'=>1))->first();
        if(is_null($posta_slanje)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($narudzbina)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }

        $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$posta_slanje_id,true);

        foreach($grouped_by_posiljalac as $stavke){
            if(count($stavke) > 0){
                DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',array_map('current',$stavke))->update(array('posta_zahtev'=>0));
            }
        }

        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }

    public function posta_odgovor(){
        $data = Request::all();
        $result = "ERROR";

        if(isset($data['ReferenceID'])){
            $reference_id = intval($data['ReferenceID']);
            $stavke_query = DB::table('web_b2c_narudzbina_stavka')->where('posta_reference_id',$reference_id);

            if($stavke_query->count() > 0){
                $status_code = isset($data['Status']) && is_numeric($data['Status']) ? intval($data['Status']) : -1;
                $status_id = DB::table('posta_status')->where(array('posta_slanje_id' => 3, 'code'=>$status_code))->pluck('posta_status_id');
                $json_data = json_encode($data);
                $upd_data = array(
                    'posta_status_id'=>$status_id,
                    'posta_response_data'=>$json_data,
                    'posta_zahtev'=>0
                    );
                $stavke_query->update($upd_data);

                $result = "OK";
            }

        }
        return $result;
    }

    public function porudzbinaDelete($web_b2c_narudzbina_id){
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();


        return Redirect::back();
       
    }



    //slanje mejla kod promene statusa

   
}