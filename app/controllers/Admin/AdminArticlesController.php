<?php


class AdminArticlesController extends Controller {

    function artikli($grupa_pr_id, $proizvodjac = null, $dobavljac = null, $tip = null, $karakteristika = null, $magacin = null, $exporti = null, $filteri = null, $flag = null, $search = null, $nabavna = null, $order = null)
    {
            $criteria = array("grupa_pr_id" => $grupa_pr_id, "proizvodjac"=>$proizvodjac, "karakteristika"=>$karakteristika, "dobavljac"=>$dobavljac, "tip"=>$tip, "magacin"=>$magacin, "exporti"=>$exporti, "filteri"=>$filteri, "flag"=>$flag, "nabavna"=>$nabavna, "search"=>$search);
            $limit = AdminOptions::limit_liste_robe();
            $pagination = array(
                'limit' => $limit,
                'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
                );
            $artikli = AdminArticles::fetchAll($criteria, $pagination, $order);
            $grupa_title = AdminGroups::find($grupa_pr_id,'grupa');
            $all_articles = array_map('current',AdminArticles::fetchAll($criteria));
            $count = count($all_articles);

            $grupa_parents = array();
            AdminSupport::grupaParents($grupa_pr_id,$grupa_parents);
            date_default_timezone_get();
            $time = date('Y-m-d');
            $data=array(
                "strana"=>'artikli',
                "title"=> $grupa_pr_id != 0 ? 'Artikli iz kategorije '.$grupa_title : 'Artikli iz svih kategorija',
                "grupa_pr_id"=>$grupa_pr_id,
                "grupa_parents"=>$grupa_parents,
                "naziv_grupe"=>$grupa_pr_id != 0 ? $grupa_title : 'Artikli iz svih grupa',
                "criteria"=>$criteria,
                "articles"=>$artikli,
                "count_products"=>$count,
                "limit"=>$limit,
                "time"=>$time,
                "all_ids" => json_encode($all_articles),
                "naziv_width" => AdminOptions::admin_artikli_kolone('naziv','width')
            );
  
            return View::make('admin/page', $data);
    }

    function product($id,$clone_id=null)
    {
        isset($clone_id) ? $id_exe = $clone_id : $id_exe = $id;

            $data=array(
                "strana"=>'product',
                "title"=>$id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : 'novi artikal',
                "naziv"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : null,
                "naziv_web"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv_web') : null,
                "flag_usluga"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_usluga') : null,
                "grupa_pr_grupa"=> $id_exe != 0 ? AdminGroups::find(AdminArticles::find($id_exe,'grupa_pr_id'),'grupa') : null,
                "proizvodjac"=> $id_exe != 0 ? DB::table('proizvodjac')->where('proizvodjac_id',AdminArticles::find($id_exe, 'proizvodjac_id'))->pluck('naziv') : null,
                "jedinica_mere"=> $id_exe != 0 ? DB::table('jedinica_mere')->where('jedinica_mere_id',AdminArticles::find($id_exe, 'jedinica_mere_id'))->pluck('naziv') : null,
                "roba_flag_cene"=> $id_exe != 0 ? DB::table('roba_flag_cene')->where('roba_flag_cene_id',AdminArticles::find($id_exe, 'roba_flag_cene_id'))->pluck('naziv') : null,
                "tip_artikla"=> $id_exe != 0 ? DB::table('tip_artikla')->where('tip_artikla_id',AdminArticles::find($id_exe, 'tip_cene'))->pluck('naziv') : null,
                "roba_grupe"=> $id_exe != 0 ? AdminGroups::moreGroups($id_exe) : array(),
                "web_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_cena')) : null,
                "kolicina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'kolicina',DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))) : 0,
                "flag_aktivan"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_aktivan') : 1,
                "flag_prikazi_u_cenovniku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_prikazi_u_cenovniku') : 1,
                "flag_zakljucan" => $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_zakljucan') : false,
                "dobavljac_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'dobavljac_id') : null,
                "sifra_d"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'sifra_d') : null,
                "model"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'model') : '',
                "tags"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tags') : null,
                "tezinski_faktor"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'tezinski_faktor'),2) : null,
                "tarifna_grupa_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tarifna_grupa_id') : 2,
                "garancija"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'garancija') : null,
                "sku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'sku') : null,
                "barkod"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'barkod') : null,
                "mpcena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'mpcena')) : null,
                "racunska_cena_nc"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'racunska_cena_nc')) : null,
                "web_marza"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_marza'),2) : 0,
                "mp_marza"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'mp_marza'),2) : 0,
                "orgj_id"=> DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),
                "web_flag_karakteristike"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'web_flag_karakteristike') : 0,
                "osobine"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'osobine') : 0,
                "flag_ambalaza"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_ambalaza') : 0,
                "ambalaza"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'ambalaza') : 0,
                "clone_id"=> $clone_id,
                "roba_id"=> $id
            );

        return View::make('admin/page', $data);

    }

    function product_edit()
    {
            $data = Input::get();
            $rules = array(
                'naziv_web' => 'required|regex:'.AdminSupport::regex().'|between:1,200|unique:roba,naziv_web,'.$data['roba_id'].',roba_id',
                'sku' => 'regex:'.AdminSupport::regex().'|between:2,8|unique:roba,sku,'.$data['roba_id'].',roba_id',
                'sifra_d' => 'max:100',
                'garancija' => 'numeric|digits_between:1,7',
                'racunska_cena_nc' => 'numeric|digits_between:1,15',
                'mpcena' => 'numeric|digits_between:1,15',
                'mp_marza' => 'required|numeric|digits_between:1,7',
                'web_cena' => 'numeric|digits_between:1,15',
                'web_marza' => 'required|numeric|digits_between:1,7',
                'kolicina' => 'numeric|digits_between:1,7',
                'model' => 'regex:'.AdminSupport::regex().'|max:100',
                'barkod' => 'regex:'.AdminSupport::regex().'|max:100',
                'tezinski_faktor' => 'numeric|digits_between:1,15',
                'ambalaza' => 'numeric|digits_between:1,7',
                'grupa_pr_grupa' => 'regex:'.AdminSupport::regex().'|max:100',
                'proizvodjac' => 'regex:'.AdminSupport::regex().'|max:100',
                'jedinica_mere' => 'regex:'.AdminSupport::regex().'|max:100',
                'roba_flag_cene' => 'regex:'.AdminSupport::regex().'|max:100',
                'tip_artikla' => 'regex:'.AdminSupport::regex().'|max:100'
            );
            $validator_messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Polje je već zauzeto!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve!',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );
            $validator = Validator::make($data, $rules, $validator_messages);
            if($validator->fails()){
                return Redirect::to(AdminOptions::base_url().'admin/product/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
            }else{
                if($data['roba_id'] == 0 && AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
                    return Redirect::back()->with('limit_message',true);
                }

                if(isset($data['flag_aktivan']) && $data['flag_aktivan'] == 0){
                    $data['flag_prikazi_u_cenovniku'] = 0;
                }

                if($data['clone_id'] != 0){
                    $clone_id = $data['clone_id'];
                }
                $slike_clone = false;
                if(isset($data['slike_clone'])){
                    $slike_clone = true;
                }

                unset($data['clone_id']);
                unset($data['slike_clone']);

                $data['grupa_pr_id'] = AdminGroups::grupa_pr_id($data['grupa_pr_grupa']);
                $data['proizvodjac_id'] = AdminSupport::proizvodjac_id($data['proizvodjac']);
                $data['jedinica_mere_id'] = $data['jedinica_mere'] == '' ? 1 : AdminSupport::jedinica_mere_id($data['jedinica_mere']);
                $data['roba_flag_cene_id'] = AdminSupport::roba_flag_cene_id($data['roba_flag_cene']);
                $data['tip_cene'] = $data['tip_artikla'] == '' ? null : AdminSupport::tip_artikla_id($data['tip_artikla']);
                unset($data['grupa_pr_grupa']);
                unset($data['proizvodjac']);
                unset($data['jedinica_mere']);
                unset($data['roba_flag_cene']);
                unset($data['tip_artikla']);

                if(!isset($data['roba_grupe'])){
                    $data['roba_grupe'] = array();
                }
                foreach($data['roba_grupe'] as $key => $grupa_id){
                    if($data['grupa_pr_id'] == $grupa_id){
                        unset($data['roba_grupe'][$key]);
                    }
                }
                AdminArticles::saveMoreGroups($data['roba_id'],$data['roba_grupe']);
                unset($data['roba_grupe']);

                $roba_id = AdminArticles::save($data);

                if(isset($clone_id)){
                    if($slike_clone){
                        AdminArticles::cloneImages($clone_id,$roba_id);
                    }
                    AdminArticles::cloneKarak($clone_id,$roba_id);
                }
                $message="Uspešno ste sačuvali podatke";
                AdminSupport::saveLog('Artikli, INSERT/EDIT roba_id -> '.$roba_id);
                return Redirect::to(AdminOptions::base_url().'admin/product/'.$roba_id)->with('message_success',$message);
           }
  
    }

    function product_short($id,$clone_id=null)
    {
        if(AdminOptions::gnrl_options(3029)==0){
            return Redirect::to(AdminOptions::base_url().'admin/product/'.$id);
        }

        isset($clone_id) ? $id_exe = $clone_id : $id_exe = $id;

            $data=array(
                "strana"=>'product_short',
                "title"=>$id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : 'novi artikal',
                "naziv"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : null,
                "naziv_web"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv_web') : null,
                "grupa_pr_id"=> $id_exe != 0 ? AdminArticles::find($id_exe,'grupa_pr_id') : null,
                "grupa_pr_grupa"=> $id_exe != 0 ? AdminGroups::find(AdminArticles::find($id_exe,'grupa_pr_id'),'grupa') : null,
                "jedinica_mere_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'jedinica_mere_id') : 0,
                "orgj_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'orgj_id') : DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),
                "roba_flag_cene"=> $id_exe != 0 ? DB::table('roba_flag_cene')->where('roba_flag_cene_id',AdminArticles::find($id_exe, 'roba_flag_cene_id'))->pluck('naziv') : null,
                "tip_artikla"=> $id_exe != 0 ? DB::table('tip_artikla')->where('tip_artikla_id',AdminArticles::find($id_exe, 'tip_cene'))->pluck('naziv') : null,
                "akcija_flag_primeni"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'akcija_flag_primeni') : 0,
                "web_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_cena')) : null,
                "kolicina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'kolicina',DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))) : 0,
                "flag_prikazi_u_cenovniku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_prikazi_u_cenovniku') : 1,
                "tarifna_grupa_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tarifna_grupa_id') : 2,
                "akcijska_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'akcijska_cena'),2) : 0,
                "web_opis"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'web_opis') : null,
                "datum_akcije_od"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_od') : null,
                "datum_akcije_do"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_do') : null,
                "sku"=> $id != 0 ? AdminArticles::find($id, 'sku') : null,
                "clone_id"=> $clone_id,
                "roba_id"=> $id
            );

        return View::make('admin/page', $data);

    }

    function product_edit_short()
    {
            $data = Input::get();
            $rules = array(
                'naziv_web' => 'required|regex:'.AdminSupport::regex().'|between:1,200|unique:roba,naziv_web,'.$data['roba_id'].',roba_id',
                'web_cena' => 'numeric|digits_between:1,20',
                'kolicina' => 'numeric|digits_between:1,10',
                'sku' => 'regex:'.AdminSupport::regex().'|between:1,9|unique:roba,sku,'.$data['roba_id'].',roba_id',
                'grupa_pr_grupa' => 'regex:'.AdminSupport::regex().'|max:100',
                'roba_flag_cene' => 'regex:'.AdminSupport::regex().'|max:100',
                'tip_artikla' => 'regex:'.AdminSupport::regex().'|max:100'
            );
            $validator_messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Vrednost već postoji u bazi!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve!',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );
            $validator = Validator::make($data, $rules, $validator_messages);
            if($validator->fails()){
                if(!Request::ajax()){
                    return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
                }else{
                    return Response::json(array('success_message' => "", 'error_messages' => $validator->messages()));
                }
            }else{
                if($data['roba_id'] == 0 && AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
                    if(!Request::ajax()){
                        return Redirect::back()->with('limit_message',true);
                    }else{
                        return Response::json(array('success_message' => '', 'error_messages' => array()));
                    }
                }
                if(isset($data['flag_aktivan']) && $data['flag_aktivan'] == 0){
                    $data['flag_prikazi_u_cenovniku'] = 0;
                }

                if($data['clone_id'] != 0){
                    $clone_id = $data['clone_id'];
                }
                $slike_clone = false;
                if(isset($data['slike_clone'])){
                    $slike_clone = true;
                }
                unset($data['clone_id']);
                unset($data['slike_clone']);


                if($data['roba_id'] != 0 && isset($data['akcija'])){
                    DB::table('web_slika')->where('roba_id',$data['roba_id'])->update(array('akcija'=>0));
                    DB::table('web_slika')->where('web_slika_id',$data['akcija'])->update(array('akcija'=>1));
                    unset($data['akcija']);
                }
                // $grupa_pr_vrednost_ids = array();
                // foreach($data as $name => $input){
                //     if(strpos($name,'grupa_pr_naziv_id_') !== false){
                //         $grupa_pr_vrednost_ids[] = $input;
                //         unset($data[$name]);
                //     }
                // }
                unset($data['slika']);


                $data['grupa_pr_id'] = AdminGroups::grupa_pr_id($data['grupa_pr_grupa']);
                $data['roba_flag_cene_id'] = AdminSupport::roba_flag_cene_id($data['roba_flag_cene']);
                $data['tip_cene'] = $data['tip_artikla'] == '' ? null : AdminSupport::tip_artikla_id($data['tip_artikla']);
                unset($data['grupa_pr_grupa']);
                unset($data['roba_flag_cene']);
                unset($data['tip_artikla']);

                $roba_id = AdminArticles::save($data);

                // //karakteristike
                // $old_vrednost_ids = array_map('current',DB::table('web_roba_karakteristike')->select('grupa_pr_vrednost_id')->where('roba_id',$roba_id)->get());
                // $delete_vrednost_ids = array_diff($old_vrednost_ids, $grupa_pr_vrednost_ids); 
                // $new_vrednost_ids = array_diff($grupa_pr_vrednost_ids, $old_vrednost_ids);
                // DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->whereIn('grupa_pr_vrednost_id',$delete_vrednost_ids)->delete();
                // foreach($new_vrednost_ids as $key => $new_vrednost_id){
                //     $grupa_pr_vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$new_vrednost_id)->first();
                //     DB::table('web_roba_karakteristike')->insert(array('roba_id'=>$roba_id,'grupa_pr_vrednost_id'=>$new_vrednost_id,'grupa_pr_naziv_id'=>$grupa_pr_vrednost->grupa_pr_naziv_id,'vrednost'=>$grupa_pr_vrednost->naziv,'rbr'=>($key+1)));
                // }

                //slike
                if(count(Input::file('slike'))>0){
                    $images = Input::file('slike');
                    $max_image_size = 10000;
                    $max_images = 10;
                    $images_success = true;

                    foreach($images as $image){
                        $image_validator = Validator::make(
                            array('slika' => $image),
                            array('slika' => 'required|mimes:jpg,png,jpeg|max:'.strval($max_image_size)),
                            array(
                                'required' => 'Niste izabrali fajl.',
                                'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png i jpeg.',
                                'max' => 'Maksimalna veličina slike je '.strval($max_image_size).'.',
                                )
                            );
                        if($image_validator->fails()){
                            $images_success = false;
                            $error_message = $image_validator->messages()->first();
                            break;
                        }
                    }
                    if($images_success){
                        foreach($images as $key => $image){
                            $slika_data['roba_id'] = $roba_id;
                            $slika_data['web_slika_id'] = 0;
                            $slika_data['slika'] = $image;
                            AdminArticles::saveImage($slika_data);
                        }                        
                    }else{
                        $validator->getMessageBag()->add('slike',$error_message);
                        if(!Request::ajax()){
                            return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
                        }else{
                            return Response::json(array('success_message' => "", 'error_messages' => $validator->messages()));
                        }
                    }
                }

                if(isset($clone_id)){
                    if($slike_clone){
                        AdminArticles::cloneImages($clone_id,$roba_id);
                    }
                    AdminArticles::cloneKarak($clone_id,$roba_id);
                }

                $message="Uspešno ste sačuvali podatke";
                AdminSupport::saveLog('Artikli, INSERT/EDIT roba_id -> '.$roba_id);
                if(!Request::ajax()){
                    return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$roba_id)->with('message_success',$message);
                }else{
                    return Response::json(array('success_message' => $message, 'error_messages' => array(),'roba_id'=>$roba_id));
                }
           }
  
    }

    function product_delete($roba_id, $check=null)
    {
        $result = AdminArticles::delete($roba_id, $check);

        AdminSupport::saveLog('Artikli, DELETE roba_id -> '.$roba_id);

        if(!Request::ajax()){
            return Redirect::to($result->redirect)->with('message',true);
        }else{
            if(!$result->success){
                $message = $result->message;
            }else{
                $message = 'Uspešno ste obrisali artikal';
            }
            return Response::json(array('success' => $result->success, 'message' => $message));
        }
    }

    function products_delete($roba_ids, $check=null)
    {

        $roba_ids = explode(',', $roba_ids);
        $mesages = '';
        foreach ($roba_ids as $roba_id) {        
            $result = AdminArticles::delete($roba_id, $check);
            $mesages .= $result->message;
        }
        echo trim($mesages);
        die;
    }

    function product_slike($id)
    {
        $data=array(
            'strana'=>'slike',
            'title'=>'slike',
            'roba_id'=>$id
        );
        return View::make('admin/page', $data);
    }

    function slika_edit()
    {

        $data = Input::get();
        $images = Input::file('slika');
        $max_image_size = 10000;
        $max_images = 10;
        $success = false;

        if(count($images) == 0){
            $error_message = 'Niste izabrali fajl.';
        }elseif(count($images) > $max_images){
            $error_message = 'Maksimalni broj slika je '.strval($max_images).'!';
        }else{
            foreach($images as $image){
                $validator = Validator::make(
                    array('slika' => $image),
                    array('slika' => 'required|mimes:jpg,png,jpeg|max:'.strval($max_image_size)),
                    array(
                        'required' => 'Niste izabrali fajl.',
                        'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png i jpeg.',
                        'max' => 'Maksimalna veličina slike je '.strval($max_image_size / 1000).' MB.',
                        )
                    );
                if($validator->fails()){
                    $success = false;
                    $error_message = $validator->messages()->first();
                    break;
                }else{
                    $success = true;
                }
            }
        }

        if($success){
            foreach($images as $key => $image){
                $data['web_slika_id'] = 0;
                $data['slika'] = $image;
                AdminArticles::saveImage($data);
            }

            AdminSupport::saveLog('Slike roba_id -> '.$data['roba_id'].', INSERT slike');
            return Redirect::to(AdminOptions::base_url().'admin/product_slike/'.$data['roba_id'])->with('message','Uspešno ste sačuvali slike.');
        }
        if(count($data) == 0){
            $error_message = 'Maksimalna veličina slike je '.strval($max_image_size / 1000).' MB.';
        }
        return Redirect::back()->withInput()->with('error',$error_message);
    }

    function slika_delete()
    {
        $data = Input::get();
        AdminArticles::deleteImageFile($data['web_slika_id']);

        $akcija = DB::table('web_slika')->where('web_slika_id',$data['web_slika_id'])->pluck('akcija');
        if($akcija == 1){
            $upd_query = DB::table('web_slika')->where('roba_id',$data['roba_id'])->where('web_slika_id','!=',$data['web_slika_id']);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }


        DB::table('web_slika')->where('web_slika_id',$data['web_slika_id'])->delete();

        AdminSupport::saveLog('Slike roba_id -> '.$data['roba_id'].', DELETE web_slika_id -> '.$data['web_slika_id']);
        return Redirect::to(AdminOptions::base_url().'admin/product_slike/'.$data['roba_id'].'/0')->with('message','Uspešno ste obrisali sliku.');;
    }
    function slike_more_delete()
    {
        $web_slika_ids = Input::get('web_slika_ids');

        foreach($web_slika_ids as $web_slika_id){
            AdminArticles::deleteImageFile($web_slika_id);
        }

        $slika_akcija = DB::table('web_slika')->whereIn('web_slika_id',$web_slika_ids)->where('akcija',1)->first();
        if(!is_null($slika_akcija)){
            $upd_query = DB::table('web_slika')->where('roba_id',$slika_akcija->roba_id)->whereNotIn('web_slika_id',$web_slika_ids);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }


        DB::table('web_slika')->whereIn('web_slika_id',$web_slika_ids)->delete();

        AdminSupport::saveLog('Slike, DELETE web_slika_ids -> '.implode(',',$web_slika_ids));
    }
    function slika_delete_get($id,$web_slika_id,$short=false)
    {
        $data = Input::get();
        AdminArticles::deleteImageFile($web_slika_id);

        $akcija = DB::table('web_slika')->where('web_slika_id',$web_slika_id)->pluck('akcija');
        if($akcija == 1){
            $upd_query = DB::table('web_slika')->where('roba_id',$id)->where('web_slika_id','!=',$web_slika_id);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }

        DB::table('web_slika')->where('web_slika_id',$web_slika_id)->delete();

        AdminSupport::saveLog('Slike roba_id -> '.$id.', DELETE web_slika_id -> '.$web_slika_id);
        if(Request::ajax()){
            exit;
        }

        if($short){
            return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$id)->with('message_delete','Uspešno ste obrisali sliku.');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/product_slike/'.$id)->with('message','Uspešno ste obrisali sliku.');
        }

    }
    function grupa_karakteristike()
    {
        $data = Request::all();
        $data=array(
            "grupa_pr_id"=>$data['grupa_pr_id'],
            'grupa_pr_vrednost_ids'=> $data['roba_id'] != 0 ? array_map('current',DB::table('web_roba_karakteristike')->select('grupa_pr_vrednost_id')->where('roba_id',$data['roba_id'])->get()) : array()
        );

        return View::make('admin/partials/ajax/grupa_generisane', $data);
    }
    
    function product_opis($id)
    {
        $data=array(
            'strana'=>'opis',
            'title'=>'opis',
            'roba_id'=>$id,
            'web_opis'=> $id != 0 ? AdminArticles::find($id, 'web_opis') : null
        );

        return View::make('admin/page', $data);
    }

    function opis_edit()
    {
        $data = Input::get();

        // $data['web_opis'] = str_replace("\n", "<br>",$data['web_opis']);
        DB::table('roba')->where('roba_id', $data['roba_id'])->update(array('web_opis'=>$data['web_opis']));

        AdminSupport::saveLog('Opis roba_id -> '.$data['roba_id'].' INSERT/EDIT');
        return Redirect::to(AdminOptions::base_url().'admin/product_opis/'.$data['roba_id']);
    }
    function product_seo($id,$jezik_id=1)
    {

        $roba=DB::table('roba')->where('roba_id',$id)->first();

        $seo = AdminSeo::article($id,$jezik_id);
        $data=array(
            'strana'=>'seo',
            'title'=>'seo',
            'roba_id'=>$id,
            'seo_title'=> $id != 0 ? $seo->title : '',
            "description"=> $id != 0 ? $seo->description : '',
            "keywords"=> $id != 0 ? $seo->keywords : '',
            'tags'=> isset($roba->tags) ? strip_tags($roba->tags) : '',
            "jezik_id"=>$jezik_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get()
        );

        return View::make('admin/page', $data);
    }

    function seo_edit()
    {
        $data = Input::get();
        $roba_id = $data['roba_id'];
        $jezik_id = $data['jezik_id'];
        
        $validator = Validator::make($data,array('seo_title' => 'max:60','description' => 'max:320', 'keywords' => 'max:159', 'tags' => 'max:159'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/product_seo/'.$roba_id)->withInput()->withErrors($validator->messages());
        }else{
            $data['title'] = str_replace("\n", "",$data['seo_title']);
            $data['description'] = str_replace("\n", "",$data['description']);
            $data['keywords'] = str_replace("\n", "",$data['keywords']);
            $data['tags'] = str_replace("\n", "",$data['tags']);
            DB::table('roba')->where('roba_id', $roba_id)->update(array('tags' => $data['tags']));

            $query = DB::table('roba_jezik')->where(array('roba_id'=>$roba_id, 'jezik_id'=>$jezik_id));

            unset($data['seo_title']);
            unset($data['tags']);
            if(!is_null($query->first())){
                unset($data['roba_id']);
                unset($data['jezik_id']);
                $query->update($data);
            }else{
                if($data['title'] || $data['description'] || $data['keywords']){
                    DB::table('roba_jezik')->insert($data);
                }
            }

            AdminSupport::saveLog('Seo roba_id -> '.$roba_id.' INSERT/EDIT');
            return Redirect::to(AdminOptions::base_url().'admin/product_seo/'.$roba_id.($jezik_id==1 ? '' : '/'.$jezik_id));
        }
    }
    function product_karakteristike($id)
    {
        $data=array(
            'strana'=>'karakteristike',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'web_karakteristike'=> AdminArticles::find($id, 'web_karakteristike')
        );

        return View::make('admin/page', $data);
    }

    function karakteristike_edit()
    {
        $data = Input::get();

        AdminArticles::karakteristike_save($data);

        AdminSupport::saveLog('Karakteristike HTML ili flag, roba_id -> '.$data['roba_id'].' INSERT/EDIT');
        return Redirect::to(AdminOptions::base_url().'admin/product_karakteristike/'.$data['roba_id']);
    }
    
    //GENERISANE
    function product_generisane($id)
    {
        $data=array(
            'strana'=>'generisane',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'nazivi'=>AdminSupport::getGrupaKarak(AdminArticles::find($id, 'grupa_pr_id'))
        );

        return View::make('admin/page', $data);
    }

    //OD DOBAVLJACA
    function product_od_dobavljaca($id)
    {
        $data=array(
            'strana'=>'dobavljac_karakteristike',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'grupe_karakteristika'=>AdminSupport::getGrupeKarak($id)
        );
        
        return View::make('admin/page', $data);
    }

    //VEZANI ARTIKLI
    function vezani_artikli($id,$vezani=null)
    {
        if(!is_null($vezani)){
            $check_exist = DB::table('vezani_artikli')->where(array('roba_id'=>$id,'vezani_roba_id'=>$vezani))->count();
            if($check_exist == 0){
                $flag_cena = AdminOptions::web_options(129);
                AdminSupport::saveLog('Vezani artikli, roba_id -> '.$id.' INSERT');
                DB::table('vezani_artikli')->insert(array('roba_id'=>$id,'vezani_roba_id'=>$vezani,'flag_cena'=>$flag_cena));
            }
        }

        $data=array(
            'strana'=>'vezani_artikli',
            'title'=>'Vezani artikli',
            'roba_id'=>$id,
            'vezani_artikli'=>DB::table('vezani_artikli')->where('roba_id',$id)->orderBy('vezani_roba_id','asc')->get()
        );
        
        return View::make('admin/page', $data);
    }

    function export_xls()
    {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=export_roba.xls");
       

        echo 'Naziv'."\t" . 'Grupa'. "\t" . 'grupa_pr_id'. "\t" . 'Web cena'."\t" . 'Nabavna cena'."\t" . 'Lager'."\t" . 'roba_id'."\t\n";
        $roba = DB::table('roba')->where('roba_id', '!=', -1)->where('grupa_pr_id', '!=', -1)->get();

        $roba_all = '';
        foreach($roba as $row)
        {
            $roba_all .= $row->naziv ."\t". AdminGroups::find($row->grupa_pr_id,'grupa') . "\t" . $row->grupa_pr_id . "\t" . $row->web_cena . "\t" . $row->racunska_cena_nc ."\t". AdminArticles::find($row->roba_id,'kolicina') ."\t". $row->roba_id ."\t\n";
        }

        echo iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE", $roba_all);

        AdminSupport::saveLog('Export svih artikala');
    }


    function uploadImageArticle(){
        $images = scandir('images/upload_image');
        unset($images[0]);
        unset($images[1]);

        $data = array(
            'strana'=>'upload_image',
            'title'=>'Upload Image',
            'images'=>$images
            );
        return View::make('admin/page', $data);
    }

    function uploadImageAdd(){
        $data = Input::get();
        $slika = Input::file('slika');

        $validator = Validator::make(array('slika' => $slika), array('slika' => 'required'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/upload-image-article');
        }else{
            $originalName = $slika->getClientOriginalName();
            $name = $originalName;
                    
            $slika->move('images/upload_image/', $name);

            $sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
            Image::make('images/upload_image/'.$name)->resize($sirina_big, null, function ($constraint) {
                $constraint->aspectRatio(); })->save();

            return Redirect::to(AdminOptions::base_url().'admin/upload-image-article')->withInput()->withErrors($validator->messages());
        }        
    }
    function uploadImageDelete($image){
        File::delete('images/upload_image/'.$image);
        return Redirect::to(AdminOptions::base_url().'admin/upload-image-article');
    }

    function product_akcija($id)
    {
        $redni_broj = AdminArticles::find($id, 'akcija_redni_broj');
        if($redni_broj == 0){
            $redni_broj = DB::select('SELECT MAX(akcija_redni_broj) FROM roba')[0]->max + 1;
        }
        
        $data=array(
            'strana'=>'product_akcija',
            'title'=>'Product Akcija',
            'roba_id'=>$id,
            'naslov'=> $id != 0 ? AdminArticles::find($id, 'akcija_naslov') : null,
            'opis'=> $id != 0 ? strip_tags(AdminArticles::find($id, 'akcija_opis')) : null,
            'redni_broj'=> $id != 0 ? $redni_broj : null,
            'akcija_primeni'=> $id != 0 ? AdminArticles::find($id, 'akcija_flag_primeni') : null,
            "akcijska_cena"=> $id != 0 ? AdminArticles::find($id, 'akcijska_cena') : null,
            "akcija_popust"=> $id != 0 ? AdminArticles::find($id, 'akcija_popust') : 0,
            "web_cena"=> $id != 0 ? AdminArticles::find($id, 'web_cena') : null,
            "mpcena"=> $id != 0 ? AdminArticles::find($id, 'mpcena') : null,
            "datum_akcije_od"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_od') : null,
            "datum_akcije_do"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_do') : null,

        );

        return View::make('admin/page', $data);
    }

    function akcija_edit()
    {
        $data = Input::get();
        $validator = Validator::make($data, array(
            'akcija_naslov' => 'regex:'.AdminSupport::regex().'|max:100',
            'akcija_opis' => 'regex:'.AdminSupport::regex().'|max:1000',
            'akcija_redni_broj' => 'numeric|digits_between:1,10',
            'akcija_popust' => 'numeric|digits_between:1,10',
            'akcijska_cena' => 'numeric|digits_between:1,20',
            'web_cena' => 'numeric|digits_between:1,20'
            ));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($data['akcijska_cena'] >= $data['web_cena'] || $data['akcijska_cena'] <= 0){
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->with('message',true);
            }else{
                // $data['akcija_opis'] = str_replace("\n", "<br>",$data['akcija_opis']);
                $update_data = $data;
                unset($update_data['roba_id']);
                if($update_data['datum_akcije_od'] == ''){
                    $update_data['datum_akcije_od'] = null;
                }
                if($update_data['datum_akcije_do'] == ''){
                    $update_data['datum_akcije_do'] = null;
                }
                if(AdminOptions::web_options(132)==0){
                    $update_data['mpcena'] = $data['web_cena'];
                    unset($update_data['web_cena']);
                }
                DB::table('roba')->where('roba_id', $data['roba_id'])->update($update_data);

                AdminSupport::saveLog('Akcija roba_id -> '.$data['roba_id'].' UPDATE');
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id']);
            }
        }
    }


    function dodatni_fajlovi($id,$extension_id=null)
    {
        $extension = "";
        if($extension_id!=null){
            $extension = " AND vrsta_fajla_id = ".$extension_id."";
        }
        $fajlovi = DB::select("SELECT * FROM web_files WHERE roba_id = ".$id."".$extension." ORDER BY vrsta_fajla_id ASC");

        $data=array(
            'strana'=>'dodatni_fajlovi',
            'title'=>'Dodatni Fajlovi',
            'roba_id'=>$id,
            'vrste_fajlova'=> $id != 0 ? AdminSupport::artikalFajlovi($id) : null,
            'extension_id'=> $id != 0 ? $extension_id : null,
            'fajlovi' => $id != 0 ? $fajlovi : null
        );

        return View::make('admin/page', $data);
    }

    function dodatni_fajlovi_upload()
    {
        $inputs = Input::get();
        $fileUpl = Input::file('upl_file');

        $usefull_arr = array(
            'roba_id' => $inputs['roba_id'],
            'naziv' => $inputs['naziv'],
            'putanja_etaz' => $inputs['putanja'],
            'vrsta_fajla_id' => $inputs['vrsta_fajla'],
            'file' => $fileUpl
        );
        $validation_arr = array(
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
            'file' => 'required'
        );

        if($inputs['check_link'] == 1){
            $validation_arr = array(
                'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'putanja_etaz' => 'required|max:300'
            );
        }

        $validator = Validator::make($usefull_arr, $validation_arr);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['check_link'] == 0){

                $size = $fileUpl->getSize();
                if($size > 1000000){
                    return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id'])->with('mess',true);
                }
                
                $originalName = $fileUpl->getClientOriginalName();
                $fileUpl->move('images/files/', $originalName);

                unset($usefull_arr['putanja_etaz']);
                unset($usefull_arr['file']);
                $usefull_arr['putanja'] = 'images/files/'.$originalName;

                DB::table('web_files')->insert($usefull_arr);
            }else{
                unset($usefull_arr['file']);
                DB::table('web_files')->insert($usefull_arr);                
            }

            AdminSupport::saveLog('Dodatni fajlovi roba_id -> '.$inputs['roba_id'].' ADD');
            return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id']);
        }
    }

    function dodatni_fajlovi_delete($roba_id,$web_file_id)
    {
        $putanja = DB::table('web_files')->where('web_file_id',$web_file_id)->first()->putanja;
        $uses = DB::table('web_files')->where('putanja',$putanja)->count();
        if($uses == 1){
            File::delete($putanja);
        }
        DB::table('web_files')->where('web_file_id',$web_file_id)->delete();

        AdminSupport::saveLog('Dodatni fajlovi roba_id -> '.$roba_id.', web_file_id -> '.$web_file_id.' DELETE');
        return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$roba_id);
    }
    
    public function ajaxArticles(){
        $action = Input::get('action');
        $roba_id = Input::get('roba_id');
        $web_slika_id = Input::get('web_slika_id');

        if ($action == 'magacin_change') {
            $roba_id = Input::get('roba_id');
            $magacin_id = Input::get('magacin_id');
            echo AdminArticles::find($roba_id,'kolicina',$magacin_id);
        }
        elseif ($action == 'execute') {

            $roba_ids = Input::get('roba_ids');
            $data = Input::get('data');
            foreach($data as $row){
                if($row['val'] == 'null'){
                    $row['val'] = null;
                }
                if($row['column'] == 'flag_zakljucan') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->update(array($row['column']=>$row['val']));
                }elseif($row['column'] == 'flag_aktivan' && $row['val'] == '0') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->update(array($row['column']=>$row['val'],'flag_prikazi_u_cenovniku'=>0));
                }elseif($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == '1') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->where('flag_aktivan', 1)->update(array($row['column']=>$row['val']));
                }else {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->update(array($row['column']=>$row['val']));
                }
                

                AdminSupport::saveLog('Akcija na artiklima, tabela -> '.$row['table'].', kolona -> '.$row['column'].', vrednost -> '.$row['val']);
            }
            
            // custom for inputs
            if($data[0]['table'] == 'roba' && $data[0]['column'] == 'tip_cene'){
                if($data[0]['val'] != 'null'){
                    return AdminSupport::tip_naziv($data[0]['val']);
                }else{
                    return '';
                }
            }
            elseif($data[0]['table'] == 'roba' && $data[0]['column'] == 'proizvodjac_id'){
                if($data[0]['val'] != 'null'){
                    return AdminSupport::find_proizvodjac($data[0]['val'],'naziv');
                }else{
                    return '';
                }
            }
          
        }
        elseif ($action == 'preracunaj_cene') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET web_cena = racunska_cena_nc*(1+web_marza/100)*(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            }
            echo json_encode($response);

        }
        elseif ($action == 'preracunaj_webcenu') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET web_cena = (racunska_cena_nc*(1+(web_marza/100))*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = r.tarifna_grupa_id) ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            }
            echo json_encode($response);

        }
        elseif ($action == 'preracunaj_mpcenu') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET mpcena = (racunska_cena_nc*(1+(mp_marza/100))*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = r.tarifna_grupa_id) ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
            }
            echo json_encode($response);

        }
        elseif ($action == 'poreska_stopa') {
            $roba_ids = Input::get('roba_ids');
            $check = Input::get('check');
            $val = Input::get('val');
            $porez = AdminSupport::find_tarifna_grupa($val,'porez');
            $stopa = 1 + $porez / 100;
            $response = array();
            $response['porez'] = $porez;
            if($check == 0){            
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba SET tarifna_grupa_id=".$val." WHERE roba_id = ".$roba_id."");
                    DB::statement("UPDATE dobavljac_cenovnik SET tarifna_grupa_id=".$val." WHERE roba_id = ".$roba_id."");
                }
                echo json_encode($response);
            }
            elseif($check == 1){            
                $response['web_cena'] = array();
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba SET tarifna_grupa_id=".$val.", web_cena = ROUND(racunska_cena_nc*(1+web_marza/100)*".$stopa.") WHERE roba_id = ".$roba_id."");
                    DB::statement("UPDATE dobavljac_cenovnik SET tarifna_grupa_id=".$val.", web_cena = cena_nc*(1+web_marza/100)*".$stopa." WHERE roba_id = ".$roba_id."");
                    $response['web_cena'][$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
                }
                echo json_encode($response);
            }
        }
        elseif ($action == 'promena_kolicine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            $magacin_id = Input::get('magacin_id');
            $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');     


            if($magacin_id == 0){
                $magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            }

            $redovi = "";

            foreach($roba_ids as $roba_id) {
                $redovi .= "(".$roba_id.", ". $magacin_id .", ". $val .", 0, 1, ". $godina_id ."),";
            }

            $redovi = substr($redovi, 0, -1);

            DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, nabavna_po, valuta_id, poslovna_godina_id) SELECT * FROM (values ". $redovi .") redovi(roba_id, orgj_id, kolicina, nabavna_po, valuta_id, poslovna_godina_id) WHERE roba_id NOT IN (SELECT roba_id FROM lager WHERE orgj_id = ".$magacin_id.")");

            DB::statement("UPDATE lager SET kolicina=".$val." WHERE roba_id IN (".implode(',',$roba_ids).") AND poslovna_godina_id = ".$godina_id." AND orgj_id = ".$magacin_id."");

        }
        elseif ($action == 'akcija') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            $akc_od = Input::get('akc_od');            
            $akc_do = Input::get('akc_do');

            $do = date_create($akc_do);
            $danasnji_dan=date_create();
            $raz =date_diff($danasnji_dan,$do);
            //var_dump($raz);die;

            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_popust'=>$val, 'akcija_flag_primeni'=>1, 'datum_akcije_od'=>$akc_od,'datum_akcije_do'=>$akc_do));

            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET akcijska_cena = (web_cena-(akcija_popust/100)*web_cena ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('akcijska_cena');
            }
            echo json_encode($response);
           
        }
        elseif ($action == 'promena_tezine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('tezinski_faktor'=>$val));
        }
        elseif ($action == 'promena_webcene') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('web_cena'=>$val));
        }
        elseif ($action == 'promena_mpcene') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('mpcena'=>$val));
        }
        elseif ($action == 'promena_tag' ) {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('tags'=>$val));
        }
        elseif ($action == 'vrednost_poreza') {
            $tarifna_grupa_id = Input::get('tarifna_grupa_id');
            echo DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->pluck('porez');
        }

        elseif ($action == 'exporti') {
            $kind = Input::get('kind');
            $roba_ids = Input::get('roba_ids');
            $export_id = Input::get('export_id');
            if($kind=='export_dodeli'){
                $redovi_data = '';
                foreach($roba_ids as $roba_id){
                    $redovi_data .= '('.$roba_id.','.$export_id.'),';
                }
                $redovi_data = substr($redovi_data,0,-1);
                DB::statement('INSERT INTO roba_export SELECT * FROM (VALUES '.$redovi_data.') redivi(roba_id,export_id) WHERE roba_id NOT IN (SELECT roba_id FROM roba_export WHERE export_id='.$export_id.')');
            }
            elseif($kind=='export_ukloni'){
                $roba_ids = Input::get('roba_ids');
                $export_id = Input::get('export_id');
                DB::table('roba_export')->where('export_id',$export_id)->whereIn('roba_id',$roba_ids)->delete();
            }
        }
        elseif ($action == 'assign_selected_image') {
            $roba_ids = Input::get('roba_ids');
            $selectedImage = Input::get('image');
            foreach($roba_ids as $roba_id){
                AdminArticles::saveSelectImage($roba_id,$selectedImage);
            }
            AdminSupport::saveLog('Akcija na artiklima, Dodeljivanje slike na vise artikala');
        }
        elseif ($action == 'selected_image_modal') {
            $images = scandir('images/upload_image');
            unset($images[0]);
            unset($images[1]);

            echo View::make('admin/partials/select_image',array('images'=>$images))->render();
        }
        elseif ($action == 'update_slika') {
            DB::table('web_slika')->where('roba_id', $roba_id)->update(array('akcija'=>0));
            DB::table('web_slika')->where('web_slika_id', $web_slika_id)->update(array('akcija'=>1));

            AdminSupport::saveLog('Slike, UPDATE GLAVNA roba_id -> '.$roba_id.', web_slika_id -> '.$web_slika_id);

        }
        elseif ($action == 'update_slika_alt') {
            $web_slika_alt = Input::get('web_slika_alt');
            DB::table('web_slika')->where('web_slika_id', $web_slika_id)->update(array('alt'=>$web_slika_alt));

            AdminSupport::saveLog('Slike, UPDATE GLAVNA ALT, web_slika_id -> '.$web_slika_id);

        }
        elseif ($action == 'prikazi') {
            $prikaz = Input::get('prikaz');
            $web_slika_ids = Input::get('web_slika_ids');

            DB::table('web_slika')->whereIn('web_slika_id', $web_slika_ids)->update(array('flag_prikazi'=>$prikaz));
            AdminSupport::saveLog('Slike, UPDATE '.($prikaz == 1 ? 'AKTIVNA' : 'NEAKTIVNA').' web_slika_ids -> '.implode(',',$web_slika_ids));

        }

        elseif ($action == 'mass_edit_marza') {
            $roba_ids = Input::get('roba_ids');
            $cena_od = Input::get('cena_od');
            $cena_za = Input::get('cena_za');
            $marza = Input::get('marza');
            $marza_check = Input::get('marza_check');

            $cena_resp_roba = "";
            $marza_query = "";
            if($cena_za=="nc"){
                $cena_resp_roba = "racunska_cena_nc";
                $cena_resp_dc = "cena_nc";
            }
            elseif($cena_za=="wc"){
                $cena_resp_roba = "web_cena";
                $cena_resp_dc = $cena_resp_roba;
                $marza_query = "web_marza";
            }
            elseif($cena_za=="mc"){
                $cena_resp_roba = "mpcena";
                $cena_resp_dc = $cena_resp_roba;
                $marza_query = "mp_marza";
            }

            if($cena_od=="nc"){
                $value_roba = "ROUND(racunska_cena_nc*(1+".number_format($marza,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1))";
                $value_dc = "ROUND(cena_nc*(1+".number_format($marza,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
            }
            elseif($cena_od=="wc"){
                $value_roba = "ROUND(web_cena*(1+".number_format($marza,2)."/100))";
                $value_dc = $value_roba;
            }
            elseif($cena_od=="mc"){
                $value_roba = "ROUND(mpcena*(1+".number_format($marza,2)."/100))";
                $value_dc = $value_roba;
            }

            $response = array();

            if($marza_check == 1 && $marza_query != ""){
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba.", ".$marza_query."=".$marza." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                    DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc.", ".$marza_query."=".$marza." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                    $response[] = array(
                        "roba_id" => $roba_id,
                        $cena_resp_roba => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba),
                        $marza_query => number_format($marza,2)
                        );
                }
            }else{
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                    DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                    $response[] = array(
                        "roba_id" => $roba_id,
                        $cena_resp_roba => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba)
                        );
                }
            }

            AdminSupport::saveLog('Masovni edit cene marza, roba_id -> '.implode(',',$roba_ids));
            echo json_encode($response); 
            // All::dd($response);           
        }
        elseif ($action == 'mass_edit_rabat') {
            $roba_ids = Input::get('roba_ids');
            $cena_od = Input::get('cena_od');
            $cena_za = Input::get('cena_za');
            $rabat = Input::get('rabat');

            $cena_resp_roba = "";
            if($cena_za=="nc"){
                $cena_resp_roba = "racunska_cena_nc";
                $cena_resp_dc = "cena_nc";
            }
            elseif($cena_za=="wc"){
                $cena_resp_roba = "web_cena";
                $cena_resp_dc = $cena_resp_roba;
            }
            elseif($cena_za=="mc"){
                $cena_resp_roba = "mpcena";
                $cena_resp_dc = $cena_resp_roba;
            }

            if($cena_od=="nc"){
                $value_roba = "ROUND(racunska_cena_nc*(1-".number_format($rabat,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1))";
                $value_dc = "ROUND(cena_nc*(1-".number_format($rabat,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
            }
            elseif($cena_od=="wc"){
                $value_roba = "ROUND(web_cena*(1-".number_format($rabat,2)."/100))";
                $value_dc = $value_roba;
            }
            elseif($cena_od=="mc"){
                $value_roba = "ROUND(mpcena*(1-".number_format($rabat,2)."/100))";
                $value_dc = $value_roba;
            }

            $response = array();

            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                $response[] = array(
                    "roba_id" => $roba_id,
                   ($cena_resp_roba == 'web_cena' ? 'JSWebCenaVrednost' : $cena_resp_roba) => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba)
                    );
            }

            AdminSupport::saveLog('Masovni edit cene rabat, roba_id -> '.implode(',',$roba_ids));
            echo json_encode($response);

        }
        //Edit texta
        elseif ($action == 'text_edit') {
            $roba_ids = Input::get('roba_ids');
            $insert = Input::get('insert');
            $naziv_check = Input::get('naziv_check');
            $end_check = Input::get('end_check');

            // $response = array();

            foreach($roba_ids as $roba_id){
            if($naziv_check && !$end_check){
                DB::statement("UPDATE roba set naziv = concat('".$insert."',' ',naziv), naziv_web = concat('".$insert."','',naziv_web) WHERE roba_id = $roba_id");
                }
            elseif($naziv_check && $end_check){
                DB::statement("UPDATE roba set naziv = concat(naziv,' ','".$insert."'), naziv_web = concat(naziv_web,' ','".$insert."') where roba_id = $roba_id");
                }
            elseif(!$naziv_check && !$end_check){
                DB::statement("UPDATE roba set web_opis = concat('".$insert."',' ',web_opis) where roba_id = $roba_id");
                }
            elseif(!$naziv_check && $end_check){
                DB::statement("UPDATE roba set web_opis = concat(web_opis,' ','".$insert."') where roba_id = $roba_id");
                }
            }

            AdminSupport::saveLog('Insertovanje u tekstu, roba_id -> '.implode(',',$roba_ids));

        }
        //Replace texta
        elseif ($action == 'text_replace') {
            $roba_ids = Input::get('roba_ids');
            $replaceFrom = Input::get('replaceFrom');
            $replaceTo = Input::get('replaceTo');
            $naziv_check = Input::get('naziv_check');
            $deoTeksta = Input::get('deoTeksta');

            $response = array('roba_ids'=>array(),'counter'=>0);

            if ($deoTeksta) {
                foreach($roba_ids as $roba_id){

                    if($naziv_check){ 
                        $new_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('naziv');
                    }elseif (!$naziv_check) {
                        $new_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_opis');
                    }

                    $new_value = str_ireplace($replaceFrom, $replaceTo, $new_value, $count);

                    $response['counter'] = $response['counter'] + $count;

                    if($naziv_check){       
                        DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                        $response['roba_ids'][$roba_id] = $new_value;
                    }elseif (!$naziv_check) {
                        DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                    }
                } 
            }elseif (!$deoTeksta) {
                
                $replace_from_arr = explode(' ',trim($replaceFrom));

                foreach($roba_ids as $roba_id){

                    if($naziv_check){ 
                        $db_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('naziv');
                    }elseif (!$naziv_check) {
                        $db_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_opis');
                    }

                    $db_value_arr = explode(" ", trim($db_value));

                    for ($i=0; $i<count($db_value_arr); $i++){
                        if(strtoupper($db_value_arr[$i]) == strtoupper($replace_from_arr[0])){
                            $j=1;
                            $k=$i+1;
                            while ($j<count($replace_from_arr) && $k<count($db_value_arr) && strtoupper($db_value_arr[$k]) == strtoupper($replace_from_arr[$j])){
                                $k++;
                                $j++;
                            }
                            if($j == count($replace_from_arr)){
                                $db_value_arr[$i] = $replaceTo;
                                array_splice($db_value_arr, $i+1, count($replace_from_arr) - 1);
                                $response['counter']++;                               
                            }                       
                        }
                    }
                
                    $new_value = implode(" ",$db_value_arr);

                    if($naziv_check){       
                        DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                        $response['roba_ids'][$roba_id] = $new_value;
                    }elseif (!$naziv_check) {
                        DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                    }   
                }
            }

            AdminSupport::saveLog('Replace u tekstu, roba_id -> '.implode(',',$roba_ids));          

            return json_encode($response);
            
        }
        elseif ($action == 'data-generisane') {
            $grupa_pr_id = Input::get('grupa_pr_id');
            $count = DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->count();
            $response = array();
            if($count > 0){
                $response['status'] = 'error';
            }else{
                $data = array(
                    'nazivi' => AdminSupport::getGrupaKarak($grupa_pr_id)
                    );
                $response['table_content'] = View::make('admin/partials/ajax/generisane-table-content',$data)->render();
                $response['status'] = 'success';
            }
            echo json_encode($response);
        }
        elseif ($action == 'add-generisane') {
            $roba_ids = Input::get('roba_ids');
            $naziv_id = Input::get('karakteristika_naziv_id');
            $vrednost_id = Input::get('karakteristika_vrednost_id');

            DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->where('grupa_pr_naziv_id',$naziv_id)->update(array('grupa_pr_vrednost_id' => $vrednost_id,'vrednost' => DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$vrednost_id)->pluck('naziv')));

            foreach($roba_ids as $roba_id){
                $check =  DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id,'grupa_pr_naziv_id'=>$naziv_id))->count();
                $active =  DB::table('roba')->where('roba_id',$roba_id)->pluck('flag_aktivan');
                if($check == 0 && $active==1){
                    $rbr_max = DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->max('rbr');
                    $insert_data = array(
                        'roba_id' => $roba_id,
                        'rbr' => isset($rbr_max) ? $rbr_max + 1 : 1,
                        'grupa_pr_naziv_id' => $naziv_id,
                        'grupa_pr_vrednost_id' => $vrednost_id,
                        'vrednost' => DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$vrednost_id)->pluck('naziv')
                        );
                    DB::table('web_roba_karakteristike')->insert($insert_data);
                }
            }
            AdminSupport::saveLog('Dodeljivanje generisanih karakteristika, roba_id -> '.implode(',',$roba_ids));
        }
        elseif ($action == 'delete-generisane') {
            $roba_ids = Input::get('roba_ids');
            $naziv_id = Input::get('karakteristika_naziv_id');
            DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->where('grupa_pr_naziv_id',$naziv_id)->delete();
            AdminSupport::saveLog('Brisanje generisanih karakteristika, roba_id -> '.implode(',',$roba_ids));
        }
        elseif ($action == 'delete-karakteristike') {
            $roba_ids = Input::get('roba_ids');
            $kind = Input::get('kind');

            if($kind=='0'){
                DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('web_karakteristike'=>null));
            }
            elseif($kind=='1'){
                DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->delete();
            }
            elseif($kind=='2'){
                DB::table('dobavljac_cenovnik_karakteristike')->whereIn('roba_id',$roba_ids)->update(array('roba_id'=>-1));
            }

            AdminSupport::saveLog("Karakteristike, DELETE ".($kind==0?"HTML":($kind==1?"GENERISANE":"OD DOBAVLJACA"))." roba_id -> ".implode(', ',$roba_ids));

        }
        elseif ($action == 'html-transfer') {
            $roba_ids = Input::get('roba_ids');
            // DB::statement("UPDATE roba SET web_opis = web_opis||'<br>'||web_karakteristike WHERE roba_id IN (".implode(', ',$roba_ids).") AND web_opis NOT LIKE '%web_karakteristike%'");
            foreach ($roba_ids as $roba_id) {
                $obj = DB::table('roba')->where('roba_id',$roba_id)->first();
                if(strpos($obj->web_opis,$obj->web_karakteristike) !== false){
                }else{
                    DB::table('roba')->where('roba_id',$roba_id)->update(array('web_opis'=>$obj->web_opis.'<br>'.$obj->web_karakteristike));
                }
            }
            AdminSupport::saveLog('Prebacivanje html karakteristika u opis, roba_id -> '.implode(',',$roba_ids));
        }

    }

    function products_import()
        {
        if(AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
            return Redirect::back()->withErrors(array('import_file'=>'Unošenje novih artikala nije dozvoljeno!'));
        }

        $file = Input::file('import_file');

        $validator = Validator::make(array('import_file' => Input::file('import_file')), array('import_file' => 'required'), array('required' => 'Niste izabrali fajl.'));
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $extension = $file->getClientOriginalExtension();
            if(Input::hasFile('import_file') && $file->isValid() && ($extension == 'xml' || $extension == 'xls' || $extension == 'xlsx' || $extension == 'csv' || $extension == 'zip')){
                
                if($extension != 'zip'){
                    $file->move("./files/", 'direct_import.'.$extension);
                }else{
                    $file->move("./files/", 'backup_images.'.$extension);
                }

                $success = false;
                if($extension == 'xml'){
                    $success = AdminDirectImport::xml_execute();
                }elseif($extension == 'xls'){
                    $success = AdminDirectImport::xls_execute();
                }elseif($extension == 'xlsx'){
                    $success = AdminDirectImport::xlsx_execute();
                }elseif($extension == 'csv'){
                    $success = AdminDirectImport::csv_execute();
                }elseif($extension == 'zip'){
                    $success = AdminSupport::extract_images();
                }

                if($extension != 'zip'){
                    File::delete('files/direct_import.'.$extension);
                }else{
                    File::delete('files/backup_images.'.$extension);
                }

                if(!$success){
                    return Redirect::back()->withErrors(array('import_file'=>'Fajl je neispravan.'));
                }
                return Redirect::back();
            }
            return Redirect::back()->withErrors(array('import_file'=>'Import iz ovog fajla nije podrzan.'));
        }
    }

}