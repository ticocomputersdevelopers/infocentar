<?php

class AdminKursController extends Controller {

	function index($valuta_id) {

		$valute = DB::table('valuta')->where('valuta_id','>',1)->where('ukljuceno', 1)->get();
		$current = DB::table('kursna_lista')->select('kupovni','srednji','prodajni','ziralni','web')->where(array('valuta_id'=>$valuta_id,'datum'=>date('Y-m-d')))->first();
		$tabela_valuta = DB::table('kursna_lista')->where('valuta_id',$valuta_id)->orderBy('kursna_lista_id', 'DESC')->get();
	
		$data=array(
            'strana' => 'kurs',
            'title' => 'Unos kursa',
            'valute' => $valute,
            'valuta_id' => $valuta_id,
            'tekuca_valuta' => isset($current) ? $current : (object) array('kupovni'=>'','srednji'=>'','prodajni'=>'','ziralni'=>'','web'=>''),
            'tabela_valuta' => $tabela_valuta
            );

		return View::make('admin/page', $data);
	}

	function edit() {
		$inputs = Input::get();
        $validator = Validator::make($inputs, array('kupovni' => 'numeric|digits_between:1,10','srednji' => 'numeric|digits_between:1,10','prodajni' => 'required|numeric|digits_between:1,10','ziralni' => 'required|numeric|digits_between:1,10','web' => 'required|numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	$current = DB::table('kursna_lista')->where(array('valuta_id'=>$inputs['valuta_id'],'datum'=>date('Y-m-d')))->first();

    		$data = array('kupovni'=>$inputs['kupovni'],'srednji'=>$inputs['srednji'],'prodajni'=>$inputs['prodajni'],'ziralni'=>$inputs['ziralni'],'web'=>$inputs['web']);
        	if(isset($current)){
                $kursna_lista_id = $current->kursna_lista_id;
                DB::table('kursna_lista')->where('kursna_lista_id',$kursna_lista_id)->update($data);
            }else{
                $data['ziralni'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
                $data['web'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
        		$data['kursna_lista_id'] = DB::table('kursna_lista')->max('kursna_lista_id')+1;
        		$data['valuta_id'] = $inputs['valuta_id'];
        		$data['datum'] = date('Y-m-d');
        		$data['paritet'] = 0;
        		DB::table('kursna_lista')->insert($data);
        	}
        	return Redirect::back()->with('message','Uspešno ste sačuvali podatke.');
        }
	}

	function kurs_nbs($id) {
		$content = file_get_contents('https://nbs.rs/kursnaListaModul/zaDevize.faces');
	    $exploded = explode('<td class="tableCell" style="text-align:right;">', $content);
		$kursevi = array(
			'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6))),
			'USD' => array('kupovni' => '','kupovni' => floatval(substr($exploded[24], 0, 6)),'srednji' => (floatval(substr($exploded[23], 0, 6))+floatval(substr($exploded[24], 0, 6)))/2,'prodajni' => floatval(substr($exploded[23], 0, 6))),
			'GBP' => array('kupovni' => '','kupovni' => floatval(substr($exploded[22], 0, 6)),'srednji' => (floatval(substr($exploded[21], 0, 6))+floatval(substr($exploded[22], 0, 6)))/2,'prodajni' => floatval(substr($exploded[21], 0, 6))),
			'CHF' => array('kupovni' => '','kupovni' => floatval(substr($exploded[20], 0, 6)),'srednji' => (floatval(substr($exploded[19], 0, 6))+floatval(substr($exploded[20], 0, 6)))/2,'prodajni' => floatval(substr($exploded[19], 0, 6)))
			);
		$data = array('kupovni' => '','kupovni' => '','srednji' => '','prodajni' => '');

		$valuta_row = DB::table('valuta')->where('valuta_id',$id)->first();
		if(isset($valuta_row)){
			if(isset($kursevi[$valuta_row->valuta_sl])){
				$data = $kursevi[$valuta_row->valuta_sl];
			}else{
				return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('alert','Nije podržano preuzimanje ovog kursa!');
			}
		}

        $validator = Validator::make($data, array('kupovni' => 'numeric|digits_between:1,10','srednji' => 'numeric|digits_between:1,10','prodajni' => 'required|numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('alert','Došlo je do greške prilikom preuzimanja kursa!');
        }else{
        	$current = DB::table('kursna_lista')->where(array('valuta_id'=>$id,'datum'=>date('Y-m-d')))->first();

    		$data_arr = array('kupovni'=>$data['kupovni'],'srednji'=>$data['srednji'],'prodajni'=>$data['prodajni']);
        	if(isset($current)){
        		$kursna_lista_id = $current->kursna_lista_id;
        		DB::table('kursna_lista')->where('kursna_lista_id',$kursna_lista_id)->update($data_arr);
        	}else{
                $data_arr['ziralni'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
                $data_arr['web'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('web');
        		$data_arr['kursna_lista_id'] = DB::table('kursna_lista')->max('kursna_lista_id')+1;
        		$data_arr['valuta_id'] = $id;
        		$data_arr['datum'] = date('Y-m-d');
        		$data_arr['paritet'] = 0;
        		DB::table('kursna_lista')->insert($data_arr);
        	}

			return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('message','Uspešno ste preuzeli kurs!');
        }

	}
}