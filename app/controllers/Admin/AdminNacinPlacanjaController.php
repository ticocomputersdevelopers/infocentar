<?php

class AdminNacinPlacanjaController extends Controller {

	public function index($id) {
		$data = array(
	                "strana"=>'nacin_placanja',
	                "title"=> 'Način plaćanja',	  
	                "nacini_placanja" => AdminNacinPlacanja::all(),
	                "web_nacin_placanja_id" => $id,
	                "nacin_placanja" => AdminNacinPlacanja::getSingle($id)       
	            );
		return View::make('admin/page', $data);
	}

	public function edit($id) {
		$inputs = Input::get();
		
		if(isset($inputs['aktivno'])){
			$aktivno = 1;
		} else {
			$aktivno = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($id == 0){
        		$current = DB::select("SELECT MAX(web_nacin_placanja_id) AS max FROM web_nacin_placanja")[0]->max;
        		$web_nacin_placanja_id = $current + 1;
        		DB::table('web_nacin_placanja')->insert(array('web_nacin_placanja_id' => $web_nacin_placanja_id, 'naziv' => $inputs['naziv'], 'selected' => $aktivno));

        		AdminSupport::saveLog('Šifarnik - Način plaćanja INSERT');

        		return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/0')->withMessage('Uspešno ste sačuvali podatke.');
			} else {
				DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->update(array('naziv' => $inputs['naziv'], 'selected' => $aktivno));

				AdminSupport::saveLog('Šifarnik - Način plaćanja EDIT id: '. $id . ' naziv: ' . $inputs['naziv']);

				return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/'.$id)->withMessage('Uspešno ste sačuvali podatke.');
			}
        }

	}

	public function delete($id) {
		DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/0')->withMessage('Uspešno ste obrisali sadržaj.');
	}





}