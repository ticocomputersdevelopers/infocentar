<?php

class AdminOsobineController extends Controller {

	function index($id = null){
		 $data=array(
            "strana" => 'osobine',
            "title" => 'Osobine',
            "osobina_naziv_id" => $id == null ? 0 : $id,
            "osobine" => AdminOsobine::getAll(),
            "osobina" => AdminOsobine::getProperty($id),
            "vrednosti" => AdminOsobine::getPropertyValues($id)
            );

		  return View::make('admin/page', $data);
	}

	function editProperty(){
		$inputs = Input::get();
		
		if(isset($inputs['aktivna'])){
			$aktivna = 1;
		} else {
			$aktivna = 0;
		}

		if(isset($inputs['prikazi_vrednost'])){
			$prikazi_vrednost = 1;
		} else {
			$prikazi_vrednost = 0;
		}

		if(isset($inputs['color_picker'])){
			$color_picker = 1;
		} else {
			$color_picker = 0;
		}

		if($inputs['rbr'] != ''){
			$rbr = $inputs['rbr'];
		} else {
			$rbr = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required', 'rbr' => 'numeric'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($inputs['osobina_naziv_id'] == 0){
        		DB::table('osobina_naziv')->insert(array('naziv' => $inputs['naziv'], 'aktivna' => $aktivna, 'prikazi_vrednost' => $prikazi_vrednost, 'color_picker' => $color_picker, 'rbr' => $rbr));

        		AdminSupport::saveLog('Šifarnik - Osobina naziv INSERT id: '. DB::select("SELECT currval('osobina_naziv_osobina_naziv_id_seq')")[0]->currval . ' naziv: ' . $inputs['naziv']);
        		$message='Uspešno ste sačuvali podatke.';
        		return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message',$message);
			} else {
				DB::table('osobina_naziv')->where('osobina_naziv_id', $inputs['osobina_naziv_id'])->update(array('prikazi_vrednost' => $prikazi_vrednost, 'naziv' => $inputs['naziv'], 'aktivna' => $aktivna, 'color_picker' => $color_picker, 'rbr' => $rbr));

				AdminSupport::saveLog('Šifarnik - Osobina naziv EDIT id: '. $inputs['osobina_naziv_id'] . ' naziv: ' . $inputs['naziv']);
				$message='Uspešno ste sačuvali podatke.';
				return Redirect::to(AdminOptions::base_url().'admin/osobine/'.$inputs['osobina_naziv_id'])->with('message',$message);
			}
        }
	}

	function editPropertyValue($id, $osobina_vrednost_id){
		$inputs = Input::get();

		if(isset($inputs['aktivna'])){
			$aktivna = 1;
		} else {
			$aktivna = 0;
		}

		if(isset($inputs['boja_css'])){
			$boja_css = $inputs['boja_css'];
		} else {
			$boja_css = '#888888';
		}

		// if($inputs['rbr'] != ''){
		// 	$rbr = $inputs['rbr'];
		// } else {
		// 	$rbr = 0;
		// }

		$validator = Validator::make($inputs, array('vrednost' => 'required', 'rbr' => 'numeric'));

		if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($osobina_vrednost_id == 0){
        		DB::table('osobina_vrednost')->insert(array('osobina_naziv_id' => $id, 'vrednost' => $inputs['vrednost'], 'aktivna' => $aktivna, 'boja_css' =>  $boja_css));

        		AdminSupport::saveLog('Šifarnik - Osobina vrednost - INSERT id: '. DB::select("SELECT currval('osobina_vrednost_osobina_vrednost_id_seq')")[0]->currval . ', vrednost: ' . $inputs['vrednost']);
        		$message='Uspešno ste sačuvali podatke.';
        		return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message',$message);
			} else {
				DB::table('osobina_vrednost')->where('osobina_vrednost_id', $osobina_vrednost_id)->update(array('vrednost' => $inputs['vrednost'], 'aktivna' => $aktivna, 'boja_css' => $boja_css));

				AdminSupport::saveLog('Šifarnik - Osobina vrednost - EDIT id: '. $osobina_vrednost_id . ' vrednost: ' . $inputs['vrednost']);
				$message='Uspešno ste sačuvali podatke.';
				return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message',$message);
			}
        }
	}

	function deleteProperty($id){
		$query_osobina_vrednost = DB::table('osobina_vrednost')->where('osobina_naziv_id', $id);
		DB::table('osobina_roba')->whereIn('osobina_vrednost_id',array_map('current',$query_osobina_vrednost->select('osobina_vrednost_id')->get()))->delete();
		DB::table('osobina_roba')->where('osobina_naziv_id',$id)->delete();
		$query_osobina_vrednost->delete();
		$delete = DB::table('osobina_naziv')->where('osobina_naziv_id', $id)->delete();

		if($delete){
			AdminSupport::saveLog('Šifarnik - Osobina naziv - DELETE id: '. $id);

			return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message', 'Uspešno ste obrisali osobinu!');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message', 'Problem sa brisanjem.');
		}
	}

	function deletePropertyValue($id, $vrednost_id){
		DB::table('osobina_roba')->where('osobina_vrednost_id',$vrednost_id)->delete();
		$query = DB::table('osobina_vrednost')->where('osobina_vrednost_id', $vrednost_id)->delete();

		if($query){
			AdminSupport::saveLog('Šifarnik - Osobina vrednost - DELETE id: '. $vrednost_id);

			return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message', 'Uspešno ste obrisali vrednost!');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message', 'Problem sa brisanjem.');
		}

	}


	// PRODUCT OSOBINE
	function product_osobine_index($id,$osobina_naziv_id){
		$data=array(
            "strana" => 'product_osobine',
            "title" => 'Osobine artikla',
            "roba_id" => $id,
            "osobina_naziv_id" => $osobina_naziv_id,
        	"osobine" => AdminOsobine::getActiveProperties($osobina_naziv_id),
        	"naziv" => AdminOsobine::getNaziv(),
        	"osobine_artikla" => AdminOsobine::getPropertiesProduct($id)
            );

		  return View::make('admin/page', $data);
	}

	function product_osobine_add($roba_id){
		$osobina_naziv_id = Input::get('osobina_naziv_id');

		if($osobina_naziv_id > 0){

			DB::table('osobina_roba')->insert(array('roba_id' => $roba_id, 'aktivna' => 0, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => 0, 'rbr' => 0));

			AdminSupport::saveLog('Product - Osobina Roba - INSERT  roba_id: '. $roba_id);

			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste dodelili osobinu.');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Morate izabrati osobinu.');
		}    
	}

	function product_osobine_add_all($roba_id,$osobina_naziv_id){
//$osobina_naziv_id = Input::get('osobina_naziv_id');var_dump($osobina_naziv_id);die;
		foreach(AdminOsobine::getActiveProperties($osobina_naziv_id) as $row){

			foreach(DB::table('osobina_vrednost')->where(array('osobina_naziv_id'=>$osobina_naziv_id, 'aktivna'=>1))->get() as $row2){

				if(DB::table('osobina_roba')->where(array('roba_id' => $roba_id, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => $row2->osobina_vrednost_id))->count() == 0){

					AdminSupport::saveLog('Product - Osobina - Add All  roba_id: '. $roba_id);

					DB::table('osobina_roba')->insert(array('roba_id' => $roba_id, 'aktivna' => 1, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => $row2->osobina_vrednost_id, 'rbr' => $row2->rbr));
				}
			}
		}
		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id);
	}

	function product_osobine_edit($id, $osobina_naziv_id){
		$rbr = Input::get('rbr');
		$osobina_vrednost_id = Input::get('osobina_vrednost_id');
		$aktCheck = Input::get('aktivna');
		$old_vrednost_id = Input::get('old_vrednost_id');

		if(isset($aktCheck)){
			$aktivna = 1;
		} else {
			$aktivna = 0;
		}

		$check = DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $osobina_vrednost_id)->first();

		if(!$check){

			DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $old_vrednost_id)->update(array('aktivna' => $aktivna, 'osobina_vrednost_id' => $osobina_vrednost_id, 'rbr' => $rbr));

			AdminSupport::saveLog('Product - Osobina - EDIT roba_id: '. $id . ', vrednost: ' . $osobina_vrednost_id);

			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id .'/'. $osobina_vrednost_id)->with('message', 'Uspešno ste sačuvali podatke.');

		} else {

			if($osobina_vrednost_id == 0){

				return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('alert', 'Morate izabrati vrednost.');

			} elseif($osobina_vrednost_id == $old_vrednost_id) {

				DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $old_vrednost_id)->update(array('aktivna' => $aktivna, 'rbr' => $rbr));

				AdminSupport::saveLog('Product - Osobina - EDIT roba_id: '. $id . ', vrednost: ' . $osobina_vrednost_id);
				return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('message', 'Uspešno ste sačuvali podatke.');

			} else {
				return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('alert', 'Nije moguće dodeliti vrednost zato što već postoji.');
			}	
		}
	}

	function product_osobine_delete($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {

		DB::table('osobina_roba')->where('roba_id', $roba_id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $osobina_vrednost_id)->delete();

		AdminSupport::saveLog('Product - Osobina - DELETE roba_id: '. $roba_id . ', vrednost: ' . $osobina_vrednost_id);

		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	}
	
	function product_osobine_grupa_delete($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {

		DB::table('osobina_roba')->where('roba_id', $roba_id)->where('osobina_naziv_id', $osobina_naziv_id)->delete();

		AdminSupport::saveLog('Product - Osobina - DELETE roba_id: '. $roba_id . ', vrednost: ' . $osobina_vrednost_id);

		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	}

	function product_osobine_delete_all($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {

		DB::table('osobina_roba')->where('roba_id', $roba_id)->delete();

		AdminSupport::saveLog('Product - Osobina - DELETE roba_id: '. $roba_id . ', vrednost: ' . $osobina_vrednost_id);

		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	}
	function position_osobine(){
        $order_arr = Input::get('order');
         
        foreach($order_arr as $key => $val){
            DB::table('osobina_vrednost')->where('osobina_vrednost_id',$val)->update(array('rbr'=>$key));
        }
        
    }


}