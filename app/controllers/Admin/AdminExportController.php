<?php
use Export\Support;
use Export\ITSvet;
use Export\PametnoRs;
use Export\Shopmania;
use Export\Magento;
use Export\Woocommerce;
use Export\Shopify;
use Export\Eponuda;
use Export\Kupindo;
use Export\CeneRS;


class AdminExportController extends Controller {

	function export($export_sifra,$kind,$key){
		$export_data = DB::table('export')->where(array('dozvoljen'=>1,'vrednost_kolone'=>$export_sifra))->first();
		if(isset($export_data->export_id)){
			if($export_data->vrednost_kolone == 'it-svet'){
				!is_null(Request::get('short')) && trim(Request::get('short')) == 'true' ? $short = true : $short = false;
				ITSvet::execute($export_data->export_id,$kind,$short);
			}
			elseif($export_data->vrednost_kolone == 'pametno-rs'){
				PametnoRs::execute($export_data->export_id,$kind);
			}
			elseif($export_data->vrednost_kolone == 'kupindo'){
				return View::make('admin/export/kupindo',array('export_id' => $export_data->export_id, 'kind' => $kind,'key' => $key));
			}
			elseif($export_data->vrednost_kolone == 'shopmania'){
				return View::make('admin/export/shopmania',array('export_id' => $export_data->export_id, 'kind' => $kind,'key' => $key));
			}
			elseif($export_data->vrednost_kolone == 'magento'){
				return View::make('admin/export/magento',array('export_id' => $export_data->export_id, 'kind' => $kind,'key' => $key));
			}
			elseif($export_data->vrednost_kolone == 'woocommerce'){
				return View::make('admin/export/woocommerce',array('export_id' => $export_data->export_id, 'kind' => $kind,'key' => $key));
			}
			elseif($export_data->vrednost_kolone == 'shopify'){
				return View::make('admin/export/shopify',array('export_id' => $export_data->export_id, 'kind' => $kind,'key' => $key));
			}
			elseif($export_data->vrednost_kolone == 'ceners'){
				
				$file = CeneRS::execute($export_data->export_id,$kind);
				return Redirect::to($file);
			}
			elseif($export_data->vrednost_kolone == 'eponuda'){
				$file = Eponuda::execute($export_data->export_id,$kind);
				return Redirect::to($file);
			}
		}else{
			return Redirect::to(AdminOptions::base_url());
		}
	}

	function direct_export($export_sifra,$kind,$key){
		$export_data = DB::table('export')->where(array('dozvoljen'=>1,'vrednost_kolone'=>$export_sifra))->first();
		if(isset($export_data->export_id)){
			if($export_data->vrednost_kolone == 'it-svet'){
				!is_null(Request::get('short')) && trim(Request::get('short')) == 'true' ? $short = true : $short = false;
				ITSvet::execute($export_data->export_id,$kind,$short);
			}
			elseif($export_data->vrednost_kolone == 'pametno-rs'){
				PametnoRs::execute($export_data->export_id,$kind);
			}
			elseif($export_data->vrednost_kolone == 'kupindo'){
				Kupindo::execute($export_data->export_id,$kind,array());
			}
			elseif($export_data->vrednost_kolone == 'ceners'){
				$file = CeneRS::execute($export_data->export_id,$kind,array());
				return Redirect::to($file);
			}
			elseif($export_data->vrednost_kolone == 'shopmania'){
				Shopmania::execute($export_data->export_id,$kind,array());
			}
			elseif($export_data->vrednost_kolone == 'magento'){
				$data = array('name' => 'on','category' => 'on','url' => 'on','title_meta' => 'on','description_meta' => 'on','keywords' => 'on','description' => 'on','short_description' => 'on','weight' => 'on','image' => 'on','additional_images' => 'on','visibility' => 'on','price' => 'on','action' => 'on','tax' => 'on','quantity' => 'on','stock' => 'on','is_full' => 'on');
				Magento::execute($export_data->export_id,$kind,$data);
			}
			elseif($export_data->vrednost_kolone == 'woocommerce'){
				$data = array('name' => 'on','category' => 'on','keywords' => 'on','price' => 'on','mp_price' => 'on','description' => 'on','short_description' => 'on','image' => 'on','brand' => 'on','link' => 'on');
				Woocommerce::execute($export_data->export_id,$kind,$data);
			}
			elseif($export_data->vrednost_kolone == 'shopify'){
				$data = array('name' => 'on','url' => 'on','title_meta' => 'on','description_meta' => 'on','tags' => 'on','description' => 'on','characteristics' => 'on','manufacturer' => 'on','weight' => 'on','barcode' => 'on','type' => 'on','image' => 'on','additional_images' => 'on','published' => 'on','price' => 'on','action' => 'on','tax' => 'on','quantity' => 'on','stock' => 'on','shiping' => 'on','is_full' => 'on');
				Shopify::execute($export_data->export_id,$kind,$data);
			}
			elseif($export_data->vrednost_kolone == 'eponuda'){
				   $file = Eponuda::execute($export_data->export_id,$kind);
				   return Redirect::to($file);
			}
		}else{
			return Redirect::to(AdminOptions::base_url());
		}
	}

	function export_external($export_sifra,$kind){
		$hash_key = AdminOptions::gnrl_options(3002,'str_data');
		return Redirect::to(AdminOptions::base_url().'export/'.$export_sifra.'/'.$kind.'/'.$hash_key);
	}

	function shopmania_config($key){
		$data = Input::get();
		Shopmania::execute($data['export_id'],$data['kind'],$data);
	}
	function magento_config($key){
		$data = Input::get();
		Magento::execute($data['export_id'],$data['kind'],$data);
	}
	function woocommerce_config($key){
		$data = Input::get();
		Woocommerce::execute($data['export_id'],$data['kind'],$data);
	}
	function shopify_config($key){
		$data = Input::get();
		Shopify::execute($data['export_id'],$data['kind'],$data);
	}
	function kupindo_config($key){
		$data = Input::get();
		Kupindo::execute($data['export_id'],$data['kind'],$data);
	}
	function ceners_config($key){
		$data = Input::get();
		CeneRS::execute($data['export_id'],$data['kind'],$data);
	}

	function shopmania_auto_export() {
		Shopmania::execute_auto();
	}



	function export_grupe($export_id,$kind,$key){
		$export = DB::table('export')->where('export_id',$export_id)->first();
		if(is_null($export)){
			return Redirect::back();
		}else{
			return View::make('admin/export/export_grupe',array('export'=>$export,'kind' => $kind,'key' => $key,'export_grupe'=>Support::export_our_groups($export->export_id)));
		}
	}

	function grupa_file(){
		$file = Input::file('imp_file');
		$export_id = Input::get('export_id');
		$kind = Input::get('kind');
		$key = Input::get('key');

        if(Input::hasFile('imp_file') && $file->isValid() && $file->getClientOriginalExtension() == 'csv'){
            $file->move('files/','export_grupe.csv');
            $groups_file = 'files/export_grupe.csv';

	        $grupe_arr = array();
			if (($handle = fopen($groups_file, "r")) !== FALSE) {
			    while (($product = fgetcsv($handle, 1000, ";")) !== FALSE) {
			    	if(is_numeric($product[0])){
			    		$grupe_arr[] = (object) array('id' => $product[0], 'name' => $product[2]);
			    	}
			    }
			}

	        $grupe_unq = array_unique(array_map('current',$grupe_arr));

	        $db_groups = array_map('current',DB::table('export_grupe')->where('export_id',$export_id)->select('grupa_id')->get());

	        $diff_groups = array_diff($db_groups,$grupe_unq);

	        if(count($diff_groups) > 0){
				DB::table('export_grupe')->where('export_id',$export_id)->whereIn('grupa_id',$diff_groups)->delete();
			}

			foreach($grupe_arr as $gr_arr) {
				$obj = DB::table('export_grupe')->where(array('export_id'=>$export_id,'grupa_id'=>$gr_arr->id))->first();
				if(!is_null($obj)){
					// $parent_id = $obj->grupa_id;
				}else{
					DB::table('export_grupe')->insert(array('grupa_id'=>$gr_arr->id, 'naziv'=>trim($gr_arr->name),'export_id'=>$export_id));
				}
			}

        	return Redirect::to(AdminOptions::base_url().'export/export-grupe/'.$export_id.'/'.$kind.'/'.$key)->with('message','Fajl je uspesno učitan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'export/export-grupe/'.$export_id.'/'.$kind.'/'.$key)->with('message','Niste izabrali fajl ili proverite strukturu fajla!');
        }
	}
	function grupe_povezi(){
		$export_id = Input::get('export_id');
		$kind = Input::get('kind');
		$key = Input::get('key');
		$grupa_id = Input::get('grupa_id');
		$grupa_pr_id = Input::get('grupa_pr_id');
        $validator = Validator::make(array('grupa_id'=>$grupa_id,'grupa_pr_id'=>$grupa_pr_id), array('grupa_id' => 'required|numeric','grupa_pr_id' => 'required|numeric'),array('required'=>'Niste izabrali grupu!'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'export/export-grupe/'.$export_id.'/'.$kind.'/'.$key)->withInput()->withErrors($validator->messages());
        }else{
        	$grupa_pr_ids = DB::table('export_grupe')->where(array('export_id'=>$export_id,'grupa_id'=>$grupa_id))->pluck('grupa_pr_ids');
        	if(!in_array($grupa_pr_id,Support::export_our_groups($export_id))){
	        	if(isset($grupa_pr_ids)){
	        		$grupa_pr_ids.=',';
	        	}
        		DB::table('export_grupe')->where('grupa_id',$grupa_id)->update(array('grupa_pr_ids'=>$grupa_pr_ids.$grupa_pr_id));
        	}
        	return Redirect::to(AdminOptions::base_url().'export/export-grupe/'.$export_id.'/'.$kind.'/'.$key);
        	// ->with('message','Grupa je uspešno povezana!');
        }

	}

	function grupe_razvezi(){
		$export_id = Input::get('export_id');
		$grupa_id = Input::get('grupa_id');
        $childs = DB::table('export_grupe')->where('parent_id',$grupa_id)->count();
		DB::table('export_grupe')->where(array('export_id'=>$export_id,'grupa_id'=>$grupa_id))->update(array('grupa_pr_ids'=>null));
		echo 'Grupe su uspešno razvezane!';
	}


}