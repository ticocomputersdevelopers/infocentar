<?php 


class B2bPartner extends Eloquent {
    
    protected $table = 'partner';
    
    public function isPartner(){
        return Auth::user()->partner_id>0;    
    }

    public static function getPartnerName(){
        $partner = self::where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        return $partner->naziv;
    }

    public static function forgotPassword($mail){
        $newPassword = B2bCommon::user_password();
        DB::table('partner')->where('mail',$mail)->update(array('password' => $newPassword));
        $body="Vi ili neko sa Vašom e-mail adresom ".$mail." je uputio zahtev sa <a href='".B2bOptions::base_url()."b2b'>".B2bOptions::base_url()."b2b</a> u vezi sa zaboravljenom lozinkom.Vaša lozinka je resetovana i glasi ovako:<br />".$newPassword;
        $subject="Promena lozinke";
        B2bCommon::send_email_to_client($body,$mail,$subject);

    }


} 