<?php 

class B2bArticle {

    public static function find_flag_cene($roba_flag_cene_id, $column)
    {
        return DB::table('roba_flag_cene')->select($column)->where('roba_flag_cene_id', $roba_flag_cene_id)->pluck($column);
    }

    public static function getStatusArticle($roba_id)
    {
        return DB::select("SELECT roba_flag_cene_id FROM roba r WHERE roba_id=".$roba_id."")[0]->roba_flag_cene_id;
    }
    public static function mostPopularArticles($grupa_pr_id){

		$popular = DB::table('roba')->where('flag_aktivan', 1)->where('grupa_pr_id', $grupa_pr_id)->get();
	    

		return $popular;
	}
    public static function procenat_m($num1,$num2){
    	$result=0;
    	$result=($num1-($num1*$num2)/100);
    	return $result;
    }
    public static function procenat_p($num1,$num2){
    	$result=0;
    	$result=($num1+($num1*$num2)/100);
    	return $result;
    }
	public static	function procenat($num1,$num2){
    	$result=0;
    	$result=($num1*$num2)/100;
    	return $result;
    }
	public static function get_product_id($naziv_web){
		
		$artikli = DB::table('roba')->select('roba_id', 'naziv_web')->where(array('flag_cenovnik'=>1,'flag_aktivan'=>1))->get();
		foreach($artikli as $row){
			if(B2bUrl::url_convert($row->naziv_web)==$naziv_web){
				$roba_id=$row->roba_id;
				break;
			}
		}
		if(!isset($roba_id)){
			$roba_id = 0;
		}
		return $roba_id;
	}
	public static function seo_title($roba_id){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
		foreach ($query_title as $row){
			return $row->naziv_web;
		}
	
	}
	public static function sifra_is($roba_id){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
		foreach ($query_title as $row){
			return $row->sifra_is;
		}
	
	}
	public static function short_title($roba_id){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
		foreach ($query_title as $row){
			if(strlen($row->naziv_web)>70){
			return substr($row->naziv_web,0,67)."...";
			}
			else {
				return $row->naziv_web;
			}
		}
	
	}
	public static function addon_title($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv_dopunski');
	}

	public static function seo_description($roba_id){
		
		$product_seo=DB::table('roba')->where('roba_id', $roba_id)->first();
	
		if($product_seo->description!="" ){
			return B2bCommon::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->description), 0, 130)." ".B2bOptions::company_name();
		}
		elseif($product_seo->web_opis!="" ){
			return B2bCommon::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->web_opis), 0, 130)." ".B2bOptions::company_name();
		}
		else {
			return B2bCommon::grupa_title($product_seo->grupa_pr_id)." ".strip_tags($product_seo->naziv_web)." ".B2bOptions::company_name();
		}
	}

	public static function seo_keywords($roba_id){
		$product_keywords=DB::table('roba')->where('roba_id',$roba_id)->first();
		if($product_keywords->keywords!=""){
			return $product_keywords->keywords;
		}
		else {
			return $product_keywords->naziv_web;
		}
	}

	public static function b2bproduct_view(){
		$cn_query=DB::table('web_options')->where('web_options_id',106)->get();
		foreach($cn_query as $row){
			return $row->int_data;
		}
	}
	public static function quantityB2b($roba_id){
		$kolicina=0;
		$rezervisano=0;
		$lager=DB::table('lager')->where('roba_id',$roba_id)->where('orgj_id',function($q){
				$q->from('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
			})->where('poslovna_godina_id',function($q){
				$q->from('poslovna_godina')->where('status',0)->limit(1)->pluck('poslovna_godina_id');
			})->first();

		if(!is_null($lager)){
			$kolicina=$lager->kolicina;
			$rezervisano=$lager->rezervisano;
		}
		return $kolicina-$rezervisano;

	}
	public static function web_slika($roba_id){
		$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." ORDER BY akcija DESC");

		if($web_slika){
			$putanja = 'images/products/big/'.$web_slika[0]->slika;;
		} else {
			$putanja="images/no-image.jpg";
		}
	     
		return $putanja;
	}
 
	//product rabat
	public static function b2bRabatProduct($roba_id, $partner_id){
		$b2b_max_rabat = DB::table('roba')->where('roba_id',$roba_id)->pluck('b2b_max_rabat');
		$art_prcenat = 0;
		if($b2b_max_rabat != 0 && !is_null($b2b_max_rabat)){
			$art_prcenat=$b2b_max_rabat;
		}
		return $art_prcenat;
	}
	//group rabat
	public static function b2bGroupRabat($grupa_pr_id){
		$grupa_rabat = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('rabat');
		return round($grupa_rabat);
	}
	//manufacturer rabat
	public static function b2bProizvodjacRabat($proizvodjac_id){
		$grupa_rabat = DB::table('proizvodjac')->where('proizvodjac_id',$grupa_pr_id)->pluck('rabat');
		return round($grupa_rabat);
	}
	//partner rabat
	public static function b2bPartnerRabat($partner_id){
		$partner_rabat = DB::table('partner')->where('partner_id',$partner_id)->pluck('rabat');
		return round($partner_rabat);
	}
	public static function b2bCombinationRabat($partner_id, $grupa_pr_id, $proizvodjac_id){
		$combination_rabat = DB::table('partner_rabat_grupa')->where(array('partner_id'=>$partner_id,'grupa_pr_id'=>$grupa_pr_id,'proizvodjac_id',$proizvodjac_id))->pluck('rabat');
		return round($combination_rabat);
	}

	public static function b2bPartnerGroupRabat($grupa_pr_id, $partner_id){
		$partner_grupa_rabat = DB::table('partner_rabat_grupa')->where('partner_id',$partner_id)->where('grupa_pr_id',$grupa_pr_id)->pluck('rabat');
		return round($partner_grupa_rabat);
	}


	public static function b2bPartnerRabatProduct($roba_id, $partner_id){
		return self::b2bRabatProduct($roba_id, $partner_id) + self::b2bPartnerGroupRabat(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'), $partner_id);
	}
	public static function b2bTax($tarifna_grupa_id){
		return DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->pluck('porez');
	}

	public static function b2bRabatCene($roba_id){
		$roba = DB::table('roba')->select('b2b_max_rabat', 'b2b_akcijski_rabat' ,'grupa_pr_id', 'stara_grupa_id', 'proizvodjac_id', 'racunska_cena_end', 'web_cena', 'tarifna_grupa_id', 'id_is')->where('roba_id',$roba_id)->first();

		$partner_id = Session::get('b2b_user_'.B2bOptions::server());
		$porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez');
		$osnovna_cena = B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+$porez/100) : $roba->racunska_cena_end;

		$ukupan_rabat = 0;
		$osnovno_racunanje = false;

		if(B2bOptions::gnrl_options(3034,'int_data')){
			$ukupna_cena = 0;
			$id_kategorije = DB::table('partner')->where('partner_id',$partner_id)->pluck('id_kategorije');

			if(!is_null($id_kategorije)){
				$ukupna_cena = DB::table('roba_partner_kategorija')->where(array('roba_id'=>$roba_id,'id_kategorije'=>$id_kategorije))->pluck('cena');
			}
			if(is_null($ukupna_cena)){
				$ukupna_cena = 0;
			}

			$cena_sa_rabatom = $ukupna_cena / (1+$porez/100);
			$ukupan_rabat = (1 - ($cena_sa_rabatom / $osnovna_cena))*100;

			return (object) array(
				'porez' => $porez,
				'ukupan_rabat' => $ukupan_rabat,
				'osnovna_cena' => $osnovna_cena,
				'cena_sa_rabatom' => $cena_sa_rabatom,
				'ukupna_cena' => $ukupna_cena,
				);			

		}else if(B2bOptions::gnrl_options(3018,'int_data')){
			if(B2bOptions::gnrl_options(3032,'int_data') == 0){
				$partner_rabat_grupa_id = 'grupa_pr_id';
				$grupa_pr_id = $roba->grupa_pr_id;
			}else{
				$partner_rabat_grupa_id = 'sifra_kategorije_logic';
				if(!is_null($roba->stara_grupa_id) && is_numeric($roba->stara_grupa_id)){
					$grupa_pr_id = intval($roba->stara_grupa_id);
				}else{
					$osnovno_racunanje = true;
				}
			}

			if(!$osnovno_racunanje){
				if($kombinacija = DB::table('partner_rabat_grupa')->where(array('partner_id'=>$partner_id, $partner_rabat_grupa_id=>$grupa_pr_id, 'proizvodjac_id'=>$roba->proizvodjac_id))->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where(array('partner_id'=>$partner_id, $partner_rabat_grupa_id=>$grupa_pr_id))->where('proizvodjac_id',-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where(array('partner_id'=>$partner_id, 'proizvodjac_id'=>$roba->proizvodjac_id))->where($partner_rabat_grupa_id,-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where('partner_id',$partner_id)->where($partner_rabat_grupa_id,-1)->where('proizvodjac_id',-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where(array($partner_rabat_grupa_id=>$grupa_pr_id, 'proizvodjac_id'=>$roba->proizvodjac_id))->where('partner_id',-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where('partner_id',-1)->where('proizvodjac_id',-1)->where($partner_rabat_grupa_id,$grupa_pr_id)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where('proizvodjac_id',$roba->proizvodjac_id)->where('partner_id',-1)->where($partner_rabat_grupa_id,-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}elseif($kombinacija = DB::table('partner_rabat_grupa')->where('partner_id',-1)->where($partner_rabat_grupa_id,-1)->where('proizvodjac_id',-1)->first()){
					$ukupan_rabat = $kombinacija->rabat;
				}else{
					$osnovno_racunanje = true;
				}
			}
		}else{
			$osnovno_racunanje = true;
		}

		if($osnovno_racunanje){
			if(B2bOptions::info_sys('infograf')){
				$discounts = Session::get('b2b_user_'.B2bOptions::server().'_discounts');
				$partner_rabat = isset($discounts[$roba->id_is]) ? intval($discounts[$roba->id_is]) : 0;
			}else{
				$partner_rabat = DB::table('partner')->where('partner_id',$partner_id)->pluck('rabat');
			}
			$grupa_rabat = DB::table('grupa_pr')->where('grupa_pr_id',$roba->grupa_pr_id)->pluck('rabat');
			$proizvodjac_rabat = DB::table('proizvodjac')->where('proizvodjac_id',$roba->proizvodjac_id)->pluck('rabat');
			$ukupan_rabat = $partner_rabat + $grupa_rabat + $proizvodjac_rabat + $roba->b2b_akcijski_rabat;			
		}

		$ukupan_rabat = $roba->b2b_max_rabat > 0 && $ukupan_rabat > $roba->b2b_max_rabat ? $roba->b2b_max_rabat : $ukupan_rabat;

		$cena_sa_rabatom = $osnovna_cena * (1-$ukupan_rabat/100);
		$ukupna_cena = $cena_sa_rabatom * (1+$porez/100);

		return (object) array(
			'porez' => $porez,
			'ukupan_rabat' => $ukupan_rabat,
			'osnovna_cena' => $osnovna_cena,
			'cena_sa_rabatom' => $cena_sa_rabatom,
			'ukupna_cena' => $ukupna_cena,
			);

	}
	public static function b2bBasicPrice($roba_id){
		// $product = DB::table('roba')->where('roba_id',$roba_id)->first();
		// return $product->web_cena / 1.2;
		// return DB::select("SELECT web_cena / (1 + (SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id = roba.tarifna_grupa_id)/100) as basic_price FROM roba WHERE roba_id = ".$roba_id."")[0]->basic_price;
		// return DB::select("SELECT racunska_cena_end FROM roba WHERE roba_id = ".$roba_id."")[0]->web_cena * 1.2;
		$roba = DB::table('roba')->select('racunska_cena_end')->where('roba_id',$roba_id)->first();
		if($roba){
			return $roba->racunska_cena_end;
		}else{
			return 0.00;
		}
	}
	public static function getName($roba_id){
		$roba = DB::table('roba')->where('roba_id',$roba_id)->first();
		return $roba->naziv_web;
	}

	public static function product_bredacrumps($grupa_pr_id,$bredacrumps=""){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
        if($first->parrent_grupa_pr_id){
    		$link = self::grupa_link($first->parrent_grupa_pr_id,Url_mod::url_convert($first->grupa));
        }else{
        	$link = Url_mod::url_convert($first->grupa);
        }
        $bredacrumps = "<li><a href='".Options::base_url()."b2b/artikli/".$link."'>".B2bCommon::grupa_title($first->grupa_pr_id)."</a></li>".$bredacrumps;
        if($first->parrent_grupa_pr_id != 0){
        	$bredacrumps =  self::product_bredacrumps($first->parrent_grupa_pr_id,$bredacrumps);
        }else{
        	$bredacrumps = "<li><a href='".B2bOptions::base_url()."b2b'>".B2bCommon::get_title_page_start()."</a></li>".$bredacrumps;
        }
        return $bredacrumps;

	}
	public static function grupa_link($grupa_pr_id,$link=''){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
        if($link!=''){
        	$link = B2bUrl::url_convert($first->grupa).'/'.$link;
        }else{
        	$link = B2bUrl::url_convert($first->grupa);
        }
        if($first->parrent_grupa_pr_id != 0){
        	$link = self::grupa_link($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}	

	public static function web_slika_big($roba_id){
			$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." ORDER BY akcija DESC");

			if($web_slika){
				$putanja = 'images/products/big/'.$web_slika[0]->slika;;
			} else {
				$putanja="images/no-image.jpg";
			}
             
			return $putanja;
	}
	public static function get_list_images($roba_id){

		$query_images=DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->take(4)->get();
		foreach ($query_images as $row) {
			echo '	<a class="elevatezoom-gallery" href="#!" data-image="'.B2bOptions::base_url().''.$row->putanja.'" data-zoom-image="'.B2bOptions::base_url().''.$row->putanja.'"> 

				<img class="img_01" src="'.B2bOptions::base_url().''.$row->putanja.'" /> </a> ';
		}

	}

	public static function get_grupaB2b($roba_id){

		$query_grupa=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');

		if(B2bCommon::grupa_level($query_grupa)==1){

			echo '<a href="'.Options::base_url().'b2b/artikli/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa)).'" itemprop="recipeCategory"> '.B2bCommon::grupa_title($query_grupa).'</a>';
		}
		else if(B2bCommon::grupa_level($query_grupa)==2){
			$query_grupa2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa)->pluck('parrent_grupa_pr_id');

			echo '<a  itemprop="recipeCategory" href="'.Options::base_url().'b2b/artikli/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa2)).'/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa)).'"> '.B2bCommon::grupa_title($query_grupa).'</a>';
		}
		else {
			$query_grupa2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa)->pluck('parrent_grupa_pr_id');
			$query_grupa3=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa2)->pluck('parrent_grupa_pr_id');

			echo '<a  itemprop="recipeCategory" href="'.Options::base_url().'b2b/artikli/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa3)).'/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa2)).'/'.B2bUrl::url_convert(B2bCommon::grupa_title($query_grupa)).'"> '.B2bCommon::grupa_title($query_grupa).'</a>';
		}

	}
	public static function get_proizvodjac($roba_id){
		return DB::table('proizvodjac')->where('proizvodjac_id',DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'))->pluck('naziv');
	}

    public static function get_opis($roba_id){
        
        foreach(DB::table('roba')->where('roba_id',$roba_id)->get() as $row){
            
            return $row->web_opis;
        }
    }

	public static function get_karakteristike($roba_id){
		$query_roba=DB::table('roba')->where('roba_id',$roba_id);
		foreach($query_roba->get() as $row){
			$web_flag_karakteristike=$row->web_flag_karakteristike;
			$web_karakteristike=$row->web_karakteristike;
			
		}
		if($web_flag_karakteristike == 0){
			echo $web_karakteristike;
		}
		else if($web_flag_karakteristike == 1)
		{	echo '<ul class="features-list row">';
			$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->orderBy('rbr', 'asc');
			foreach($query_generisane->get() as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				$naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('naziv');
			echo "<li class='medium-6 columns'>".$naziv.": </li> <li class='medium-6 columns'>".$vrednos."</li>";
				
			
			}
			echo "</ul>";
		}
		else if($web_flag_karakteristike == 2){
		if(B2bCommon::check_ewe($roba_id)==1){
		
			$query_type_ewe=DB::select("select Distinct karakteristika_grupa, redni_br_grupe from dobavljac_cenovnik_karakteristike where roba_id=? and redni_br_grupe>0 order by redni_br_grupe asc",array($roba_id));
			foreach($query_type_ewe as $grupa_kar){
			$karakteristika_grupa=$grupa_kar->karakteristika_grupa;
			echo '<ul class="generated-features-list row">

                <li class="medium-4 columns features-list-title"><span>'.$karakteristika_grupa.'</span></li>
                <li class="medium-8 columns features-list-items">';
			echo '<ul class="row">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where(array('karakteristika_grupa'=>$karakteristika_grupa,'roba_id'=>$roba_id))->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<li class='medium-6 columns'>".$karakteristike_naziv.": </li><li class='medium-6 columns'>".$karakteristike_vrednost."</li>";
				
			}
			echo "</ul></li></ul>
";
			
			}
			}
			else {
					echo '<ul class="features-list row">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<li  class='medium-6 columns'>".$karakteristike_naziv."</li> <li  class='medium-6 columns'>".$karakteristike_vrednost."</li>";
				
			}
			echo "</ul>";
			
			}
		}
	
	}
    public static function tags_count($roba_id){
        $tags=DB::table('roba')->where('roba_id',$roba_id)->pluck('tags');
        
        $niz=explode(',',$tags);
	       
        return count($niz);
    }
	public static function get_tags($roba_id){
		 echo '<ul class="tags row">';
		foreach(DB::table('roba')->where('roba_id',$roba_id)->get() as $row){
			$tags=$row->tags;
		}
		
		$niz=explode(',',$tags);
		
		foreach($niz as $row2){
			echo "<li>".$row2."</li>";
		}
		echo '</ul>';
	}
	public static function get_relatedB2b($roba_id){

		$grupa_pr_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');

		$query_realize=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('roba_id','!=',$roba_id)->where('grupa_pr_id',$grupa_pr_id)->take(4);

		foreach($query_realize->get() as $row){

			$cartAvailable = self::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
			echo '
			<div class="product col-md-3 col-sm-4 col-xs-12 no-padding">
				<div class="product-content">
					<a href="'.B2bOptions::base_url().'b2b/artikal/'.B2bUrl::url_convert($row->naziv_web).'" class="product-image-wrapper">
						<img class="product-image img-responsive" src="'.B2bOptions::base_url().''.self::web_slika($row->roba_id).'" alt="'.$row->naziv_web.'" />
					</a>

					<div class="product-info sm-text-center">
					<div class="product-price"><span>'.B2bBasket::cena(self::b2bRabatCene($row->roba_id)->ukupna_cena).'</span></div>
					<a class="product-title" href="'.B2bOptions::base_url().'b2b/artikal/'.B2bUrl::url_convert($row->naziv_web).'">'.$row->naziv_web.'</a>
					 
					';
					if($cartAvailable>0){
					echo '
					<div class="add-to-cart-container"> 
						<button data-product-id="'.$row->roba_id.'"  data-max-quantity="'.$cartAvailable.'" class="add-to-cart addCart">
							<i class="fa fa-shopping-cart"></i> U korpu
						</button>
					</div>
					';
					}
					else {
					echo '
					<div class="add-to-cart-container"> 
						<button class="dodavnje not-available">Nije dostupno</button>
					</div>	';
					}
					echo '</section>
				</div>
		      </div>
		    </div>
            ';
		}
	}
	public static function subGroups($grupa_pr_id) {
		$q = DB::select('SELECT grupa_pr_id, grupa, putanja_slika FROM grupa_pr  WHERE parrent_grupa_pr_id = '.$grupa_pr_id.' AND web_b2b_prikazi = 1 ORDER BY redni_broj');
		return $q;
	}

}
 