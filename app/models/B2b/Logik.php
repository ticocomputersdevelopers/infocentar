<?php

class Logik {

    public static function customer_cart($partner_id){
        return DB::connection('logik')->select("SELECT * FROM kartica_kupca WHERE sifra_kupca_logik = ".strval($partner_id)."");
    }

    public static function customer_cart_body($items,$partner_id){
        $result_arr = array();
        $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

        foreach($items as $item) {

            $web_b2b_kartica_id++;
            $datum_dokumenta = isset($item->datum_dokumenta) ? $item->datum_dokumenta : '1970-00-00T00:00:00+02:00';
            $vrsta_dokumenta = pg_escape_string(substr($item->vrsta_dokumenta,0,300));
            $opis = isset($item->opis) ? $item->opis : pg_escape_string(substr($item->opis,0,300));
            $duguje = isset($item->duguje) ? $item->duguje : 0.00;
            $potrazuje = isset($item->potrazuje) ? $item->potrazuje : 0.00;
            $saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

            $id_is = $partner_id.'-'.$web_b2b_kartica_id;

            $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dokumenta."')::date,'".$opis."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";

        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function customer_cart_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }
        // DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");
        DB::statement("DELETE FROM web_b2b_kartica WHERE partner_id=".$partner_id."");
        //insert
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp.")");
        // DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
    }


    public static function updateCart($partner_id=null){
        if(is_null($partner_id)){
            $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        }
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $items = self::customer_cart(intval($partner->id_is));

        if(count($items) > 0){
            $resultCart = self::customer_cart_body($items,$partner->partner_id);
            if(isset($resultCart->body) && $resultCart->body != ''){
                self::customer_cart_insert_update($resultCart->body,$partner->partner_id);
            }
            DB::connection('logik')->statement("UPDATE kartica_kupca SET semaphore = 0 WHERE sifra_kupca_logik = ".strval($partner->id_is)."");
        }

    }

    public static function order($partner,$nacin_isporuke,$nacin_placanja,$napomena=''){
        $web_b2b_narudzbina_id = 1;
        $web_b2b_narudzbina = DB::select("SELECT (MAX(web_b2b_narudzbina_id) + 1) AS web_b2b_narudzbina_id FROM web_b2b_narudzbina");
        if(count($web_b2b_narudzbina) > 0 && !is_null($web_b2b_narudzbina[0]->web_b2b_narudzbina_id)){
            $web_b2b_narudzbina_id = $web_b2b_narudzbina[0]->web_b2b_narudzbina_id;
        }
        $current_order_id = DB::connection('logik')->select("SELECT MAX(sifra_narudzbine_logik) as current_order_id FROM narudzbine")[0]->current_order_id + 1;
        $sql = "INSERT INTO narudzbine (sifra_narudzbine_logik,sifra_narudzbine_tico,sifra_kupca_logik,sifra_kupca_tico,vrsta_narudzbina,datum_narudzbine,sifra_nacin_isporuke,naziv_nacin_isporuke,sifra_nacin_placanja,naziv_nacin_placanja,narudzbina_prihvacena,narudzbina_stornirana,narudzbina_realizovana,roba_poslata,sluzba_slanje,broj_posiljke,napomena,semaphore) VALUES (".strval($current_order_id).",'".strval($web_b2b_narudzbina_id)."',".strval($partner->id_is).",'".strval($partner->partner_id)."',1,'".date('Y-m-d')."',1,'".$nacin_isporuke."',1,'".$nacin_placanja."',0,0,0,0,'Nedefinisana',NULL,'".$napomena."',1)";
        DB::connection('logik')->statement($sql);
        DB::statement("SELECT setval('web_b2b_narudzbina_web_b2b_narudzbina_id_seq', ".strval($web_b2b_narudzbina_id).", FALSE)");
        return (object) array('id_is'=>$current_order_id,'web_b2b_narudzbina_id'=>$web_b2b_narudzbina_id);
    }
    public static function addOrderStavka($order_ids,$article,$item_number,$quantity){
        $sql = "INSERT INTO narudzbine_stavke (sifra_narudzbine_logik,sifra_narudzbine_tico,sifra_artikla_logik,sifra_artikla_tico,redni_broj_stavke,kolicina,sifra_tarifne_grupe,nabavna_cena,dilerska_cena,end_cena,maloprodajna_cena,web_cena,prodajna_cena,semaphore) VALUES (".strval($order_ids->id_is).",'".strval($order_ids->web_b2b_narudzbina_id)."',".strval($article->id_is).",'".strval($article->roba_id)."',".strval($item_number).",".strval($quantity).",".strval($article->tarifna_grupa_id).",".$article->racunska_cena_nc.",".$article->racunska_cena_nc.",".$article->web_cena.",".$article->mpcena.",".$article->web_cena.",".($article->web_cena*(1+(1 / DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez')))).",1)";
        DB::connection('logik')->statement($sql);
    }

    public static function createOrder($cartItems,$delivery,$payment,$note=''){
        $success = false;
        $order_id = 0;

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        if(!is_null($partner)){
            $order_ids = self::order($partner,$delivery,$payment,$note);

            foreach($cartItems as $key => $stavka){
                $article = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                $b2bRabatCene = B2bArticle::b2bRabatCene($stavka->roba_id);
                self::addOrderStavka($order_ids,$article,($key+1),$stavka->kolicina);
            }

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

}
