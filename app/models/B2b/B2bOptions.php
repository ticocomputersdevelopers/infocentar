<?php 

class B2bOptions {

	public static function base_url(){
		
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
		
	}
	public static function domain(){
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
	}
    public static function server(){
        return DB::table('options')->where('options_id',1326)->pluck('str_data');
    }	
	public static function checkB2B(){
		if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(1,2))){
			return true;
		}else{
			return false;
		}
	}
	
	public static function gnrl_options($options_id,$kind='int_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}
	public static function web_options($options_id,$kind='int_data'){
		return DB::table('web_options')->where('web_options_id',$options_id)->pluck($kind);
	}

	public static function category_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',115)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
   	public static function category_type(){
				$cn_query=DB::table('web_options')->where('web_options_id',112)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function header_type(){
		$cn_query=DB::table('web_options')->where('web_options_id',114)->get();
		foreach($cn_query as $row){
		return $row->int_data;
		}
	}
	public static function product_number(){
				$cn_query=DB::table('web_options')->where('web_options_id',107)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function product_currency(){
				$cn_query=DB::table('web_options')->where('web_options_id',109)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function product_sort(){
				$cn_query=DB::table('web_options')->where('web_options_id',108)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function company_name(){
			$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->naziv;
			}
			
		}
		public static function company_logo(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->logo;
			}
	}
		
		
	public static function company_adress(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->adresa;
			}
	}
	public static function company_phone(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->telefon;
			}
	}
	public static function company_fax(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->fax;
			}
			
	}

	public static function company_ziro(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->ziro;
			}
	}
	public static function company_pib(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->pib;
			}
	}
	public static function company_email(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->email;
			}
	}
	public static function company_delatnost_sifra(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->delatnost_sifra;
			}
	}
	public static function company_city(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				$mesto=$row->mesto;
			}
			foreach(DB::table('mesto')->where('mesto',$mesto)->get() as $row2){
				return $row2->ptt." ".$row2->mesto;
			}
			
	}
	public static function poslovna_godina(){
	
			return DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
	} 

	public static function vodjenje_lagera(){
				$cn_query=DB::table('web_options')->where('web_options_id',103)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    public static function css_prev(){
        $css="<style>";
        foreach(DB::table('css_class')->get() as $row){
          $css.=$row->naziv."{";
          foreach(DB::table('css_class_atribute')->where('css_class_id',$row->css_class_id)->get() as $row2){
            $css.=$row2->naziv_promenjive.":".$row2->vrednost_active.";";
          }
          $css.="}";
        }
        $css.="</style>";
        
        return $css;
    }
	public static function kurs(){
		$obj = DB::table('kursna_lista')->orderBy('kursna_lista_id','desc')->first();
		if(isset($obj)){
			$izabrani_kurs = self::web_options(205,'str_data');
			if($izabrani_kurs == 'kupovni'){
				return $obj->kupovni;
			}elseif($izabrani_kurs == 'srednji'){
				return $obj->srednji;
			}elseif($izabrani_kurs == 'ziralni'){
				return $obj->ziralni;
			}elseif($izabrani_kurs == 'prodajni'){
				return $obj->prodajni;
			}else{
				return $obj->web;
			}
		}else{
			return 0;
		}
	}
    public static function info_sys($sifra){
        return DB::table('informacioni_sistem')->where(array('sifra'=>$sifra,'aktivan'=>1,'izabran'=>1))->first();
    }

    public static function trajanje_banera($id){ 
        
    foreach(DB::table('baneri_b2b')->where('baneri_id',$id)->get() as $row){            
            
        if(isset($row->datum_do) && isset($row->datum_od)){
            $do = date_create($row->datum_do);
            $od = date_create($row->datum_od);
            $danasnji_dan = date_create();
        
            $trajanje_banera = date_diff($do,$danasnji_dan);        

            if($trajanje_banera->invert == 1 ){
            return 1;
            }else{
            return 0;     
            }
        } else{
            return 1;
        }        
    }
    }
    public static function future_banners($id){ 
        
        foreach(DB::table('baneri_b2b')->where('baneri_id',$id)->get() as $row){            
            
            if(isset($row->datum_do) && isset($row->datum_od)){
            $trajanje = date_create($row->datum_od);
            $danasnji_dan = date_create(); 

            $trajanje_banera = date_diff($trajanje,$danasnji_dan);
            //var_dump($trajanje_banera->invert);die;
            
            if($trajanje_banera->invert == 0){
            return 1;
            }else{
            return 0;     
            }
        }else{
            return 1;
        }
        }         
    }
    public static function popup_banners(){         
       return DB::table('baneri_b2b')->where('tip_prikaza',9)->where('aktivan',1)->get();     
    }
    public static function pozadinska_slika(){         
       return DB::table('baneri_b2b')->where('tip_prikaza',3)->where('aktivan',1)->get();     
    }
    public static function getBGimg(){
        return DB::table('baneri_b2b')->where('tip_prikaza', 3)->pluck('img');
    }
    public static function getlink(){
        return DB::table('baneri_b2b')->where('tip_prikaza', 3)->pluck('link');
    }

    public static function getlink2(){
        return DB::table('baneri_b2b')->where('tip_prikaza', 3)->pluck('link2');
    }


}