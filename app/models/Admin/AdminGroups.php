<?php

class AdminGroups {

	public static function find($grupa_pr_id, $column)
	{
		return DB::table('grupa_pr')->select($column)->where('grupa_pr_id', $grupa_pr_id)->pluck($column);

	}

	public function grupa($roba_id){
		return DB::table('roba')
            ->select('grupa_pr.grupa')
            ->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->where('roba_id', $roba_id)
            ->pluck('grupa_pr.grupa');
	}

	public static function subgroups($grupa_pr_id)
	{
		return DB::table('grupa_pr')->where('parrent_grupa_pr_id', $grupa_pr_id)->orderBy('redni_broj','asc')->get();
	}

	public static function moreGroups($roba_id)
	{
		$roba_grupe = DB::table('roba_grupe')->select('grupa_pr_id')->where('roba_id',$roba_id)->get();
		return array_map('current',$roba_grupe);
	}

	public static function karakteristikeNaziv($grupa_pr_id)
	{
		return DB::table('grupa_pr_naziv')->where('grupa_pr_id', $grupa_pr_id)->orderBy('rbr','asc')->get();

	}

	public static function karakteristikeVrednost($grupa_pr_naziv_id)
	{
		return DB::table('grupa_pr_vrednost')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->orderBy('rbr','asc')->get();

	}

	public static function sifraGenerate($parent_id)
	{
		$rb_parent = DB::table('grupa_pr')->where('grupa_pr_id', $parent_id)->pluck('sifra');
		$sifre = DB::table('grupa_pr')->select('sifra')->where('parrent_grupa_pr_id', $parent_id)->get();

		$new_sifre = array();
		foreach($sifre as $row){
			$new_sifre[] = intval($row->sifra);
		}
		rsort($new_sifre);

		$rb_grupe = sprintf("%02d", intval(substr($new_sifre[0],-2)) + 1);
		return $rb_parent.$rb_grupe;
	}

	public static function group_link($group_id,$lang=null)
	{
		$grupa = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$group_id))->first();
		$slug = AdminOptions::url_convert($grupa->grupa,$lang);
		if($grupa->parrent_grupa_pr_id != 0){
			$slug = self::group_link($grupa->parrent_grupa_pr_id).'/'.$slug;
		}
		return $slug;
	}

	public static function grupa_pr_id($naziv){
		if(trim($naziv) != ''){
			$grupa_pr = DB::table('grupa_pr')->where('grupa',$naziv)->first();
			if(!is_null($grupa_pr)){
				return $grupa_pr->grupa_pr_id;
			}
			$grupa_pr_id_new = DB::select("SELECT MAX(grupa_pr_id) AS max FROM grupa_pr")[0]->max + 1;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id_new,
				'grupa' => trim($naziv),
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id_new,
				'web_b2c_prikazi' => 1,
				'prikaz' => 1,
				));
			return $grupa_pr_id_new;
		}
		return -1;
	}

}
