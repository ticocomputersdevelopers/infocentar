<?php

class AdminFront {

	public static function saveSale($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			DB::table('web_b2c_seo')->where('naziv_stranice','akcija')->update(array('seo_title'=>strip_tags($new_content)));
		}
	}

	public static function saveBlogs($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			DB::table('web_b2c_seo')->where('naziv_stranice','blog')->update(array('seo_title'=>strip_tags($new_content)));
		}
	}

	public static function saveAllArticles($new_content)
	{
		if(strlen(strip_tags($new_content)) < 255){
			DB::table('web_b2c_seo')->where('naziv_stranice','pocetna')->update(array('seo_title'=>strip_tags($new_content)));
		}
	}

	public static function saveType($new_content,$type_id)
	{
		if(strlen(strip_tags($new_content)) < 200){
			DB::table('tip_artikla')->where('tip_artikla_id',$type_id)->update(array('naziv'=>strip_tags($new_content)));
		}
	}

	public static function saveSlideTitle($new_content,$slide_id,$jezik_id)
	{
		if(strlen($new_content) < 100){

			$lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('baneri_jezik')->insert(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id,'naslov'=>$new_content,'nadnaslov'=>'','sadrzaj'=>'')); 
			}else{
				DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->update(array('naslov'=>$new_content)); 
			}
		}
	}

	public static function saveSlidePreTitle($new_content,$slide_id,$jezik_id)
	{
		if(strlen($new_content) < 100){
			$lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('baneri_jezik')->insert(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id,'naslov'=>'','nadnaslov'=>$new_content,'sadrzaj'=>'')); 
			}else{
				DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->update(array('nadnaslov'=>$new_content)); 
			}
		}
	}

	public static function saveSlideContent($new_content,$slide_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 260){
			$lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('baneri_jezik')->insert(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id,'naslov'=>'','nadnaslov'=>'','sadrzaj'=>$new_content)); 
			}else{
				DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
			}
		}
	}

	public static function saveSlideButton($new_content,$slide_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 260){
			$lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('baneri_jezik')->insert(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id,'naslov'=>'','nadnaslov'=>'','sadrzaj'=>'','naslov_dugme'=>$new_content)); 
			}else{
				DB::table('baneri_jezik')->where(array('baneri_id'=>$slide_id,'jezik_id'=>$jezik_id))->update(array('naslov_dugme'=>$new_content)); 
			}
		}
	}

	public static function saveFooterSectionLabel($new_content,$futer_sekcija_id,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 250){
			$lang_data = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('futer_sekcija_jezik')->insert(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id, 'sadrzaj'=>'','naslov'=>strip_tags($new_content))); 
			}else{
				DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->update(array('naslov'=>strip_tags($new_content))); 
			}
		}
	}

	public static function saveFooterSectionContent($new_content,$futer_sekcija_id,$jezik_id)
	{
		$lang_data = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('futer_sekcija_jezik')->insert(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id, 'naslov'=>'','sadrzaj'=>$new_content)); 
		}else{
			DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
		}
	}

	public static function saveNewslatterLabel($new_content,$jezik_id)
	{
		if(strlen(strip_tags($new_content)) < 250){
			$lang_data = DB::table('web_b2c_newslatter_description')->where(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id))->first();
			if(is_null($lang_data)){
				DB::table('web_b2c_newslatter_description')->insert(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id, 'sadrzaj'=>'','naslov'=>strip_tags($new_content))); 
			}else{
				DB::table('web_b2c_newslatter_description')->where(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id))->update(array('naslov'=>strip_tags($new_content))); 
			}
		}
	}

	public static function saveNewslatterContent($new_content,$jezik_id)
	{
		$lang_data = DB::table('web_b2c_newslatter_description')->where(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('web_b2c_newslatter_description')->insert(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id, 'naslov'=>'','sadrzaj'=>$new_content)); 
		}else{
			DB::table('web_b2c_newslatter_description')->where(array('web_b2c_newslatter_description_id'=>1,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$new_content)); 
		}
	}

	public static function saveFrontAdminLabel($content,$id)
	{
		if(strlen(strip_tags($content)) < 500){
			$first = DB::table('front_admin_labele')->where(array('front_admin_labele_id'=>$id))->first();
			if(is_null($first)){
				DB::table('front_admin_labele')->insert(array('front_admin_labele_id'=>$id,'labela'=>$content)); 
			}else{
				DB::table('front_admin_labele')->where(array('front_admin_labele_id'=>$id))->update(array('labela'=>$content));
			}
		}
	}
	public static function saveFrontAdminContent($content,$id,$jezik_id)
	{
		$lang_data = DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id))->first();
		if(is_null($lang_data)){
			DB::table('front_admin_labele_jezik')->insert(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id,'sadrzaj'=>$content)); 
		}else{
			DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id))->update(array('sadrzaj'=>$content)); 
		}
	}

}