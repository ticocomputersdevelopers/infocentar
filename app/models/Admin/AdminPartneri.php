<?php

class AdminPartneri {

	public static function kupciSelect($web_kupac_id=null){
		$render = '<option value="0"></option>';
        foreach(DB::table('web_kupac')->select('web_kupac_id','ime','prezime','naziv', 'flag_vrsta_kupca')->where('web_kupac_id','!=',-1)->get() as $row){

        	if($row->flag_vrsta_kupca == 0) {
	            if($row->web_kupac_id == $web_kupac_id){
	                $render .= '<option value="'.$row->web_kupac_id.'" selected>'.$row->ime.' '.$row->prezime.'</option>';
	            }else{
	                $render .= '<option value="'.$row->web_kupac_id.'">'.$row->ime.' '.$row->prezime.'</option>';
	            }
        	}elseif($row->flag_vrsta_kupca == 1){
	            if($row->web_kupac_id == $web_kupac_id){
	                $render .= '<option value="'.$row->web_kupac_id.'" selected>'.$row->naziv.'</option>';
	            }else{
	                $render .= '<option value="'.$row->web_kupac_id.'">'.$row->naziv.'</option>';
	            }
        	}
        }
        return $render;
	}

	public static function nacinPlacanja($web_nacin_placanja_id = null)
	{
		$render = '';
		foreach(DB::table('web_nacin_placanja')->get() as $row){

			if($row->selected == 1){

				if($web_nacin_placanja_id == $row->web_nacin_placanja_id){

					$render .= '<option value="'. $row->web_nacin_placanja_id .'" selected>'. $row->naziv .'</option>"';
				} else {

					$render .= '<option value="'. $row->web_nacin_placanja_id .'">'. $row->naziv .'</option>"';
				}
			}

		}

		return $render;


	}

	public static function nacinIsporuke($web_nacin_isporuke_id = null)
	{
		$render = '';
		foreach(DB::table('web_nacin_isporuke')->get() as $row){

			if($row->selected == 1){

				if($web_nacin_isporuke_id == $row->web_nacin_isporuke_id){

					$render .= '<option value="'. $row->web_nacin_isporuke_id .'" selected>'. $row->naziv .'</option>"';
				} else {

					$render .= '<option value="'. $row->web_nacin_isporuke_id .'">'. $row->naziv .'</option>"';
				}
			}

		}

		return $render;


	}

	public static function kurirskaSluzba($posta_slanje_id = null) {

		$render = '';
		foreach(DB::table('posta_slanje')->get() as $row){

			if($row->aktivna == 1 && $row->posta_slanje_id != -1){

				if($posta_slanje_id == $row->posta_slanje_id){

					$render .= '<option value="'. $row->posta_slanje_id .'" selected>'. $row->naziv .'</option>"';
				} else {

					$render .= '<option value="'. $row->posta_slanje_id .'">'. $row->naziv .'</option>"';
				}
			}

		}

		return $render;
	}
	
	public static function statusnarudzbine($narudzbina_status_id = null) {

		$render = '';
		foreach(DB::table('narudzbina_status')->get() as $row){

			if($row->selected == 1 ){

				if($narudzbina_status_id == $row->narudzbina_status_id){

					$render .= '<option value="'. $row->narudzbina_status_id .'" selected>'. $row->naziv .'</option>"';
				} else {

					$render .= '<option value="'. $row->narudzbina_status_id .'">'. $row->naziv .'</option>"';
				}
			}

		}

		return $render;
	}

	public static function partneri() {
		return DB::table('partner')->where('partner_id','!=',-1)->get();
	}

	public static function partner($partner_id) {
		return DB::table('partner')->where('partner_id','!=',-1)->where('partner_id',$partner_id)->first();
	}

	public static function poste() {
		return DB::table('posta_slanje')->where('api_aktivna',1)->get();
	}

    public static function getB2Bkartica($id,$paginate=20){
        return DB::table('web_b2b_kartica')->where('partner_id',$id)->orderBy('datum_dokumenta','desc')->paginate($paginate);
    }
    public static function getDuguje($id){
        $duguje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('duguje');
       
        return $duguje;
    }
    public static function getPotrazuje($id){
        $potrazuje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('potrazuje');
       
        return $potrazuje;
    }
    public static function getSum($id){
        $duguje = self::getDuguje($id);
        $potrazuje = self::getPotrazuje($id);

        $saldo = $duguje - $potrazuje;
        return $saldo;
    }
    
}
