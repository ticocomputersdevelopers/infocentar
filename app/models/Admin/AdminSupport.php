<?php

class AdminSupport {
    public static function regex(){
        return '/(^[A-Za-z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\'\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ\а\б\в\г\д\ђ\е\ж\з\и\ј\к\л\љ\м\н\њ\о\п\р\с\т\ћ\у\ф\х\ц\ч\џ\ш\А\Б\В\Г\Д\Ђ\Е\Ж\З\И\Ј\К\Л\Љ\М\Н\Њ\О\П\Р\С\Т\Ћ\У\Ф\Х\Ц\Ч\Џ\Ш]+$)+/';
        }
    public function one_digit($field, $value, $parameters){
        // if (preg_match("/(?<!\d)\d{1}(?!\d)/",$value)) {
        if (preg_match("/(?i)^(?=.*[a-z])(?=.*\d)/",$value)) {
            return true;
        }else{
            return false;
        }
    }
    
    public static function searchQueryString($search_arr,$column){
        $string_query = "";
        foreach($search_arr as $wrd){
            $string_query .= " ".$column." ILIKE '%".$wrd."%' AND";
        }
        return substr($string_query,0,-4);
    }
    
    public static function cena_round($value,$decimale=null)
    {
        is_null($decimale) ? $decimale = AdminOptions::web_options(209) : $decimale = 0;

        // $add_str = '';
        // if($decimale == 0){
        //     $add_str = '.00';
        // }
        // return round($value,$decimale,PHP_ROUND_HALF_UP).$add_str;

        return round($value, $decimale);
    }

    public static function query_gr_level($grupa_pr_id)
    {
    return DB::select("SELECT grupa_pr_id, parrent_grupa_pr_id, grupa, redni_broj FROM grupa_pr WHERE parrent_grupa_pr_id = ".$grupa_pr_id." ORDER BY redni_broj ASC");
    }

    public static function dobavljac($id) 
    {
      return DB::table('partner')->where('partner_id', $id)->pluck('naziv');
    }
    public static function tekst_pocetna() 
    {
      return DB::table('web_b2c_seo')->where('web_b2c_seo_id', 1)->pluck('tekst');
    }

    public static function dobavljac_ime() {
        return DB::table('dobavljac_cenovnik_kolone')->join('partner', 'dobavljac_cenovnik_kolone.partner_id', '=', 'partner.partner_id')->select('dobavljac_cenovnik_kolone.partner_id', 'partner.naziv')->where('dobavljac_cenovnik_kolone.partner_id','!=','-1')->get();
    }
    public static function trajanje_banera($id){ 
        
        foreach(DB::table('baneri')->where('baneri_id',$id)->whereNotNull('datum_do')->whereNotNull('datum_od')->get() as $row){            
            
           
            $do = date_create($row->datum_do);
            $danasnji_dan = date_create();

            $trajanje_banera = date_diff($do, $danasnji_dan);
//var_dump($trajanje_banera);die;
            if($trajanje_banera->invert == 1){
            return $trajanje_banera->days+1;
            }else{
            return 0;     
            }
        }         
    }
    
    public static function parrent_strana($id){ 

     foreach (DB::table('web_b2c_seo')->where('web_b2c_seo_id',$id)->where('parrent_id','>',0)->get() as $row){
        $parrent =$row->parrent_id;
        if ($parrent > 0) {
            return 1;
        }else{
            return 0;
        }
        }
    }

    public static function getProizvodjaci()
    {
        return DB::table('proizvodjac')->select('proizvodjac_id', 'naziv')->where('proizvodjac_id', '<>', -1)->where('proizvodjac_id', '<>', 0)->orderBy('naziv', 'ASC')->get();
    }
    
    public static function getDobavljaci()
    {
        return DB::table('partner')->select('partner_id', 'naziv')->where('partner_id', '<>', -1)->orderBy('naziv', 'ASC')->get();
    }

    public static function getKarakNaziv($grupa_pr_id)
    {   
       
        $childs= DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id))->count() >0;
        if(!$childs){
        return DB::table('grupa_pr_naziv')->where('grupa_pr_id', $grupa_pr_id )->orderBy('rbr', 'ASC')->get();  
        }else{
        return array();
        }
       
    }

    public static function getKarak()
    {   
        return DB::table('grupa_pr_naziv')->select('grupa_pr_naziv_id', 'naziv')->orderBy('rbr', 'ASC')->get();
       
    }

    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }
    public static function selectGroups($grupa_pr_id=null){
        $render = '<option value="-1">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

    public static function listGroups(){
        $render = '<ul id="JSListaGrupe" class="fast-add-group" hidden="hidden">';
        foreach(self::getLevelGroups(0) as $row_gr){                
            $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr->grupa_pr_id.'" data-grupa="'.$row_gr->grupa.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</li>';
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr1->grupa_pr_id.'" data-grupa="'.$row_gr1->grupa.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</li>';
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr2->grupa_pr_id.'" data-grupa="'.$row_gr2->grupa.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</li>';
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr3->grupa_pr_id.'" data-grupa="'.$row_gr3->grupa.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</li>';
                    }
                }
            }
        }
        return $render.'<ul>';
    }

    public static function select2Groups($grupa_pr_ids=array()){
        $render = '';
        foreach(self::getLevelGroups(0) as $row_gr){
            if(in_array($row_gr->grupa_pr_id,$grupa_pr_ids)){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if(in_array($row_gr1->grupa_pr_id,$grupa_pr_ids)){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if(in_array($row_gr2->grupa_pr_id,$grupa_pr_ids)){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if(in_array($row_gr3->grupa_pr_id,$grupa_pr_ids)){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

    public static function selectParentGroups($grupa_pr_id, $current_group = null){
        $render = '<option value="0">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    // foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                    //     if($row_gr3->grupa_pr_id == $grupa_pr_id){
                    //         $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                    //     }else{
                    //         $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                    //     }
                    // }
                }
            }
        }
        return $render;
    }

    public static function getPoreskeGrupe($active=true)
    {
        $query=DB::table('tarifna_grupa')->where('tarifna_grupa_id','!=',-1);
        if($active){
            $query = $query->where('active', 1);
        }
        return $query->get();
    }

    
    public static function getPorez($roba_id)
    {
        $tg= DB::table('roba')->where('roba_id',$roba_id)->pluck('tarifna_grupa_id');
        $tg1= DB::table('tarifna_grupa')->where('tarifna_grupa',$tg)->pluck('porez');
        return $tg1;
    }
    public static function getTags($roba_id)
    {
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv');
    }
    
    public static function getFlagCene($all=false)
    {
        if($all){
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }else{
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('selected',1)->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }
    }
    public static function getFlagStatus($all=false)
    {
        if($all){
            return DB::table('narudzbina_status')->select('narudzbina_status_id', 'naziv')->where('narudzbina_status_id', '<>', -1)->orderBy('narudzbina_status_id','ASC')->get();
        }else{
            return DB::table('narudzbina_status')->select('narudzbina_status_id', 'naziv')->where('aktivan',1)->where('narudzbina_status_id', '<>', -1)->orderBy('narudzbina_status_id','ASC')->get();
        }
    }
    public static function getKurirskaSluzba($all=false)
    {
        if($all){
            return DB::table('posta_slanje')->select('posta_slanje_id', 'naziv')->where('posta_slanje_id', '<>', -1)->orderBy('posta_slanje_id','ASC')->get();
        }else{
            return DB::table('posta_slanje')->select('posta_slanje_id', 'naziv')->where('aktivna',1)->where('posta_slanje_id', '<>', -1)->orderBy('posta_slanje_id','ASC')->get();
        }
    }
    public static function getGrupaKarak($id)
    {
        return DB::table('grupa_pr_naziv')->select('grupa_pr_id', 'naziv', 'active', 'grupa_pr_naziv_id', 'rbr')->where(array('grupa_pr_id'=>$id, 'active'=>1))->orderBy('rbr', 'ASC')->get();
    }

    public static function getVrednostKarak($id,$active=null)
    {
        isset($active) ? $cond = array('grupa_pr_naziv_id'=>$id) : $cond = array('grupa_pr_naziv_id'=>$id, 'active'=>1);
        return DB::table('grupa_pr_vrednost')->select('grupa_pr_naziv_id', 'grupa_pr_vrednost_id', 'rbr', 'naziv', 'active')->where($cond)->orderBy('rbr', 'ASC')->get();
    }
    public static function naziv($roba_id) {
        
        $roba_id=DB::table('web_b2b_narudzbina_stavka')->where('roba_id',$roba_id)->pluck('roba_id');
       
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv');;
        
    }

    public static function getGenerisane($roba_id)
    {
        return DB::table('web_roba_karakteristike')->leftJoin('grupa_pr_naziv','web_roba_karakteristike.grupa_pr_naziv_id','=','grupa_pr_naziv.grupa_pr_naziv_id')
        ->select('grupa_pr_naziv.naziv', 'web_roba_karakteristike.grupa_pr_naziv_id', 'web_roba_karakteristike.grupa_pr_vrednost_id', 'web_roba_karakteristike.rbr')
        ->where('roba_id', $roba_id)->orderBy('web_roba_karakteristike.rbr', 'ASC')->get();
    } 

    public static function getTipovi($active=null)
    {
        if($active){
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('tip_artikla_id','<>',-1)->where('tip_artikla_id','<>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv')->where('active',1)->where('tip_artikla_id','<>',-1)->where('tip_artikla_id','<>',0)->orderBy('rbr','asc')->get();
        }  
    }
    public static function tip_naziv($tip_id)
    {
        return DB::table('tip_artikla')->where('tip_artikla_id', $tip_id)->pluck('naziv');
    } 
    public static function getJediniceMere()
    {
        return DB::table('jedinica_mere')->select('jedinica_mere_id','naziv')->where('jedinica_mere_id','<>',-1)->where('jedinica_mere_id','<>',0)->orderBy('jedinica_mere','asc')->get();

    }

    public static function find_proizvodjac($proizvodjac_id, $column)
    {
        return DB::table('proizvodjac')->select($column)->where('proizvodjac_id', $proizvodjac_id)->pluck($column);
    }

    public static function find_tip_artikla($tip_artikla_id, $column)
    {
        return DB::table('tip_artikla')->select($column)->where('tip_artikla_id', $tip_artikla_id)->pluck($column);
    }
    public static function find_jedinica_mere($tip_artikla_id, $column)
    {
        return DB::table('jedinica_mere')->select($column)->where('jedinica_mere_id', $tip_artikla_id)->pluck($column);
    }
    public static function find_tarifna_grupa($tarifna_grupa_id, $column)
    {
        return DB::table('tarifna_grupa')->select($column)->where('tarifna_grupa_id', $tarifna_grupa_id)->pluck($column);
    }

  

    public static function find_flag_cene($roba_flag_cene_id, $column)
    {
        return DB::table('roba_flag_cene')->select($column)->where('roba_flag_cene_id', $roba_flag_cene_id)->pluck($column);
    }
    public static function getStatusArticle($roba_id)
    {
        return DB::select("SELECT roba_flag_cene_id FROM roba r WHERE roba_id=".$roba_id."")[0]->roba_flag_cene_id;
    }
    public static function getDobavljKarak($roba_id,$kind=null,$karakteristika_grupa=null)
    {
        return DB::table('dobavljac_cenovnik_karakteristike')->select('dobavljac_cenovnik_karakteristike_id','karakteristika_naziv','karakteristika_vrednost')->where('roba_id', $roba_id)->orderBy('dobavljac_cenovnik_karakteristike_id','asc')->get();
    }

    public static function getGrupeKarak($roba_id)
    {
        return DB::table('dobavljac_cenovnik_karakteristike')->distinct()->select('karakteristika_grupa','redni_br_grupe')->where('roba_id',$roba_id)->whereNotNull('karakteristika_grupa')->orderBy('redni_br_grupe','asc')->get();
    }

    public static function getGrupeNazivKarak($roba_id,$karakteristika_grupa)
    {
        return DB::table('dobavljac_cenovnik_karakteristike')->select('dobavljac_cenovnik_karakteristike_id','karakteristika_naziv','karakteristika_vrednost')->where(array('roba_id'=>$roba_id,'karakteristika_grupa'=>$karakteristika_grupa))->orderBy('dobavljac_cenovnik_karakteristike_id','asc')->get();
    }

    public static function lager($roba_id){
        $kolicina = 0;
        $kolicina_query = DB::table('lager')->where('roba_id', $roba_id)->select('kolicina');
        if($kolicina_query->count() > 0){
            if($kolicina_query->pluck('kolicina') != 0){
                $kolicina = $kolicina_query->pluck('kolicina');
            }
        }
        return round($kolicina);
    }

    public static function selectCheck($selected = null){
        $string = '';
        if($selected == 1){
            $string .= '<option value="1" selected>DA</option>'.
                       '<option value="0" >NE</option>';
        }else{
            $string .= '<option value="1" >DA</option>'.
                       '<option value="0" selected>NE</option>';
        } 
        echo $string;      
    }

    public static function aktivnaCheck($aktivna = null){
        $string = '';
        if($aktivna == 1){
            $string .= '<option value="1" selected>DA</option>'.
                       '<option value="0" >NE</option>';
        }else{
            $string .= '<option value="1" >DA</option>'.
                       '<option value="0" selected>NE</option>';
        } 
        echo $string;      
    }

    public static function rbrCheck($rbr = null){
        $string = '';
        if($rbr == 1){
            $string .= '<option value="1" selected>DA</option>'.
                       '<option value="0" >NE</option>';
        }else{
            $string .= '<option value="1" >DA</option>'.
                       '<option value="0" selected>NE</option>';
        } 
        echo $string;      
    }

    public static function lagerDob($roba_id){
        $kol_fr = DB::table('dobavljac_cenovnik')->select('kolicina')->where('roba_id',$roba_id)->orderBy('kolicina','desc')->orderBy('web_cena','asc')->first();
        if($kol_fr != null){
            return intval($kol_fr->kolicina);
        }else{
            return '';
        }
    }

    public static function nameOfCharact($web_flag_karakteristike){
        if($web_flag_karakteristike == 2){
            return 'Dob.';
        }
        elseif($web_flag_karakteristike == 1){
            return 'Gen.';
        }
        else{
            return 'HTML';
        }
    }

    public static function statusKarak($roba_id) {
        $data = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_karakteristike');
        $generisane = DB::table('web_roba_karakteristike')->where('roba_id', $roba_id)->count();
        $dobavljaca = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id', $roba_id)->count();
        
        if($data != null || $generisane > 0 || $dobavljaca > 0) {
            return 'DA';
        } else {
            return 'NE';
        }
        
    }

    public static function nivoPristupa($nivo_id){
        return DB::table('ac_group')->where(array('ac_group_id'=>intval($nivo_id)))->pluck('naziv');
    }


    public static function saveLog($report){
        if(Session::has('b2c_admin'.AdminOptions::server())){
            
            $filename = "files/logs/users/administrators.txt";
            $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();        
            $myfile = fopen($filename, "a");
            $line = date("d-m-Y H:i:s").' - Korisnik: '.$user->ime.' '.$user->prezime.' ('.$user->imenik_id.') - '.trim($report)."\r\n";
            fwrite($myfile, $line);
            fclose($myfile);        
        }
    }
    public static function aktivniAdminModuli($parent_id=null){
        if(isset($parent_id)){
            $parent = ' AND am.parrent_ac_module_id = '.$parent_id.' AND tip = 1';
        }else{
            $parent = ' AND tip = 0';
        }
        $is_shop = "";
        if(!AdminOptions::is_shop()){
            $is_shop .= " AND ac_module_id IN (5600,5800,6000,6600,7000,7100,8000)";
        }

        if(Session::get('b2c_admin'.AdminOptions::server()) == 0){
            return DB::select("SELECT ac_module_id, opis FROM ac_module am WHERE aktivan = 1".$parent."".$is_shop." ORDER BY ac_module_id ASC");
        }else{
            return DB::select("SELECT am.ac_module_id, am.opis FROM ac_group_module agm INNER JOIN ac_module am ON agm.ac_module_id = am.ac_module_id WHERE ac_group_id = 0".$parent."".$is_shop." AND alow = 1 AND aktivan = 1 ORDER BY am.ac_module_id ASC");
        }
    }

    public static function artikalFajlovi($roba_id){
        return DB::select("SELECT DISTINCT vrsta_fajla_id, (SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = wf.vrsta_fajla_id) AS ext FROM web_files wf WHERE roba_id = ".$roba_id."");
    }
    public static function find_status_narudzbine($narudzbina_status_id, $column)
    {
        return DB::table('narudzbina_status')->select($column)->where('narudzbina_status_id', $narudzbina_status_id)->pluck($column);
    }

    public static function find_kurirska_sluzba($posta_slanje_id, $column)
    {
        return DB::table('posta_slanje')->select($column)->where('posta_slanje_id', $posta_slanje_id)->pluck($column);
    }

    public static function posta_status($posta_status_id)
    {
        return DB::table('posta_status')->where('posta_status_id', $posta_status_id)->first();
    }

    public static function getExtension($extension_id){
        return DB::select("SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = ".$extension_id."")[0]->ekstenzija;
    }
    public static function getExporti(){
        return DB::table('export')->where('dozvoljen',1)->get();
    }
    public static function encodeForPdf($string){
        return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
        // return $string;
    }

    public static function categoryOpen($active_id,$parents,$curr_id){
        $status = '';
        if($active_id == $curr_id){
            $status = 'active-cat';
        }
        elseif(in_array($curr_id,$parents)){
            $status = 'jstree-open';
        }
        return $status;

    }

    public static function grupaParents($grupa_id,&$parents){

        $parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_id)->pluck('parrent_grupa_pr_id');
        if($parrent_grupa_pr_id > 0){
            array_push($parents,$parrent_grupa_pr_id);
            self::grupaParents($parrent_grupa_pr_id,$parents);
        }
    }
    public static function find_mesto($mesto_id,$column){
        return DB::table('mesto')->where('mesto_id',$mesto_id)->pluck($column);
    }
    public static function find_mesto_narudzbina($mesto_id){
        return DB::table('web_kupac')->where('mesto',$mesto_id)->pluck('mesto');
    }
    public static function narudzbina_kupac_pdf($web_b2c_narudzbina_id){
        $web_kupac=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');

        foreach(DB::table('web_kupac')->where('web_kupac_id',$web_kupac)->get() as $row){
        if($row->flag_vrsta_kupca == 0){
            $name = '<td>Ime i Prezime:</td>
            <td>'.$row->ime.' '.$row->prezime.'</td>';
        }else{
            $name = '<td>Firma i PIB:</td>
            <td>'.$row->naziv.' '.$row->pib.'</td>';
        }

        echo ' 
            <table>
            <tr>
            '.$name.'
            </tr>
            <tr>
            <td>Adresa:</td>
            <td>'.$row->adresa.'</td>
            </tr>
            <tr>
            <td>Mesto:</td>
            <td>'.$row->mesto.'</td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td>'.$row->telefon.'</td>
            </tr>
            <tr>
            <td>Email:</td>
            <td>'.$row->email.'</td>
            </tr>
            </table>';
        }
    } 

    public static function allComments(){
        $comments = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->orderBy('web_b2c_komentar_id', 'DESC')->get();
        return $comments;
    }

    public static function getComment($id) {
        $comment = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', $id)->first();
        return $comment;
    }

    public static function countNewComments(){
        return DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->where('komentar_odobren', 0)->count();
    }

    public static function getBGimg(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('img');
    }

    public static function getlink(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('link');
    }

    public static function getlink2(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('link2');
    }


    public static function grupa_title($grupa_pr_id){
        
        foreach(DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->get() as $row){
                
                return $row->grupa;
            }
        
    }

    public static function seo_description($roba_id){
        
        $product_seo=DB::table('roba')->where('roba_id', $roba_id)->first();
        
        if($product_seo->description!="" ){
            return self::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->description), 0, 130)." ".AdminOptions::company_name();
        }
        elseif($product_seo->web_opis!="" ){
            return self::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->web_opis), 0, 130)." ".AdminOptions::company_name();
        }
        else {
            return self::grupa_title($product_seo->grupa_pr_id)." ".strip_tags($product_seo->naziv_web)." ".AdminOptions::company_name();
        }
    }

    public static function getCurrencyName($id) {
        return DB::table('valuta')->where('valuta_id', $id)->pluck('valuta_naziv');
    }

    public static function getLevelGroupsShopmania($grupa_id){
        return DB::table('shopmania_grupe')->where('parent_id',$grupa_id)->orderBy('shopmania_grupa_id', 'asc')->get();
    }
    public static function selectGroupsShopmania($grupa_id=null){
        $render = '<option value="">Izaberite grupu</option>';
        foreach(self::getLevelGroupsShopmania(0) as $row_gr){
                $render .= '<option value="'.$row_gr->shopmania_grupa_id.'" class="'.(!isset($row_gr->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr->shopmania_grupa_id?'selected':'').'>'.$row_gr->naziv.'  <->  '.(isset($row_gr->grupa_pr_ids)?self::linkedGroups($row_gr->grupa_pr_ids):'').'</option>';
            foreach(self::getLevelGroupsShopmania($row_gr->shopmania_grupa_id) as $row_gr1){
                    $render .= '<option value="'.$row_gr1->shopmania_grupa_id.'" class="'.(!isset($row_gr1->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr1->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->naziv.'  <->  '.(isset($row_gr1->grupa_pr_ids)?self::linkedGroups($row_gr1->grupa_pr_ids):'').'</option>';
                foreach(self::getLevelGroupsShopmania($row_gr1->shopmania_grupa_id) as $row_gr2){
                        $render .= '<option value="'.$row_gr2->shopmania_grupa_id.'" class="'.(!isset($row_gr2->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr2->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->naziv.'  <->  '.(isset($row_gr2->grupa_pr_ids)?self::linkedGroups($row_gr2->grupa_pr_ids):'').'</option>';
                    foreach(self::getLevelGroupsShopmania($row_gr2->shopmania_grupa_id) as $row_gr3){
                            $render .= '<option value="'.$row_gr3->shopmania_grupa_id.'" class="'.(!isset($row_gr3->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr3->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->naziv.'  <->  '.(isset($row_gr3->grupa_pr_ids)?self::linkedGroups($row_gr3->grupa_pr_ids):'').'</option>';
                    }
                }
            }
        }
        return $render;
    }
    public static function linkedGroups($grupa_pr_ids){
        $groups = '';
        foreach(explode(',',$grupa_pr_ids) as $grupa_pr_id){
            $groups .= AdminGroups::find($grupa_pr_id,'grupa').' <> ';
        }
        return substr($groups,0,-4);
    }
    public static function selectParentGroupsShopmania($grupa_id=null){
        $render = '<option value="0">Bez parenta</option>';
        foreach(self::getLevelGroupsShopmania(0) as $row_gr){
                $render .= '<option value="'.$row_gr->shopmania_grupa_id.'" '.($grupa_id==$row_gr->shopmania_grupa_id?'selected':'').'>'.$row_gr->naziv.'</option>';
            foreach(self::getLevelGroupsShopmania($row_gr->shopmania_grupa_id) as $row_gr1){
                    $render .= '<option value="'.$row_gr1->shopmania_grupa_id.'" '.($grupa_id==$row_gr1->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->naziv.'</option>';
                foreach(self::getLevelGroupsShopmania($row_gr1->shopmania_grupa_id) as $row_gr2){
                        $render .= '<option value="'.$row_gr2->shopmania_grupa_id.'" '.($grupa_id==$row_gr2->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->naziv.'</option>';
                }
            }
        }
        return $render;
    }
    public static function selectGroupsSm($shopmania_grupe,$grupa_pr_id=null){
        $render = '<option value="">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':'warn').'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':'warn').'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

    public static function getTemplate($roba_id) {
        $template = DB::table('roba')
            ->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->join('grupa_pr_jezik', 'grupa_pr_jezik.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->select('grupa_pr_jezik.sablon_opis')
            ->where(array('grupa_pr_jezik.jezik_id'=> 1,'roba.roba_id'=> $roba_id))
            ->get();

        return $template;
    }

    public static function custom_encrypt($string){
        return base64_encode($string);
    }

    public static function custom_decrypt($string){
        return base64_decode($string);
    }

    public static function DBAOPConnection(){
        $db_host = Config::get('database.connections')['pgsql']['host'];
        $db_port = Config::get('database.connections')['pgsql']['port'];
        $db_name = 'aop';
        $db_user = Config::get('database.connections')['pgsql']['username'];
        $db_pass = Config::get('database.connections')['pgsql']['password'];

        $conn = pg_connect("host=".$db_host." port=".$db_port." user=".$db_user." password=".$db_pass." password=".$db_pass." dbname=".$db_name."") or die('Connection to DB failed');
        return $conn;
    }
    public static function closeDBConnenction($connection){
        pg_close($connection);
    }

    public static function updateAOPUser($user){
        if(AdminOptions::gnrl_options(3013) == 1 && $user->imenik_id = 1){
            $conn = self::DBAOPConnection();

            $users = pg_fetch_all(pg_query($conn, "SELECT * FROM public.user WHERE email = '".$user->login."' AND id <> ".$user->aop_user_id.""));
            
            if(!isset($users[0])){
              pg_query($conn, "UPDATE public.user SET name = '".$user->ime." ".$user->prezime."', email = '".$user->login."', password = '".$user->password."' WHERE id = ".$user->aop_user_id."");          
            }
            self::closeDBConnenction($conn);

            if(isset($users[0])){
                return false;
            }
        }
        return true;
    }

    public static function trial_date(){
        $user = DB::table('imenik')->where('imenik_id',1)->first();
        if(!is_null($user->datum_otvaranja)){
            $open_date = $user->datum_otvaranja;
            $open_date = DateTime::createFromFormat('Y-m-d H:i:s', $open_date);
            $now_date = new DateTime();
            $limit_date = 14*86400;

            $difference_date = $limit_date - ($now_date->getTimestamp() - $open_date->getTimestamp());
        
            if($difference_date > 0){
                $result_array = self::seconds_to_time($difference_date);
                $result_array['timestamp'] = $open_date->getTimestamp();
                $result_array['difference_timestamp'] = $difference_date;
                return (object) $result_array;
            }
        }
        return false;
    }

    public static function seconds_to_time($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        $diff = $dtF->diff($dtT);
        return array(
            'days' => $diff->format('%a')
            );
    }

    public static function zip_images(){
        $path = 'images/products/big/';
        $files = array_diff(scandir($path), array('.', '..'));
        $zipname = 'files/exporti/backup/backup_images.zip';
        if(File::exists($zipname)){
            File::delete($zipname);
        }
        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE);
        foreach($files as $file){
            $zip->addFile($path.$file);
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=backup_images.zip');
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);

        File::delete($zipname);
    }
    public static function extract_images(){
        $path = 'files/backup_images.zip';
        $destination = './';
        $zip = new ZipArchive;
        if($zip->open($path) === TRUE) {
            $zip->extractTo($destination);
            $zip->close();
            return true;
        }
        return false;
    }

    public static function proizvodjac_id($naziv){
        if(trim($naziv) != ''){
            $proizvodjac = DB::table('proizvodjac')->where('naziv',$naziv)->first();
            if(!is_null($proizvodjac)){
                return $proizvodjac->proizvodjac_id;
            }
            $next_id = DB::select("SELECT MAX(proizvodjac_id) AS max FROM proizvodjac")[0]->max + 1;
            DB::table('proizvodjac')->insert(array(
                'proizvodjac_id' => $next_id,
                'naziv' => trim($naziv)
                ));
            DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac), FALSE)");
            return $next_id;
        }
        return -1;
    }
    public static function jedinica_mere_id($naziv){
        if(trim($naziv) != ''){
            $jedinica_mere = DB::table('jedinica_mere')->where('naziv',$naziv)->first();
            if(!is_null($jedinica_mere)){
                return $jedinica_mere->jedinica_mere_id;
            }
            $jedinica_mere_id_new = DB::select("SELECT MAX(jedinica_mere_id) AS max FROM jedinica_mere")[0]->max + 1;
            DB::table('jedinica_mere')->insert(array(
                'jedinica_mere_id' => $jedinica_mere_id_new,
                'naziv' => trim($naziv)
                ));
            return $jedinica_mere_id_new;
        }
        return 1;
    }
    public static function roba_flag_cene_id($naziv){
        if(trim($naziv) != ''){
            $roba_flag_cene = DB::table('roba_flag_cene')->where('naziv',$naziv)->first();
            if(!is_null($roba_flag_cene)){
                return $roba_flag_cene->roba_flag_cene_id;
            }
            $roba_flag_cene_id_new = DB::select("SELECT MAX(roba_flag_cene_id) AS max FROM roba_flag_cene")[0]->max + 1;
            DB::table('roba_flag_cene')->insert(array(
                'roba_flag_cene_id' => $roba_flag_cene_id_new,
                'naziv' => trim($naziv),
                'selected' => 1
                ));
            return $roba_flag_cene_id_new;
        }
        return 1;
    }
    public static function tip_artikla_id($naziv){
        if(trim($naziv) != ''){
            $tip_artikla = DB::table('tip_artikla')->where('naziv',$naziv)->first();
            if(!is_null($tip_artikla)){
                return $tip_artikla->tip_artikla_id;
            }

            DB::table('tip_artikla')->insert(array(
                'naziv' => trim($naziv),
                'active' => 1,
                'rbr' => DB::select("SELECT MAX(rbr) AS max FROM tip_artikla")[0]->max + 1
                ));
            return DB::select("SELECT currval('tip_artikla_tip_artikla_id_seq') as new_id")[0]->new_id;
        }
        return -1;
    }
    
    public static function xml_node($xml,$name,$value,$parent){
        $xmltag = $xml->createElement($name);
        $xmltagText = $xml->createTextNode($value);
        $xmltag->appendChild($xmltagText);
        $parent->appendChild($xmltag);
    }

    public static function upload_directory_opis(){
        $dir    = "./images/upload_opis";
        //return $dir;
        $files = scandir($dir);
        $slike="<ul>";
        $br=0;
           foreach ($files as $row) {
         if($br>1){
         
               $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='#' data-url='/images/upload_opis/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
               }
          $br+=1;
            }
        $slike.="</ul>";
        return $slike;
    }

    public static function footer_section_name($futer_sekcija_id){
        $futer_sekcija_jezik = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id')))->first();
        if(!is_null($futer_sekcija_jezik)){
            return $futer_sekcija_jezik->naslov;
        }
        return '';
    }
    public static function broj_cerki($web_b2c_seo_id){     
        return DB::table('web_b2c_seo')->where(array('parrent_id'=>$web_b2c_seo_id,'header_menu'=>1))->count();
    }
}
