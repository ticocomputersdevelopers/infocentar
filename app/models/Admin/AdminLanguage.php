<?php
use \Statickidz\GoogleTranslate;

class AdminLanguage {

    //prevodi reci za dati jezik
	public static function trans($string,$lang=null){
        if($lang==null){
            $lang = Language::lang();
        }

        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id');

        if($izabrani_jezik_id == $jezik_id){
        	return $string;
        }else{
        	$query =DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$string))->where('reci','!=','')->whereNotNull('reci');
        	if($query->count() > 0){
        		return $query->first()->reci;
        	}else{
        		return $string;
        		$str_arr = explode(' ',$string);
        		$trans_arr = array();
        		$not_translate = false;
        		foreach($str_arr as $str){
        			$query_str = DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$str))->where('reci','!=','')->whereNotNull('reci');
		        	if($query_str->count() > 0){
		        		$trans_arr[] = $query_str->first()->reci;
		        	}else{
		        		$not_translate = true;
		        		$trans_arr[] = $str;
		        	}
        		}
        		return implode(' ',$trans_arr);
        	}
        }
	}
	
	public static function slug_trans($string,$lang=null){
        if($lang==null){
            $lang = self::lang();
        }
        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1));
        $izabrani_jezik_id = $izabrani_jezik->pluck('jezik_id');
        $izabrani_jezik_kod = $izabrani_jezik->pluck('kod');
        if($izabrani_jezik_id != $jezik_id){
        	$query =DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$string))->where('reci','!=','')->whereNotNull('reci');
        	if(!is_null($query->first())){
        		$string = $query->first()->reci;
        	}
        }
        return $string;
	}

	public static function multi(){
		return DB::table('jezik')->where('aktivan',1)->count() > 1;
	}
	
	public static function lang(){
		if(Session::has('lang')){
			return Session::get('lang');
		}
		return DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
	}

	public static function save($string,$string_old=null,$is_slig=false)
	{
		$languages = DB::table('jezik')->where('izabrani','!=',1)->where('aktivan',1)->get();
		if(count($languages) > 0){
			$default = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->first();
			$trans = new GoogleTranslate();
			$data = array();
			foreach($languages as $language){
				$data = array(
					'izabrani_jezik_id' => $default->jezik_id,
					'izabrani_jezik_reci' => $string,
					'jezik_id' => $language->jezik_id,
					'reci' => $trans->translate($default->kod, $language->kod, $string)
					);

				$prevodilac = null;
				if(!is_null($string_old)){
					$prevodilac = DB::table('prevodilac')->where('jezik_id',$language->jezik_id)->where('izabrani_jezik_reci',$string_old)->first();
				}
				if($prevodilac){
					DB::table('prevodilac')->where('prevodilac_id',$prevodilac->prevodilac_id)->update($data);
				}else{
					DB::table('prevodilac')->insert($data);
				}

				if($is_slig){
					$string_slug = AdminOptions::url_convert($string);
					$str = str_replace('-',' ',$string_slug);
					$str_slug = $trans->translate($default->kod, $language->kod, $str);

					$data = array(
						'izabrani_jezik_id' => $default->jezik_id,
						'izabrani_jezik_reci' => $string_slug,
						'jezik_id' => $language->jezik_id,
						'reci' => str_replace(' ','-',$str_slug)
						);

					$prevodilac = null;
					if(!is_null($string_old)){
						$prevodilac = DB::table('prevodilac')->where('jezik_id',$language->jezik_id)->where('izabrani_jezik_reci',AdminOptions::url_convert($string_old))->first();
					}
					if($prevodilac){
						DB::table('prevodilac')->where('prevodilac_id',$prevodilac->prevodilac_id)->update($data);
					}else{
						DB::table('prevodilac')->insert($data);
					}
				}
				
			}
		}

	}


}
