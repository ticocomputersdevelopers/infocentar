<?php
namespace Export;
use AdminOptions;
use AdminCommon;
use DB;
use AdminGroups;

class Support {

	public static function encodeTo1250($string){
		return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
	}
	public static function encodeToUtf8($string){
		return iconv("WINDOWS-1250", "UTF-8//TRANSLIT//IGNORE",$string);
	}
	public static function string_format($string){
		// return preg_replace('/[^A-Za-z0-9 !%&()=*,.+-_@:;<>\'čćžšđČĆŽŠĐ\-]/', '',iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8//TRANSLIT//IGNORE",$string));
		return $string;
	}
	public static function xml_node($xml,$name,$value,$parent){
		$xmltag = $xml->createElement($name);
	    $xmltagText = $xml->createTextNode($value);
	    $xmltag->appendChild($xmltagText);
	    $parent->appendChild($xmltag);
	}
	public static function xml_node_cdata($xml,$name,$value,$parent){
		$xmltag = $xml->createElement($name);
	    $xmltagText = $xml->createCDATASection($value);
	    $xmltag->appendChild($xmltagText);
	    $parent->appendChild($xmltag);
	}
	public static function slike($roba_id){
        return array_map('current',DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get());	
	}
	public static function slanje($roba_id){
        $id= DB::table('web_b2c_narudzbina_stavka')->where('roba_id',$roba_id)->pluck('web_b2c_narudzbina_id');	
        return array_map('current', DB::table('web_b2c_narudzbina')->select('posta_slanje_id')->where('web_b2c_narudzbina_id',$id)->get());
        
	}
	public static function product_link($roba_id){
		return AdminOptions::base_url().AdminOptions::url_convert(AdminCommon::seo_title($roba_id));
	}
	
	public static function product_link_ceners($roba_id){
		return AdminOptions::base_url().'artikal/'.AdminOptions::url_convert(AdminCommon::seo_title($roba_id));
	}

	public static function major_image($roba_id){
		$image = 'images/no-image.jpg';
		$row = DB::table('web_slika')->where('roba_id',$roba_id)->orderBy('akcija','desc')->first();
		if(isset($row)){
			$image = $row->putanja;
		}
		return AdminOptions::base_url().$image;
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return $karakteristike;
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '';
			foreach($generisane as $row){
				$generisane_karakteristike .= $row->naziv.': '.$row->vrednost.', ';
			}
			return substr($generisane_karakteristike,0,-2);
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$dobavljac_karakteristike = '';
			foreach($dobavljac as $row){
				$dobavljac_karakteristike .= $row->karakteristika_naziv.': '.$row->karakteristika_vrednost.', ';
			}
			return substr($dobavljac_karakteristike,0,-2);
		}		
	}

	public static function troskovi_isporuke($tezina){
		$response = 'besplatna isporuka';
		if($tezina > 0){			
			foreach(DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','asc')->get() as $row){
				if($tezina < $row->tezina_gr){
					$response = $row->cena;
					break;
				}
			}
			if($response=='besplatna isporuka'){
				$last = DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->first();
				if(isset($last)){
					$response = $last->cena;
				}
			}
		}
		return $response;
	}

	public static function export_our_groups($export_id){
		$shpm_gr_str = '';
		foreach (DB::table('export_grupe')->where('export_id',$export_id)->get() as $row) {
			if(isset($row->grupa_pr_ids)){
				$shpm_gr_str .= trim($row->grupa_pr_ids) . ",";
			}
		}
		return explode(',',$shpm_gr_str);
	}

    public static function getLevelExportGroups($export_id,$grupa_id=null){
    	if(is_null($grupa_id)){
        	return DB::table('export_grupe')->where('export_id',$export_id)->whereNull('parent_id')->orderBy('grupa_id', 'asc')->get();
    	}else{
	        return DB::table('export_grupe')->where('export_id',$export_id)->where('parent_id',$grupa_id)->orderBy('grupa_id', 'asc')->get();
	    }
    }

    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }

    public static function linkedGroups($grupa_pr_ids){
        $groups = '';
        foreach(explode(',',$grupa_pr_ids) as $grupa_pr_id){
            $groups .= AdminGroups::find($grupa_pr_id,'grupa').' <> ';
        }
        return substr($groups,0,-4);
    }
    public static function selectGroups($export_id,$grupa_id=null){
        $render = '<option value="">Izaberite grupu</option>';
        foreach(self::getLevelExportGroups($export_id) as $row_gr){
                $render .= '<option value="'.$row_gr->grupa_id.'" class="'.(!isset($row_gr->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr->grupa_id?'selected':'').'>'.$row_gr->naziv.'  <->  '.(isset($row_gr->grupa_pr_ids)?self::linkedGroups($row_gr->grupa_pr_ids):'').'</option>';
            foreach(self::getLevelExportGroups($export_id,$row_gr->grupa_id) as $row_gr1){
                    $render .= '<option value="'.$row_gr1->grupa_id.'" class="'.(!isset($row_gr1->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr1->grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->naziv.'  <->  '.(isset($row_gr1->grupa_pr_ids)?self::linkedGroups($row_gr1->grupa_pr_ids):'').'</option>';
                foreach(self::getLevelExportGroups($export_id,$row_gr1->grupa_id) as $row_gr2){
                        $render .= '<option value="'.$row_gr2->grupa_id.'" class="'.(!isset($row_gr2->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr2->grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->naziv.'  <->  '.(isset($row_gr2->grupa_pr_ids)?self::linkedGroups($row_gr2->grupa_pr_ids):'').'</option>';
                    foreach(self::getLevelExportGroups($export_id,$row_gr2->grupa_id) as $row_gr3){
                            $render .= '<option value="'.$row_gr3->grupa_id.'" class="'.(!isset($row_gr3->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr3->grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->naziv.'  <->  '.(isset($row_gr3->grupa_pr_ids)?self::linkedGroups($row_gr3->grupa_pr_ids):'').'</option>';
                    }
                }
            }
        }
        return $render;
    }

    public static function selectGroupsSm($export_id,$export_grupe,$grupa_pr_id=null){
        $render = '<option value="">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$export_grupe)?'':'warn').'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$export_grupe)?'':'warn').'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$export_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$export_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$export_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$export_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$export_grupe)?'':'warn').'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$export_grupe)?'':'warn').'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$export_grupe)?'':' (NIJE POVEZANO)').'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

	public static function export_grupe($export_id){
        $results = array();
        foreach(DB::table('export_grupe')->where('export_id',$export_id)->get() as $row){
        	if(isset($row->grupa_pr_ids) && $row->grupa_pr_ids != ''){
        		foreach(explode(',',$row->grupa_pr_ids) as $grupa_pr_id){
        			$results[$grupa_pr_id] = (object) array('grupa_id' => $row->grupa_id, 'naziv' => $row->naziv);
        		}
        	}
        }
        return $results;
	}

}
