<?php
namespace Export;

use DB;
use AdminOptions;
use AdminCommon;
use AdminSupport;
use AdminOsobine;

class Shopify {

	public static function execute($export_id,$kind,$config){

		$export_products = DB::select("SELECT roba_id, sku, web_cena, REPLACE(REPLACE(naziv_web,';',' '),',',' ') AS naziv_web,(SELECT REPLACE(REPLACE(naziv,';',' '),',',' ') FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, web_flag_karakteristike, web_karakteristike, REPLACE(REPLACE(web_opis,';',' '),',',' ') AS web_opis, flag_prikazi_u_cenovniku, tarifna_grupa_id, akcija_flag_primeni, akcijska_cena, barkod, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='csv'){
			self::csv_exe($export_id,$export_products,$config);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function csv_exe($export_id,$products,$config){

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="shopify-export.csv"');
		$max_options = self::get_max_options(isset($config['characteristics'])?true:false,$products);
		$key_blank = 2*$max_options;

		$header = array(
			0 => isset($config['url'])?'Handle':'null',
			1 => isset($config['name'])?'Title':'null',
			2 => isset($config['description'])?'Body (HTML)':'null',
			3 => isset($config['manufacturer'])?'Vendor':'null',
			4 => isset($config['type'])?'Type':'null',
			5 => isset($config['tags'])?'Tags':'null',
			6 => isset($config['published'])?'Published':'null',
			$key_blank+7 => 'Variant SKU',
			$key_blank+8 => isset($config['weight'])?'Variant Grams':'null',
			$key_blank+9 => isset($config['stock'])?'Variant Inventory Tracker':'null',
			$key_blank+10 => isset($config['quantity'])?'Variant Inventory Quantity':'null',
			$key_blank+11 => isset($config['stock'])?'Variant Inventory Policy':'null',
			$key_blank+12 => isset($config['is_full'])?'Variant Fulfillment Service':'null',
			$key_blank+13 => isset($config['price'])?'Variant Price':'null',
			$key_blank+14 => isset($config['action'])?'Variant Compare at Price':'null',
			$key_blank+15 => isset($config['shiping'])?'Variant Requires Shipping':'null',
			$key_blank+16 => isset($config['tax'])?'Variant Taxable':'null',
			$key_blank+17 => isset($config['barcode'])?'Variant Barcode':'null',
			$key_blank+18 => isset($config['image'])?'Image Src':'null',
			$key_blank+19 => isset($config['image'])?'Image Position':'null',
			$key_blank+20 => isset($config['image'])?'Image Alt Text':'null',
			$key_blank+21 => isset($config['is_full'])?'Gift Card':'null',
			$key_blank+22 => isset($config['additional_images'])?'Variant Image':'null',
			$key_blank+23 => isset($config['is_full'])?'Variant Weight Unit':'null',
			$key_blank+24 => isset($config['tax'])?'Variant Tax Code':'null',
			$key_blank+25 => isset($config['title_meta'])?'SEO Title':'null',
			$key_blank+26 => isset($config['description_meta'])?'SEO Description':'null',
			$key_blank+27 => isset($config['is_full'])?'Google Shopping / Google Product Category':'null',
			$key_blank+28 => isset($config['is_full'])?'Google Shopping / Gende':'null',
			$key_blank+29 => isset($config['is_full'])?'Google Shopping / Age Group':'null',
			$key_blank+30 => isset($config['is_full'])?'Google Shopping / MPN':'null',
			$key_blank+31 => isset($config['is_full'])?'Google Shopping / Adwords Grouping':'null'
			);

		$head_arr = array_diff($header,array('null'));
		if(isset($config['characteristics'])){
			$head_arr_merged = $head_arr+self::max_variants($max_options);
		}else{
			$head_arr_merged = $head_arr;
		}
		ksort($head_arr_merged);
		$products_CSV[0] = $head_arr_merged;

		foreach($products as $article){
			$meta_title = AdminCommon::seo_title($article->roba_id);
			$base_image = self::base_image($article->roba_id);
			$add_image = self::add_image($article->roba_id);

			$row = array(
			0 => isset($config['url'])?AdminOptions::url_convert($meta_title):'null',
			1 => isset($config['name'])?$article->naziv_web:'null',
			2 => isset($config['description'])?(isset($article->web_opis)?"<p>".$article->web_opis."</p>":"").self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike):'null',
			3 => isset($config['manufacturer'])?$article->proizvodjac:'null',
			4 => isset($config['type'])?'':'null',
			5 => isset($config['tags'])?'':'null',
			6 => isset($config['published'])?$article->flag_prikazi_u_cenovniku:'null',
			$key_blank+7 => $article->roba_id,
			$key_blank+8 => isset($config['weight'])?'500 g':'null',
			$key_blank+9 => isset($config['stock'])?'':'null',
			$key_blank+10 => isset($config['quantity'])?(is_null($article->kolicina)?'0':$article->kolicina):'null',
			$key_blank+11 => isset($config['stock'])?AdminOptions::vodjenje_lagera():'null',
			$key_blank+12 => isset($config['is_full'])?'':'null',
			$key_blank+13 => isset($config['price'])?$article->web_cena:'null',
			$key_blank+14 => isset($config['action'])?($article->akcijska_cena>0 && $article->akcija_flag_primeni==1?$article->akcijska_cena:$article->web_cena):'null',
			$key_blank+15 => isset($config['shiping'])?'':'null',
			$key_blank+16 => isset($config['tax'])?'1':'null',
			$key_blank+17 => isset($config['barcode'])?isset($article->barkod)?$article->barkod:'':'null',
			$key_blank+18 => isset($config['image'])?$base_image:'null',
			$key_blank+19 => isset($config['image'])?self::images($article->roba_id):'null',
			$key_blank+20 => isset($config['image'])?($base_image!=''?$meta_title:''):'null',
			$key_blank+21 => isset($config['is_full'])?'':'null',
			$key_blank+22 => isset($config['additional_images'])?$add_image:'null',
			$key_blank+23 => isset($config['is_full'])?'':'null',
			$key_blank+24 => isset($config['tax'])?AdminSupport::find_tarifna_grupa($article->tarifna_grupa_id,'porez'):'null',
			$key_blank+25 => isset($config['title_meta'])?$meta_title:'null',
			$key_blank+26 => isset($config['description_meta'])?strtolower(AdminSupport::seo_description($article->roba_id)):'null',
			$key_blank+27 => isset($config['is_full'])?'':'null',
			$key_blank+28 => isset($config['is_full'])?'':'null',
			$key_blank+29 => isset($config['is_full'])?'':'null',
			$key_blank+30 => isset($config['is_full'])?'':'null',
			$key_blank+31 => isset($config['is_full'])?'':'null'
			);

			$row_arr = array_diff($row,array('null'));
			if(isset($config['characteristics'])){				
				$options = self::variant_arrays($article->roba_id,$max_options);
				$row_arr_merged = $row_arr+$options;
			}else{
				$row_arr_merged = $row_arr;
			}
			ksort($row_arr_merged);
			$products_CSV[] = $row_arr_merged;
		}
		$fp = fopen('php://output', 'w');
		foreach ($products_CSV as $line) {
		    fputcsv($fp, $line, ';');
		}
		fclose($fp);	
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return '<p>'.$karakteristike.'</p>';
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '<table>';
			foreach($generisane as $row){
				$generisane_karakteristike .= '<tr><td>'.$row->naziv.'</td><td>'.$row->vrednost.'</td></tr>';
			}
			return $generisane_karakteristike.'</table>';
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$dobavljac_karakteristike = '<table>';
			foreach($dobavljac as $row){
				$dobavljac_karakteristike .= '<tr><td>'.$row->karakteristika_naziv.'</td><td>'.$row->karakteristika_vrednost.'</td></tr>';
			}
			return $dobavljac_karakteristike.'</table>';
		}		
	}

	public static function base_image($roba_id){
        $obj_slika = DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->orderBy('akcija','desc')->first();
	    if(isset($obj_slika)){		    	
		    return AdminOptions::base_url().$obj_slika->putanja;
	    }else{
	    	return '';
	    }
	}

	public static function images($roba_id){
        $slike = DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get();
	    $sl_str = '';
	    foreach($slike as $sl){
	    	$sl_str .= AdminOptions::base_url().$sl->putanja.'|';
	    }
	    if($sl_str == ''){
	    	return $sl_str;
	    }else{
	    	return substr($sl_str,0,-1);
	    }
	}

	public static function add_image($roba_id){
    	$image_addon = DB::table('web_files')->where(array('roba_id'=>$roba_id,'vrsta_fajla_id'=>6))->orderBy('web_file_id','asc')->first();
    	if(isset($image_addon)){
    		return '/'.$image_addon->putanja;
    	}else{
    		return '';
    	}
    }

	public static function max_variants($max){
		$arrays = array();
		if($max > 0){		
			$mod_arr = range(0,$max);
			$nk = 6;
			foreach($mod_arr as $k => $val){
				$nk++;
				$arrays[$nk] = 'Option '.strval($k+1).' Name';
				$nk++;
				$arrays[$nk] = 'Option '.strval($k+1).' Value';
			}
		}
		return $arrays;
	}

	public static function variant_arrays($roba_id,$max){
		$obj = DB::table('osobina_roba')->where(array('aktivna'=>1,'roba_id'=>$roba_id));
		$count = $obj->count();
		$rows = $obj->get();

		$arrays = array();
		$nk = 6;
		foreach($rows as $row){
			$nk++;
			$arrays[$nk] = AdminOsobine::findProperty($row->osobina_naziv_id,'naziv');
			$nk++;
			$arrays[$nk] = AdminOsobine::findPropertyValue($row->osobina_vrednost_id,'vrednost');
		}
		if($max-$count > 0){		
			$mod_arr = range(0,$max-$count);
			foreach($mod_arr as $k => $val){
				$nk++;
				$arrays[$nk] = '';
				$nk++;
				$arrays[$nk] = '';
			}
		}
		return $arrays;
	}

	public static function iteration($config_check,$roba_id){
		$check = 0;
		if(AdminOsobine::check_osobine($roba_id) && $config_check){			
			$count = DB::table('osobina_roba')->where(array('aktivna'=>1,'roba_id'=>$roba_id))->count();
			if($count > 0){
				$check = $count;
			}
		}
		return $check;
	}
	public static function get_max_options($config_check,$articles){
		$max = 0;
		foreach($articles as $row){
			$iteration = self::iteration($config_check,$row->roba_id);
			if($max < $iteration){
				$max = $iteration;
			}
		}
		return $max;
	}
}