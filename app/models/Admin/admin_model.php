<?php

class Admin_model {

    public static function check_admin($ac_module_ids=null){
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        if($imenik_id == 0){
            return true;
        }
        if($ac_module_ids == 0){
            return false;
        }       
        $ac_group_id = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
        $count = DB::table('ac_group_module')->where(array('ac_group_id'=>intval($ac_group_id),'alow'=>1))->whereIn('ac_module_id',$ac_module_ids)->count();

        if($count > 0){
            return true;
        }else{
            return false;
        }
    }
    public static function get_admin(){
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();
        return $user->ime.' '.$user->prezime;
    }

 
    public static function narudzbina_kupac($web_b2c_narudzbina_id){
        
        // Dodao sam $row->naziv ukoliko je firma da ispise ime firme
        $kupac_id=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
        foreach(DB::table('web_kupac')->where('web_kupac_id',$kupac_id)->get() as $row){
            return $row->ime." ".$row->prezime." ".$row->naziv;
        }
        
        
    }
    
    public static function css_prev(){
        $css="<style>";
        foreach(DB::table('css_class')->get() as $row){
          $css.=$row->naziv."{";
          foreach(DB::table('css_class_atribute')->where('css_class_id',$row->css_class_id)->get() as $row2){
            $css.=$row2->naziv_promenjive.":".$row2->vrednost_active.";";
          }
          $css.="}";
        }
        $css.="</style>";
        
        return $css;
    }
    
    public static function css_value($css_class_atribute_id){
        return DB::table('css_class_atribute')->where('css_class_atribute_id',$css_class_atribute_id)->pluck('vrednost_active');
    }

    public static function css_logo($css_class_atribute_id){
           $link=0;
        
           $niz=array();
            $vrednost = DB::table('css_class_atribute')->where('css_class_atribute_id',$css_class_atribute_id)->pluck('vrednost_active');

           $string = strrpos($vrednost, 'px');
          
            $link=substr($vrednost,0,$string);
            return  $link;

    }
    
    public static function logo(){
        $logo=DB::table('preduzece')->where('preduzece_id',1)->pluck('logo');
        if($logo==null or $logo==''){
            
            return AdminOptions::base_url()."images/logo.png";
            
        }
        else {
            return AdminOptions::base_url()."".$logo;
        }
    }

    public static function upload_directory(){
        $dir    = "./images/upload";
        //return $dir;
        $files = scandir($dir);
            $slike="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1){
       
           $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='javascript:void(0)' title='Obriši' data-url='/images/upload/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
           }
           
        
            $br+=1;
        }
        $slike.="</ul>";
        return $slike;
    }

    public static function shopmania_files(){
        
         $dir    = "./cenovnici/shopmania";
        //return $dir;
        $files = scandir($dir);
            $cenovnici="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1){
       
           $cenovnici.="<li><a class='uploaded-link' href='".AdminOptions::base_url()."cenovnici/shopmania/".$row."'>".AdminOptions::base_url()."cenovnici/shopmania/".$row."</a>";
           }
           
        
            $br+=1;
        }
        $cenovnici.="</ul>";
        return $cenovnici;
        
    }
    
    public static function eponuda_files(){
        
         $dir    = "./cenovnici/eponuda";
        //return $dir;
        $files = scandir($dir);
            $cenovnici="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1){
       
           $cenovnici.="<li><a class='uploaded-link' href='".AdminOptions::base_url()."cenovnici/eponuda/".$row."'>".AdminOptions::base_url()."cenovnici/eponuda/".$row."</a>";
           }
           
        
            $br+=1;
        }
        $cenovnici.="</ul>";
        return $cenovnici;
        
    }

}