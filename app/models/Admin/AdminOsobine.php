<?php

class AdminOsobine {

	public static function getAll(){
		$all = DB::table('osobina_naziv')->orderBy('osobina_naziv_id')->get();
		return $all;
	}

	public static function getActiveProperties($osobina_naziv_id){

		return DB::table('osobina_naziv')->where('aktivna', 1)->where('osobina_naziv_id', $osobina_naziv_id)->orderBy('osobina_naziv_id')->get();
	}
	public static function getNaziv(){

		return DB::table('osobina_naziv')->where('aktivna', 1)->orderBy('osobina_naziv_id')->get();
	}

	public static function getProperty($id){
		return DB::table('osobina_naziv')->where('osobina_naziv_id', $id)->get();
	}

	public static function getPropertyValues($osobina_naziv_id){
		return DB::table('osobina_vrednost')->where('osobina_naziv_id', $osobina_naziv_id)->orderBy('rbr')->get();
	}

	public static function getPropertiesProduct($roba_id){
		return DB::table('osobina_roba')->where('roba_id', $roba_id)->orderBy('osobina_naziv_id')->orderBy('rbr')->get();
	}

	public static function findProperty($osobina_naziv_id, $column){
		return DB::table('osobina_naziv')->where('osobina_naziv_id', $osobina_naziv_id)->pluck($column);
	}

	public static function findPropertyValue($osobina_vrednost_id, $column){

		return DB::table('osobina_vrednost')->where('osobina_vrednost_id', $osobina_vrednost_id)->pluck($column);
	}
	public static function getOsobineStr($roba_id,$osobina_vrednost_ids){
		$str = '';
        if($osobina_vrednost_ids != null){
        	$vrednosti_ids = array();
        	foreach(DB::table('osobina_vrednost')->whereIn('osobina_vrednost_id',explode('-',$osobina_vrednost_ids))->whereIn('osobina_naziv_id',array_map('current',DB::table('osobina_naziv')->select('osobina_naziv_id')->get()))->get() as $row){
        		$vrednosti_ids[] = $row->osobina_vrednost_id;
			}
			if(count($vrednosti_ids)>0){			
	    		$str .= '(';
	            foreach($vrednosti_ids as $osobina_vrednost_id){
	    			$str .= self::findProperty(self::findPropertyValue($osobina_vrednost_id,'osobina_naziv_id'),'naziv').': '. self::findPropertyValue($osobina_vrednost_id,'vrednost').', ';
	            }
	            $str = substr($str,0,-2);
				$str .= ')';
			}
        }
        return $str;
	}
	public static function check_osobine($roba_id){
		$check = false;
		if(DB::table('roba')->where('roba_id',$roba_id)->pluck('osobine')==1){
			if(DB::table('osobina_roba')->where(array('roba_id'=>$roba_id,'aktivna'=>1))->count() > 0){
				$check = true;
			}
		}
		return $check;
	}
	 public static function upload_directory_opis(){
        $dir    = "./images/upload_opis";
        //return $dir;
        $files = scandir($dir);
      $slike="<ul>";
    $br=0;
       foreach ($files as $row) {
     if($br>1){
     
           $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='#' data-url='/images/upload_opis/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
           }
       
    
      $br+=1;
        }
    $slike.="</ul>";
    return $slike;
    }
	
}