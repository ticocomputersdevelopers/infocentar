<?php

class AdminAnalitika {

	public static function getObradjene() {
		$realizovane = DB::table('web_b2c_narudzbina')->where('realizovano', 1)->where('web_b2c_narudzbina_id', '!=', '-1')->where('stornirano', '!=', 1)->count();
		return $realizovane;
	}

	public static function getUkupnoPorudzbina() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->count();
		return $ukupno;
	}

	public static function getNaCekanju() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}

	public static function getNew() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}
	public static function getStornirane() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('stornirano', '=', 1)->count();
		return $ukupno;
	}
	public static function getRealizovane() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}
	public static function getPrihvacene() {
		$ukupno = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}
	public static function ukupnoArtikala() {
		$ukupno = DB::table('roba')->where('roba_id', '!=', -1)->where('flag_aktivan', 1)->count();
		return $ukupno;
	}

	public static function ukupnoKorisnika() {
		$korisnici = DB::table('web_kupac')->where('web_kupac_id', '!=', -1)->count();
		return $korisnici;
	}

	public static function ukupanPrihod() {
		
		$query = DB::table('web_b2c_narudzbina')->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('web_b2c_narudzbina.realizovano', 1)->where('web_b2c_narudzbina.stornirano', 0)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;
	}

	public static function jucerasnjiPrihod() {
		$juce = date('Y-m-d', strtotime(date('Y-m-d')." -1 day"));
		
		$query = DB::table('web_b2c_narudzbina')->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('web_b2c_narudzbina.realizovano', 1)->where('web_b2c_narudzbina.datum_dokumenta', $juce)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;
	}

	public static function mesecniPrihod() {
		$mesec = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
		
		
		$query = DB::table('web_b2c_narudzbina')->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('web_b2c_narudzbina.realizovano', 1)->whereBetween('web_b2c_narudzbina.datum_dokumenta', array($mesec, date('Y-m-d')))->select('web_b2c_narudzbina_stavka.jm_cena', 'web_b2c_narudzbina_stavka.kolicina')->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;

	}
	public static function razlika() {
	
		$mesec = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
		
		
		$query = DB::table('web_b2c_narudzbina')->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('web_b2c_narudzbina.realizovano', 1)->where('racunska_cena_nc', '>', 0)->whereBetween('web_b2c_narudzbina.datum_dokumenta', array($mesec, date('Y-m-d')))->select('web_b2c_narudzbina_stavka.jm_cena', 'web_b2c_narudzbina_stavka.racunska_cena_nc')->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena - $q->racunska_cena_nc;
		}
		return $ukupno;

	}
	public static function ukupanRUC() {
		
		$query = DB::table('web_b2c_narudzbina')->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('realizovano', '=', 1)->where('racunska_cena_nc', '>', 0)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->kolicina * ($q->jm_cena)/1.2 - $q->racunska_cena_nc*$q->kolicina;
		}
		return $ukupno;
	}

	public static function orderYears($datum_od=null, $datum_do=null) {
		$min_query = DB::table('web_b2c_narudzbina');
		if(!is_null($datum_od) && !is_null($datum_do) && $datum_od!='' && $datum_do!=''){
			$min_query = $min_query->whereBetween('datum_dokumenta',array($datum_od, $datum_do));
		}
		$min_narudzbina_datum=$min_query->orderBy('datum_dokumenta','asc')->pluck('datum_dokumenta');

		$max_query = DB::table('web_b2c_narudzbina');
		if(!is_null($datum_od) && !is_null($datum_do) && $datum_od!='' && $datum_do!=''){
			$max_query = $max_query->whereBetween('datum_dokumenta',array($datum_od, $datum_do));
		}
		$max_narudzbina_datum=$max_query->orderBy('datum_dokumenta','desc')->pluck('datum_dokumenta');

		if(is_null($min_narudzbina_datum) || is_null($max_narudzbina_datum) ){
			return array();
		}

		$min = date("Y", strtotime($min_narudzbina_datum));
		$max = date("Y", strtotime($max_narudzbina_datum));

		return range($min,$max);
	}

	public static function mesecnaAnalitika($m,$y,$datum_od=null, $datum_do=null) {
		$query = DB::table('web_b2c_narudzbina');
		if(!is_null($datum_od) && !is_null($datum_do) && $datum_od!='' && $datum_do!=''){
			$query = $query->whereBetween('datum_dokumenta',array($datum_od, $datum_do));
		}
		$ukupno = $query->where('web_b2c_narudzbina_id', '!=', '-1')->whereBetween('datum_dokumenta', array(date($y.'-'.$m.'-01'), date( "Y-m-d", strtotime( date($y) . "-" . $m . "-01 +1 month" ))))->count();

		return $ukupno;
	}

	public static function mesecnaAnalitika2($m, $y, $datum_od=null, $datum_do=null) {
		$query = DB::table('web_b2c_narudzbina');
		if(!is_null($datum_od) && !is_null($datum_do) && $datum_od!='' && $datum_do!=''){
			$query = $query->whereBetween('datum_dokumenta',array($datum_od, $datum_do));
		}
		$query = $query->leftJoin('web_b2c_narudzbina_stavka', 'web_b2c_narudzbina.web_b2c_narudzbina_id', '=', 'web_b2c_narudzbina_stavka.web_b2c_narudzbina_id')->where('web_b2c_narudzbina.realizovano', 1)->where('web_b2c_narudzbina.stornirano', 0)->whereBetween('web_b2c_narudzbina.datum_dokumenta', array(date($y.'-'.$m.'-01'), date( "Y-m-d", strtotime( date($y) . "-" . $m . "-01 +1 month" ))))->select('web_b2c_narudzbina_stavka.jm_cena', 'web_b2c_narudzbina_stavka.kolicina')->get();
		
		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;

	}
	public static function prikazGrupa($id) {
		return DB::select("select(select naziv from roba where roba_id = wbns.roba_id and grupa_pr_id=$id),
                sum(kolicina), sum(kolicina*jm_cena) AS ukupno, sum((kolicina*jm_cena/1.2)-(racunska_cena_nc*kolicina)) AS razlika, racunska_cena_nc
                FROM web_b2c_narudzbina_stavka wbns WHERE  web_b2c_narudzbina_id IN 
                (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY naziv, racunska_cena_nc
                order by sum desc");
	}

}