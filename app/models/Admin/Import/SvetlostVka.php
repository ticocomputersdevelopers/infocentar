<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class SvetlostVka {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/svetlostvka/svetlostvka_excel/svetlostvka.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

			
			for ($row = 28; $row <= $lastRow; $row++) {
	            $colA = $worksheet->getCell('A'.$row)->getValue();
	            $colB = $worksheet->getCell('B'.$row)->getValue();
	            $colC = $worksheet->getCell('C'.$row)->getValue();
	            $colD = $worksheet->getCell('D'.$row)->getValue();
	            $colE = $worksheet->getCell('E'.$row)->getValue();
				$colF = $worksheet->getCell('F'.$row)->getValue();
	            $colG = $worksheet->getCell('G'.$row)->getValue();
				$colH = $worksheet->getCell('H'.$row)->getValue();

				if(!empty($colA) && empty($colB) && empty($colC) && empty($colD) && empty($colE) && empty($colF) && empty($colG) && empty($colH) )
					{
					}else{
						$grupa=$colA;
						//if(!empty($colB) &&  !($colA=='Model' || $colB=='Sifra' || $colC=='WEB link' || $colD=='Opis' )){
						if(!empty($colB) && $colB!='Model'){

							$sPolja = '';
							$sVrednosti = '';


							if(!empty($colI)){
								$rabat=$colI;
							}else{
								if(!empty($colG))
									$rabat=$colG;
								else $rabat=0.00;
							}

							$ncena=$colE;
							$ncena=number_format((float)$ncena, 2, '.', '');
							$ncena = $ncena-($ncena*$rabat);
							$ncena=round($ncena, 2);

							if(!empty($colH)){
								$pmp=$colH;
							}else{
								if(!empty($colE))
									$pmp=$colE;
								else $pmp=0.00;
							}

							$naziv=$colD;
							// if (strpos($naziv,',') !== false) {
							//     $pos=strpos ( $naziv , ',');
							//     $naziv=substr($naziv, 0, $pos);
							// }

							if(!empty($colA)){						
								$naziv=$colA.' '.$naziv;
							}

							$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
							$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($colB) . "',";
							if(!empty($colA)){
								$sPolja .= " model,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($colA)) . "',";
							}
							$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv))  . "' ,";
							$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
							$sPolja .= " grupa,";					$sVrednosti .= " 'Veliki kucni aparati',";
							$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric((float)$pmp,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
							//$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($colA)) . "',";
							//$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($colB)) . "',";
							$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', ''). ",";
							$sPolja .= " mpcena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric((float)$pmp,1,$kurs,$valuta_id_nc),2, '.', '') . "";


						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}
					
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/svetlostvka/svetlostvka_excel/svetlostvka.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
			
			for ($row = 1; $row <= $lastRow; $row++) {
	            $colA = $worksheet->getCell('A'.$row)->getValue();
	            $colB = $worksheet->getCell('B'.$row)->getValue();
	            $colC = $worksheet->getCell('C'.$row)->getValue();
	            $colD = $worksheet->getCell('D'.$row)->getValue();
	            $colE = $worksheet->getCell('E'.$row)->getValue();
				$colF = $worksheet->getCell('F'.$row)->getValue();
	            $colG = $worksheet->getCell('G'.$row)->getValue();
				$colH = $worksheet->getCell('H'.$row)->getValue();


				if(!empty($colA) && empty($colB) && empty($colC) && empty($colD) && empty($colE) && empty($colF) && empty($colG) && empty($colH) )
					{
					}else{
						$grupa=$colA;
						if(!empty($colB) && $colB!='Model'){

							$sPolja = '';
							$sVrednosti = '';
							


							if(!empty($colI)){
								$rabat=$colI;
							}else{
								if(!empty($colG))
									$rabat=$colG;

								else $rabat=0.00;
							}


							$ncena=$colE;
							$ncena=number_format((float)$ncena, 2, '.', '');
							$ncena = $ncena-($ncena*$rabat);
							$ncena=round($ncena, 2);

							if(!empty($colH)){
								$pmp=$colH;
							}else{
								if(!empty($colE))
									$pmp=$colE;
								else $pmp=0.00;
							}

							$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
							$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric((float)$pmp,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
							$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', ''). ",";
							$sPolja .= " mpcena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric((float)$pmp,1,$kurs,$valuta_id_nc),2, '.', '') . "";

						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
				}
					
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}