<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Woobyhouse {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/woobyhouse/woobyhouse_excel/woobyhouse.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('C'.$row)->getValue();
	            $rabat_ex = $worksheet->getCell('D'.$row)->getValue();

				if(!empty($sifra) and !($sifra=='Šifra' || $naziv=='Naziv robe' || $cena=='VP Cena' || $rabat_ex=='Rabat') && isset($cena) && is_numeric($cena)){
					
					$sPolja = '';
					$sVrednosti = '';
		
					$ncena=number_format((float)$cena, 2, '.', '');

					$rabat = $rabat_ex!='' ? (float) $rabat_ex : '0.00';
					$mp_marza=$rabat;

					$rabat_procenat=$rabat;
					$mp_marza_procenat=$mp_marza-1;

					$rabat=$rabat/100.00;
					$mp_marza=$mp_marza_procenat/100.00;
					
					$ncena=$ncena-($ncena*$rabat);
					$ncena=round($ncena,2);

					$mpcena = $cena * 1.2;
					

					if(empty($rabat_ex) || strpos($sifra,'P') !== false){
						$aktivan=0;
					}else{
						$aktivan=1;
					}

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id. ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250(mb_strtolower($naziv))) ." ( ".addslashes($sifra)." ) ',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', ''). ",";
					$sPolja .= " flag_aktivan,";			$sVrednosti .= " " . $aktivan. ",";
					// $sPolja .= " pmp_cena,";				$sVrednosti .= " " . $rabat_procenat. ",";
					//$sPolja .= " mp_marza,";				$sVrednosti .= " " . $mp_marza_procenat. ",";
					$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($ncena,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}


			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/woobyhouse/woobyhouse_excel/woobyhouse.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('C'.$row)->getValue();
	            $rabat_ex = $worksheet->getCell('D'.$row)->getValue();

				if(!empty($sifra) and !($sifra=='Šifra' || $naziv=='Naziv robe' || $cena=='VP Cena' || $rabat_ex=='Rabat') && isset($cena) && is_numeric($cena)){
					
					
					$sPolja = '';
					$sVrednosti = '';
		
					$ncena=number_format((float)$cena, 2, '.', '');

					$rabat = $rabat_ex!='' ? (float) $rabat_ex : '0.00';
					$mp_marza=$rabat;

					$rabat_procenat=$rabat;
					$mp_marza_procenat=$mp_marza-1;

					$rabat=$rabat/100.00;
					$mp_marza=$mp_marza_procenat/100.00;
					
					$ncena=$ncena-($ncena*$rabat);
					$ncena=round($ncena,2);

					$mpcena = $cena * 1.2;
					

					if(empty($rabat_ex) || strpos($sifra,'P') !== false){
						$aktivan=0;
					}else{
						$aktivan=1;
					}

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id. ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', ''). ",";
					$sPolja .= " flag_aktivan,";			$sVrednosti .= " " . $aktivan. ",";
					$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($ncena,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}

			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}