<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class Kimtec {
	public static function execute($dobavljac_id){

	Support::initIniSetup();
	Support::initQueryExecute();

	$responce = self::serviceResponse($dobavljac_id,'GetProductsList','productsList');
	if($responce != null){
		return $responce;
	}
	$responce = self::serviceResponse($dobavljac_id,'GetProductsPriceList','productsPrice');
	if($responce != null){
		return $responce;
	}
	$responce = self::serviceResponse($dobavljac_id,'GetProductsSpecification','productsSpecification');
	if($responce != null){
		return $responce;
	}
		
		$productsSpecification = simplexml_load_file('files/kimtec/kimtec_xml/productsSpecification.xml'); 
		
		//xml sa karakteristikama
		foreach ($productsSpecification as $spec) {

			$vrednosti= simplexml_load_string($spec->SpecificationItemValues);
			
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $spec->ProductCode)) . ",";	
			$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $spec->SpecificationItemName)) . ",";
			$k_vrednost = '';
			foreach ($vrednosti as $vrednost)
			{
				$k_vrednost .= "". $vrednost .", ";
			}
			$k_vrednost=trim($k_vrednost, ' ');
			$k_vrednost=trim($k_vrednost, ',');
			$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($k_vrednost)) . "";			
			
				
			DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			
		}
		$productsSpecification = NULL;

	    $productsList = simplexml_load_file('files/kimtec/kimtec_xml/productsList.xml'); 

		//podaci o proizvodima
		foreach ($productsList as $product) {

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->ProductCode)) . ",";
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Brand)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ProductName)) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ProductType)) . ",";
			$sPolja .= "model,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->Model)) . ",";
		
			$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "" . $product->MarketingDescription == "" ? "0," : "1,";
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( pg_escape_string($product->MarketingDescription))) . ",";
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";

			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $product->ProductImageUrl == "" ? "0" : "1" . "";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			//slike
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->ProductCode)) . ",";
			$sPolja .= "akcija,";					$sVrednosti .= "" . 1 . ",";
			$sPolja .= "putanja";				    $sVrednosti .= "'" . Support::encodeTo1250($product->ProductImageUrl). "'";
				
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");


		}

		$productsList=NULL;

	    $productsPrice = simplexml_load_file('files/kimtec/kimtec_xml/productsPrice.xml'); 

		//xml sa cenama
		foreach ($productsPrice as $price) {

			$cena_nc = $price->ProductPartnerPrice;
			//$mpcena = $price->RecommendedRetailPrice;
			$dostupnost = $price->ProductAvailability == "0" ? "0" : "1";
			$pdv = $price->TaxRate;	
			$sifra_kod_dobavljaca = Support::quotedStr(Support::encodeTo1250($price->ProductCode));	
			$pmp_cena = $price->RecommendedRetailPrice;			
			
			DB::statement("UPDATE dobavljac_cenovnik_temp SET cena_nc= $cena_nc,mpcena= 0, pmp_cena= $pmp_cena, kolicina = $dostupnost, pdv = $pdv WHERE partner_id= $dobavljac_id AND sifra_kod_dobavljaca = $sifra_kod_dobavljaca");		
			
		}
		$productsPrice=NULL;
		
		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	}

	public static function executeShort($dobavljac_id){

		Support::initIniSetup();
		Support::initQueryExecute();

		self::serviceResponse($dobavljac_id,'GetProductsPriceList','productsPrice');

	    $productsPrice = simplexml_load_file('files/kimtec/kimtec_xml/productsPrice.xml'); 

		foreach ($productsPrice as $price) {
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($price->ProductCode)) . ",";
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . $price->ProductPartnerPrice . ",";
			$sPolja .= "mpcena,";					$sVrednosti .= " 0,";
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $price->RecommendedRetailPrice . ",";
			$sPolja .= "kolicina";					$sVrednosti .= "" . $price->ProductAvailability == "0" ? "0" : "1" . "";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
		}
		
		//Support::queryShortExecute($dobavljac_id);
	}

	public static function serviceResponse($partner_id,$serviceLink,$xmlFile){
	    if(!File::exists("files/kimtec/certs/ca.pem") || !File::exists("files/kimtec/certs/client.pem") || !File::exists("files/kimtec/certs/key.pem"))
	    {
	        return "Nedostaje sertifikat fajl u files/kimtec/certs/*!.<br>";
	        die();
	    }
        if(!File::exists("files/kimtec/kimtec_xml/productsList.xml") || !File::exists("files/kimtec/kimtec_xml/productsPrice.xml") || !File::exists("files/kimtec/kimtec_xml/productsSpecification.xml")){
            return "Ne postoji fajl u files/kimtec/kimtec_xml/*!";
            die();
        }
	    
            $ch =curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://b2b.kimtec.rs/B2BService/HTTP/Product/".$serviceLink.".aspx");
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSLVERSION,3);
            curl_setopt($ch, CURLOPT_CAINFO, realpath("files/kimtec/certs/ca.pem"));
            curl_setopt($ch, CURLOPT_SSLCERT, realpath("files/kimtec/certs/client.pem"));
            curl_setopt($ch, CURLOPT_SSLKEY, realpath("files/kimtec/certs/key.pem"));
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSLKEYPASSWD, Support::servicePassword($partner_id)); // pin vezan za B2B certifikat
            $productList = curl_exec ($ch);
			// echo $productList;
   //          echo curl_error($ch);
            curl_close ($ch);

        $myfile = fopen("files/kimtec/kimtec_xml/".$xmlFile.".xml", "w");
        fwrite($myfile, $productList);
        fclose($myfile);

        $xmlfile=file_get_contents("files/kimtec/kimtec_xml/".$xmlFile.".xml");
        $xmlfile2 = preg_replace('/<b2bexception[^>]*>.*?<\/b2bexception>/i', '', $xmlfile);
        file_put_contents("files/kimtec/kimtec_xml/".$xmlFile.".xml", $xmlfile2);

	}
	
    public static function getKimtecImageContent($partner_id,$image,$destination){
		$ch =curl_init($image);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
        curl_setopt($ch, CURLOPT_CAINFO, realpath("files/kimtec/certs/ca.pem"));
        curl_setopt($ch, CURLOPT_SSLCERT, realpath("files/kimtec/certs/client.pem"));
        curl_setopt($ch, CURLOPT_SSLKEY, realpath("files/kimtec/certs/key.pem"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, Support::servicePassword($partner_id));
        $content = curl_exec ($ch);
         
        echo curl_error($ch);
        curl_close ($ch);

        $fp = fopen($destination, "w"); 
        fwrite($fp, $content); 
        fclose($fp);        
    }

}


?>

