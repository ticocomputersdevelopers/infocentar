<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Ekka {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/ekka/ekka_excel/ekka.xlsx');
			$products_file = "files/ekka/ekka_excel/ekka.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 2; $row <= $lastRow; $row++) {

	            $sifra_dobavljaca = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
	            $proizvodjac= $worksheet->getCell('F'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('G'.$row)->getValue();
	            $opis = $worksheet->getCell('H'.$row)->getValue();
	            $slika = $worksheet->getCell('I'.$row)->getValue();
	            $grupa = $worksheet->getCell('K'.$row)->getValue();
	            $podgrupa = $worksheet->getCell('L'.$row)->getValue();
	            $kolicina = $worksheet->getCell('Q'.$row)->getValue();


	            if(isset($sifra_dobavljaca) && isset($naziv) && isset($cena_nc)){
					
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra_dobavljaca)) . "',";	
					$sPolja .= " naziv,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv)). " ( ".addslashes(Support::encodeTo1250($sifra_dobavljaca)) . " ) ',";	
					$sPolja .= " proizvodjac,";					
					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)). "',";	
					if(!empty($opis)){
					$sPolja .= " flag_opis_postoji,";		$sVrednosti .= " 1,";	
					$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis) . "',";
					}	
					if (!empty($slika)) {
						$sPolja .= " flag_slika_postoji,";		$sVrednosti .= "1,";
					}
					$sPolja .= " grupa,";					$sVrednosti .= " '". Support::encodeTo1250($grupa) ."',";
					$sPolja .= " podgrupa,";					$sVrednosti .= " '". Support::encodeTo1250($podgrupa) ."',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina),2,'.','') . ",";
					$sPolja .= " cena_nc";					
					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp(" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					if(!empty($slika)){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra_dobavljaca."','".$slika."',1 )");	
				}
				}
				
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/ekka/ekka_excel/ekka.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {

	            $sifra_dobavljaca = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('G'.$row)->getValue();
	            $kolicina = $worksheet->getCell('Q'.$row)->getValue();

	            if(isset($sifra_dobavljaca) && isset($naziv) && isset($cena_nc)){
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra_dobavljaca)) . "',";	
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina),2,'.','') . ",";

					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc-(($cena_nc*13)/100),1,$kurs,$valuta_id_nc),2, '.', ''). "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}