<?php
namespace Import;
use Import\Support;
use DB;
use File;

class AudioPlaneta {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/audioplaneta/audioplaneta_xml/audioplaneta.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}
		$products = simplexml_load_file($products_file);

		if($continue){
			Support::initQueryExecute();
	        
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->naziv))) ." ( ". $product->sifra ." )"."',";
				$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->kategorija))) . "',";
				$sPolja .= "opis,";						$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->opis))) . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= " 1,";
				$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
				$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";
				$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";
				$sPolja .= "pdv,";						$sVrednosti .= "" . $product->stopa . ",";							
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->cena_bez_pdv . ",";
				$sPolja .= "web_cena,";					$sVrednosti .= "" . $product->mp_cena . ",";
				$sPolja .= "mpcena,";					$sVrednosti .= "" . $product->mp_cena . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $product->mp_cena . "";	
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	

				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->sifra."','".addslashes(Support::encodeTo1250($product->link))."',1 )");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/audioplaneta/audioplaneta_xml/audioplaneta.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}
		$products = simplexml_load_file($products_file);

		if($continue){
			Support::initQueryExecute();
	        
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra . "',";
				$sPolja .= "pdv,";						$sVrednosti .= "" . $product->stopa . ",";							
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->cena_bez_pdv . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $product->mp_cena . "";	
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}