<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Ibg {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/ibg/ibg_xml/ibg.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

		    $products = simplexml_load_file($products_file);
		    // $products_2 = $products->xpath('PRICES/PRICE');
		    foreach ($products as $product):
		        $sifra = $product->SIFRA;
		        $naziv = $product->ARTIKAL;
		        $grupa = $product->KATEGORIJA;
		        $podgrupa = $product->PODKATEGORIJA;
		        $barkod = $product->barcode;
		        $kolicina = $product->KOLICINA;
		        $cena_nc = $product->CENA;
		        $akcija = $product->akcija;

		        if(!empty($sifra)){

		            $sPolja = '';
		            $sVrednosti = '';
		            $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
		            $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) . "',";
		            $sPolja .= "naziv,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv)) . "',";
		            $sPolja .= "grupa,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($grupa)) . "',";
		            $sPolja .= "podgrupa,";                 $sVrednosti .= "'" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
		            $sPolja .= "barkod,";                   $sVrednosti .= "'" . addslashes(Support::encodeTo1250($barkod)) . "',";
		            $sPolja .= "kolicina,";                 $sVrednosti .= "" . floatval($kolicina) . ",";
		            $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc)), 2, '.', '') . "";
		            
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");    
		        }

		    endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/ibg/ibg_xml/ibg.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);
		    foreach ($products as $product):
		        $sifra = $product->SIFRA;
		        $kolicina = $product->KOLICINA;
		        $cena_nc = $product->CENA;

		        if(!empty($sifra)){

		            $sPolja = '';
		            $sVrednosti = '';
		            $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
		            $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) . "',";
		            $sPolja .= "kolicina,";                 $sVrednosti .= "" . floatval($kolicina) . ",";
		            $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc)), 2, '.', '') . "";
		            
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");    
		        }

		    endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}