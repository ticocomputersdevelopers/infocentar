<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class FormaVs {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/formavs/formavs_excel/formavs.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 2; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
	            $kolicina = $worksheet->getCell('D'.$row)->getValue();
				$cena_nc = $worksheet->getCell('E'.$row)->getValue();
				$pmp_web = $worksheet->getCell('F'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc)){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . $sifra . "',";
					$sPolja .= " naziv,";	$sVrednosti .= " '" . Support::encodeTo1250($naziv). " ( ".Support::encodeTo1250($sifra) . " ) ',";	

					if ($kolicina != 0) {
						$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					}

					$sPolja .= " cena_nc,";					
					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). ",";

					if($pmp_web != 0){
						$sPolja .= " mpcena,";					
						$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmp_web,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
						$sPolja .= " web_cena,";					
						$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmp_web,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
						$sPolja .= " pmp_cena";					
						$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmp_web,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					}
					

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		
				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/formavs/formavs_excel/formavs.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 2; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena_nc = $worksheet->getCell('E'.$row)->getValue();
				

				if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc)){
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" .$sifra . "',";
					$sPolja .= " naziv,";	$sVrednosti .= " '" . Support::encodeTo1250($naziv). " ( ".Support::encodeTo1250($sifra) . " ) ',";	
					$sPolja .= " cena_nc";					
					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";	

					DB::statement("INSERT INTO dobavljac_cenovnik_temp(" . $sPolja . ") VALUES (" . $sVrednosti . ")");		
				}
			}
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}