<?php
namespace Import;
use Import\Support;
use DB;
use File;
use AdminOptions;

class Josipovic {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/josipovic/josipovic_xml/josipovic.json');
			$products_file = "files/josipovic/josipovic_xml/josipovic.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;
	        	$sifra = $product->ean;
	        	$proizvodjac = $product->manufacturer;
	        	$naziv = $product->name;
	        	$grupa = $product->group;
	        	$cena_nc = $product->price;
	        	$kolicina = $product->quantity;
	        	$barkod = $product->ean;
	        	$slika = $product->images['normal'];

				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){

					$cena_nc = $cena_nc;

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv)) . "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)) . "',";
					$sPolja .= " barkod,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($barkod)) . "',";
					$sPolja .= " pdv,";						$sVrednosti .= " " . number_format(20.00,2,'.','') . ",";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'" . addslashes(Support::encodeTo1250($sifra)) . "','".Support::encodeTo1250($slika)."',1 )");

				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/josipovic/josipovic_json/josipovic.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
			
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;
	        	$sifra = $product->ean;
	        	$cena_nc = $product->price;
	        	$kolicina = $product->quantity;

				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){
					$cena_nc = $cena_nc;
					
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			//Support::queryShortExecute($dobavljac_id, $type);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}