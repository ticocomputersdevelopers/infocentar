<?php
namespace Import;
use DB;
use File;
use AdminOptions;
use Mail;
use View;
use PHPExcel; 
use PHPExcel_IOFactory;
use Service\Mailer;


class Support {

	public static function zameni_karaktere_import($OriginalString){
		$NekiKarakteriZaZamenu = array("Ä", "Ä‡", "Å¡","Ä‘","Å¾","ÄŒ", "Ä†", "Å ","Ä","Å½","&scaron;","¢");
		$KarakteriKojiMenjaju = array("è", "æ", "š","ð","ž","È", "Æ", "Š","Ð","Ž","Š","");
		$ZamenjenString = str_replace($NekiKarakteriZaZamenu, $KarakteriKojiMenjaju, $OriginalString);								
		return $ZamenjenString;
	}

	public static function convertChars($string) {
		$NekiKarakteriZaZamenu = array("è", "Ă¨", "Ĺˇ", "Ĺľ", "ð");
		$KarakteriKojiMenjaju = array("č", "c", "s", "z", "đ");
		$ZamenjenString = str_replace($NekiKarakteriZaZamenu, $KarakteriKojiMenjaju, $string);								
		return $ZamenjenString;
	}

	public static function quotedStr($string){
		$string = str_replace(chr(047), "", $string);
		$string = str_replace(chr(057), "", $string);
		$string = str_replace(chr(134), "", $string);
		$string = chr(047).$string.chr(047);
		return $string;
	}
	public static function quotedStrPC($string){
		$string = str_replace("/", "-", $string);
		$string = str_replace(chr(057), "", $string);
		$string = str_replace(chr(134), "", $string);
		$string = chr(047).$string.chr(047);
		return $string;
	}
	public static function encodeTo1250($string){
		return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
	}
	public static function encodeToIso($string){
		return iconv("UTF-8", "ISO-8859-1//TRANSLIT//IGNORE",$string);
	}
	public static function charset_decode_utf_8($string)
    {
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        return $string;
    }
	public static function file_name($dir){
		$files = scandir($dir, 1);
		$results = false;
		for($i=0;$i<count($files)-2;$i++){		
			if(isset($files[$i])){
				if(File::extension($dir.$files[$i])!='zip'){				
					$results = $files[$i];
					break;
				}
			}
		}
		return $results;
	}
	public static function import_file_path($obj){
		$import_naziv = str_replace('import_','',str_replace('excel','',$obj->vrednost_kolone));
		if($obj->file_type == 'xml'){
			$suffix = "_xml";
		}
		elseif($obj->file_type == 'csv'){
			$suffix = "_csv";
		}else{
			$suffix = "_excel";
		}
		return (object) array("path"=>"files/".$import_naziv."/".$import_naziv.$suffix."/", "name"=>$import_naziv.".".$obj->file_type);
	}
	public static function replace_empty_numeric($numeric,$valuta_id,$kurs,$valuta_id_nc){
		$kurs = floatval($kurs);
		$numeric = floatval($numeric);
		if($numeric==""){
			$numeric = "0.00";
		}else{
			//Proveri koja vrsta cena za nabavnu cenu je namestena u B2C Admin
			//NC je euro u B2C Admin
			if($valuta_id_nc == "2"){
				//Cena iz cenovnika je u evrima
				if($valuta_id == "2"){
					$numeric = $numeric;
				//Cena iz cenovnika je u dinarima
				}else{
					$numeric = $numeric/$kurs;
				}
			//NC je dinar u B2C Admin
			}else{
				//Cena iz cenovnika je u evrima
				if($valuta_id == "2"){
					$numeric = $numeric*$kurs;
				//Cena iz cenovnika je u dinarima
				}else{
					$numeric = $numeric;
				}
			}
		}
		return $numeric;
	}

	public static function serviceUsername($partner_id){
	    return DB::table('dobavljac_cenovnik_kolone')->where('partner_id',$partner_id)->pluck('service_username');
	}
	public static function servicePassword($partner_id){
	    return DB::table('dobavljac_cenovnik_kolone')->where('partner_id',$partner_id)->pluck('service_password');
	}
	public static function autoLink($partner_id){
	    return DB::table('dobavljac_cenovnik_kolone')->where('partner_id',$partner_id)->pluck('auto_link');
	}
	public static function autoDownload($link,$file){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_FILE, fopen($file, "w"));
        curl_exec ($ch);
        curl_close($ch);
	}
	public static function initIniSetup(){
		ini_set('max_execution_time', 5000);
		ini_set('max_input_time', 5000);
		ini_set('max_input_vars', 5000);
		ini_set('memory_limit', '512M');
	}
	public static function initQueryExecute($encode=null){
		DB::statement("TRUNCATE dobavljac_cenovnik_karakteristike_temp");
		DB::statement("TRUNCATE dobavljac_cenovnik_slike_temp");
		DB::statement("TRUNCATE dobavljac_cenovnik_temp");
		if($encode == null){
			DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		}else{
			DB::statement("SET CLIENT_ENCODING TO '".$encode."'");
		}
	}
	public static function queryExecute($dobavljac_id,array $dobavljac,array $slike,array $karakteristike){

		if(count($dobavljac)>0){
			if(DB::table('dobavljac_cenovnik_temp')->count() > 1){			
				DB::statement("UPDATE dobavljac_cenovnik SET kolicina = 0 WHERE partner_id = ".$dobavljac_id." ");
				if(in_array('i',$dobavljac) && $dobavljac_id != 17 ){
					DB::statement(" INSERT INTO dobavljac_cenovnik (SELECT * FROM dobavljac_cenovnik_temp WHERE NOT EXISTS( "
			        ." SELECT * FROM dobavljac_cenovnik dc WHERE dc.sifra_kod_dobavljaca = "
			        ." dobavljac_cenovnik_temp.sifra_kod_dobavljaca AND dc.partner_id=dobavljac_cenovnik_temp.partner_id)) ");				
				}elseif(in_array('i',$dobavljac) && $dobavljac_id == 17){
					DB::statement(" INSERT INTO dobavljac_cenovnik (SELECT * FROM dobavljac_cenovnik_temp WHERE NOT EXISTS( "
			        ." SELECT * FROM dobavljac_cenovnik dc WHERE dc.sifra_kod_dobavljaca = "
			        ." dobavljac_cenovnik_temp.sifra_kod_dobavljaca AND dc.partner_id=dobavljac_cenovnik_temp.partner_id) AND cena_nc >0) ");	
				}
				if(in_array('u',$dobavljac)){
					DB::statement(" UPDATE dobavljac_cenovnik dc SET flag_opis_postoji = dct.flag_opis_postoji,flag_slika_postoji = dct.flag_slika_postoji,kolicina = dct.kolicina,zadnji_kurs = dct.zadnji_kurs, cena_nc = CASE WHEN dct.cena_nc > 0 THEN dct.cena_nc ELSE dc.cena_nc END, mpcena = CASE WHEN dct.mpcena > 0 THEN dct.mpcena ELSE dc.mpcena END,	web_cena = CASE WHEN dct.web_cena > 0 THEN dct.web_cena ELSE dc.web_cena END, pmp_cena = CASE WHEN dct.pmp_cena > 0 THEN dct.pmp_cena ELSE dc.pmp_cena END, naziv = CASE WHEN (select int_data from options where options_id=3033) = 1 THEN dct.naziv ELSE dc.naziv END FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=dct.partner_id ");				
				}
				DB::statement("TRUNCATE dobavljac_cenovnik_temp");
			}
		}

		if(count($slike)>0){
			if(in_array('i',$slike)){
				DB::statement(" INSERT INTO dobavljac_cenovnik_slike (SELECT * FROM dobavljac_cenovnik_slike_temp WHERE NOT EXISTS( "
		        ." SELECT * FROM dobavljac_cenovnik_slike dc WHERE dc.sifra_kod_dobavljaca = "
		        ." dobavljac_cenovnik_slike_temp.sifra_kod_dobavljaca AND dc.partner_id=dobavljac_cenovnik_slike_temp.partner_id)) ");				
			}
			if(in_array('u',$slike)){
				//			
			}
			DB::statement("TRUNCATE dobavljac_cenovnik_slike_temp");
		}

		if(count($karakteristike)>0){			
			if(in_array('i',$karakteristike)){
				DB::statement(" INSERT INTO dobavljac_cenovnik_karakteristike (SELECT * FROM dobavljac_cenovnik_karakteristike_temp WHERE NOT EXISTS( "
		        ." SELECT * FROM dobavljac_cenovnik_karakteristike dck WHERE dck.sifra_kod_dobavljaca = "
		        ." dobavljac_cenovnik_karakteristike_temp.sifra_kod_dobavljaca AND dck.partner_id=dobavljac_cenovnik_karakteristike_temp.partner_id)) ");			
			}
			if(in_array('u',$karakteristike)){
				$karak_grupa = "";
			    if(Support::checkPartner($dobavljac_id,'import_ewe')){
			        $karak_grupa = "dc.karakteristika_grupa=dct.karakteristika_grupa AND ";
			    }
				DB::statement("UPDATE dobavljac_cenovnik_karakteristike dc SET karakteristika_vrednost=dct.karakteristika_vrednost "
		        ." FROM dobavljac_cenovnik_karakteristike_temp dct WHERE "
		        . $karak_grupa ."dc.karakteristika_naziv=dct.karakteristika_naziv AND dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca ");		
			}
			DB::statement("TRUNCATE dobavljac_cenovnik_karakteristike_temp");
		}

		// //Radimo update rednog_broja_grupe u tabelu dobavljac_cenovnik_karakteristike za postojece karakteristike
		// DB::statement(" UPDATE dobavljac_cenovnik_karakteristike dc SET redni_br_grupe = dct.redni_br_grupe "
  //       ." FROM dobavljac_cenovnik_karakteristike_temp dct WHERE "
  //       ."dc.karakteristika_naziv=dct.karakteristika_naziv AND dc.karakteristika_vrednost=dct.karakteristika_vrednost AND dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca ");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function queryShortExecute($dobavljac_id, $type=null){
		
		$preracunavanje = DB::table('partner')->where('partner_id', $dobavljac_id)->pluck('preracunavanje_cena');
		$exist = DB::table('dobavljac_cenovnik_temp')->where('partner_id', $dobavljac_id)->count();

		if(DB::table('dobavljac_cenovnik_temp')->count() > 1 && $exist > 1){
			if(isset($type) && $type == 'lager') {
				DB::statement("UPDATE dobavljac_cenovnik SET kolicina = 0 WHERE partner_id = ".$dobavljac_id."");
				DB::statement("UPDATE dobavljac_cenovnik dc SET kolicina = dct.kolicina FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");
			} else if (isset($type) && $type == 'cene') {
				if(isset($preracunavanje) && $preracunavanje == 1) {
					DB::statement("UPDATE dobavljac_cenovnik dc SET zadnji_kurs = dct.zadnji_kurs, cena_nc = dct.cena_nc, mpcena = dct.mpcena, pmp_cena=dct.pmp_cena, web_cena = ROUND((dct.cena_nc+(dct.cena_nc*dc.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)) FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");
				} else {
					DB::statement("UPDATE dobavljac_cenovnik dc SET zadnji_kurs = dct.zadnji_kurs, cena_nc = dct.cena_nc, mpcena = dct.mpcena, pmp_cena=dct.pmp_cena, web_cena = 
					CASE 
						WHEN dct.web_cena <> 0
						THEN dct.web_cena
						WHEN dct.pmp_cena <> 0
						THEN dct.pmp_cena
						WHEN dct.cena_nc <> 0
						THEN dct.cena_nc 
					END
					FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");
				}

			} else {
				DB::statement("UPDATE dobavljac_cenovnik SET kolicina = 0 WHERE partner_id = ".$dobavljac_id."");
				DB::statement("UPDATE dobavljac_cenovnik dc SET kolicina = dct.kolicina FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");

				if(isset($preracunavanje) && $preracunavanje == 1) {
					DB::statement("UPDATE dobavljac_cenovnik dc SET zadnji_kurs = dct.zadnji_kurs, cena_nc = dct.cena_nc, mpcena = dct.mpcena, pmp_cena=dct.pmp_cena, web_cena = ROUND((dct.cena_nc+(dct.cena_nc*dc.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)) FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");
				} else {
					DB::statement("UPDATE dobavljac_cenovnik dc SET zadnji_kurs = dct.zadnji_kurs, cena_nc = dct.cena_nc, mpcena = dct.mpcena, pmp_cena=dct.pmp_cena, web_cena = 
					CASE 
						WHEN dct.web_cena <> 0
						THEN dct.web_cena
						WHEN dct.pmp_cena <> 0
						THEN dct.pmp_cena
						WHEN dct.cena_nc <> 0
						THEN dct.cena_nc 
					END
					FROM dobavljac_cenovnik_temp dct WHERE dc.sifra_kod_dobavljaca=dct.sifra_kod_dobavljaca AND dc.partner_id=".$dobavljac_id." AND dc.flag_zakljucan=0");
				}

			}

			DB::statement("TRUNCATE dobavljac_cenovnik_temp");
		}

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
	
	//Azurira samo lager i flag prikazi u cenovniku (short import)
	public static function autoLager(){
		//dobavljac cenovnik update flag prikazi u cenovniku
		DB::statement("UPDATE dobavljac_cenovnik dc SET flag_prikazi_u_cenovniku= CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND cena_nc>0 THEN 1 ELSE 0 END ELSE dc.flag_prikazi_u_cenovniku END WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id IN (SELECT partner_id FROM dobavljac_cenovnik_kolone WHERE auto_import_short=1)");

		//lager update
		DB::statement("UPDATE lager l SET kolicina=dcn.kolicina 
			FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn
			WHERE l.roba_id=dcn.roba_id AND l.orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1) AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0)");
		//lager insert
		DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) SELECT roba_id, (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1), (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0), SUM(kolicina) FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND roba_id NOT IN (SELECT roba_id FROM lager WHERE orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1) AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0)) GROUP BY dc.roba_id HAVING SUM(kolicina) > 0");

	}

	// Azurira samo cene i flag prikazi u cenovniku (short import)
	public static function autoCene(){
		//dobavljac cenovnik update flag prikazi u cenovniku
		DB::statement("UPDATE dobavljac_cenovnik dc SET flag_prikazi_u_cenovniku = CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND cena_nc>0 THEN 1 ELSE 0 END ELSE dc.flag_prikazi_u_cenovniku END WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id IN (SELECT partner_id FROM dobavljac_cenovnik_kolone WHERE auto_import_short=1)");

		//roba update za sve artikle samo ako se prati najniza cena
		DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, flag_prikazi_u_cenovniku=dcn.flag_prikazi_u_cenovniku, web_cena= 
			CASE WHEN (select int_data from options where options_id=3007) = 0 
			THEN 
					CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1
					THEN
						ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
					ELSE
						dcn.web_cena
					END
			ELSE 
				dcn.pmp_cena 
			END, 
			web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = 
			CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1 
				THEN 
					ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				ELSE
					dcn.mpcena
				END
			FROM (SELECT
			CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
			ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
			ELSE
			ROW_NUMBER() OVER (PARTITION BY roba_id)
			END 
			AS r,
			dc.roba_id, dc.partner_id, CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND dc.cena_nc>0 THEN 1 ELSE 0 END ELSE (select flag_prikazi_u_cenovniku from roba where roba_id = dc.roba_id) END AS flag_prikazi_u_cenovniku, dc.web_cena, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_marza, dc.mp_marza
			FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id <> (select int_data from options where options_id=3008)) dcn
			WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");

		//roba update za artikle sa kolicinom vecom od 0  prati se i najniza cena
		if(DB::table('options')->where('options_id',3010)->pluck('int_data')==1){
			DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, flag_prikazi_u_cenovniku=dcn.flag_prikazi_u_cenovniku, web_cena= 
				CASE WHEN (select int_data from options where options_id=3007) = 0 
				THEN 
					CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1
					THEN
						ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
					ELSE
						dcn.web_cena
					END 
				ELSE dcn.pmp_cena END, 
				web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				FROM (SELECT
				CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
				ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
				ELSE
				ROW_NUMBER() OVER (PARTITION BY roba_id)
				END 
				AS r,
				dc.roba_id, dc.partner_id, CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND dc.cena_nc>0 THEN 1 ELSE 0 END ELSE (select flag_prikazi_u_cenovniku from roba where roba_id = dc.roba_id) END AS flag_prikazi_u_cenovniku, dc.web_cena, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_marza, dc.mp_marza
				FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.kolicina > 0 AND dc.partner_id <> (select int_data from options where options_id=3008)) dcn
				WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");
		}

		if(DB::table('options')->where('options_id',3008)->pluck('int_data')!=0){
			// //dobavljac_cenovnik update (preracunavanje cena) za artikle odredjenog dobavljaca
			// DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena=ROUND((cena_nc+(cena_nc*web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)), mpcena=ROUND((cena_nc+(cena_nc*mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)) WHERE partner_id=(select int_data from options where options_id=3008) AND flag_zakljucan=0");			

			//roba update za artikle odredjenog dobavljaca prati se i najniza cena
			DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, web_cena= CASE WHEN (select int_data from options where options_id=3012) = 1 THEN CASE WHEN (select int_data from options where options_id=3007) = 0 THEN ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				ELSE dcn.pmp_cena END ELSE dcn.web_cena END, 
				web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = CASE WHEN (select int_data from options where options_id=3012) = 1 THEN ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) ELSE dcn.mpcena END
				FROM (SELECT
				CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
				ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
				ELSE
				ROW_NUMBER() OVER (PARTITION BY roba_id)
				END 
				AS r,
				dc.roba_id, dc.partner_id, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_cena, dc.web_marza, dc.mp_marza
				FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id=(select int_data from options where options_id=3008)) dcn
				WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");
		}

	}



	//Azurira i cene i lager (short import)
	public static function autoQueryExecute(){
	//dobavljac cenovnik update flag prikazi u cenovniku
		DB::statement("UPDATE dobavljac_cenovnik dc SET flag_prikazi_u_cenovniku = CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND web_cena>0 THEN 1 ELSE 0 END ELSE dc.flag_prikazi_u_cenovniku END WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id IN (SELECT partner_id FROM dobavljac_cenovnik_kolone WHERE auto_import_short=1)");


		//roba update za sve artikle samo ako se prati najniza cena
		DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, flag_prikazi_u_cenovniku=dcn.flag_prikazi_u_cenovniku, web_cena= 
			CASE WHEN (select int_data from options where options_id=3007) = 0 
			THEN 
					CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1
					THEN
						ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
					ELSE
						dcn.web_cena
					END
			ELSE 
				dcn.pmp_cena 
			END, 
			web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = 
			CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1 
				THEN 
					ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				ELSE
					dcn.mpcena
				END
			FROM (SELECT
			CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
			ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
			ELSE
			ROW_NUMBER() OVER (PARTITION BY roba_id)
			END 
			AS r,
			dc.roba_id, dc.partner_id, CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND dc.cena_nc>0 THEN 1 ELSE 0 END ELSE (select flag_prikazi_u_cenovniku from roba where roba_id = dc.roba_id) END AS flag_prikazi_u_cenovniku, dc.web_cena, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_marza, dc.mp_marza
			FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id <> (select int_data from options where options_id=3008)) dcn
			WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");

		//roba update za artikle sa kolicinom vecom od 0  prati se i najniza cena
		if(DB::table('options')->where('options_id',3010)->pluck('int_data')==1){
			DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, flag_prikazi_u_cenovniku=dcn.flag_prikazi_u_cenovniku, web_cena= 
				CASE WHEN (select int_data from options where options_id=3007) = 0 
				THEN 
					CASE WHEN (select preracunavanje_cena from partner where partner_id = dcn.partner_id) = 1
					THEN
						ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
					ELSE
						dcn.web_cena
					END 
				ELSE dcn.pmp_cena END, 
				web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				FROM (SELECT
				CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
				ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
				ELSE
				ROW_NUMBER() OVER (PARTITION BY roba_id)
				END 
				AS r,
				dc.roba_id, dc.partner_id, CASE WHEN (select int_data from options where options_id=3011) = 1 THEN CASE WHEN (select sum(kolicina) from dobavljac_cenovnik where roba_id = dc.roba_id) > 0 AND dc.cena_nc>0 THEN 1 ELSE 0 END ELSE (select flag_prikazi_u_cenovniku from roba where roba_id = dc.roba_id) END AS flag_prikazi_u_cenovniku, dc.web_cena, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_marza, dc.mp_marza
				FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.kolicina > 0 AND dc.partner_id <> (select int_data from options where options_id=3008)) dcn
				WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");
		}

		if(DB::table('options')->where('options_id',3008)->pluck('int_data')!=0){
			// //dobavljac_cenovnik update (preracunavanje cena) za artikle odredjenog dobavljaca
			// DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena=ROUND((cena_nc+(cena_nc*web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)), mpcena=ROUND((cena_nc+(cena_nc*mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)) WHERE partner_id=(select int_data from options where options_id=3008) AND flag_zakljucan=0");			

			//roba update za artikle odredjenog dobavljaca prati se i najniza cena
			DB::statement("UPDATE roba r SET dobavljac_id=dcn.partner_id, web_cena= CASE WHEN (select int_data from options where options_id=3012) = 1 THEN CASE WHEN (select int_data from options where options_id=3007) = 0 THEN ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) 
				ELSE dcn.pmp_cena END ELSE dcn.web_cena END, 
				web_marza = dcn.web_marza, mp_marza = dcn.mp_marza, racunska_cena_nc = dcn.cena_nc, mpcena = CASE WHEN (select int_data from options where options_id=3012) = 1 THEN ROUND((dcn.cena_nc+(dcn.cena_nc*dcn.mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) ELSE dcn.mpcena END
				FROM (SELECT
				CASE WHEN (select int_data from options where options_id=3009) = 1 THEN 
				ROW_NUMBER() OVER (PARTITION BY roba_id ORDER BY cena_nc ASC)
				ELSE
				ROW_NUMBER() OVER (PARTITION BY roba_id)
				END 
				AS r,
				dc.roba_id, dc.partner_id, dc.cena_nc, dc.mpcena, dc.pmp_cena, dc.web_cena, dc.web_marza, dc.mp_marza
				FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND dc.partner_id=(select int_data from options where options_id=3008)) dcn
				WHERE dcn.r = 1 AND r.roba_id=dcn.roba_id AND r.flag_zakljucan='false'");
		}




		//lager update
		DB::statement("UPDATE lager l SET kolicina=dcn.kolicina 
			FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn
			WHERE l.roba_id=dcn.roba_id AND l.orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1) AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0)");
		//lager insert
		DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) SELECT roba_id, (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1), (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0), SUM(kolicina) FROM dobavljac_cenovnik dc WHERE dc.roba_id<>-1 AND dc.flag_zakljucan=0 AND roba_id NOT IN (SELECT roba_id FROM lager WHERE orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1) AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0)) GROUP BY dc.roba_id HAVING SUM(kolicina) > 0");
	}

	public static function checkPartner($partner_id,$partner_var){
		$check = false;
		$import_naziv = DB::table('dobavljac_cenovnik_kolone')->where('partner_id',$partner_id)->pluck('import_naziv_web');
		if(isset($import_naziv)){	

			$podrzan_import = DB::table('podrzan_import')->where(array('vrednost_kolone'=>$import_naziv, 'dozvoljen'=>1))->first();
		
			if($partner_var == $import_naziv && !is_null($podrzan_import)){
				$check = true;
			}
		}
		return $check;
	}

	public static function xls_to_xlsx($tmp_name,$destination){
        try {
            $reader = PHPExcel_IOFactory::createReader('Excel5');
            $workbook = $reader->load($tmp_name);
            $writer = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
            $writer->save($destination);
            return true;
        } catch (Exception $e) {
            return false;
        }		
	}
	public static function sendMail($body,$partner = null){
		$subject = 'Automatika - '.AdminOptions::company_name();
    	$content = $body;

		Mailer::send(AdminOptions::company_email(),'automatika@tico.rs', $subject, $content);
    
	}

	public static function sendMailAll($body){
		$subject = 'Import report ('.AdminOptions::company_name().')';

		if(AdminOptions::gnrl_options(3003)){ 
	        Mail::send('admin.email.import_error', array('body'=>$body), function($message) use ($subject)
	        {
	            $message->from('automatika@tico.rs','Server');
	            $message->to('automatika@tico.rs')->subject($subject);
	        }); 
	    }else{
	    	$content = View::make('admin.email.import_error',array('body'=>$body))->render();
			Mailer::send(AdminOptions::company_email(),'automatika@tico.rs', $subject, $content);
	    }
	}

	public static function reload_xml($file){
		$file = str_replace(AdminOptions::base_url(),'',$file);
		$str=file_get_contents($file);
		$str=preg_replace('/(<\?xml[^?]+?)Windows-1250/i', '$1utf-8', $str);
		file_put_contents($file, $str);
		return simplexml_load_file($file);
	}
}
