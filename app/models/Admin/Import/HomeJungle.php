<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class HomeJungle {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/homejungle/homejungle_excel/homejungle.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('D'.$row)->getValue() * 0.85 / 1.2;
				$opis = $worksheet->getCell('G'.$row)->getValue();
				$karak = $worksheet->getCell('F'.$row)->getValue();
				$cena = $worksheet->getCell('D'.$row)->getValue();

				if(!empty($sifra) && empty($naziv) && empty($cena_nc) && empty($opis)) {
					$grupa = substr($sifra, 3);
				} else {

					if(!isset($grupa)) {
						$grupa = '';
					}

					if(isset($sifra) && isset($naziv) && isset($cena_nc)){
						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
						$sPolja .= " grupa,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
						$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv) . " ( " . Support::encodeTo1250($sifra)) . " ) " . "',";
						$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1, 2,'.','') . ",";
						$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " pmp_cena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " web_cena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " opis";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($opis)) . "\n". addslashes(Support::encodeTo1250($karak)) ." '";

						// $karakteristike = explode("\n", $karak);
						// foreach ($karakteristike as $row){
						// 	DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
						// 		."VALUES(".$dobavljac_id."," . Support::quotedStr($sifra) . ",".Support::quotedStr(addslashes(Support::encodeTo1250(str_replace('"', '', explode(" ", $row)[0])))).","
						// 		." ".Support::quotedStr(addslashes(Support::encodeTo1250($value))).") ");
						// 	$flag_opis_postoji = "1";
						// }


						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		
					}
				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/homejungle/homejungle_excel/homejungle.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	        	$sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('D'.$row)->getValue() * 0.8 * 0.85;
				$cena = $worksheet->getCell('D'.$row)->getValue();


				if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc)){
					if(!isset($pmp_cena) || !is_numeric($pmp_cena)){
						$pmp_cena = 0;
					}
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1, 2,'.','') . ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " web_cena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($cena),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}