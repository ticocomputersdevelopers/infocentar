<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Westaco {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/westaco/westaco_excel/westaco.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        $kat='';
	        for ($row = 5; $row <= $lastRow; $row++) {
	        	$grupa = $worksheet->getCell('A'.$row)->getValue();
	            $sifra = $worksheet->getCell('E'.$row)->getValue();
	            $proizvodjac = $worksheet->getCell('C'.$row)->getValue();
	            $barcode = $worksheet->getCell('B'.$row)->getValue();
	            $opis = $worksheet->getCell('F'.$row)->getValue();
	            $cena1 = $worksheet->getCell('G'.$row)->getValue();
				$cena2 = $worksheet->getCell('H'.$row)->getValue();

				if(!empty($grupa) && empty($cena1) && empty($barcode) && empty($sifra) ){
					$kat = $grupa;
				}

				if(isset($barcode) && isset($sifra) && isset($proizvodjac) ){					
						
					$cena1 = str_replace("NETO", "", $cena1);
					$cena1 = str_replace(",00", "", $cena1);
					$cena1 = str_replace(",", "", $cena1);

					$cena2 = str_replace("NETO", "", $cena2);
					$cena2 = str_replace(",00", "", $cena2);
					$cena2 = str_replace(",", "", $cena2);

					$kat = str_replace("NOVO    NOVO     NOVO     NOVO     NOVO     NOVO     NOVO     NOVO     NOVO     NOVO     NOVO     NOVO  ", "", $kat);
					$kat = str_replace("- SVE BATERIJE SU 119,00/ KOM","",$kat);
					if(empty($kat)){
						$kat = 'VENTILATOR';
					}

					// $naz = str_replace(array("ORI","ERI","ATI","LERI","JKE","ICE","ŠOI","SKE","VAGE","BOCE","POKLOPCI","PARNE","PEGLE","AKE"),array("R","ER","AT","R","JKA","ICA","ŠO","SKA","VAGA","BOCA","POKLOPAC","PARNA","PEGLA","KA"),$kat);
					$proiz_vel=strtoupper($proizvodjac);
					if($proiz_vel == 'CLIP SONIC'){
						$proiz_vel = 'DOMOCLIP';
					}
					if($proiz_vel == 'CN CONTINENTAL'){
						$proiz_vel = 'CONTINENTAL';
					}
					if($proiz_vel == 'ESPERANZA'){
						$proiz_vel = 'ESPERANZA%20/ESPERANZA%201/';
					}
					if($sifra == 'AR5E16BI'){
						$slika = 'http://cosmonet.rs/image/cache/catalog/Arddes/5e1616bi-1000x1000.jpg';
					}else{
						$slika = "http://cosmonet.rs/image/cache/catalog/".$proiz_vel."/".$sifra."-1000x1000.jpg";

					$url = $slika;
					$array = get_headers($url);
					$string = $array[0];

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . Support::encodeTo1250($sifra) ."',";

					$sPolja .= "naziv,";					$sVrednosti .= "'". Support::encodeTo1250($proizvodjac)." " .Support::encodeTo1250($kat). " " .Support::encodeTo1250($sifra). "',";
					$sPolja .= "proizvodjac,";		        $sVrednosti .= "'" . Support::encodeTo1250($proizvodjac) ."',";
					$sPolja .= "grupa,";		        	$sVrednosti .= "'" . Support::encodeTo1250($kat) ."',";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";
					if(strpos($string,"200")) {
					$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "1,";	
					}
					//$sPolja .= "barcode,";					$sVrednosti .= "" . $barcode . ",";
					$sPolja .= "cena_nc,";					$sVrednosti .= " " . $cena1 . ",";
					if($cena2== 'KOM'){
					$sPolja .= "pmp_cena";					$sVrednosti .= " " . $cena1 . "";	
					}else{
					$sPolja .= "pmp_cena";					$sVrednosti .= " " . $cena2 . "";
					}

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					if(strpos($string,"200")){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$url."',1 )");		
				}

				}
			}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			$products_file = "files/westaco/westaco_excel/westaco.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        $naziv = '';
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = trim($worksheet->getCell('C'.$row)->getValue());
	            $naziv_check = trim($worksheet->getCell('B'.$row)->getValue());
	            $proizvodjac = trim($worksheet->getCell('D'.$row)->getValue());
	            $model = trim($worksheet->getCell('E'.$row)->getValue());
				$cena1 = trim($worksheet->getCell('H'.$row)->getValue());
				$cena2 = trim($worksheet->getCell('I'.$row)->getValue());

				if($naziv_check != ''){
					$naziv = $naziv_check;
				}

				if(isset($naziv) && isset($proizvodjac) && isset($model) && isset($sifra) && isset($cena1) && is_numeric($cena1) && isset($cena2) && is_numeric($cena2)){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";
					$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena1*0.95 . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= "" . $cena2 . "";
			

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	

				}
			}

			//Support::queryShortExecute($dobavljac_id);
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function lower($string){
		$firstletter = str_split($string)[0];
		$lower = mb_strtolower($string);
		return $firstletter.substr($lower,1);
	}

}