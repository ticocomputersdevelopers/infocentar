<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class CobraToys {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/cobratoys/cobratoys_excel/cobratoys.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = trim($worksheet->getCell('A'.$row)->getValue());
	            $naziv = trim($worksheet->getCell('B'.$row)->getValue());
				$cena = trim($worksheet->getCell('D'.$row)->getValue());
				// $cena2 = trim($worksheet->getCell('I'.$row)->getValue());
				$kolicina = trim($worksheet->getCell('C'.$row)->getValue());

				// if(isset($cena1) && is_numeric($cena1)){
				// 	$cena = $cena1;
				// }
				// elseif(isset($cena2) && is_numeric($cena2)){
				// 	$cena = $cena2;
				// }
				// else{
				// 	$cena = null;
				// }

				if(isset($sifra) && isset($naziv) && isset($cena)){
					if(isset($kolicina) && is_numeric($kolicina)){
						$kolicina = intval($kolicina);
					}else{
						$kolicina = 0.00;
					}

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250(Support::convertChars($sifra))) ."',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250(Support::convertChars($naziv))) ." ( ".addslashes(Support::encodeTo1250(Support::convertChars($sifra)))." )',";
					$sPolja .= "kolicina,";					$sVrednosti .= "".$kolicina.",";		
					// $sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena*1.2 . ",";
					// $sPolja .= "web_cena,";					$sVrednosti .= "" . $cena*1.2 . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena - (($cena*20)/100) . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			$products_file = "files/cobratoys/cobratoys_excel/cobratoys.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = trim($worksheet->getCell('B'.$row)->getValue());
	            $naziv = trim($worksheet->getCell('C'.$row)->getValue());
				$cena1 = trim($worksheet->getCell('G'.$row)->getValue());
				$cena2 = trim($worksheet->getCell('I'.$row)->getValue());
				$kolicina = trim($worksheet->getCell('E'.$row)->getValue());

				if(isset($cena1) && is_numeric($cena1)){
					$cena = $cena1;
				}
				elseif(isset($cena2) && is_numeric($cena2)){
					$cena = $cena2;
				}
				else{
					$cena = null;
				}

				if(isset($sifra) && isset($naziv) && isset($cena)){
					if(isset($kolicina) && is_numeric($kolicina)){
						$kolicina = intval($kolicina);
					}else{
						$kolicina = 0.00;
					}

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "kolicina,";					$sVrednosti .= "".$kolicina.",";		
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $cena*1.2 . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . $cena*1.2 . ",";
					$sPolja .= "mpcena";					$sVrednosti .= "" . $cena*1.2 . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
				}
			}
			//Support::queryShortExecute($dobavljac_id, $type);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}