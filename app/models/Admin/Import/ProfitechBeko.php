<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class ProfitechBeko {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/profitechbeko/profitech_excel/profitech.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {

	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $kategorija = $worksheet->getCell('D'.$row)->getValue();
	            $model = $worksheet->getCell('G'.$row)->getValue();
	            $opis = $worksheet->getCell('H'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('N'.$row)->getFormattedValue();
	            $pmp_cena = $worksheet->getCell('M'.$row)->getValue();
	            $mp_cena = $worksheet->getCell('I'.$row)->getValue();
	            $opis = $worksheet->getCell('H'.$row)->getValue();

	            $cena_nc=str_replace(",", "", $cena_nc);
	            $opis = str_replace(",", '<br />', $opis);
	            $opis = ucfirst($opis);

	            if(isset($sifra) && isset($kategorija) && isset($mp_cena) && is_numeric($mp_cena)){

	            	$proizvodjac = "Beko";

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)). " " . addslashes(Support::encodeTo1250($model)) . " " . addslashes(Support::encodeTo1250($kategorija)) . " ',";	
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
					$sPolja .= " grupa,";			     	$sVrednosti .= " '" . Support::encodeTo1250($kategorija). "',";
					$sPolja .= " model,";			     	$sVrednosti .= "'" . $model . "',";
					$sPolja .= " opis,";			     	$sVrednosti .= " '" . Support::encodeTo1250($opis). "',";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). ",";	
					if(isset($pmp_cena)){				
					$sPolja .= " pmp_cena";				    $sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmp_cena,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					}else{
					$sPolja .= " pmp_cena";				    $sVrednosti .= " " . number_format(Support::replace_empty_numeric($mp_cena,1,$kurs,$valuta_id_nc),2, '.', ''). "";	
					}
					  
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
				}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/profitech/profitech_excel/profitech.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('D'.$row)->getValue();
	            $mpcena = $worksheet->getCell('C'.$row)->getValue();

	            if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc) && isset($mpcena) && is_numeric($mpcena)){
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " mpcena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}