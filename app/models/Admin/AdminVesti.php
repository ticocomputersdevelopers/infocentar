<?php

class AdminVesti {

	public static function getVesti($active=null){
		if($active==null){
			$vesti = DB::table('web_vest_b2c')->orderBy('rbr', 'desc')->orderBy('datum', 'dsc')->paginate(20);
		}elseif($active==1 || $active==0){
			$vesti = DB::table('web_vest_b2c')->where('aktuelno',$active)->orderBy('rbr', 'desc')->orderBy('datum', 'dsc')->paginate(20);
		}
		return $vesti;
	}

	public static function findTitle($web_vest_b2c_id) {
		$jezik = DB::table('web_vest_b2c_jezik')->where('web_vest_b2c_id', $web_vest_b2c_id)->orderBy('jezik_id', 'asc')->first();
		if(!is_null($jezik)){
			return $jezik->naslov;
		}
		return '';
	}
    	public static function findB2BTitle($web_vest_b2b_id) {
		$jezik = DB::table('web_vest_b2b_jezik')->where('web_vest_b2b_id', $web_vest_b2b_id)->orderBy('jezik_id', 'asc')->first();
		if(!is_null($jezik)){
			return $jezik->naslov;
		}
		return '';
	}
	public static function find($web_vest_b2c_id, $column) {
		$info = DB::table('web_vest_b2c')->where('web_vest_b2c_id', $web_vest_b2c_id)->pluck($column);
		return $info;
	}

	public static function news_link($web_vest_b2c_id,$lang=null){
		$link = AdminOptions::base_url();
		if(AdminLanguage::multi()){
			if(is_null($lang)){
				$lang = AdminLanguage::lang();
			}
			$link .= $lang.'/';
		}
		
        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        if(is_null($jezik_id)){
        	$jezik_id = DB::table('jezik')->where(array('izabrani'=>1))->pluck('jezik_id');
        }
        $naslov = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$web_vest_b2c_id, 'jezik_id'=>$jezik_id))->pluck('naslov');
		return $link .= AdminOptions::convert_url('blog',$lang).'/'.AdminOptions::url_convert($naslov);
	}


}