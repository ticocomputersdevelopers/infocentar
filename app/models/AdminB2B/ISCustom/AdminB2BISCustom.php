<?php
namespace ISCustom;
use ISTables\CategoriesExcel;
use AdminB2BIS;

use PHPExcel;
use PHPExcel_IOFactory;
use DB;


use ISCustom\CategoriesExcelNiansa;
use ISCustom\ManufacturersExcelNiansa;
use ISCustom\PoreskeStopeExcelNiansa;
use ISCustom\JediniceMereExcelNiansa;
use ISCustom\ArticlesExcelNiansa;
use ISCustom\LagerExcelNiansa;
use ISCustom\PartnersExcelNiansa;
use All;

class AdminB2BISCustom {

	public static function xlsx_exe(){
		$messages = array();
		AdminB2BIS::files_exists_messages($messages,'xlsx');
		if(count($messages)>0){
			return $messages;
		}
		$categories = CategoriesExcelNiansa::table_body();
		$manufacturers = ManufacturersExcelNiansa::table_body();
		$poreske_stope = PoreskeStopeExcelNiansa::table_body();
		$jedinice_mere = JediniceMereExcelNiansa::table_body();
		$articles = ArticlesExcelNiansa::table_body();
		$partners = PartnersExcelNiansa::table_body();

		$validation_messages = self::validation_is($categories->ids,$manufacturers->ids,$poreske_stope->ids,$jedinice_mere->ids,$partners->codes,$articles->codes,$articles->category_ids,$articles->manufacturer_ids,$articles->poreske_stope_ids,$articles->jedinice_mere_ids);
		if(count($validation_messages)>0){
			return $validation_messages;
		}
		else{
			try{

				LagerExcelNiansa::truncate_lager();
				// ArticlesExcelNiansa::query_delete($articles->body);	
				// // partneri	
				// AdminB2BIS::query_delete('tarifna_grupa',$poreske_stope->body);
				// AdminB2BIS::query_delete('jedinica_mere',$jedinice_mere->body);
				// AdminB2BIS::query_delete('proizvodjac',$manufacturers->body);
				// AdminB2BIS::query_delete('grupa_pr',$categories->body);

				CategoriesExcelNiansa::query_insert_update('grupa_pr',$categories->body,array(),false);
				AdminB2BIS::query_insert_update('proizvodjac',$manufacturers->body,array(),true);
				AdminB2BIS::query_insert_update('tarifna_grupa',$poreske_stope->body,array(),false);
				AdminB2BIS::query_insert_update('jedinica_mere',$jedinice_mere->body,array(),false);
				PartnersExcelNiansa::query_insert_update($partners->body);
				ArticlesExcelNiansa::query_insert_update($articles->body);
				LagerExcelNiansa::query_insert_update(LagerExcelNiansa::table_body()->body);
	
			}
            catch(Exception $e){
                DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
                $messages[] = $e->getMessage();
            }
			return $messages;
		}
	}

	public static function validation_is(array $category_ids, array $manufacturer_ids, array $poreske_stope_ids, array $jedinice_mere_ids, array $partner_codes, array $article_codes, array $article_category_ids, array $article_manufacturer_ids, array $article_poreske_stope_ids, array $article_jedinice_mere_ids){
		$messages = array();
		$category_duplicates = AdminB2BIS::unique_ids($category_ids);
		if(count($category_duplicates)>0){
			$messages[] = 'Šifra kategorije nije unikatna! '.implode(',',array_keys($category_duplicates));
		}
		$manufacturer_duplicates = AdminB2BIS::unique_ids($manufacturer_ids);
		if(count($manufacturer_duplicates)>0){
			$messages[] = 'Šifra proizvođača nije unikatna! '.implode(',',array_keys($manufacturer_duplicates));
		}
		$article_duplicates = AdminB2BIS::unique_ids($article_codes);
		if(count($article_duplicates)>0){
			$messages[] = 'Šifra artikla nije unikatna! '.implode(',',array_keys($article_duplicates));
		}
		$poreske_stope_duplicates = AdminB2BIS::unique_ids($poreske_stope_ids);
		if(count($poreske_stope_duplicates)>0){
			$messages[] = 'Šifra poreske stope nije unikatna! '.implode(',',array_keys($poreske_stope_duplicates));
		}
		$jedinice_mere_duplicates = AdminB2BIS::unique_ids($jedinice_mere_ids);
		if(count($jedinice_mere_duplicates)>0){
			$messages[] = 'Šifra jedinice mere nije unikatna! '.implode(',',array_keys($jedinice_mere_duplicates));
		}
		$partner_duplicates = AdminB2BIS::unique_ids($partner_codes);
		if(count($partner_duplicates)>0){
			$messages[] = 'Šifra partnera nije unikatna! '.implode(',',array_keys($partner_duplicates));
		}

		$cat_diff = array_diff($article_category_ids,$category_ids);
		if(count($cat_diff)>0){
			$messages[] = 'Šifra kategorije ne postoji! '.implode(',',array_unique($cat_diff));
		}
		$mnf_diff = array_diff($article_manufacturer_ids,$manufacturer_ids);
		if(count($mnf_diff)>0){
			$messages[] = 'Šifra proizvođača ne postoji! '.implode(',',array_unique($mnf_diff));
		}
		$po_sto_diff = array_diff($article_poreske_stope_ids,$poreske_stope_ids);
		if(count($po_sto_diff)>0){
			$messages[] = 'Šifra poreske stope ne postoji! '.implode(',',array_unique($po_sto_diff));
		}
		$jed_mer_diff = array_diff($article_jedinice_mere_ids,$jedinice_mere_ids);
		if(count($jed_mer_diff)>0){
			$messages[] = 'Šifra jedinice mere ne postoji! '.implode(',',array_unique($jed_mer_diff));
		}

		return $messages;
	}


	public static function import_partner_mesto() {

		$products_file = "files/partneri_nijansa.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		for ($row = 2; $row <= $lastRow; $row++) {
		    $sifra = $worksheet->getCell('A'.$row)->getValue();
		    $mesto_id = $worksheet->getCell('B'.$row)->getValue();
		    

		    if(isset($sifra) && isset($mesto_id)){
				DB::table('partner')->where('sifra', $sifra)->update(['mesto_id' => $mesto_id]);
	    	}

		}

	}

}