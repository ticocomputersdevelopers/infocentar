<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsInfograf\DBData;
use IsInfograf\Support;
use IsInfograf\Group;
use IsInfograf\Manufacturer;
use IsInfograf\Article;
use IsInfograf\Stock;
use IsInfograf\Partner;
use IsInfograf\Course;
use IsInfograf\Image;
use IsInfograf\SendArticleData;


class AdminIsInfograf {

    public static function execute(){
        try {

            //kurs
            $courses = DBData::courses();
            Course::query_insert_update($courses);

            //groups
            $groups = DBData::groups();
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body,array('grupa','parrent_grupa_pr_id'));
            Group::query_update_unexists($resultGroup->body);
            Support::updateGroupsParent($groups);

            //manufacturers
            $manufacturers = DBData::manufacturers();
            $resultManufacturer = Manufacturer::table_body($manufacturers);
            Manufacturer::query_insert_update($resultManufacturer->body,array('naziv'));

            //articles
            if(AdminB2BOptions::info_sys('infograf')->b2b_magacin){
                $articles = DBData::articles(AdminB2BOptions::info_sys('infograf')->b2b_magacin);
                $resultArticle = Article::table_body($articles);
                Article::query_insert_update($resultArticle->body,array('naziv','naziv_dopunski','grupa_pr_id','jedinica_mere_id','proizvodjac_id','naziv_displej','naziv_web','web_opis','barkod','racunska_cena_nc','racunska_cena_end','akcija_flag_primeni','flag_prikazi_u_cenovniku','flag_cenovnik'));
                Article::query_update_unexists($resultArticle->body);

                // DB::statement("UPDATE roba r SET web_cena = ROUND((racunska_cena_nc+(racunska_cena_nc*web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)), mpcena = ROUND((racunska_cena_nc+(racunska_cena_nc*mp_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) WHERE flag_zakljucan='false'");
                DB::statement("UPDATE roba r SET web_cena = ROUND((racunska_cena_nc+(racunska_cena_nc*web_marza/100))*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1)) WHERE flag_zakljucan='false'");
            }

            //b2b stock
            if(AdminB2BOptions::info_sys('infograf')->b2b_magacin){
                $stock = DBData::stock(AdminB2BOptions::info_sys('infograf')->b2b_magacin);
                $resultStock = Stock::table_body($stock);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if(AdminB2BOptions::info_sys('infograf')->b2c_magacin){
                $stock = DBData::stock(AdminB2BOptions::info_sys('infograf')->b2c_magacin);
                $resultStock = Stock::table_body($stock,1);
                Stock::query_insert_update($resultStock->body);
            }

            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners,array('id_kategorije'));
            Partner::query_insert_update($resultPartner->body);

            //images
            $images = DBData::images();
            $resultImage = Image::table_body($images);
            $new_images = Image::new_images($resultImage->body);
            Image::query_insert_update($resultImage->body);
            Image::query_update_unexists($resultImage->body);
            Support::save_image_files($images,$new_images);

            //send article data
            SendArticleData::query_insert_update();

            return (object) array('success'=>true);
        }catch (Exception $e){
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}