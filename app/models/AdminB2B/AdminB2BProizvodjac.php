<?php

class AdminB2BProizvodjac {

	public static function getManufacturers() {
		return DB::table('proizvodjac')->where('proizvodjac_id', '!=', -1)->orderBy('naziv')->get();
	}

	public static function getPartnerRabatGrupa($limit=30) {
		return DB::table('partner_rabat_grupa')
			->join('partner', 'partner_rabat_grupa.partner_id', '=', 'partner.partner_id')
			->join('grupa_pr', 'partner_rabat_grupa.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
			->join('proizvodjac', 'partner_rabat_grupa.proizvodjac_id', '=', 'proizvodjac.proizvodjac_id')
			->select('partner_rabat_grupa.*', 'partner.naziv as partner_naziv', 'grupa_pr.grupa', 'proizvodjac.naziv as proizvodjac_naziv')
			->orderBy('id')
			->paginate($limit);
	}

	// $users = DB::table('users')
 //            ->join('contacts', 'users.id', '=', 'contacts.user_id')
 //            ->join('orders', 'users.id', '=', 'orders.user_id')
 //            ->select('users.*', 'contacts.phone', 'orders.price')
 //            ->get();

	public static function getPartnerNaziv($partner_id) {
		if($partner_id == -1) {
			return 'Svi partneri';
		} else {
			return DB::table('partner')->where('partner_id', $partner_id)->pluck('naziv');
		}
	}
	public static function partnerSelect($partner_id=null){
		$render = '<option value="0"></option>';
        foreach(DB::table('partner')->select('partner_id','naziv')->where('partner_id','!=',-1)->get() as $row){

	            if($row->partner_id == $partner_id){
	                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
	            }else{
	                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
	            }
        }
        return $render;
	}

	public static function partnerPodaci($partner_id) {
		if($partner_id){
			$partner = DB::table('partner')->where('partner_id',$partner_id)->first(); 
		}else{ 
			$partner = (object) array('adresa'=>'','mesto'=>'','telefon'=>'','email'=>'');
		}
		return $partner;
	}

	public static function getProizvodjacNaziv($proizvodjac_id) {
		if($proizvodjac_id == -1) {
			return 'Svi proizvođači';
		} else {
			return DB::table('proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->pluck('naziv');
		}
	}

	public static function getGrupaNaziv($grupa_pr_id) {
		if($grupa_pr_id == -1) {
			return 'Sve grupe';
		} else {
			return DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('grupa');
		}
	}

	public static function getLevelGroups($grupa_pr_id) {
		return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
	}

	public static function getGroups($grupa_pr_id = null) {
        $render = '<option value="-1">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
	}

	public static function getPartners() {
		return DB::table('partner')->orderBy('partner_id', 'ASC')->get();
	}

}