<?php

class AdminB2BSupport {

    public static function getProizvodjaci()
    {
        return DB::table('proizvodjac')->select('proizvodjac_id', 'naziv')->where('proizvodjac_id', '<>', -1)->where('proizvodjac_id', '<>', 0)->orderBy('proizvodjac_id', 'ASC')->get();
    }

    public static function getDobavljaci()
    {
        return DB::table('partner')->select('partner_id', 'naziv')->where('partner_id', '<>', -1)->orderBy('naziv', 'ASC')->get();
    }
    public static function find_tarifna_grupa($tarifna_grupa_id, $column)
    {
        return DB::table('tarifna_grupa')->select($column)->where('tarifna_grupa_id', $tarifna_grupa_id)->pluck($column);
    }
    public static function dobavljac($id) 
    {
      return DB::table('partner')->where('partner_id', $id)->pluck('naziv');
    }
    public static function lagerDob($roba_id){
        $kol_fr = DB::table('dobavljac_cenovnik')->select('kolicina')->where('roba_id',$roba_id)->orderBy('kolicina','desc')->orderBy('web_cena','asc')->first();
        if($kol_fr != null){
            return intval($kol_fr->kolicina);
        }else{
            return '';
        }
    }

    //funkcija za vracanje kategorije
    public static function getKategorije()
    {
        return DB::table('partner_kategorija')->where('id_kategorije','!=',-1)->get();
    }
    
  //izvuci iz kategorije
    public static function find_u_kategoriji($id_kategorije, $column)
    {
        return DB::table('partner_kategorija')->select($column)->where('id_kategorije', $id_kategorije)->pluck($column);
    }
    public static function getB2BPartner($id, $column){
        $partner = DB::table('web_b2b_kartica')->where('partner_id', $id)->pluck($column);
        return $partner;
    }
    public static function getVesti($active=null){
        if($active==null){
            $vesti = DB::table('web_vest_b2b')->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }elseif($active==1 || $active==0){
            $vesti = DB::table('web_vest_b2b')->where('aktuelno',$active)->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }
        return $vesti;
    }
    public static function find($web_vest_b2b_id, $column) {
        $info = DB::table('web_vest_b2b')->where('web_vest_b2b_id', $web_vest_b2b_id)->pluck($column);
        return $info;
    }
    public static function pages_menu_b2b_show($flag_b2b_show){

        switch($flag_b2b_show){
            case"0":
                echo '  
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" > 
                        <span class="label-text">Header meniju</span>
                    </label>

                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            case"1":
                echo '  
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;

            case"2":
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu"  >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            case"3":
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            default:
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
        }

    }
     public static function upload_directory(){
        $dir    = "./images/upload";
        //return $dir;
        $files = scandir($dir);
            $slike="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1){
       
           $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='javascript:void(0)' title='Obriši' data-url='/images/upload/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
           }
           
        
            $br+=1;
        }
        $slike.="</ul>";
        return $slike;
    }
    public static function query_gr_level($grupa_pr_id)
    {
    return DB::select("SELECT grupa_pr_id, parrent_grupa_pr_id, grupa, redni_broj, rabat FROM grupa_pr WHERE parrent_grupa_pr_id = ".$grupa_pr_id." ORDER BY redni_broj ASC");
    }

    public static function searchPartneri($word){
        $search = DB::table('partner')->
            where('partner_id', '!=', -1)->
            where('naziv', 'ILIKE', "%$word%")->
            orWhere('pib', 'ILIKE', "%$word%")->
            orderBy('partner_id', 'ASC')->
            paginate(20);
        return $search;
    }

    public static function quotedStr($string){
        $string = str_replace('\'', '', $string);
        $string = str_replace("'", "", $string);
        $string = str_replace(chr(047), "", $string);
        // $string = str_replace(chr(057), "", $string);
        $string = str_replace(chr(134), "", $string);
        $string = chr(047).$string.chr(047);
        return $string;
    }
    public static function getB2BPartneri(){
        $partneri = DB::table('partner')->where('partner_id', '!=', -1)->orderBy('partner_id', 'ASC')->paginate(20);
        return $partneri;
    }
    public static function getB2Bkartica($id){
        $kartica = DB::table('web_b2b_kartica')->where('partner_id',$id)->get();
       
        return $kartica;
    }
    public static function getDuguje($id){
        $duguje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('duguje');
       
        return $duguje;
    }
    public static function getPotrazuje($id){
        $potrazuje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('potrazuje');
       
        return $potrazuje;
    }
    public static function getSum($id){
        $duguje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('duguje');
        $potrazuje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('potrazuje');

        $saldo = $potrazuje - $duguje;
        return $saldo;
    }
    public static function narudzbina_kupac_pdf($web_b2b_narudzbina_id){
        $partner=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('partner_id');

        foreach(DB::table('partner')->where('partner_id',$partner)->get() as $row){

            $name = '<td>Firma i PIB:</td>
            <td>'.$row->naziv.' '.$row->pib.'</td>';
        

        echo ' 
            <table>
            <tr>
            '.$name.'
            </tr>
            <tr>
            <td>Adresa:</td>
            <td>'.$row->adresa.'</td>
            </tr>
            <tr>
            <td>Mesto:</td>
            <td>'.$row->mesto.'</td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td>'.$row->telefon.'</td>
            </tr>
            
            </table>';
        }
    }
    
    public static function page_link_b2b($naziv_stranice){
        Session::put('b2b_user_'.Options::server(),1);
        return AdminB2BOptions::base_url().'b2b/'.$naziv_stranice;
    }

}
