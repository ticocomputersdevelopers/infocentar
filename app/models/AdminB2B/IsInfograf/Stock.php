<?php
namespace IsInfograf;
use DB;

class Stock {

	public static function table_body($articles,$stockroom=4){

		$result_arr = array();
		$roba_id = -1;
		foreach($articles as $article) {
    		$roba_id--;

			$sifra = $article->idartikal;
			$kolicina = $article->kol;
			if(intval($article->kol) < 0){
				$kolicina = 0;
			}

			$result_arr[] = "(".strval($roba_id).",".intval($stockroom).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}
		DB::statement("UPDATE lager t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=lager_temp.sifra_is::varchar AND t.orgj_id=lager_temp.orgj_id");

		//insert
		DB::statement("INSERT INTO lager (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM lager t WHERE t.sifra_is=lager_temp.sifra_is::varchar AND t.orgj_id=lager_temp.orgj_id))");

		DB::statement("UPDATE lager l SET roba_id = roba.roba_id FROM roba WHERE l.sifra_is=roba.id_is");
		DB::statement("DELETE FROM lager WHERE roba_id < 0");

	}

}