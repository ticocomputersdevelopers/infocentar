<?php
namespace IsInfograf;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
	    	$partner_id++;

            $id_is=$partner->id;
            $sifra= isset($partner->auto_id) ? $partner->auto_id : null;
            $naziv=isset($partner->naziv) ? pg_escape_string(substr(Support::convert($partner->naziv),0,250)) : null;
            $mesto=isset($partner->grad) ? pg_escape_string(substr(Support::convert($partner->grad),0,250)) : null;
            $adresa=isset($partner->adresa) ? pg_escape_string(substr(Support::convert($partner->adresa),0,250)) : null;
            $telefon=isset($partner->telefon) ? pg_escape_string(substr($partner->telefon,0,250)) : null;
            $pib=isset($partner->pib) ? pg_escape_string(substr($partner->pib,0,100)) : null;
            $mail=isset($partner->email) ? pg_escape_string(substr($partner->email,0,100)) : null;
            $broj_maticni=isset($partner->mat) ? pg_escape_string(substr($partner->mat,0,29)) : null;
            $racun=isset($partner->zr) ? pg_escape_string(substr($partner->zr,0,250)) : null;
            $rabat= isset($partner->frabat) ? $partner->frabat : 0;
            // $rabat= 0;
            $login=isset($partner->korisnik) ? pg_escape_string(substr(Support::convert($partner->korisnik),0,250)) : null;
            $password=isset($partner->pass) ? pg_escape_string(substr($partner->pass,0,250)) : null;

	        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim($naziv)."',0,0,1,1,0,'".$login."','".$password."','".strval($racun)."',NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::integer)";
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}