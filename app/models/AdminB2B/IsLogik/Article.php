<?php
namespace IsLogik;
use DB;

class Article {

	public static function table_body($articles){
		$result_arr = array();
		$ids_is = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		foreach($articles as $article) {
			$id_is = $article->sifra_artikla_logik;

			$roba_id++;
			$sifra_k++;
			// $sifra_is = $article->sifra_artikla_logik;
			$sifra_is = pg_escape_string(substr($article->barkod,0,30));
			$naziv = pg_escape_string(Support::convert(substr($article->naziv_artikla,0,300)));
			$web_opis = pg_escape_string(nl2br(htmlspecialchars(Support::convert($article->opis))));
			$jedinica_mere_id = Support::getJedinicaMereId($article->naziv_jedinice_mere);
			$proizvodjac_id = !is_null($article->naziv_proizvodjaca) && trim($article->naziv_proizvodjaca) != '' ? Support::getProizvodjacId(Support::convert($article->naziv_proizvodjaca)) : -1;
			$tarifna_grupa_id = Support::getTarifnaGrupaId($article->naziv_tarifne_grupe,$article->vrednost_tarifne_grupe);

			$flag_aktivan = $article->aktivan == 'y' ? '1' : '0';
			$grupa_pr_id = Support::getGrupaId($article->sifra_kategorije);
			$stara_grupa_id = !is_null($article->sporedna_grupa_id) ? "'".strval($article->sporedna_grupa_id)."'" : 'NULL';
			$racunska_cena_nc = intval($article->nabavna_cena);
			$mpcena = intval($article->maloprodajna_cena); //(1+intval($article->porez)/100)
			$web_cena = intval($article->web_cena);

			$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,1,0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,".$stara_grupa_id.",NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."')";

			$ids_is[] = $id_is;
		}
		
		return (object) array("body"=>implode(",",$result_arr),"ids_is"=>$ids_is);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar");
		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba))");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}