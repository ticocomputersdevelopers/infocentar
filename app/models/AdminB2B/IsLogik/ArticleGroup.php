<?php
namespace IsLogik;
use DB;

class ArticleGroup {

	public static function table_body($articles_groups){
		$mapped_groups = Support::getMappedGroups();
		$mapped_articles = Support::getMappedArticles();

		$result_arr = array();

		foreach($articles_groups as $article_group) {

			if(isset($mapped_groups[$article_group->sifra_kategorije_logik]) && isset($mapped_articles[$article_group->sifra_artikla_logik])){

				$result_arr[] = "(".strval($mapped_articles[$article_group->sifra_artikla_logik]).",".strval($mapped_groups[$article_group->sifra_kategorije_logik]).")";
			}

		}
		return (object) array("body"=>implode(",",$result_arr),"mapped_articles"=>$mapped_articles);
	}

	public static function query_insert_delete($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$table_temp = "(VALUES ".$table_temp_body.") roba_grupe_temp(roba_id, grupa_pr_id)";

		$where = "";
		if(DB::table('roba_grupe')->count() > 0){
			$where .= " WHERE (roba_id, grupa_pr_id) NOT IN (SELECT roba_id, grupa_pr_id FROM roba_grupe)";
		}
		//INSERT
		DB::statement("INSERT INTO roba_grupe (SELECT * FROM ".$table_temp.$where.")");
		//UPDATE
		DB::statement("DELETE FROM roba_grupe WHERE (roba_id, grupa_pr_id) NOT IN (SELECT roba_id, grupa_pr_id FROM ".$table_temp.")");
	}

}