<?php
namespace IsLogik;

use DB;
use AdminB2BOptions;

class PartnerGroup {

	public static function table_body($partner_groups){
		$result_arr = array();
		$mapped_result = array();

		$partner_group_id = DB::select("SELECT nextval('partner_rabat_grupa_id_seq')")[0]->nextval;
		foreach($partner_groups as $partner_group) {
		    $partner_group_id++;

			$sifra_kupca_logik = $partner_group->sifra_kupca_logik;
			$sifra_kategorije_logik = $partner_group->sifra_kategorije_logik;
			$rabat = $partner_group->rabat;
			$partner_id = Support::getPartnerId($sifra_kupca_logik);
			$grupa_pr_id = Support::getGrupaId($sifra_kategorije_logik);

			if(!is_null($partner_id) && !is_null($sifra_kategorije_logik) && $sifra_kategorije_logik != ''){
				$result_arr[] = "(".strval($partner_id).",".strval($grupa_pr_id).",".strval($rabat).",".strval($sifra_kupca_logik).",".strval($sifra_kategorije_logik).",-1,(NULL)::integer,".strval($partner_group_id).")";

				$mapped_result[] = (object) array('sifra_kupca_logik' => $sifra_kupca_logik, 'sifra_kategorije_logik' => $sifra_kategorije_logik, 'partner_id' => strval($partner_id), 'grupa_pr_id' => strval($grupa_pr_id));
			}
		}

		return (object) array("body"=>implode(",",$result_arr), "mapped_result" => $mapped_result);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_rabat_grupa'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_rabat_grupa_temp(".implode(',',$columns).")";
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_kupca_logic" && $col!="sifra_kategorije_logic" && $col!="id"){
		    	$updated_columns[] = "".$col." = partner_rabat_grupa_temp.".$col."";
			}
		}

		DB::statement("UPDATE partner_rabat_grupa prg SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE prg.sifra_kupca_logic=partner_rabat_grupa_temp.sifra_kupca_logic AND prg.sifra_kategorije_logic=partner_rabat_grupa_temp.sifra_kategorije_logic");

		//insert
		DB::statement("INSERT INTO partner_rabat_grupa (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner_rabat_grupa prg WHERE prg.sifra_kupca_logic=partner_rabat_grupa_temp.sifra_kupca_logic AND prg.sifra_kategorije_logic=partner_rabat_grupa_temp.sifra_kategorije_logic))");

		DB::statement("SELECT setval('partner_rabat_grupa_id_seq', (SELECT MAX(id) FROM partner_rabat_grupa), FALSE)");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			DB::statement("TRUNCATE partner_rabat_grupa");
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_rabat_grupa'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_rabat_grupa_temp(".implode(',',$columns).")";

		DB::statement("DELETE FROM partner_rabat_grupa t WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_kupca_logic=partner_rabat_grupa_temp.sifra_kupca_logic AND t.sifra_kategorije_logic=partner_rabat_grupa_temp.sifra_kategorije_logic)");
	}

}