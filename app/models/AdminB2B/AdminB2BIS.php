<?php
use ISTables\CategoriesExcel;

class AdminB2BIS {

	public static function xlsx_exe(){
		$messages = array();
		self::files_exists_messages($messages,'xlsx');
		if(count($messages)>0){
			return $messages;
		}

		$categories = CategoriesExcel::table_body();
		$manufacturers = ManufacturersExcel::table_body();
		$articles = ArticlesExcel::table_body();
		$art_cat = ArtCatExcel::table_body();

		$validation_messages = self::validation_is($categories->ids,$manufacturers->ids,$articles->ids,$articles->category_ids,$articles->manufacturer_ids,$art_cat->cat_ids,$art_cat->art_ids);
		if(count($validation_messages)>0){
			return $validation_messages;
		}
		else{
			try{

				DB::statement("TRUNCATE roba_grupe");
				self::query_delete('roba',$articles->body);		
				self::query_delete('proizvodjac',$manufacturers->body);
				self::query_delete('grupa_pr',$categories->body);

				self::query_insert_update('grupa_pr',$categories->body,array());
				self::query_insert_update('proizvodjac',$manufacturers->body,array());
				self::query_insert_update('roba',$articles->body,array());
				ArtCatExcel::query_execute($art_cat->body);
	
			}
            catch(Exception $e){
                DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
                $messages[] = $e->getMessage();
            }
			return $messages;
		}
	}

	public static function query_insert_update($table,$table_temp_body,$upd_cols=array(),$is_upd_seq) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='".$table."'"));
		$table_temp = "(VALUES ".$table_temp_body.") ".$table."_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		//update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		foreach($columns as $col){
			if($col!=$table."_id" && $col!="sifra"){
		    	$updated_columns[] = "".$col." = ".$table."_temp.".$col."";
			}
		}
		DB::statement("UPDATE ".$table." t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.".$table."_id=".$table."_temp.".$table."_id ");

		//insert
		DB::statement("INSERT INTO ".$table." (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM ".$table." t WHERE t.".$table."_id=".$table."_temp.".$table."_id))");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		if($is_upd_seq){
			DB::statement("SELECT setval('".$table."_".$table."_id_seq', (SELECT MAX(".$table."_id) FROM ".$table."), FALSE)");
		}
	}

	public static function query_delete($table,$table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='".$table."'"));
		$table_temp = "(VALUES ".$table_temp_body.") ".$table."_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM ".$table." t WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.".$table."_id=".$table."_temp.".$table."_id)");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function validation_is(array $category_ids, array $manufacturer_ids, array $article_ids, array $article_category_ids, array $article_manufacturer_ids, array $rg_cat_ids, array $rg_art_ids){
		$messages = array();
		$category_duplicates = self::unique_ids($category_ids);
		if(count($category_duplicates)>0){
			$messages[] = 'Šifra kategorije nije unikatna! '.implode(',',array_keys($category_duplicates));
		}
		$manufacturer_duplicates = self::unique_ids($manufacturer_ids);
		if(count($manufacturer_duplicates)>0){
			$messages[] = 'Šifra proizvođača nije unikatna! '.implode(',',array_keys($manufacturer_duplicates));
		}
		$article_duplicates = self::unique_ids($article_ids);
		if(count($article_duplicates)>0){
			$messages[] = 'Šifra artikla nije unikatna! '.implode(',',array_keys($article_duplicates));
		}

		$cat_diff = array_diff($article_category_ids,$category_ids);
		if(count($cat_diff)>0){
			$messages[] = 'Šifra kategorije artikla ne postoji! '.implode(',',$cat_diff);
		}

		$mnf_diff = array_diff($article_manufacturer_ids,$manufacturer_ids);
		if(count($mnf_diff)>0){
			$messages[] = 'Šifra proizvođača artikla ne postoji! '.implode(',',$mnf_diff);
		}

		$rg_cat_diff = array_diff($rg_cat_ids,$category_ids);
		if(count($rg_cat_diff)>0){
			$messages[] = 'Šifra dodatne kategorije ne postoji u kategorijama! '.implode(',',$rg_cat_diff);
		}
		$rg_art_diff = array_diff($rg_art_ids,$article_ids);
		if(count($rg_art_diff)>0){
			$messages[] = 'Šifra artikla kod dodatnih kategorija ne postoji u robi! '.implode(',',$rg_art_diff);
		}

		return $messages;
	}

	public static function unique_ids(array $ids){
		return array_filter(array_count_values($ids), function ($var) {return $var > 1;});
		// return (count($ids) !== count(array_unique($ids)));
	}

	public static function files_exists_messages(&$messages,$kind) {
        if($kind == 'xml'){
            $suffix = "xml";
        }
        elseif($kind == 'csv'){
            $suffix = "csv";
        }else{
            $suffix = "excel";
        } 		
		if(!File::exists("files/IS/".$suffix."/grupa_pr.".$kind."")){
			$messages[] = "Fajl za grupe ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/proizvodjac.".$kind."")){
			$messages[] = "Fajl za proizvođače ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/roba.".$kind."")){
			$messages[] = "Fajl za artikle ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/tarifna_grupa.".$kind."")){
			$messages[] = "Fajl za poreske stope ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/jedinica_mere.".$kind."")){
			$messages[] = "Fajl za jedinice mere ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/lager.".$kind."")){
			$messages[] = "Fajl za lager ne postoji!";
		}
		if(!File::exists("files/IS/".$suffix."/partner.".$kind."")){
			$messages[] = "Fajl za kupce ne postoji!";
		}
	}

	public static function encodeTo1250($string){
		return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		// return $string;
		
	    // $encoding = mb_detect_encoding($string, mb_detect_order(), false);
	    // if($encoding == "UTF-8"){$string = mb_convert_encoding($string,'UTF-8','UTF-8');}
	    // return iconv(mb_detect_encoding($string, mb_detect_order(), false), "UTF-8//TRANSLIT//IGNORE", $string);
	}
	
	public static function encodeToUTF8($string){
		return iconv("WINDOWS-1250", "UTF-8//TRANSLIT//IGNORE",$string);
		// return $string;
	}
}