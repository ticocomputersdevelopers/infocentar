<?php 
use \Statickidz\GoogleTranslate;

class Language {
	public static function multi(){
        return false;
		return DB::table('jezik')->where('aktivan',1)->count() > 1;
	}
	public static function segment_offset(){
		return self::multi() ? 1 : 0;
	}	
	public static function lang(){
		if(!Session::has('lang')){
            $lang = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
            Session::put('lang', $lang);
        }else{
            $lang = Session::get('lang');
        }
        return $lang;
	}
    public static function lang_id(){
        return 1;
        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
        }
        $jezik = DB::table('jezik')->where(array('kod'=>$lang, 'aktivan'=>1))->first();
        if(!is_null($jezik)){
            return $jezik->jezik_id;
        }
        return 1;
    }

    //prevodi reci za dati jezik
	public static function trans($string,$lang=null){
        return $string;
        if($lang==null){
            $lang = Language::lang();
        }

        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id');

        if($izabrani_jezik_id == $jezik_id){
        	return $string;
        }else{
        	$query =DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$string))->where('reci','!=','')->whereNotNull('reci');
        	if($query->count() > 0){
        		return $query->first()->reci;
        	}else{
        		return $string;
        		$str_arr = explode(' ',$string);
        		$trans_arr = array();
        		$not_translate = false;
        		foreach($str_arr as $str){
        			$query_str = DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$str))->where('reci','!=','')->whereNotNull('reci');
		        	if($query_str->count() > 0){
		        		$trans_arr[] = $query_str->first()->reci;
		        	}else{
		        		$not_translate = true;
		        		$trans_arr[] = $str;
		        	}
        		}
        		return implode(' ',$trans_arr);
        	}
        }
	}

    //prevodi link za dati jezik
	public static function slug_trans($string,$lang=null){
        return $string;
        if($lang==null){
            $lang = Language::lang();
        }
        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1));
        $izabrani_jezik_id = $izabrani_jezik->pluck('jezik_id');
        $izabrani_jezik_kod = $izabrani_jezik->pluck('kod');
        if($izabrani_jezik_id != $jezik_id){
        	$query =DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'izabrani_jezik_reci'=>$string))->where('reci','!=','')->whereNotNull('reci');
        	if(!is_null($query->first())){
        		$string = $query->first()->reci;
        	}
        }
        return $string;
	}

    //prevodi link iz datog jezika u defaultni link
	public static function slug_convert($string,$lang=null){
        if($lang==null){
            $lang = Language::lang();
        }
        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1));
        $izabrani_jezik_id = $izabrani_jezik->pluck('jezik_id');
        $izabrani_jezik_kod = $izabrani_jezik->pluck('kod');
        if($izabrani_jezik_id != $jezik_id){
        	$query =DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabrani_jezik_id,'jezik_id'=>$jezik_id,'reci'=>$string))->where('izabrani_jezik_reci','!=','')->whereNotNull('izabrani_jezik_reci');
        	if(!is_null($query->first())){
        		$string = $query->first()->izabrani_jezik_reci;
        	}
        }
        return $string;
	}

    public static function validator_messages(){
        $messages = array();
        if(self::lang() == 'en'){
            return $messages;
        }
        $validator_messages = DB::table('validator')->select('kod','poruka')->join('validator_jezik','validator.validator_id','=','validator_jezik.validator_id')->where(array('aktivan'=>1,'jezik_id'=>self::lang_id()))->get();
        foreach($validator_messages as $message){
            $messages[$message->kod] = $message->poruka;
        }
        return $messages;
    }

}