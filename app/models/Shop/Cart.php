<?php 
    class Cart {
    
        public static function kupac_id(){
            $kupac_id = 0;
            if(Session::has('b2c_kupac')){
                $kupac_id = Session::get('b2c_kupac');
            }
            return $kupac_id; 
        }
        public static function korpa_id(){
            if(!Session::has('b2c_korpa')){
                if(DB::table('web_options')->where('web_options_id',131)->pluck('int_data')==1 && self::kupac_id() != 0){
                    $query = DB::table('web_b2c_korpa')->where(array('web_kupac_id'=>self::kupac_id(),'naruceno'=>0))->orderBy('web_b2c_korpa_id','desc')->first();
                    if($query){
                        $korpa_id = $query->web_b2c_korpa_id;
                    }else{
                        $korpa_id = DB::select("SELECT nextval('web_b2c_korpa_web_b2c_korpa_id_seq') AS korpa_id")[0]->korpa_id;
                    }
                }else{
                    $korpa_id = DB::select("SELECT nextval('web_b2c_korpa_web_b2c_korpa_id_seq') AS korpa_id")[0]->korpa_id;
                }
                Session::put('b2c_korpa',$korpa_id);
            }
            return Session::get('b2c_korpa');
        }

        public static function kolicina_lager($roba_id){
            $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
            $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            $query = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
            $lager = 0;
            if($query){
                $lager = $query->kolicina;
            }
            return $lager;
        }

        public static function kolicina_korpa($roba_id,$korpa_id){
            $kolicina = 0;
            $query_korpa = DB::select("SELECT SUM(kolicina) as ukupno FROM web_b2c_korpa_stavka WHERE roba_id=".$roba_id." AND web_b2c_korpa_id=".$korpa_id." GROUP BY roba_id");
            if(count($query_korpa) > 0){
                $kolicina = $query_korpa[0]->ukupno;
            }
            return $kolicina;
        }

        public static function check_avaliable($roba_id){
            if(Options::vodjenje_lagera()==0){ 
                return 1000;
            }
            $korpa = 0;
            if(DB::table('web_options')->where('web_options_id',131)->pluck('int_data')==0 && Session::has('b2c_korpa')){
                $korpa =self::kolicina_korpa($roba_id,self::korpa_id());
            }

            return self::kolicina_lager($roba_id) - $korpa;
        }
        public static function add_to_cart($kupac_id,$korpa_id,$roba_id,$kolicina,array $osobine){
            $exist = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',$korpa_id)->count();
            if($exist==0){
                DB::table('web_b2c_korpa')->insert(array('web_b2c_korpa_id'=>$korpa_id,'web_kupac_id'=>$kupac_id,'datum'=>date("Y-m-d")));
            }
            self::add_stavka($korpa_id,$roba_id,$kolicina,$osobine);
        }

        public static function cart_ukupno_korpa(){
            $valuta=' rsd';
            $ukupno=0;
            if(Session::has('b2c_korpa')){
                 foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->get() as $row){
                   $ukupno+=$row->jm_cena*$row->kolicina;
                 }
            }
            $ukupno= number_format($ukupno, 2, ',' , '.');
            return $ukupno.$valuta;
        }

        public static function add_stavka($korpa_id,$roba_id,$kolicina,array $osobine){
            $query = DB::table('web_b2c_korpa_stavka')->where(array('roba_id'=>$roba_id,'web_b2c_korpa_id'=>$korpa_id));

            $first = DB::table('roba')->where('roba_id',$roba_id)->first(); 
            $data=array(
            'web_b2c_korpa_id'=>$korpa_id,
            'roba_id'=>$roba_id,
            'broj_stavke'=>1,
            'kolicina'=>$kolicina,
            'jm_cena'=>Product::get_price($roba_id,true,true),
            'tarifna_grupa_id'=>$first->tarifna_grupa_id,
            'racunska_cena_nc'=>$first->racunska_cena_nc,
            );
            if(!Product::check_osobine($roba_id)){
                if($query->count() == 0){
                    DB::table('web_b2c_korpa_stavka')->insert($data);
                }else{
                    $first = $query->first();
                    $new_kol = $first->kolicina + intval($kolicina);
                    $stavka_id = $first->web_b2c_korpa_stavka_id;
                    DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol));
                }
            }else{
                if($query->count() == 0){
                    if(count($osobine) > 0){
                        $data['osobina_vrednost_ids'] = implode('-',$osobine);
                    }
                    DB::table('web_b2c_korpa_stavka')->insert($data);
                }else{
                    if(count($osobine) <= 0){
                        $last_savka = $query->orderBy('web_b2c_korpa_stavka_id','desc')->first();
                        $stavka_id = $last_savka->web_b2c_korpa_stavka_id;
                        $new_kol = $last_savka->kolicina + intval($kolicina);
                        DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol));
                    }else{

                        $stavka_id = null;
                        foreach($query->get() as $row){
                            if($row->osobina_vrednost_ids != null && $row->osobina_vrednost_ids != ''){
                                $vrednosti = explode('-',$row->osobina_vrednost_ids);
                                if(count(array_diff($vrednosti,$osobine))==0 && count(array_diff($osobine,$vrednosti))==0){
                                    $stavka_id = $row->web_b2c_korpa_stavka_id;
                                    break;
                                }
                            }
                        }

                        if($stavka_id == null){
                            $data['osobina_vrednost_ids'] = implode('-',$osobine);
                            DB::table('web_b2c_korpa_stavka')->insert($data);
                        }else{
                            $new_kol = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('kolicina') + intval($kolicina);
                            DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol));                        
                        }
                    }
                }
            }

            self::rezervacija($roba_id,$kolicina);
        }

        public static function rezervacija($roba_id,$kolicina){
            if(Options::vodjenje_lagera()==1 && DB::table('web_options')->where('web_options_id',131)->pluck('int_data')==1){
                $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
                $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
                $query = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id));
                $new_kolicina = $query->first()->kolicina - $kolicina;
                $query->update(array('kolicina'=>$new_kolicina));
            }   
        }
   
        public static function broj_cart(){
 $ukupno=0;
        if(Session::has('b2c_korpa')){
              foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->get() as $row){
                $ukupno+=$row->kolicina;
              }
        }
        return $ukupno;
    }    

    public static function cena($cena,$valuta_show = true){
        $decimale = Options::web_options(209);

        //Dinar
        if(Session::get('valuta')!="2"){ 
            $valuta = ' <span> din.</span>';          
            if($cena!="0"){            
                $iznos = number_format(round(floatval($cena),$decimale), 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        //Euro
        }else{
            $kurs=Options::kurs();
            $valuta = ' <span> eur.</span>'; 
            if($cena!="0"){            
                //$iznos = number_format(round(floatval($cena/$kurs),$decimale), 2, ',', '.');
                $iznos = number_format(floatval($cena/$kurs), 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        }
        if($valuta_show == true){
            return $iznos.$valuta;
        }else{
            return $iznos;
        }
    }

    public static function cart_ukupno(){
        $ukupno=0;
        if(Session::has('b2c_korpa')){
              foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->get() as $row){
                $ukupno+=$row->jm_cena*$row->kolicina;
              }
        }
        if(AdminOptions::web_options(152)==1){
            return ceil( $ukupno/10 )*10;
        }else{
            return $ukupno;   
        }
    }
    public static function troskovi_isporuke(){
        $ukupno=0;
        if(Session::has('b2c_korpa')){
              foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->get() as $row){
                $ukupno+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
              }
        }
        $cena = 0;
        $obj=DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->first();
        if(isset($obj)){
            $cena = $obj->cena;
        }
        foreach (DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->get() as $row) {
            if(round($ukupno) <= $row->tezina_gr){
                $cena = $row->cena;
            }
        }
        return $cena;
    } 
    public static function cena_dostave(){

        foreach(DB::table('cena_isporuke')->get() as $row){
            $cena = $row->cena;
        }
        return $cena;

    }
    public static function cena_do(){

        foreach(DB::table('cena_isporuke')->get() as $row){
            $cena_do = $row->cena_do;
        }
        return $cena_do;

    }
    public static function add_to_wish($kupac_id,$roba_id){
        $item = DB::table('lista_zelja')->where(array('roba_id'=>$roba_id,'web_kupac_id'=>$kupac_id))->first();
        if(is_null($item)){
            DB::table('lista_zelja')->insert(array('roba_id'=>$roba_id,'web_kupac_id'=>$kupac_id));
            return true;
        }
        return false;
    }
    public static function delete_wish($roba_id){
        DB::table('lista_zelja')->where('roba_id',$roba_id)->delete();        
    }
    public static function broj_wish(){
        if(Session::has('b2c_kupac')){
            return DB::table('lista_zelja')->where('web_kupac_id',Session::get('b2c_kupac'))->count();
        }
        else {
            return 0;
        }
    }   

    public static function api_posta($posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna' => 1))->first();
        if(is_null($posta_slanje)){
            return false;
        }else{
            return true;
        }
    }

    public static function opstine(){
        return DB::table('narudzbina_opstina')->orderBy('naziv','asc')->get();
    }

    public static function mesta($opstina_old_id){
        return DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',($opstina_old_id ? $opstina_old_id : DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
    }

    public static function ulice($mesto_old_id){
        return DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',DB::table('narudzbina_mesto')->where('narudzbina_mesto_id',($mesto_old_id ? $mesto_old_id : DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id)->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->first()->narudzbina_mesto_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
    }
    
    public static function mesto($ulica_id){
        $ulica = self::ulica($ulica_id);
        if(!is_null($ulica)){
            $mesto = DB::table('narudzbina_mesto')->where('code',$ulica->narudzbina_mesto_code)->first();
            if(!is_null($mesto)){
                return $mesto;
            }
        }
        return null;
    }
    public static function opstina($ulica_id){
        $mesto = self::mesto($ulica_id);
        if(!is_null($mesto)){
            $opstina = DB::table('narudzbina_opstina')->where('code',$mesto->narudzbina_opstina_code)->first();
            if(!is_null($opstina)){
                return $opstina;
            }
        }
        return null;
    }
    public static function ulica($ulica_id){
        return DB::table('narudzbina_ulica')->where('narudzbina_ulica_id',$ulica_id)->first();
    }
    public static function mesto_id($ulica_id){
        $mesto = self::mesto($ulica_id);
        if(!is_null($mesto)){
            return $mesto->narudzbina_mesto_id;
        }
        return 0;
    }
    public static function opstina_id($ulica_id){
        $opstina = self::opstina($ulica_id);
        if(!is_null($opstina)){
            return $opstina->narudzbina_opstina_id;
        }
        return 0;
    }

} 