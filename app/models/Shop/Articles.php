<?php 
class Articles {

	public static function get_sort(){
    	if(Session::has('order')){
    		$sessija=Session::get('order');
    		if($sessija=='price_asc'){
    			return "Cena: Min";
    		}
    		else if($sessija=='price_desc'){
    			return "Cena: Max";
    		}
    		else if($sessija=='news'){
    			return "Najnovije";
    		}
    		else if($sessija=='name'){
    			return "Prema nazivu";
    		}
    	}
    	else {
    		return Options::web_options(207) == 0 ? "Cena: Min" : "Cena: Max";
    	}
    }

    public static function get_valuta(){
        if(Session::has('valuta')){
          if(Session::get('valuta')==1){
            return "Din";
          }  
          else {
            return "Eur";
          }
        }
        else {
            return "Din";
        }
    }

    public static function artikli_count($grupa_pr_id){

		$gr=Groups::vratiSveGrupe($grupa_pr_id);
		$queryOsnovni=DB::table('roba')
                ->select([DB::RAW('DISTINCT(roba.roba_id)')])
               	->leftJoin('roba_grupe', 'roba_grupe.roba_id','=','roba.roba_id');

		if(Product::checkImage() != ''){
			$queryOsnovni->leftJoin('web_slika', 'roba.roba_id','=','web_slika.roba_id');
		} 
     
        $queryOsnovni->where(function($queryOsnovni) use ($gr){
                	$queryOsnovni->whereIn('roba.grupa_pr_id', $gr)
                	      ->orWhereIn('roba_grupe.grupa_pr_id', $gr);
                })
				->where(array('roba.flag_aktivan'=>1, 'roba.flag_prikazi_u_cenovniku'=>1));

		if(Product::checkPrice() != ''){
			$queryOsnovni->where(Options::checkCena()=='web_cena'?'web_cena':'mpcena','>',0);
		}
		if(Product::checkImage() != ''){
			$queryOsnovni->whereNotNull('web_slika.roba_id');
		}
		if(Product::checkDescription() != ''){
			$queryOsnovni->whereNotNull('web_opis');
		}

		return count($queryOsnovni->get());					
	}	

	public static function brojiArtikleRekurzivno($grupa_pr_id)
	{
		$result = '';
		if(Options::gnrl_options(1306)){
			$result = Articles::artikli_count($grupa_pr_id);
		}
		return $result;

	}

	public static function bestSeller($limit=20){
		$roba_ids_string = '';
		$roba_ids = DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");
		if(count($roba_ids) > 0){
			$roba_ids_string = 'WHERE roba_id IN ('.implode(',',array_map('current',$roba_ids)).')';
		}
		if($roba_ids_string == ''){
			return (object) array();
		}else{
			return DB::select('
				SELECT roba_id, SUM(kolicina) as count 
				FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
				ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id 
				'.$roba_ids_string.' AND realizovano = 1 AND stornirano != 1
				GROUP BY roba_id 
				ORDER BY count 
				DESC LIMIT '.$limit.'');
		}
	}

	public static function latestAdded($limit=20){
		// DB::table('roba')->where('flag_aktivan', 1)->where('flag_prikazi_u_cenovniku', 1)->orderBy('roba_id', 'DSC')->limit(5)->get();
		$latest = DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id DESC LIMIT ".$limit."");
		return $latest;
	}

	public static function artikli_tip($tip,$limit=null,$offset=null){
		$query = "SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND tip_cene = ".$tip." ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id ASC"; // ORDER BY r.roba_id ASC

		if(isset($offset) && isset($limit)){
			$offset = ($offset-1)*$limit;
			$query .= " LIMIT ".$limit." OFFSET ".$offset."";
		}elseif(isset($limit)){
			$query .= " LIMIT ".$limit."";
		}

        return $array_tip = DB::select($query);
	}

	public static function akcija($grupa_pr_id=null,$limit=null,$offset=null){
		$datum = date('Y-m-d');
		$query = "SELECT DISTINCT r.roba_id, akcija_redni_broj FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' 
		WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' 
		WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' 
		ELSE 1 = 1 END ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."";

		if(isset($grupa_pr_id)){
			$grupe = Groups::vratiSveGrupe($grupa_pr_id);
			$query .= " AND grupa_pr_id IN (".implode(',',$grupe).")";
		}

		$query .= " ORDER BY akcija_redni_broj, r.roba_id ASC";		
		if(isset($offset) && isset($limit)){
			$offset = ($offset-1)*$limit;
			$query .= " LIMIT ".$limit." OFFSET ".$offset."";
		}elseif(isset($limit)){
			$query .= " LIMIT ".$limit."";
		}

		return $array_tip = DB::select($query);
	}

	public static function mostPopularArticles($limit=20){
		// $popular = DB::table('roba')->where('flag_aktivan', 1)->where('flag_prikazi_u_cenovniku', 1)->orderBy('pregledan_puta', 'DSC')->limit(5)->get();
	    $popular=DB::select("SELECT DISTINCT r.roba_id, r.pregledan_puta,r.naziv FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.pregledan_puta DESC LIMIT ".$limit."");

		return $popular;
	}

	public static function mostPopularArticlesSearch($limit=20){

	    $popular=DB::select("SELECT DISTINCT r.roba_id, r.pregledan_puta,r.naziv,gr.grupa FROM roba r LEFT JOIN grupa_pr gr ON gr.grupa_pr_id = r.grupa_pr_id WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1 ORDER BY r.pregledan_puta DESC LIMIT ".$limit."");

		return $popular;
	}

	public static function subGroups($grupa_pr_id) {
		$q = DB::select('SELECT grupa_pr_id, grupa, putanja_slika FROM grupa_pr  WHERE parrent_grupa_pr_id = '.$grupa_pr_id.' AND web_b2c_prikazi = 1 ORDER BY redni_broj');
		return $q;
	}




}