<?php
namespace Service;

use DB;
use AdminPartneri;
use AdminNarudzbine;
use AdminOptions;
use All;

class DExpress {

	public static function convertToCsv($data){
        $path = "dexpress.csv";
        $file = fopen($path,"w");
		$list = array(
			array('ReferenceID','SBranchID','SName','SAddress','STownID','STown','SCName','SCPhone','PuBranchID','PuName','PuAddress','PuTownID','PuTown','PuCName','PuCPhone','RBranchID','RName','RAddress','RTownID','RTown','RCName','RCPhone','DlTypeID','PaymentBy','PaymentType','BuyOut','BuyOutAccount','BuyOutFor','Value','Mass','ReturnDoc','Packages','Note','Content')
			);

		foreach($data as $row){
			$list[] = $row;
		}

		foreach ($list as $line){
			fputcsv($file,$line,'|');
		}
		fclose($file);

		return (object) array('success' => true, 'path' => $path);
	}

	public static function convertToString($data){
		$result = '';
		$list = array(
			array('ReferenceID','SBranchID','SName','SAddress','STownID','STown','SCName','SCPhone','PuBranchID','PuName','PuAddress','PuTownID','PuTown','PuCName','PuCPhone','RBranchID','RName','RAddress','RTownID','RTown','RCName','RCPhone','DlTypeID','PaymentBy','PaymentType','BuyOut','BuyOutAccount','BuyOutFor','Value','Mass','ReturnDoc','Packages','Note','Content')
			);

		foreach($data as $row){
			$list[] = $row;
		}

		foreach ($list as $line){
			$result .= implode('|', $line)."\n";
		}
		return (object) array('success' => true, 'result' => $result);
	}

	public static function generate_data($stavke,$posiljalac_id,$kupac_id){
		$nasa_sifra = DB::table('posta_slanje')->where(array('posta_slanje_id'=>3,'aktivna'=>1))->pluck('nasa_sifra');
        if(is_null($nasa_sifra) || $nasa_sifra == ''){
            return (object) array('success'=>false, 'message'=>'Nedostaje naša šifra za ovu kurirsku službu.');
        }

		$preduzece=DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece) 
        	|| (!is_null($preduzece) && (
        		is_null($preduzece->ulica_id) || $preduzece->ulica_id == '' 
        		|| is_null($preduzece->broj) || $preduzece->broj == '' 
        		|| is_null($preduzece->naziv) || $preduzece->naziv == '' 
        		|| is_null($preduzece->kontakt_osoba) || $preduzece->kontakt_osoba == '' 
        		|| is_null($preduzece->telefon) || $preduzece->telefon == '' 
        		))){
            return (object) array('success'=>false, 'message'=>'Nedostaju svi podaci nalogodavca.');
        }

        $web_kupac = DB::table('web_kupac')->where(array('web_kupac_id'=>$kupac_id))->first();
        if(is_null($web_kupac) 
        	|| (!is_null($web_kupac) && ((
        		is_null($web_kupac->ulica_id) || $web_kupac->ulica_id == '' 
        		|| is_null($web_kupac->broj) || $web_kupac->broj == '' 
        		|| is_null($web_kupac->telefon) || $web_kupac->telefon == '' 
        		) || ($web_kupac->flag_vrsta_kupca == 0 && (
        			is_null($web_kupac->ime) || $web_kupac->ime == '' 
        			|| is_null($web_kupac->prezime) || $web_kupac->prezime == '')
        		 || ($web_kupac->flag_vrsta_kupca == 1 && (
        		 	is_null($web_kupac->naziv) || $web_kupac->naziv == ''
        		 	))
        	)))){
        	return (object) array('success'=>false, 'message'=>'Nedostaju svi podaci primaoca.');
        }

        
        if($posiljalac_id == 1){
        	$posiljalac = (object) array('naziv'=>$preduzece->naziv,'ulica_id'=>$preduzece->ulica_id,'broj'=>$preduzece->broj,'kontakt_osoba'=>$preduzece->kontakt_osoba,'telefon'=>$preduzece->telefon);
        	$broj_paketa = AdminNarudzbine::broj_paketa($stavke);
        }else{
        	$posiljalac = AdminPartneri::partner($posiljalac_id);
        	$broj_paketa = 1;
	        if(is_null($posiljalac) 
	        	|| (!is_null($posiljalac) && (
	        		is_null($posiljalac->ulica_id) || $posiljalac->ulica_id == '' 
	        		|| is_null($posiljalac->broj) || $posiljalac->broj == '' 
	        		|| is_null($posiljalac->naziv) || $posiljalac->naziv == '' 
	        		|| is_null($posiljalac->kontakt_osoba) || $posiljalac->kontakt_osoba == '' 
	        		|| is_null($posiljalac->telefon) || $posiljalac->telefon == '' 
	        		))){
	            return (object) array('success'=>false, 'message'=>'Nedostaju svi podaci pošiljaoca.');
	        }
        }

		$posta_info = AdminNarudzbine::posta_info($stavke);
        $ulica_broj = AdminNarudzbine::ulica_broj($stavke);
        $ulica_id = $ulica_broj->ulica_id;
        $broj = $ulica_broj->broj;

        if($ulica_id==0){
        	$ulica_id = $web_kupac->ulica_id;
        }
        if(is_null($broj)){
        	$broj = $web_kupac->broj;
        }


    	$reference_id = intval(self::generate_reference_id($stavke[0],$posiljalac_id));
    	$nalogodavac_mesto = AdminNarudzbine::mesto($preduzece->ulica_id);
    	$posiljalac_mesto = AdminNarudzbine::mesto($posiljalac->ulica_id);
    	$kupac_mesto = AdminNarudzbine::mesto($web_kupac->ulica_id);
    	if($web_kupac->flag_vrsta_kupca == 0){
    		$ime_kupca = $web_kupac->ime.' '.$web_kupac->prezime;
    	}else{
    		$ime_kupca = $web_kupac->naziv;
    	}
    	$transport_placa = $posta_info->transport_placa;
    	$nacin_pacanja = $posta_info->web_nacin_placanja_id == 1 ? 1 : 2;
    	$cena_otkupa = round($posta_info->cena_otkupa);
    	$racun_otkupa = $posta_info->racun_otkupa;
    	$otkup_prima = $posta_info->otkup_prima;
    	$odgovor = $posta_info->odgovor;
    	$napomena = !is_null($posta_info->napomena) ? $posta_info->napomena : '';
    	$opis = !is_null($posta_info->opis) ? $posta_info->opis : 'Tehnika';
    	$vrednost_masa = self::vrednost_masa($stavke);
    	$packages = '';
    	if($posiljalac_id == 1){
        	foreach(range(1,$broj_paketa) as $pack){
        		$packages .= 'TT'.self::generate_package_code($stavke[0],$pack).'#';
        	}
        }

		$data = array($reference_id,DB::table('posta_slanje')->where(array('posta_slanje_id'=>3,'aktivna'=>1))->pluck('nasa_sifra'),self::clear($preduzece->naziv),self::clear(AdminNarudzbine::ulica($preduzece->ulica_id)->naziv.' '.$preduzece->broj),$nalogodavac_mesto->code,self::clear($nalogodavac_mesto->naziv),self::clear($preduzece->kontakt_osoba),self::clear($preduzece->telefon),'',self::clear($posiljalac->naziv),self::clear(AdminNarudzbine::ulica($posiljalac->ulica_id)->naziv.' '.$posiljalac->broj),$posiljalac_mesto->code,self::clear($posiljalac_mesto->naziv),self::clear($posiljalac->kontakt_osoba),self::clear($posiljalac->telefon),'',self::clear($ime_kupca),self::clear(AdminNarudzbine::ulica($web_kupac->ulica_id)->naziv.' '.$web_kupac->broj),$kupac_mesto->code,self::clear($kupac_mesto->naziv),self::clear($ime_kupca),self::clear($web_kupac->telefon),'2',$transport_placa,$nacin_pacanja,$cena_otkupa,$racun_otkupa,$otkup_prima,intval($vrednost_masa->vrednost),intval($vrednost_masa->masa),$odgovor,$packages,$napomena,$opis);

		DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',array_map('current',$stavke))->update(array('posta_reference_id'=>$reference_id));
        return (object) array('success'=>true, 'data'=>$data);
	}

	public static function generate_reference_id($stavka,$posiljalac_id){
		$narudzbina_id = $stavka->web_b2c_narudzbina_id;
		$stavka_id = $stavka->web_b2c_narudzbina_stavka_id;

		$reference_id = self::number_to_string($posiljalac_id,4);
		$reference_id .= self::number_to_string($narudzbina_id,4);
		$reference_id .= self::number_to_string($stavka_id,4);
		return $reference_id;
	}

	public static function convert_reference_id($reference_id){
		$reference_id = self::number_to_string($reference_id,12);
		return (object) array(
			'posiljalac_id' => intval(substr($reference_id,0,4)),
			'web_b2c_narudzbina_id' => intval(substr($reference_id,4,4)),
			'web_b2c_narudzbina_stavka_id' => intval(substr($reference_id,8,4)),
			);
	}

	public static function generate_package_code($stavka,$pack){
		$narudzbina_id = $stavka->web_b2c_narudzbina_id;
		$stavka_id = $stavka->web_b2c_narudzbina_stavka_id;

		$reference_id = self::number_to_string($narudzbina_id,4);
		$reference_id .= self::number_to_string($stavka_id,4);
		$reference_id .= self::number_to_string($pack,2);
		return $reference_id;
	}

	public static function convert_package_code($reference_id){
		$reference_id = self::number_to_string($reference_id,10);
		return (object) array(
			'web_b2c_narudzbina_id' => intval(substr($reference_id,0,4)),
			'web_b2c_narudzbina_stavka_id' => intval(substr($reference_id,4,4)),
			'pack' => intval(substr($reference_id,8,2)),
			);
	}

	public static function vrednost_masa($stavke){
		$vrednost = 0;
		$masa = 0;
		foreach($stavke as $stavka){
			$vrednost += $stavka->vrednost;
			$masa += $stavka->masa;
		}
		return (object) array('vrednost' => $vrednost, 'masa' => $masa);
	}

	public static function number_to_string($number,$length){
		$str = strval($number);
		if(strlen($str) > $length){
			return substr($str,0,$length);
		}elseif(strlen($str) == $length){
			return $str;
		}else{
			$str = '0'.$str;
			return self::number_to_string($str,$length);
		}
	}

	public static function clear($string){
		return addslashes(str_replace('|','',$string));
	}

}